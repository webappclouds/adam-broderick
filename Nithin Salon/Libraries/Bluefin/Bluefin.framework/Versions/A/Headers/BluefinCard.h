//
//  BluefinCard.h
//  Bluefin
//
//  Created by Carl Hill-Popper on 5/8/13.
//  Copyright (c) 2013 Bluefin Payment Systems. All rights reserved.
//

#import <Bluefin/BluefinObject.h>


/// A default implementation of the BluefinCardType protocol.
@interface BluefinCard : BluefinObject <BluefinCardType>

@property NSString *cardType;
@property NSString *cardBrand;
@property NSString *cardNumber;
@property NSString *cardExpiration;
@property NSString *cardCVC;
@property NSString *firstName;
@property NSString *lastName;
@property NSString *address;
@property NSString *addressAdditionalLine;
@property NSString *city;
@property NSString *state;
@property NSString *zipCode;
@property NSString *phone;
@property NSString *email;

@property BOOL addressVerified;
@property BluefinAVSResponse addressVerification;
@property BOOL cvcVerified;
@property BluefinCVVResponse cvcVerification;

@property NSString *authorizationCode;
@property NSString *authorizationMessage;

@end

/// A default implementation of the BluefinSwipedCardType protocol
@interface BluefinSwipedCard : BluefinCard <BluefinSwipedCardType>

@property NSString *trackData;

@end
