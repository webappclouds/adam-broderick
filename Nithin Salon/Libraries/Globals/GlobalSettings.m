//
//  GlobalSettings.m
//  Nithin Reddy
//
//  Created by Nithin Reddy on 04/03/16.
//  Copyright © 2016 Nithin Reddy. All rights reserved.
//

#import "GlobalSettings.h"
#import "UIViewController+NRFunctions.h"
#import "AppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>
@implementation GlobalSettings

+ (id)sharedManager {
    static GlobalSettings *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

-(void) httpPostJSONBody : (NSString *) urlStr BODY:(NSDictionary *) bodyDict COMPLETIONHANDLER:(void (^) (NSData *data, NSURLResponse *urlResponse, NSError *error)) completionHandler
{
    
    // Here we set up a NSNotification observer. The Reachability that caused the notification
    // is passed in the object parameter
  
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //This is optional and only for requests after Login
    
    [request setHTTPMethod:@"POST"];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:bodyDict options:0 error:&error];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(data, response, error);
        });
        
        
    }];
    [task resume];
    
}

-(void) httpGet : (NSString *) urlStr PARAMS:(NSDictionary *) params COMPLETIONHANDLER:(void (^) (NSData *data, NSURLResponse *urlResponse, NSError *error)) completionHandler
{
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    if(params!=Nil)
    {
        NSURLComponents *components = [NSURLComponents componentsWithString:urlStr];
        NSMutableArray *queryItems = [NSMutableArray array];
        for (NSString *key in params) {
            [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:params[key]]];
        }
        components.queryItems = queryItems;
        url = [components URL];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    [request setHTTPMethod:@"GET"];
    //    [request addValue:[self getBasicAuthorizationValue] forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(data, response, error);
        });
    }];
    [task resume];
}


-(void) httpGet : (NSString *) urlStr PARAMS:(NSDictionary *) params HEADERS:(NSDictionary *) headers COMPLETIONHANDLER:(void (^) (NSData *data, NSURLResponse *urlResponse, NSError *error)) completionHandler
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    if(params!=Nil)
    {
        NSURLComponents *components = [NSURLComponents componentsWithString:urlStr];
        NSMutableArray *queryItems = [NSMutableArray array];
        for (NSString *key in params) {
            [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:params[key]]];
        }
        components.queryItems = queryItems;
        url = [components URL];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];
    
    for (NSString *key in headers) {
        [request addValue:headers[key] forHTTPHeaderField:key];
    }
    //    [request addValue:[self getBasicAuthorizationValue] forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(data, response, error);
        });
    }];
    [task resume];
}
-(void) uploadImageOverHttp : (UIImage *) image URL:(NSString *) urlStr HEADERS:(NSDictionary *) headers PARAMS:(NSDictionary *) params COMPLETIONHANDLER:(void (^) (NSData *data, NSDictionary *responseDict, NSError *error)) completionHandler
{
    // Build the request body
    NSString *boundary = @"Boundary";
    NSMutableData *body = [NSMutableData data];
    
    for(NSString *key in params)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    if (imageData) {
        
        //        long long milliseconds = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
        //        NSString *strTimeStamp = [NSString stringWithFormat:@"%lld",milliseconds];
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpg\"\r\n", @"image", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    
    // Create the session
    // We can use the delegate to track upload progress
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    // Data uploading task. We could use NSURLSessionUploadTask instead of NSURLSessionDataTask if we needed to support uploads in the background
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = body;
    for (NSString *key in headers) {
        [request addValue:headers[key] forHTTPHeaderField:key];
    }
    //    [request addValue:[self getBasicAuthorizationValue] forHTTPHeaderField:@"Authorization"];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)urlResponse statusCode];
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:Nil];
            NSLog(@"%@", responseDict);
            completionHandler(data, responseDict, error);
        });
    }];
    [uploadTask resume];
}


-(void) processHTTPNetworkCall : (NSString *) method URL:(NSString *) urlStr HEADERS: (NSDictionary *) headers GETPARAMS:(NSDictionary *) getParams POSTPARAMS:(NSDictionary *) postParams COMPLETIONHANDLER:(void (^) (id responseDict, NSURLResponse *urlResponse, NSError *error)) completionHandler
{
    
    NSLog(@"URL is : %@",urlStr);
    
    NSLog(@"Post Params : %@",postParams);

    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
     [appDelegate reachabilityChanged];
    
    if(appDelegate.netAvailability == YES){
   
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    if(getParams!=Nil)
    {
        NSURLComponents *components = [NSURLComponents componentsWithString:urlStr];
        NSMutableArray *queryItems = [NSMutableArray array];
        for (NSString *key in getParams) {
            [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:getParams[key]]];
        }
        components.queryItems = queryItems;
        url = [components URL];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:method];
    for (NSString *key in headers) {
        [request addValue:headers[key] forHTTPHeaderField:key];
    }
    //    [request addValue:[self getBasicAuthorizationValue] forHTTPHeaderField:@"Authorization"];
    
    if(postParams!=Nil)
    {
        NSMutableArray * content = [NSMutableArray array];
        
        for(NSString * key in postParams)
            [content addObject: [NSString stringWithFormat: @"%@=%@", key, postParams[key]]];
        
        NSString * body = [content componentsJoinedByString:@"&"];
        NSData * bodyData = [body dataUsingEncoding: NSUTF8StringEncoding];
        [request setHTTPBody: bodyData];
        [request setHTTPMethod:@"POST"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[bodyData length]] forHTTPHeaderField:@"Content-Length"];
//        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//         [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    }
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *error;
            if(data){
                NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                if(!error)
                    completionHandler(responseDict, response, error);
                else
                    completionHandler((NSArray *) responseDict, response, error);
            }
            else
                completionHandler(nil, response, error);
        });
    }];
    [task resume];
    }
    
    else{
        [appDelegate hideHUD];
        [self showAlert];
        
                   }
    
}


-(void) uploadImageOverHttp : (UIImage *) image URL:(NSString *) urlStr HEADERS:(NSDictionary *) headers COMPLETIONHANDLER:(void (^) (NSData *data, NSDictionary *responseDict, NSError *error)) completionHandler
{
    // Build the request body
    NSString *boundary = @"Boundary";
    NSMutableData *body = [NSMutableData data];
    // Body part for "textContent" parameter. This is a string.
    //    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //    if([claimId length]!=0)
    //    {
    //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"claim_no"] dataUsingEncoding:NSUTF8StringEncoding]];
    //        [body appendData:[[NSString stringWithFormat:@"%@\r\n", claimId] dataUsingEncoding:NSUTF8StringEncoding]];
    //    }
    
    // Body part for the attachament. This is an image.
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    if (imageData) {
        
        long long milliseconds = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
        NSString *strTimeStamp = [NSString stringWithFormat:@"%lld",milliseconds];
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpg\"\r\n", @"image", strTimeStamp] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    
    // Create the session
    // We can use the delegate to track upload progress
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    // Data uploading task. We could use NSURLSessionUploadTask instead of NSURLSessionDataTask if we needed to support uploads in the background
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = body;
    for (NSString *key in headers) {
        [request addValue:headers[key] forHTTPHeaderField:key];
    }
    //    [request addValue:[self getBasicAuthorizationValue] forHTTPHeaderField:@"Authorization"];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)urlResponse statusCode];
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:Nil];
            
            completionHandler(data, responseDict, error);
        });
    }];
    [uploadTask resume];
}

+ (void) clearAllPreferences
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
}

-(void)showAlert{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network error" message:@"Could not connect to the server. Please check your network connectivity." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];

}
    @end

