//
//  UIViewController+NRFunctions.m
//  NRUtils
//
//  Created by Nithin Reddy on 15/12/15.
//  Copyright © 2015 Nithin Reddy. All rights reserved.
//

#import "UIViewController+NRFunctions.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "BottomBar.h"
#import "ContactUs.h"
#import "AppDelegate.h"
@implementation UIViewController (NRFunctions)
+(BOOL) isValidEmail : (NSString *) email
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+(NSString *) formatPhone
{
    AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSRange range;
    range.length = 3;
    range.location = 3;
    
    NSString * strPhoneNo=objAppdel.salonPhone;
    
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@"(" withString:@""];
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@")" withString:@""];
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSString *areaCode = [strPhoneNo substringToIndex:3];
    NSString *phone1 = [strPhoneNo substringWithRange:range];
    NSString *phone2 = [strPhoneNo substringFromIndex:6];
    
    NSString *ph=[NSString stringWithFormat:@"Do you want to call (%@)%@-%@",areaCode,phone1,phone2];
    return ph;
}


-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==2){
            if(self.navigationController.viewControllers.count>0){
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
        if(tag==100)
         [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)hideBackButton{
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void) showAlert : (NSString *) message
{
    [self showAlert:@"" MSG:message tag:0];
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message BLOCK:(void (^)(void))callbackBlock
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        callbackBlock();
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

+ (void) addBottomBar : (UIViewController *) con
{
       BottomBar *bottomBar = [[BottomBar alloc] initWithNibName:@"BottomBar" bundle:nil];
    if([con isKindOfClass:[ContactUs class]])
        [bottomBar setShouldEmailNotWork:1];
    bottomBar.view.frame = CGRectMake(0, con.view.frame.size.height-bottomBar.view.frame.size.height, con.view.frame.size.width, bottomBar.view.frame.size.height+4);
    [con.view addSubview:bottomBar.view];
    [con addChildViewController:bottomBar];
    bottomBar.definesPresentationContext = YES;
   
}
-(void) showHud
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void) hideHud
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
   // [MBProgressHUD hideAllHUDsForView:appDelegate.window animated:YES];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void) showHudWithMessage:(NSString *)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = NO;
    hud.labelText = message;
   // hud.label.text = message;
}

-(void) saveSharedPreferenceValue : (NSString *)val KEY:(NSString *)key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:val forKey:key];
    [prefs synchronize];
}

-(NSString *) loadSharedPreferenceValue : (NSString *)key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [self checkNull:[prefs valueForKey:key]];
}


-(void) saveDictionary:(NSDictionary*)dict key:(NSString *)key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:dict forKey:key];
    [prefs synchronize];
}
-(NSDictionary *) loadDictionary : (NSString *)key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs valueForKey:key];
}


-(NSString *) checkNull:(NSObject *) val
{
    NSString *value = [NSString stringWithFormat:@"%@",val];
    
    if(value == nil || [val isEqual:[NSNull null]] || [value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"]||[value length] == 0)
        value = @"";
    ///sr
    return value;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (NSString *) getPathByFileName : (NSString *) fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    return [docsPath stringByAppendingPathComponent:fileName];
}

+(NSString *) returnShareUrl
{
    AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString *shareUrl = @"";
    if([objAppdel.itunesUrl length]>0)
        shareUrl = [NSString stringWithFormat:@"%@\n\n", objAppdel.itunesUrl];
    if([objAppdel.playUrl length]>0)
        shareUrl = [NSString stringWithFormat:@"%@%@", shareUrl, objAppdel.playUrl];
    
    if([shareUrl length]>0)
        shareUrl = [NSString stringWithFormat:@"\nYou can download the app at %@", shareUrl];
    return shareUrl;
}

+(NSString *) getShareLongText
{
    return [NSString stringWithFormat:@"Check out the %@ app!\n%@", SALON_NAME, [self returnShareUrl]];
}
+(BOOL) isGeoFencingOn
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger isGeofencing = [prefs integerForKey:@"geofence"];
    if(isGeofencing==9)
        return NO;
    return YES;
}

+(void) setGeoFencing : (BOOL) status
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    int statusInt = 1;
    if(status==NO)
        statusInt = 9;
    [prefs setInteger:statusInt forKey:@"geofence"];
    [prefs synchronize];
}

+(void) loadDirections
{
    AppDelegate *objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString *SPALocation = [NSString stringWithFormat:@"%@,%@", objAppdel.spaLatitude, objAppdel.spaLongitude];
    NSString *urlStr = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=Current+Location&daddr=%@&z=17",SPALocation];
    if([[UIApplication sharedApplication] canOpenURL:
        [NSURL URLWithString:@"comgooglemaps://"]])
        urlStr = [NSString stringWithFormat:@"comgooglemaps://?q=%@&center=Current+Location&daddr=%@&z=17",SALON_NAME, SPALocation];
    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    //urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
}
-(void)removeNavgationBackButtonTitle
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
}

-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", laxString];
    return [emailTest evaluateWithObject:checkString];
}
@end
