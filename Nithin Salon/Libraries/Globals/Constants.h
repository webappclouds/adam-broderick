//
//  Constants.h
//  Nithin Salon
//
//  Created by Nithin Reddy on 19/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
#import "GlobalSettings.h"
#import "ModuleObj.h"
#import "SpecialsList.h"
#import <SIAlertView/SIAlertView.h>
#import "UIViewController+NRFunctions.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServiceObj.h"
#import "RequestAppointment.h"
#import "StaffList.h"
#import <MWPhotoBrowser.h>
#import <Stripe/Stripe.h>

#define SALON_ID @"691"
#define SALON_NAME @"Adam Broderick Salon & Spa"


#define TEST_CONST 1010
#define PRODUCTION_CONST 1011
#define INTEGRATION_ENABLED 1 //1 enabled, 0 disabled

#define URL_IMAGE_UPLOAD SERVER_URL "uploads/"
#define URL_CONTACT_US SERVER_URL "wsplus/information/"
#define ONLINE_SERVER_URL @"https://salonclouds.plus/API/"
#define URL_RESERVATION_UPDATE_RECOMMENDED SERVER_URL @"wsappointments/appointment_set_response/"
#define URL_STRIPE SERVER_URL "wsgiftcard2/ws_stripe/"


#define URL_PUSH_TOKEN SERVER_URL "wsplus/add_push_token/"

#define URL_APPOINTMENTS SERVER_URL "wsuserlogin/get_appointments_by_slc_id/"
//User login and signup
#define URL_USER_LOGIN SERVER_URL "wsuserlogin/user_login/"
#define URL_USER_FRGT_PSWD SERVER_URL "wsuserlogin/user_forgot_password/"
#define URL_GC_BALANCE_WITH_SLC_ID SERVER_URL @"wsappointments/gc_balance_customer_points/"

//book online
#define URL_PAST_APPOINTMENTS SERVER_URL "wsuserlogin/get_past_appointments_by_slc_id/"
#define URL_ONLINE_BOOK_REG SERVER_URL "online_booking_api/userregister/"

//Payment Method
#define PAYMENT_STATUS TEST_CONST//PRODUCTION_CONST //Test or Production

#define URL_RESERVATION_RESPONSE_UPDATE SERVER_URL "wsintegration_texting/appointments_pushing_update_response/"


//Last Min Appts
#define URL_LAST_MIN_APPTS SERVER_URL "wslast_min_appointments/appointments_list/"

#define URL_LAST_MIN_APPT_ADD SERVER_URL "wslast_min_appointments/last_min_appointments_add/"

#define SHARE_LONG_TEXT_MENU @"Check out the " SALON_NAME " app service!"
//Auth.net PAYMENT
#define URL_AUTH_NET SERVER_URL "auth/index.php"

#define SALON_LATITUDE 51.508530
#define SALON_LONGITUDE -0.076132

//Admin panel
// lat : 26.106929
// Long : -80.105832

// Testing purpose Locations Office address
//#define SALON_LATITUDE 17.442558
//#define SALON_LONGITUDE 78.480181

//Cpay
#define URL_CPAY SERVER_URL "wsgiftcard2/ws_cpay/"

//Beacons

#define URL_GET_BEACON_INFO SERVER_URL "wsbeacons/get_beacon_info/"

#define PAYMENT_KEY @"PAYMENT_TYPE"
#define AUTH_NET_CONST_STR @"AUTHNET"
#define PAYPAL_CONST_STR @"PAYPAL"
#define CPAY_CONST_STR @"CPAY"
#define BLUEFIN_CONST_STR @"BLUEFIN"
#define STRIPE_CONST_STR @"STRIPE"

//Appoitments
#define URL_RESERVATION_LIST SERVER_URL "wsappointments/appointments_list/"

#define SERVER_URL @"https://saloncloudsplus.com/"

#define URL_MODULES_LIST SERVER_URL @"wssaloninfomodules/get_salon_information_modules/"
#define URL_SPECIALS SERVER_URL "wsspecials/specials_list/"
#define URL_MENU SERVER_URL "wsmenu/menu_list/"
#define URL_MENU_DETAILS SERVER_URL "wsmenu/sub_menu_list/"
#define URL_GALLERY SERVER_URL "wsgallery/gallery_list/"
#define IMAGES_URL SERVER_URL @"uploads/"
#define URL_STAFF SERVER_URL "wsprovider/wsstaff_list/"
#define APP_PLACEHOLDER_IMAGE @"profile_picture.png"

#define URL_ADDTO_WALLET_URL SERVER_URL @"wswallet/add_to_wallet/"
#define URL_My_WALLET_URL SERVER_URL @"wswallet/my_wallet/"

//https://saloncloudsplus.com/wswallet/my_wallet

//Events
#define EVENTS_URL SERVER_URL @"wsevents/events_list/"

//Lottery
#define URL_LOTTERY_CHECK SERVER_URL "wslottery/lottery/"

#define URL_LOTTERY_SUBMIT_WINNER SERVER_URL "wslottery/saveLotteryWinner/"

#define URL_RESERVATION SERVER_URL "wsappointments/appointments_add_edit/"

//Reviews
//Reviews
#define URL_REVIEW_SAVE SERVER_URL "wsreviews/savereviews/"
#define URL_GET_REVIEWS SERVER_URL "wsreviews/getreviews/"
#define URL_REVIEWS_TNC SERVER_URL "tnc/reviews_terms.txt"


//loyalty
#define URL_LOYALTY_GET_POINTS SERVER_URL "wsloyalty/get_user_punch/"
#define URL_LOYALTY_REGISTER SERVER_URL "wsloyalty/user_register/"
#define URL_LOYALTY_VALIDATE_CODE SERVER_URL "wsloyalty/set_user_punch/"

#define URL_STAFF2_PRODUCTS_PICKUP SERVER_URL "wssaloninfo/insert_staff2_products/"
//checkScratchTrial/$salon_id
#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

//Customize all these from admin panel
#define BOOK_ONLINE YES

//Newsletter
#define URL_NEWSLETTER_SIGNUP SERVER_URL "wssaloninfo/newsletters/"


#define SPECIALS_FLAG 1012
#define SERVICES_FLAG 1013

#define CONST_ABOUT_US 1003
#define CONST_HOURS 1004
#define CONST_REQSERVICES 100
#define CONST_REQPROVIDERS 101



#define URL_ABOUT_US SERVER_URL "wsplus/information/"
#define URL_HOURS SERVER_URL "wsplus/information/"

#define URL_SALON_DETAILS SERVER_URL "wssaloninfo/get_salon_information/"

//Notifications List

#define URL_NOTIFICATIONS_LIST SERVER_URL "wsplus/notifications_list/"


//Giftcards
#define URL_CHECK_GIFTCARD SERVER_URL "wsgiftcard2/check/"
#define URL_GIFTCARD_DETAILS SERVER_URL "wsgiftcard2/giftcard_details/"
#define URL_GIFTCARD_DROPDOWN SERVER_URL "wsgiftcard2/settings/"
#define URL_ADD_GIFTCARD SERVER_URL "wsgiftcard2/add_giftcard/"


//My Stylists
#define URL_MY_STYLISTS SERVER_URL "wsuserlogin/my_stylists/"

//Integration Appt
#define URL_INTEGRATION_APPTS SERVER_URL "wsappointments/get_type_of_appointments/"

#define URL_INTEGRATION_TYPE SERVER_URL @"wssaloninfo/is_integration/"

#define URL_GET_PRESCRIPTIONS SERVER_URL @"wsappointments/prescriptions_of_client_by_slc_id/"

#define URL_GET_PRESCRIPTION_PRODUCTS SERVER_URL @"wsappointments/prescription_products/"

#define URL_PRESCRIPTION_PRODUCTS_ORDER SERVER_URL @"wsappointments/add_prescription_product_order/"

#define URL_PRESCRIPTION_PRODUCTS_ORDER_ALL SERVER_URL @"wsappointments/order_all_prescription_products/"



#define SHARE_SUBJECT SALON_NAME " Mobile App"
#define SHARE_LONG_TEXT_SPECIAL @"Check out the " SALON_NAME " app special!"
#define SHARE_LONG_TEXT_MENU @"Check out the " SALON_NAME " app service!"



//Referral2
#define URL_REFERAL2_GET_REFID SERVER_URL "wsreferrals2/add_referrer_bySlcId/"
#define URL_REFERAL2_LINK SERVER_URL "getData/referral2_voucherCode/"
#define URL_REFERRAL2_REWARDS SERVER_URL "wsreferrals2/user_rewards_bySlcId/"
#define URL_REFERRAL2_FRIENDS SERVER_URL "wsreferrals2/user_referrals_bySlcId/"

//Constants

#define GOOGLE_MAPS_API_KEY @"AIzaSyDcrStblaR6Ag4Q7tOdhV66xCa7iCVW3F8"
#define GOOGLE_SIGNIN_CLIENTID @"916775253550-ejp58pnhu85c5ud9r9ptj9du55pbdno3.apps.googleusercontent.com"

//BOOK ONLINE
#define URL_CANCEL_APPT ONLINE_SERVER_URL @"cancelappointment/"
#define SERVICES_LIST ONLINE_SERVER_URL @"servicetypes/"
#define SUB_SERVICES_LIST ONLINE_SERVER_URL @"services/"
#define SERVICES_STAFF_LIST ONLINE_SERVER_URL @"serviceproviders/"
#define SERVICES_STAFF_LIST_CHERRY ONLINE_SERVER_URL @"cherry_serviceproviders/"

#define CHECK_APPT_LIST ONLINE_SERVER_URL @"client_scanforopening/"
#define CHECK_APPT_LIST_REST ONLINE_SERVER_URL @"client_scanforopening_rest/"


#define BOOKING_CONFIRMATION ONLINE_SERVER_URL @"bookingconfirmation/"
#define BOOKING_CONFIRMATION_REST ONLINE_SERVER_URL @"bookingconfirmation_rest/"





//Terms and Conditions
#define URL_TNC SERVER_URL "tnc/termsandconditions.txt"
#define APPT_TNC SERVER_URL "terms.html"
#define APPT_PRIVACY SERVER_URL "privacy.html"
#define URL_APPT_HELP SERVER_URL "tnc/appt_help.html"
#define URL_GLAMOUR_TNC SERVER_URL "tnc/glamourimages.txt"
#define URL_GIFTCARDS_TNC SERVER_URL "tnc/giftcards_tnc.txt"
#define URL_BEFORE_AFTER SERVER_URL "tnc/beforeafter.html"
#define URL_GIFTCARDS_CONTENT_GUIDELINES SERVER_URL @"tnc/content_guidelines.txt"
#define URL_USERS_LOGIN_HELP SERVER_URL "applogin.html"
#define URL_BEFORE_AND_OFTER_HELP SERVER_URL "before_after.html"

#define UIColorMake(r,g,b)[[UIColor alloc] initWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];


//check_in service URls


#define LAST_ORDER_SERVICECALL SERVER_URL "wsbeverages/last_ordered_beverage"
#define PLACE_ORDER_URL SERVER_URL "wsbeverages/beverages_order"
#define BEVEREGS_LIST_URL SERVER_URL "wsbeverages/beverages_list/"
#define BEVEREGE_PLACE_ORDER_URL SERVER_URL "wsbeverages/beverages_order/"
#define GEOFENCE_WITHBEACONS_URL SERVER_URL "mobile_checkin/checkInBeacon"
#define GEOFENCE_WITHLOCATION_URL SERVER_URL"mobile_checkin/checkInGeoFence"

//Checkout

#define MY_GALLERY_URL @"https://saloncloudsplus.com/wsimagesharing/imageInsert"
#define LOAD_ALL_IMAGES_URL @"https://saloncloudsplus.com/wsimagesharing/allImages/"

//Profile
#define GET_PROFILE_INFO SERVER_URL"ws_clientprofile/getClientProfile"
#define GET_CLIENT_REWARD_POINTS SERVER_URL "wswallet/client_reward_points"
#define  GET_AVAILBLE_COUPONS SERVER_URL "wswallet/available_client_coupons"
#define GET_REEDEM_AVAILBLE_COUPONS SERVER_URL"wswallet/available_coupons"
#define GET_REDEEM_COUPON_POINTS SERVER_URL"wswallet/redeem_points_to_coupon"
#define UPDATE_PROFILE_URL SERVER_URL"ws_clientprofile/updateClientProfile"
                        
//Ecommerce system
//

#define CATEGORIES_SERVER_URL @"http://dev3.saloncloudsplus.com/"

#define IMAGE_BASE_URL CATEGORIES_SERVER_URL @"voga/image/%@"

#define CATEGORIES_URL CATEGORIES_SERVER_URL @"voga/index.php?route=ws/fetchcategory"

#define CATEGORIES_LIST_URL CATEGORIES_SERVER_URL @"voga/index.php?route=ws/fetchproducts&path="

#define CATEGORIES_INFO_URL CATEGORIES_SERVER_URL @"voga/index.php?route=ws/productdetail&path="




#endif /* Constants_h */



