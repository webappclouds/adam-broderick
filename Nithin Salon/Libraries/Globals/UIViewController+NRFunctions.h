//
//  UIViewController+NRFunctions.h
//  NRUtils
//
//  Created by Nithin Reddy on 15/12/15.
//  Copyright © 2015 Nithin Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (NRFunctions)

//Alert
-(void) showAlert : (NSString *) title MSG:(NSString *) message;
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag;
-(void) showAlert : (NSString *) message;
-(void) showAlert : (NSString *) title MSG:(NSString *) message BLOCK:(void (^)(void))callbackBlock;
+(NSString *) formatPhone;
//HUD
-(void) showHud;
-(void) hideHud;
-(void) showHudWithMessage : (NSString *)message;
-(void)hideBackButton;
//NSUserDefaults
-(void) saveSharedPreferenceValue : (NSString *)val KEY:(NSString *)key;
-(NSString *) loadSharedPreferenceValue : (NSString *) key;
-(NSString *) checkNull : (NSObject *) val;

- (UIColor *)colorFromHexString : (NSString *) hexString;
- (NSString *) getPathByFileName : (NSString *) fileName;
+(BOOL) isValidEmail : (NSString *) email;
+(void) loadDirections;
+(NSString *) getShareLongText;
+(NSString *) returnShareUrl;
+ (void) addBottomBar : (UIViewController *) con;

-(void) saveDictionary:(NSDictionary*)dict key:(NSString *)key;
-(NSDictionary *) loadDictionary : (NSString *)key;

-(void)removeNavgationBackButtonTitle;
-(BOOL)NSStringIsValidEmail:(NSString *)checkString;
@end
