//
//  ReceiptObj.h
//  Template
//
//  Created by Nithin Reddy on 04/09/14.
//
//

#import <Foundation/Foundation.h>

@interface ReceiptObj : NSObject

@property (nonatomic, retain) NSString *subtotal;
@property (nonatomic, retain) NSString *tax;
@property (nonatomic, retain) NSString *total;
@property (nonatomic, retain) NSString *tender;

@property (nonatomic, retain) NSMutableArray *services;

@end
