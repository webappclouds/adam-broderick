//
//  MyStylists.h
//  Template
//
//  Created by Nithin Reddy on 06/02/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MyStylists : UITableViewController

@property (nonatomic, retain) NSMutableArray *data;

@end
