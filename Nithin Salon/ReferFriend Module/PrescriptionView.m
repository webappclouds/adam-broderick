//
//  PrescriptionView.m
//  Template
//
//  Created by Nithin Reddy on 25/11/14.
//
//

#import "PrescriptionView.h"
#import "Constants.h"
#import "PrescriptionTimer.h"
#import "AppDelegate.h"
@interface PrescriptionView ()
{
    AppDelegate *appDelegate;
}
@end

@implementation PrescriptionView
{
    int requestFlag;
}

@synthesize productsTableView;
@synthesize obj;
@synthesize productsData;
@synthesize dateTime, stylistName, notes;
@synthesize pickedProdObj;
@synthesize pickAllButton;
@synthesize productsLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    requestFlag = 1;
    
    [self setTitle:@"Prescription"];
    productsData = [[NSMutableArray alloc] init];
    

    [self reloadData];
    
    [dateTime setText:[NSString stringWithFormat:@"Date: %@", obj.dateTime]];
    [stylistName setText:[NSString stringWithFormat:@"Service Provider: %@", obj.stylistName]];
    [notes setText:obj.note];
    [notes setFont:[UIFont systemFontOfSize:14.0f]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadData)];
}

-(void) reloadData
{
    
    [appDelegate showHUD];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", [appDelegate addSalonIdTo:URL_GET_PRESCRIPTION_PRODUCTS], obj.prescId];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
        }}];
    
    
}

-(void) getResponse:(NSMutableDictionary *)dict
{
    
    if(requestFlag==1)
    {
        NSArray *productsArr = [dict objectForKey:@"products"];
        BOOL status = [[dict objectForKey:@"status"] boolValue];
        if(!status || [productsArr count]==0)
            return;
        
        [pickAllButton setHidden:NO];
        [self.productsTableView setHidden:NO];
        [self.productsLabel setHidden:NO];
        [self.recommendedInfoButton setHidden:NO];
        if(productsArr.count)
        [productsData removeAllObjects];
        for(NSDictionary *tempObj in productsArr)
        {
            PrescProdObj *pObj = [[PrescProdObj alloc] init];
            [pObj setProductId:[tempObj objectForKey:@"prescription_product_id"]];
            [pObj setProductName:[tempObj objectForKey:@"product_name"]];
            [pObj setProductDesc:[tempObj objectForKey:@"product_description"]];
            [pObj setIsOrdered:[[tempObj objectForKey:@"is_ordered"] intValue]];
            [productsData addObject:pObj];
        }
        
        [self.productsTableView reloadData];
    }
    else
    {
        BOOL status = [[dict objectForKey:@"status"] boolValue];
        if(status)
            [self showAlert:@"Thank you" MSG:@"Your request has been sent to the Salon/Spa"];
        else
            [self showAlert:@"Error" MSG:@"Error occured, please try again later"];
    }
}

-(IBAction)recommendedProducts:(id)sender
{
    [self showAlert:@"Help" MSG:@"Please click the Pick up button if you would like to pickup the recommended products the next time you visit the Salon/Spa."];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productsData count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell2"];
    if(cell==Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell2"];
    
    PrescProdObj *prodObj = [productsData objectAtIndex:indexPath.row];
    [cell.textLabel setText:prodObj.productName];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.accessoryView = Nil;
    
    UIButton *pickButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    pickButton.frame = CGRectMake(cell.frame.size.width-100, 7.0f, 60, 30.0f);
    [pickButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    pickButton.tag = indexPath.row;
    [pickButton setBackgroundColor:[UIColor blackColor]];
    if(prodObj.isOrdered)
        [pickButton setBackgroundColor:[UIColor grayColor]];
    [pickButton setTitle:@"Pick up" forState:UIControlStateNormal];
    [pickButton addTarget:self action:@selector(pickClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.accessoryView = pickButton;
    
    return cell;
}

-(IBAction)pickClicked:(id)sender
{
    UIButton *b = (UIButton*)sender;
    pickedProdObj = [productsData objectAtIndex:[b tag]];
    if(pickedProdObj.isOrdered==1)
        [self showAlert:@"Already Sent" MSG:@"You have already sent a request to pick this product up."];
    else
        [self openDatePage:1];
}

-(IBAction)pickAll:(id)sender
{
    pickedProdObj = Nil;
    BOOL isAtleastOneNotOrdered = NO;
    for(PrescProdObj *prescProdObj in productsData)
        if(prescProdObj.isOrdered==0)
        {
            isAtleastOneNotOrdered = YES;
            break;
        }
    if(isAtleastOneNotOrdered)
        [self openDatePage:2];
    else
        [self showAlert:@"Already Sent" MSG:@"You have already sent a request to pick the products up."];
}

-(void) openDatePage : (int) flag
{
    PrescriptionTimer *timer =[self.storyboard instantiateViewControllerWithIdentifier:@"PrescriptionTimer"];
    [timer setFlag:flag];
    [timer setPrescId:obj.prescId];
    [timer setPrescViewDelegate:self];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    if(flag==1)
        [timer setProdObj:pickedProdObj];
    
    [self.navigationController pushViewController:timer animated:YES];
}

-(void) orderSuccessfulCallback
{
    [self reloadData];
    [self.productsTableView reloadData];
}

@end
