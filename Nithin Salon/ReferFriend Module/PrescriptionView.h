//
//  PrescriptionView.h
//  Template
//
//  Created by Nithin Reddy on 25/11/14.
//
//

#import <UIKit/UIKit.h>
#import "PrescriptionObj.h"
#import "PrescProdObj.h"

@interface PrescriptionView : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) PrescriptionObj *obj;

@property (nonatomic, retain) IBOutlet UITableView *productsTableView;

@property (nonatomic, retain) IBOutlet UILabel *productsLabel;
@property (nonatomic, retain) IBOutlet UIButton *pickAllButton;
@property (nonatomic, retain) IBOutlet UIButton *recommendedInfoButton;

@property (nonatomic, retain) IBOutlet UILabel *dateTime;
@property (nonatomic, retain) IBOutlet UILabel *stylistName;
@property (nonatomic, retain) IBOutlet UITextView *notes;

@property (nonatomic, retain) NSMutableArray *productsData;

@property (nonatomic, retain) PrescProdObj *pickedProdObj;

@property (nonatomic, retain) NSMutableArray *alreadyOrderedProducts;

-(IBAction)recommendedProducts:(id)sender;

-(void) orderSuccessfulCallback;

@end
