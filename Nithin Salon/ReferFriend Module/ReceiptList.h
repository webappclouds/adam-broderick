//
//  ReceiptList.h
//  Nithin Salon
//
//  Created by Webappclouds on 28/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormViewController.h"
#import "XLForm.h"
#import "ReceiptObj.h"

@interface ReceiptList : XLFormViewController
@property (nonatomic, retain) ReceiptObj *obj;

@end
