


//
//  MyAccountList.m
//  Template
//
//  Created by Nithin Reddy on 05/02/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import "MyAccountList.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "ReceiptObj.h"
#import "MillApptList.h"
#import "PrescriptionList.h"
#import "MyStylists.h"
#import "ReceiptList.h"
#import "ViewController.h"
#import "PendingAppoitmentsVC.h"
#import "BeveragesList.h"
#import "ProfileViewController.h"
#import "WalletMainTableViewController.h"
#import "TGLViewController.h"
#import "MyPointsVC.h"

@interface MyAccountList ()
{
    AppDelegate *appDelegate;
    BOOL isDistance;
    UIBarButtonItem *checkin;
    UIButton *transparentButton;
}
@end

@implementation MyAccountList

@synthesize data,fromLogin;
@synthesize gcBal, loyaltyPoints;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    UIBarButtonItem *logout = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logoutCheck)];
    [logout setTintColor:[UIColor whiteColor]];
    //[self.navigationItem setRightBarButtonItem:logout animated:YES];
    
    
    checkin = [[UIBarButtonItem alloc] initWithTitle:@"Check-In" style:UIBarButtonItemStylePlain target:self action:@selector(checkInProcess)];
    [checkin setTintColor:[UIColor whiteColor]];
    NSMutableArray *buttonArray=[[NSMutableArray alloc]initWithCapacity:2];
    [buttonArray addObject:logout];
//    [buttonArray addObject:checkin];
    self.navigationItem.rightBarButtonItems = buttonArray;
    // self.navigationItem.rightBarButtonItems = ;
    //[self.navigationItem setRightBarButtonItem:logout animated:YES];
    
    self.navigationItem.hidesBackButton = NO;
    // UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chevron"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    //self.navigationItem.leftBarButtonItem = backButton;
    
    data = [[NSMutableArray alloc] init];
    self.title = @"My Account";
    
    
    
    if ([appDelegate.dynamicApptList containsObject:@"Request a new Appt"]) {
        [data addObject:@"My Pending Appointments"];
    }
    [data addObject:@"My Appointments"];
    //[data addObject:@"My Past Appointments"];
    [data addObject:@"Prescription"];
    [data addObject:@"My Service Providers"];
    [data addObject:@"My Profile"];
    [data addObject:@"My Wallet"];
    
    NSString *myPoinstStr=[self loadSharedPreferenceValue:@"myPoints"];
    if([myPoinstStr isEqualToString:@"1"])
    {
        [data addObject:@"My Points"];

    }

    
    
    if([[self loadSharedPreferenceValue:@"appt_type"] isEqualToString:@"IRIS"] || [[self loadSharedPreferenceValue:@"appt_type"] isEqualToString:@"Salonbiz"])
    {
        [data addObject:@"Giftcard Balance"];
        [data addObject:@"Loyalty Points"];
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[self loadSharedPreferenceValue:@"slc_id"],@"slc_id", nil];
        [appDelegate showHUD];
        
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_GC_BALANCE_WITH_SLC_ID] HEADERS:nil GETPARAMS:nil POSTPARAMS:params COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
                [self getResponse:dict];
            }}];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    transparentButton = [[UIButton alloc] init];
    [transparentButton setFrame:CGRectMake(0,0, 50, 40)];
    [transparentButton setBackgroundColor:[UIColor clearColor]];
    [transparentButton addTarget:self action:@selector(backk) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:transparentButton];
}

- (void) backk{
    
    [transparentButton removeFromSuperview];
    
    for(UIViewController *controller in self.navigationController.viewControllers){
        if([controller isKindOfClass:[ViewController class]]){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*-(void)viewDidDisappear:(BOOL)animated{
 if([fromLogin isEqualToString:@"YES"]){
 for(UIViewController *controller in self.navigationController.viewControllers){
 if([controller isKindOfClass:[ViewController class]]){
 
 
 [self.navigationController popToRootViewControllerAnimated:YES];
 }
 }
 }
 }*/

#pragma mark check in Process
-(void)checkInProcess{
    [transparentButton removeFromSuperview];
    if ([CLLocationManager locationServicesEnabled]){
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            [locationManager requestWhenInUseAuthorization];
        
        [locationManager startUpdatingLocation];
        
    }
    else{
        
        if(appDelegate.checInBeaconsDictionary.count){
            
            NSString *major =[appDelegate.checInBeaconsDictionary objectForKey:@"major"];
            NSString *minor =[appDelegate.checInBeaconsDictionary objectForKey:@"minor"];
            [self geoFenceServiceCallRequest:major minor:minor];
        }
        else{
            
            UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Alert"  message:@"We were not able to find you within the range of the Salon"  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    //NSLog(@"salon latitude and longitude: %f,%f",appDelegate.spaLatitude,appDelegate.spaLongitude);
    locationManager.delegate = nil;
    [locationManager stopUpdatingLocation];
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:[appDelegate.spaLatitude doubleValue] longitude:[appDelegate.spaLongitude doubleValue]];
    
    CLLocationDistance distanceInMeters = [locA distanceFromLocation:locB];
    
    NSLog(@"distance: %f",distanceInMeters);
    int distancevalue = (int) round(distanceInMeters);
    NSLog(@"distance: %i",distancevalue);
    if(distancevalue<=100)
    {
        [self geoFenceServiceCallRequest:@"" minor:@""];
    }
    else{
        if(appDelegate.checInBeaconsDictionary.count){
            NSString *major =[appDelegate.checInBeaconsDictionary objectForKey:@"major"];
            NSString *minor =[appDelegate.checInBeaconsDictionary objectForKey:@"minor"];
            [self geoFenceServiceCallRequest:major minor:minor];
        }
        else{
            UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Alert"  message:@"We were not able to find you within the range of the Salon"  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}



-(void)geoFenceServiceCallRequest:(NSString *)majorVal minor:(NSString*)minorVal{
    
    [appDelegate showHUD];
    
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"HH:mm"];
    
    NSString *currentDate = [df stringFromDate:[NSDate date]];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    [dict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    //[dict setObject:currentDate forKey:@"checkInTime"];
    [dict setObject:appDelegate.salonId forKey:@"salon_id"];
    NSString *urlString;
    if(majorVal.length && minorVal.length){
        [dict setObject:majorVal?:@"" forKey:@"major"];
        [dict setObject:minorVal?:@"" forKey:@"minor"];
        urlString =GEOFENCE_WITHBEACONS_URL;
        
    }
    else{
        urlString =GEOFENCE_WITHLOCATION_URL;
    }
    NSLog(@"server dict: %@",dict);
    
    
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            BOOL status = [[dict objectForKey:@"status"] boolValue];
            
            if(!status)
                [self showAlert:@"Check-In" MSG:[dict objectForKey:@"message"] BLOCK:^{
                }];
            else
            {
                [self showAlertView:@"Thank you for checking in" message:@"Please take a seat, the stylist will see you in a few minutes. Would you like a free beverage?"];
            }
        }}];
}

-(void) getResponse:(NSMutableDictionary *)dict
{
    
    gcBal = [[NSString alloc] initWithString:[dict objectForKey:@"gc_bal"]];
    loyaltyPoints = [[NSString alloc] initWithString:[dict objectForKey:@"loyalty_pts"]];
    
    if(![[self loadSharedPreferenceValue:@"appt_type"] isEqualToString:@"IRIS"])
        return;
    
    NSString *subTotal = [dict objectForKey:@"subtotal"];
    NSString *tax = [dict objectForKey:@"tax"];
    NSString *total = [dict objectForKey:@"total"];
    NSString *tender = [dict objectForKey:@"tender"];
    NSArray *services = [dict objectForKey:@"services"];
    
    if(tender==Nil || tender.length==0 || [tender isEqualToString:@"0"])
        return;
    
    [self saveSharedPreferenceValue:subTotal KEY:@"receiptSubTotal"];
    [self saveSharedPreferenceValue:total KEY:@"receiptTotal"];
    [self saveSharedPreferenceValue:tax KEY:@"receiptTax"];
    [self saveSharedPreferenceValue:tender KEY:@"receiptTender"];
    
    NSMutableArray *servicesTempArr = [[NSMutableArray alloc] init];
    for(NSDictionary *subDict in services)
    {
        NSString *servName = [subDict objectForKey:@"service"];
        NSString *cost = [subDict objectForKey:@"price"];
        NSString *subStr = [NSString stringWithFormat:@"%@SUBSUB%@", servName, cost];
        [servicesTempArr addObject:subStr];
    }
    
    if([servicesTempArr count]>0)
    {
        NSString *mainStr = [servicesTempArr componentsJoinedByString:@"MNYMYN"];
        [self saveSharedPreferenceValue:mainStr KEY:@"receiptServices"];
    }
}

-(void)goBackToHome{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)logoutCheck
{   fromLogin =@"NO";
    [transparentButton removeFromSuperview];
    [self saveSharedPreferenceValue:@"" KEY:@"slc_id"];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"MyAccountCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.backgroundColor = [UIColor clearColor];
    if(self.data.count){
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        cell.textLabel.text = [data objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"Open Sans-Bold" size:18];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}



#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    fromLogin =@"NO";
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    
    AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    if ([appDelegate.dynamicApptList containsObject:@"Request a new Appt"]) {
        
        if(indexPath.row==0)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
            PendingAppoitmentsVC *tableViewController =[self.storyboard instantiateViewControllerWithIdentifier:@"PendingAppoitmentsVC"];
            tableViewController.appointmentModule =_appointmentMOdule;
            [self .navigationController pushViewController:tableViewController animated:YES];
            
        }
        
//        else if(indexPath.row==1)
//        {
//            
//            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//            
//            MillApptList *applist = [self.storyboard instantiateViewControllerWithIdentifier:@"MillApptList"];
//            applist.flagStr =@"ConfirmedAppt";
//            [self.navigationController pushViewController:applist animated:YES];
//            
//        }
        
        else if(indexPath.row==1)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            
            MillApptList *applist = [self.storyboard instantiateViewControllerWithIdentifier:@"MillApptList"];
            [self.navigationController pushViewController:applist animated:YES];
        }
        
        
        else if(indexPath.row==2)
        {
            
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            
            PrescriptionList *applist = [self.storyboard instantiateViewControllerWithIdentifier:@"PrescriptionList"];
            [self.navigationController pushViewController:applist animated:YES];
            
            
        }
        else if(indexPath.row==3)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            MyStylists *applist = [self.storyboard instantiateViewControllerWithIdentifier:@"MyStylists"];
            [self.navigationController pushViewController:applist animated:YES];
        }
        else if(indexPath.row==4)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Gallery" bundle:nil];
            ProfileViewController *applist = [storyBoard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            [self.navigationController pushViewController:applist animated:YES];
            
        }
        else if(indexPath.row==5)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            // UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Gallery" bundle:nil];
//            WalletMainTableViewController *wmt = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletMainTableViewController"];
//            [self.navigationController pushViewController:wmt animated:YES];
            TGLViewController *tglViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TGLViewController"];
            [self.navigationController pushViewController:tglViewController animated:YES];
            
        }
        else if (indexPath.row==6)
        {
            [self showMyPoints];
            return;

            if(gcBal!=nil)
                [self showAlert:@"Giftcard Balance" MSG:[NSString stringWithFormat:@"Your current giftcard balance is $%@", gcBal]];
        }
        else if(indexPath.row==7)
        {
            if(loyaltyPoints!=nil)
                [self showAlert:@"Loyalty Points" MSG:[NSString stringWithFormat:@"Your current loyalty points is %@", loyaltyPoints]];
        }
    }
    else{
        
        if(indexPath.row==0)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            
            MillApptList *applist = [self.storyboard instantiateViewControllerWithIdentifier:@"MillApptList"];
            applist.flagStr =@"ConfirmedAppt";
            [self.navigationController pushViewController:applist animated:YES];
            
        }
//        else if(indexPath.row==1)
//        {
//            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//            
//            MillApptList *applist = [self.storyboard instantiateViewControllerWithIdentifier:@"MillApptList"];
//            
//            [self.navigationController pushViewController:applist animated:YES];
//        }
        else if(indexPath.row==1)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
            PrescriptionList *prescriptionList = [self.storyboard instantiateViewControllerWithIdentifier:@"PrescriptionList"];
            [self.navigationController pushViewController:prescriptionList animated:YES];
            
        }
        else if(indexPath.row==2)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
            
            MyStylists *stylists = [self.storyboard instantiateViewControllerWithIdentifier:@"MyStylists"];
            [self.navigationController pushViewController:stylists animated:YES];
            
        }
        else if(indexPath.row==3)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Gallery" bundle:nil];
            ProfileViewController *applist = [storyBoard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            [self.navigationController pushViewController:applist animated:YES];
            
        }
        else if(indexPath.row==4)
        {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            // UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Gallery" bundle:nil];
//            WalletMainTableViewController *wmt = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletMainTableViewController"];
//            [self.navigationController pushViewController:wmt animated:YES];
            TGLViewController *tglViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TGLViewController"];
            [self.navigationController pushViewController:tglViewController animated:YES];
            
        }
        else if (indexPath.row==5)
        {
            [self showMyPoints];
            return;
            
            if(gcBal!=nil)
                [self showAlert:@"Giftcard Balance" MSG:[NSString stringWithFormat:@"Your current giftcard balance is $%@", gcBal]];
        }
        else if(indexPath.row==6)
        {
            if(loyaltyPoints!=nil)
                [self showAlert:@"Loyalty Points" MSG:[NSString stringWithFormat:@"Your current loyalty points is %@", loyaltyPoints]];
        }
        //        [data addObject:@"My Points"];

    }
    
}
-(void) showMyPoints
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    MyPointsVC *applist = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPointsVC"];
    [self.navigationController pushViewController:applist animated:YES];

}

-(void) lastReceipt
{
    ReceiptObj *receiptObj = [self getLastReceipt];
    if(receiptObj==nil)
    {
        [self showAlert:@"Empty" MSG:@"No receipts found"];
        return;
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    ReceiptList *list = [self.storyboard instantiateViewControllerWithIdentifier:@"ReceiptList"];
    [list setObj:[self getLastReceipt]];
    [self.navigationController pushViewController:list animated:YES];
}

-(ReceiptObj *) getLastReceipt
{
    NSString *subTotal = [self loadSharedPreferenceValue:@"receiptSubTotal"];
    NSString *total = [self loadSharedPreferenceValue:@"receiptTotal"];
    NSString *tax = [self loadSharedPreferenceValue:@"receiptTax"];
    NSString *servicesMainStr = [self loadSharedPreferenceValue:@"receiptServices"];
    NSString *tender = [self loadSharedPreferenceValue:@"receiptTender"];
    
    if([subTotal length]==0 || [total length]==0 || [tax length]==0 || [servicesMainStr length]==0)
        return Nil;
    
    ReceiptObj *obj = [[ReceiptObj alloc] init];
    [obj setSubtotal:subTotal];
    [obj setTax:tax];
    [obj setTotal:total];
    [obj setTender:tender];
    obj.services = [[NSMutableArray alloc] init];
    
    NSArray *tempArr = [servicesMainStr componentsSeparatedByString:@"MNYMYN"];
    
    for(NSString *str in tempArr)
    {
        ServiceObj *servObj = [[ServiceObj alloc] init];
        NSArray *subTemp = [str componentsSeparatedByString:@"SUBSUB"];
        servObj.serviceName = [subTemp objectAtIndex:0];
        servObj.cost = [subTemp objectAtIndex:1];
        [obj.services addObject:servObj];
        
    }
    return obj;
}
-(void)showAlertView:(NSString *)title message:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
        BeveragesList *bvl = [self.storyboard instantiateViewControllerWithIdentifier:@"BeveragesList"];
        [self.navigationController pushViewController:bvl animated:YES];
        
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [transparentButton removeFromSuperview];
}

@end
