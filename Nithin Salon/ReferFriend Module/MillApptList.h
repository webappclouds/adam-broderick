//
//  MyAppointMentsTblViewCon.h
//  Restaurants_Webapp
//
//  Created by Bhalchandra Deogaonkar on 30/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>


@class MillApptList;

@interface MillApptList : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic,retain) NSMutableArray *filteredData;
@property (nonatomic,retain) NSMutableArray *allData;
//@property (nonatomic,retain) NSMutableArray *arrJson;
@property (nonatomic,retain) NSString *flagStr;
@property (nonatomic,retain)IBOutlet UISegmentedControl *segController;
- (IBAction)timeSegmentAction:(id)sender;

@end
