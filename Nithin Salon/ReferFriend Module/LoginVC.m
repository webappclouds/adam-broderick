//
//  LoginVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "LoginVC.h"
#import "RegisterVC.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "ReferFriendVC.h"
#import "OnlineBookindViewCon.h"
#import "MyAccountList.h"
#import "AppDelegate.h"
#import "ReviewUsVC.h"
#import "MacAddressHelper.h"
#import "OnlineServicesViewController.h"
#import "BeforeAfterVc.h"
@interface LoginVC ()
{
    int type;
    int isRequestForForgotPassword;
    int shouldLogoutFbFlag;
    AppDelegate *appDelegate;
}
@end

@implementation LoginVC
@synthesize key1,key2;
- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title =@"Login";
//    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"login5.png"]]];
    _emailTF.layer.borderColor=[UIColor whiteColor].CGColor;
    _emailTF.layer.borderWidth=1.0;
    _passwordTF.layer.borderColor=[UIColor whiteColor].CGColor;
    _passwordTF.layer.borderWidth=1.0;
    
    _passwordTF.secureTextEntry =YES;
    
    [self textField:self.emailTF   setPlaceHolderColor:[UIColor lightGrayColor] text:self.emailTF.placeholder];
    [self textField:self.passwordTF    setPlaceHolderColor:[UIColor lightGrayColor] text:self.passwordTF.placeholder];
    // Do any additional setup after loading the view.
}

-(void)textField:(UITextField*)textField setPlaceHolderColor:(UIColor*)color text:(NSString*)placeholderText
{
    if ([textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholderText attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
}

-(IBAction)loginClick:(id)sender{
    NSString *emailAddrStr = self.emailTF.text?:@"";
    NSString *passwordStr =self.passwordTF.text?:@"";
    
    
    if([emailAddrStr length]==0)
        [self showAlert:@"Email Address" MSG:@"Please enter your email address"];
    else if(![UIViewController isValidEmail:emailAddrStr])
        [self showAlert:@"Email Address" MSG:@"Please enter a valid email address"];
    else if([passwordStr length]==0)
        [self showAlert:@"Password" MSG:@"Please enter your password"];
    else
    {
        [self.emailTF resignFirstResponder];
        [self.passwordTF resignFirstResponder];
        
        [self attemptToLogin];
    }

}
-(IBAction)forgotPasswordClick:(id)sender{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Forgot password"
                                  message:@"Please enter your email address"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   [self forgotServiceCall:[[alert textFields][0] text]];
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter your email";
        textField.text = _emailTF.text?:@"";
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(IBAction)singUpClick:(id)sender{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    RegisterVC *registerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    [self.navigationController pushViewController:registerVC animated:YES];
}


-(void)showAlert : (NSString *) title MSG:(NSString *) message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark login action
-(void) attemptToLogin
{
    [self setTitle:@"Login"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@"1" forKey:@"type"];
    
   //token
    if(appDelegate.pushToken==nil || [appDelegate.pushToken length]==0)
        appDelegate.pushToken=[MacAddressHelper getMacAddress];
    else
         appDelegate.pushToken = [self loadSharedPreferenceValue:@"token"];
    
    
        [params setObject:appDelegate.pushToken?:@"" forKey:@"token"];
        [params setObject:_emailTF.text forKey:@"login_email"];
        [params setObject:_passwordTF.text forKey:@"login_password"];
        [self saveSharedPreferenceValue:_emailTF.text KEY:@"user_login_email"];
        [self saveSharedPreferenceValue:_passwordTF.text KEY:@"user_login_password"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_USER_LOGIN] HEADERS:nil GETPARAMS:nil POSTPARAMS:params COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            [self saveSharedPreferenceValue:[NSString stringWithFormat:@"%@ ",_emailTF.text] KEY:@"RegEmail"];
            
            [self serviceResponse:responseArray];
        }
    }];
    
    
}

-(void)serviceResponse:(NSArray*)responseArray{
    if(responseArray.count){
        NSMutableDictionary *dict = (NSMutableDictionary*)responseArray;
        
        BOOL status = [[dict objectForKey:@"status"] boolValue];
        
        NSLog(@"Login Details : %@",dict);

        if(isRequestForForgotPassword==1){
            
            isRequestForForgotPassword = 0;
            if(status)
                [self showAlert:@"Success" MSG:@"Your password has been sent to your email address"];
            else
                [self showAlert:@"Error" MSG:@"We are not able to find your email address in our database. Please check your email address and try again."];
            return;
        }
        
        if(!status){
            if(type==1)
                [self showAlert:@"Login error" MSG:@"Please check your credentials or register"];
            else{
                 [self showAlert:@"Login error" MSG:@"Please check your credentials or register"];
            }
            
        }
        else    //Successfully logged in
        {
            
            [self saveDictionary:nil key:@"CheckInBeacon"];
            [self hideBackButton];
            AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
            NSString *integrationType = [dict objectForKey:@"appt_type"];
            [self saveSharedPreferenceValue:integrationType KEY:@"appt_type"];
            [self saveSharedPreferenceValue:[dict objectForKey:@"slc_id"] KEY:@"slc_id"];
            [self saveSharedPreferenceValue:[dict objectForKey:@"clientid"] KEY:@"clientid"];
            [self saveSharedPreferenceValue:[dict objectForKey:@"myPoints"] KEY:@"myPoints"];

            [self saveSharedPreferenceValue:[dict objectForKey:@"name"] KEY:@"name"];

            if ([objAppdel.flgObj isEqualToString:@"referFrndFlg"]){
                [self referFriend];
            }
            else if([objAppdel.flgObj isEqualToString:@"FromPush"]){
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                ReviewUsVC *galleryVc=[storyboard instantiateViewControllerWithIdentifier:@"ReviewUsVC"];
                galleryVc.comparisonFlag =@"FromPush";
                [self.navigationController pushViewController:galleryVc animated:YES];
            }
            
           else  if ([objAppdel.flgObj isEqualToString:@"onlineBookFlag"]){
               self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

               self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
               OnlineServicesViewController *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineServicesViewController"];
               
               [self.navigationController pushViewController:onlineBookindViewCon animated:YES];

            }
           else if([objAppdel.flgObj isEqualToString:@"beforeAndAfter"]){
//               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//               ReviewUsVC *galleryVc=[storyboard instantiateViewControllerWithIdentifier:@"ReviewUsVC"];
//               galleryVc.comparisonFlag =@"FromPush";
//               [self.navigationController pushViewController:galleryVc animated:YES];
               
               self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
               
               BeforeAfterVc *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"BeforeAfterVc"];
               
               [self.navigationController pushViewController:scratch animated:YES];

           }

            
            else{
                
                //
                if(integrationType==nil || [integrationType length]==0){
                    
                    [self showAlert:@"Error" MSG:@"Error occured. Try again later."];
                    return;
                }
                else{
                   
                    if([objAppdel.flgObj isEqualToString:@"checkApptFlg"]){
                        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

                        MyAccountList *account = [self.storyboard instantiateViewControllerWithIdentifier:@"MyAccountList"];
                        account.fromLogin =@"YES";
                        account.appointmentMOdule = self.appointmentModule;
                        [self.navigationController pushViewController:account animated:YES];

                    }
                    
            }
        }
        
        
    }
}
}

-(void)forgotServiceCall:(NSString *)emailStr{
    NSString *emailToSend = emailStr;
    if([emailToSend length]==0 || ![UIViewController isValidEmail:emailToSend])
        [self showAlert:@"Email" MSG:@"Please enter a valid email address"];
    else
    {
        isRequestForForgotPassword = 1;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
        [dict setObject:emailToSend forKey:@"login_email"];
        
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_USER_FRGT_PSWD] HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                [self serviceResponse:responseArray];
            }
        }];
    }
    
}
-(void)referFriend{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    ReferFriendVC *staffListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReferFriendVC"];
    [self.navigationController pushViewController:staffListVC animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
