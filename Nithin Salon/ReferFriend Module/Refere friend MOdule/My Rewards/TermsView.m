//
//  EventFullDetails.m
//  Pure Extensions
//
//  Created by Nithin Reddy on 14/06/13.
//
//

#import "TermsView.h"

@interface TermsView ()

@end

@implementation TermsView

@synthesize textView;
@synthesize testStr;

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

#pragma mark -
#pragma mark Lifecycle

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization.
    }
    return self;
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self setTitle:@"Terms"];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [textView.layer setCornerRadius:10.0];
    [textView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [textView.layer setBorderWidth:1.0];
    
    [textView setEditable:YES];
    [textView setFont:[UIFont systemFontOfSize:16]];
    //[textView setText:@"Hi"];
    [textView setText:testStr];
    [textView setEditable:NO];
    
}



@end
