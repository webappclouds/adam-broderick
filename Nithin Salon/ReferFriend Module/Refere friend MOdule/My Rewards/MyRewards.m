//
//  MyRewards.m
//  DJW
//
//  Created by Nithin Reddy on 30/03/13.
//
//

#import "MyRewards.h"
#import "ReferralCell.h"
#import "ReferralGlobals.h"
#import "ReferralObj.h"
#import "GlobalSettings.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "MyRewardsDetail.h"

@interface MyRewards ()

@end

@implementation MyRewards
{
    NSMutableArray *giftCardsList;
    NSMutableArray *giftCardsObjList;
    NSString *slcIdStr;
    NSString *clientIdStr;
    AppDelegate *appDelegate;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    giftCardsList = [ReferralGlobals getVoucherCodes];
    
    giftCardsObjList = [[NSMutableArray alloc] init];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"My Rewards";
    
    slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    clientIdStr=[self loadSharedPreferenceValue:@"clientid"];
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setObject:slcIdStr forKey:@"SlcId"];
    [params setObject:clientIdStr forKey:@"ClientId"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_REFERRAL2_REWARDS] HEADERS:Nil GETPARAMS:Nil POSTPARAMS:params COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            [self getResponse:responseDict];
        }
    }];
 }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [giftCardsObjList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *infoCell=@"myrewardsCell";
    ReferralCell *cell=(ReferralCell *)[tableView dequeueReusableCellWithIdentifier:infoCell];
    
    if (cell == nil) {
        cell = [[ReferralCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:infoCell];
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.label3.text = [[giftCardsObjList objectAtIndex:indexPath.row] titleStr];
    cell.label2.text = [NSString stringWithFormat:@"Reward Amount: $%@",[[giftCardsObjList objectAtIndex:indexPath.row] rewardAmount]];
    cell.label1.text = [[giftCardsObjList objectAtIndex:indexPath.row] voucherCode];
    [cell.moreInfo setTag:indexPath.row];
    UIButton *button = (UIButton *)[cell moreInfo];
    [button setTag:indexPath.row];
    [button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(IBAction)buttonTapped:(id)sender
{
    UIButton *b =(UIButton *)sender;
    ReferralObj *obj = [giftCardsObjList objectAtIndex:[b tag]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    //    NSString *voucher = [giftCardsList objectAtIndex:[sender tag]];
    MyRewardsDetail *myGiftCardDetail = [[MyRewardsDetail alloc] initWithNibName:@"MyRewardsDetail" bundle:nil];
    [myGiftCardDetail setReferralObj:obj];
    [self pushController:myGiftCardDetail withTransition:UIViewAnimationOptionTransitionFlipFromLeft];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

-(void) showAlert : (NSString *) title MSG : (NSString *)msg
{
    [self showAlert:title MSG:msg TAG:0];
}

-(void) showAlert : (NSString *) title MSG : (NSString *)msg TAG : (int) tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert setTag:tag];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if([alertView tag]==1)
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)pushController:(UIViewController *)controller withTransition:(UIViewAnimationTransition)transition{
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:
     UIViewAnimationTransitionFlipFromRight
                           forView:self.navigationController.view cache:NO];
    
    
    [self.navigationController pushViewController:controller animated:YES];
    [UIView commitAnimations];
    
}

#pragma mark -
#pragma mark SendRequest delegate
-(void)getResponse:(NSDictionary *)strResponse{
    
    NSDictionary *dict =strResponse;
    if([[dict objectForKey:@"status"] boolValue])
    {
        NSMutableArray *vouchers = [dict objectForKey:@"reward_details"];
        for(int i=0;i<[vouchers count];i++)
        {
            ReferralObj *obj = [[ReferralObj alloc] init];
            NSDictionary *subDict = [vouchers objectAtIndex:i];
            [obj setVoucherCode:[subDict objectForKey:@"voucherCode"]];
            [obj setTitleStr:[subDict objectForKey:@"reward_title"]];
            [obj setRewardAmount:[subDict objectForKey:@"RewardAmount"]];
            [obj setExpiryDate:[subDict objectForKey:@"rewardExpiryDate"]];
            [obj setDescriptionStr:[subDict objectForKey:@"reward_description"]];
            [giftCardsObjList addObject:obj];
        }

        if([vouchers count]>0)
            [self.tableView reloadData];
        else
            [self showAlert:@"None" MSG:@"No vouchers found." TAG:1];
    }
    else
    {
        [self showAlert:@"Error" MSG:@"Error occured. Please try again later."];
    }
}

@end
