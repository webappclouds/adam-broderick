//
//  ReferFriendVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ReferFriendVC.h"
#import "Globals.h"
#import "Constants.h"
#import "GlobalSettings.h"
#import "UIViewController+NRFunctions.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>
#import "TermsView.h"
#import "MyRewards.h"
#import "RefFriendsList.h"
#import "AppDelegate.h"
#import "ViewController.h"
@interface ReferFriendVC ()
{
    int refId;
    NSString *slcIdStr;
    NSString *clientIdStr;
    AppDelegate *appDelegate;
    UIButton *transparentButton;
}

@end


@implementation ReferFriendVC
@synthesize refTitleLable,amountLable,propleRefLabel,DescTextView;


- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setTitle:@"REFER"];

    _peopleBgView.layer.cornerRadius = 5.0;
    _peopleBgView.layer.borderColor  = [UIColor blackColor].CGColor;
    _peopleBgView.layer.borderWidth = 2.0f;

    _rewardsButton.layer.cornerRadius = 5.0;
    _rewardsButton.layer.borderColor  = [UIColor clearColor].CGColor;
    _rewardsButton.layer.borderWidth = 2.0f;

    
    _sentReferralsButton.layer.cornerRadius = 5.0;
    _sentReferralsButton.layer.borderColor  = [UIColor clearColor].CGColor;
    _sentReferralsButton.layer.borderWidth = 2.0f;


    // Do any additional setup after loading the view.
}

-(void)back3{
   
        
        [transparentButton removeFromSuperview];
        
        for(UIViewController *controller in self.navigationController.viewControllers){
            if([controller isKindOfClass:[ViewController class]]){
                
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
        
        
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.DescTextView setContentOffset:CGPointZero animated:NO];

    transparentButton = [[UIButton alloc] init];
    [transparentButton setFrame:CGRectMake(0,0, 50, 40)];
    [transparentButton setBackgroundColor:[UIColor clearColor]];
    [transparentButton addTarget:self action:@selector(back3) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:transparentButton];
    [self serviceCallforReferral];
}
-(void)serviceCallforReferral
{
    slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    clientIdStr=[self loadSharedPreferenceValue:@"clientid"];
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setObject:slcIdStr forKey:@"SlcId"];
    [params setObject:clientIdStr forKey:@"ClientId"];
    
    [appDelegate showHUD];
    
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_REFERAL2_GET_REFID] HEADERS:Nil GETPARAMS:Nil POSTPARAMS:params COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            [self getResponse:responseDict];
        }
    }];
}
-(void) getResponse:(NSDictionary *)strResponse
{
    NSLog(@"Refer =%@",strResponse);
    NSDictionary *dict =strResponse;
    BOOL status = [[dict objectForKey:@"status"] boolValue];
    if(status)
    {
        refId = [[dict objectForKey:@"id"] intValue];
        
        NSDictionary *refeDetailsDict=[dict objectForKey:@"referrer_details"];
        NSString *nameStr=[refeDetailsDict objectForKey:@"name"];
        
        NSDictionary *refeDict=[dict objectForKey:@"referral_settings"];
        [refTitleLable setText:[refeDict objectForKey:@"referrals_title"]];
        NSString *refDesc=[refeDict objectForKey:@"referrals_description"];
        
        NSString *html = refDesc;
        NSAttributedString *attr = [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                              NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                         documentAttributes:nil
                                                                      error:nil];
        NSString *finalString = [attr string];
        NSString *rewardDesc=[refeDict objectForKey:@"reward_description"];
        NSString *desc=[NSString stringWithFormat:@"%@\n%@",rewardDesc,finalString];
        NSString *termsStr=[refeDict objectForKey:@"terms_conditions"];
        
        [DescTextView setEditable:YES];
        [DescTextView setFont:[UIFont systemFontOfSize:14]];
        [DescTextView setText:desc];
        [DescTextView setEditable:NO];
        
        [self.DescTextView setContentOffset:CGPointZero animated:NO];
//        self.DescTextView.contentInset = UIEdgeInsetsMake(-20.0f, 0.0f, 0.0f, 0.0f);

        [propleRefLabel setText:[dict objectForKey:@"total_referrals_count"]];
        NSString *amount=[NSString stringWithFormat:@"$%@",[dict objectForKey:@"rewards_total_amount"]];
        [amountLable setText:amount];
        
        NSString *regularParam = [NSString stringWithFormat:@"%@-%d", appDelegate.salonId, refId];
        
        [self saveSharedPreferenceValue:regularParam KEY:@"RefrerralLink"];
        [self saveSharedPreferenceValue:nameStr KEY:@"NameKey"];
        [self saveSharedPreferenceValue:termsStr KEY:@"TermsKey"];
        
    }
    else{
        
        [self showAlert:@"Error" MSG:@"Error occured. Please try again"];

    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshPropertyList{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(IBAction)email:(id)sender
{
    NSString *link=[self loadSharedPreferenceValue:@"RefrerralLink"];
    NSString *regularParam = [NSString stringWithFormat:@"%@-%@",link,@"m"];
    NSString *base64String = [self base64Encode:regularParam];
    base64String = [self  base64Encode:base64String];
    base64String = [self  base64Encode:base64String];
    
    //URL Encode it
    base64String = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                         NULL,
                                                                                         (CFStringRef)base64String,
                                                                                         NULL,
                                                                                         (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                         kCFStringEncodingUTF8 ));
    NSString *refLink = [NSString stringWithFormat:@"%@%@", URL_REFERAL2_LINK, base64String];
    
    NSString *name=[self loadSharedPreferenceValue:@"NameKey"];
    [self displayComposerSheet:name LINK:refLink];
    
}
-(IBAction)message:(id)sender
{
    NSString *link=[self loadSharedPreferenceValue:@"RefrerralLink"];
    NSString *regularParam = [NSString stringWithFormat:@"%@-%@",link,@"s"];
    NSString *base64String = [self base64Encode:regularParam];
    base64String = [self  base64Encode:base64String];
    base64String = [self  base64Encode:base64String];
    
    //URL Encode it
    base64String = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                         NULL,
                                                                                         (CFStringRef)base64String,
                                                                                         NULL,
                                                                                         (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                         kCFStringEncodingUTF8 ));
    NSString *refLink = [NSString stringWithFormat:@"%@%@", URL_REFERAL2_LINK, base64String];
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = refLink;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else
    {
        [self showAlert:@"SMS" MSG:@"This feature is not available on your device"];
    }
}
-(IBAction)facebook:(id)sender
{
    [self isSocialAvailable];
    
    NSString *link=[self loadSharedPreferenceValue:@"RefrerralLink"];
    NSString *regularParam = [NSString stringWithFormat:@"%@-%@",link,@"f"];
    NSString *base64String = [self base64Encode:regularParam];
    base64String = [self  base64Encode:base64String];
    base64String = [self  base64Encode:base64String];
    
    //URL Encode it
    base64String = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                         NULL,
                                                                                         (CFStringRef)base64String,
                                                                                         NULL,
                                                                                         (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                         kCFStringEncodingUTF8 ));
    NSString *refLink = [NSString stringWithFormat:@"%@%@", URL_REFERAL2_LINK, base64String];
    SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [composeViewController setInitialText:refLink];
    
    [composeViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
            {
                NSLog(@"Post Sucessful");
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   // Some code
                                   [self showAlert:@"Success" MSG:@"Shared successfully."];
                                   
                               });
                
            }
                break;
                
            default:
                break;
        }
    }];

    [self presentViewController:composeViewController animated:YES completion:nil];
}
-(IBAction)twitter:(id)sender
{
    [self isSocialAvailable];
    NSString *link=[self loadSharedPreferenceValue:@"RefrerralLink"];
    NSString *regularParam = [NSString stringWithFormat:@"%@-%@",link,@"t"];
    NSString *base64String = [self base64Encode:regularParam];
    base64String = [self  base64Encode:base64String];
    base64String = [self  base64Encode:base64String];
    
    //URL Encode it
    base64String = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                         NULL,
                                                                                         (CFStringRef)base64String,
                                                                                         NULL,
                                                                                         (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                         kCFStringEncodingUTF8 ));
    NSString *refLink = [NSString stringWithFormat:@"%@%@", URL_REFERAL2_LINK, base64String];
    SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [composeViewController setInitialText:refLink];
    [composeViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
            {
                NSLog(@"Post Sucessful");
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   // Some code
                                   [self showAlert:@"Success" MSG:@"Shared successfully."];
                                   
                               });
                
            }
                break;
                
            default:
                break;
        }
    }];

    [self presentViewController:composeViewController animated:YES completion:nil];
}
-(IBAction)referralLink:(id)sender
{
    NSString *link=[self loadSharedPreferenceValue:@"RefrerralLink"];
    NSString *regularParam = [NSString stringWithFormat:@"%@-%@",link,@"o"];
    NSString *base64String = [self base64Encode:regularParam];
    base64String = [self  base64Encode:base64String];
    base64String = [self  base64Encode:base64String];
    
    //URL Encode it
    base64String = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                         NULL,
                                                                                         (CFStringRef)base64String,
                                                                                         NULL,
                                                                                         (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                         kCFStringEncodingUTF8 ));
    NSString *refLink = [NSString stringWithFormat:@"%@%@", URL_REFERAL2_LINK, base64String];
    
    NSArray *itemsToShare = @[refLink];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    NSString *name=[self loadSharedPreferenceValue:@"NameKey"];

    [activityVC setValue:[NSString stringWithFormat:@"%@ referred you to %@", name, SALON_NAME] forKey:@"subject"];

    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
    [self presentViewController:activityVC animated:YES completion:nil];
    
    
    //    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    //    [pasteboard setString:refLink];
    //    [self showAlert:@"Success" MSG:@"The link has been copied to clipboard." TAG:0];
    //    //[self getRefLink:1];
}
- (NSString *)base64Encode:(NSString *)plainText
{
    NSData *plainTextData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainTextData base64Encoding];
    return base64String;
}

-(BOOL)isSocialAvailable {
    return NSClassFromString(@"SLComposeViewController") != nil;
}

#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields.
-(void)displayComposerSheet : (NSString *) name LINK:(NSString *) link
{
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    NSString *subject = [NSString stringWithFormat:@"%@ referred you to %@", name, SALON_NAME];
    [picker setSubject:subject];
    
    //	NSArray *toRecipients = [NSArray arrayWithObject:self.strRestEmailId];
    NSString *body = [NSString stringWithFormat:@"Hi,\n %@ \n You are referred to %@ by %@.\nFill your information and click Submit to get the referral code to your email.",link,SALON_NAME, name];
    //	[picker setToRecipients:toRecipients];
    
    // Fill out the email body text
    [picker setMessageBody:body isHTML:NO];
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    if(result==MFMailComposeResultSent)
    {
        //[self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)terms:(id)sender
{
    NSString *terms=[self loadSharedPreferenceValue:@"TermsKey"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    TermsView *termsView = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsView"];
    termsView.testStr=terms;
    [self.navigationController pushViewController:termsView animated:YES];
  
}
-(IBAction)myRewards:(id)sender
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    MyRewards *myRewards = [self.storyboard instantiateViewControllerWithIdentifier:@"myrewardVC"];
    [self.navigationController pushViewController:myRewards animated:YES];
}

//-(IBAction)faq:(id)sender
//{
//    WebViewLoad *webViewLoad = [[WebViewLoad alloc] initWithNibName:@"WebViewLoad" bundle:nil];
//    [webViewLoad setUrl:URL_REFERRAL_FAQ];
//    [webViewLoad setTitle:@"FAQ"];
//    [self.navigationController pushViewController:webViewLoad animated:YES];
//    [webViewLoad release];
//}

-(IBAction)friendsBtn:(id)sender
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    RefFriendsList *list = [self.storyboard instantiateViewControllerWithIdentifier:@"RefFriendsList"];
    [self.navigationController pushViewController:list animated:YES];
    
//        FriendsList *list = [[FriendsList alloc] initWithNibName:@"FriendsList" bundle:Nil];
//        [self.navigationController pushViewController:list animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [transparentButton removeFromSuperview];
}

@end
