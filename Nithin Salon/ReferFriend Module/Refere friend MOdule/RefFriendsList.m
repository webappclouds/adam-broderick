//
//  NotificationsList.m
//  Template
//
//  Created by Nithin Reddy on 06/08/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import "RefFriendsList.h"
#import "Constants.h"
#import "Globals.h"
#import "FriendObj.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "FriendDetailsView.h"
#import "AppDelegate.h"
@interface RefFriendsList ()

@end

@implementation RefFriendsList{
    NSString *slcIdStr;
    NSString *clientIdStr;
    NSString *nameStr;
    AppDelegate *appDelegate;
}

@synthesize data;

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [self setTitle:@"Referrals you sent"];
    
    data = [[NSMutableArray alloc] init];
    
    slcIdStr=[Globals loadSharedPreferenceValue:@"slc_id"];
    clientIdStr=[Globals loadSharedPreferenceValue:@"clientid"];
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setObject:slcIdStr forKey:@"SlcId"];
    [params setObject:clientIdStr forKey:@"ClientId"];
    

    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_REFERRAL2_FRIENDS] HEADERS:nil GETPARAMS:nil POSTPARAMS:params COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
        }}];

    

    UIBarButtonItem *rightBar = [[UIBarButtonItem alloc] initWithTitle:@"Note" style:UIBarButtonItemStylePlain target:self action:@selector(note)];
    [rightBar setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = rightBar;


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) note
{
    [self showAlert:@"Referrals you sent" MSG:@"You will only see information on your referral once your friend has clicked on your link."];
}



-(void) getResponse:(NSDictionary *)dict
{
   
    if([[dict objectForKey:@"status"] boolValue])
    {
        NSArray *referred = [dict objectForKey:@"referral_details"];
        if([referred count]==0)
            [self showAlert:SALON_NAME MSG:@"You have no referrals" BLOCK:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        else
        {
            for(NSDictionary *subDict in referred)
            {
                FriendObj *friendObj = [[FriendObj alloc] init];
                [friendObj setName:[subDict objectForKey:@"recepient_name"]];
                [friendObj setEmail:[subDict objectForKey:@"recepient_email"]];
                [friendObj setDate:[subDict objectForKey:@"dateAdded"]];
                [friendObj setRedeemed:[[subDict objectForKey:@"isRedeemed"] boolValue]];
                [friendObj setRemind:[[subDict objectForKey:@"isRemindButton"] boolValue]];
                [data addObject:friendObj];
               
            }
            [self.tableView reloadData];
        }
    }
    else
    {
        [self showAlert:SALON_NAME MSG:@"You have no referrals" BLOCK:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];

    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
     tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if(data.count){
        
         tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *dateLabel = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *redeemedLabel = (UILabel *)[cell.contentView viewWithTag:3];
    UIButton *remindButton = (UIButton *)[cell.contentView viewWithTag:4];
    
    [remindButton setHidden:YES];
    FriendObj *friendObj = [data objectAtIndex:indexPath.row];
    //[cell.nameLabel setFont:[UIFont systemFontOfSize:15]];
    [nameLabel setText:friendObj.name];
    [dateLabel setText:friendObj.date];
    
    if(friendObj.redeemed){
       [redeemedLabel setText:[NSString stringWithFormat:@"Redeemed:Yes"]];
        redeemedLabel.textColor = [UIColor blueColor];

    }
    else{
       [redeemedLabel setText:[NSString stringWithFormat:@"Redeemed:No"]];
       redeemedLabel.textColor = [UIColor redColor];
      

    }
    
    if(friendObj.remind){
        [remindButton setHidden:NO];
        nameStr=friendObj.name;
        [remindButton setTag:indexPath.row];
       
        [remindButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    }
    return cell;
}

-(IBAction)buttonTapped:(id)sender
{
    UIButton *b = (UIButton *)sender;
    NSString *link=[self loadSharedPreferenceValue:@"RefrerralLink"];
    NSString *regularParam = [NSString stringWithFormat:@"%@-%@",link,@"o"];
    NSString *base64String = [self base64Encode:regularParam];
    base64String = [self  base64Encode:base64String];
    base64String = [self  base64Encode:base64String];
    
    //URL Encode it
    base64String = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                         NULL,
                                                                                         (CFStringRef)base64String,
                                                                                         NULL,
                                                                                         (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                         kCFStringEncodingUTF8 ));
    NSString *refLink = [NSString stringWithFormat:@"%@%@", URL_REFERAL2_LINK, base64String];

    NSArray *itemsToShare = @[refLink];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
    [self presentViewController:activityVC animated:YES completion:nil];

    
//    NSString *link=[Globals loadSharedPreferenceValue:@"RefrerralLink"];
//    link = [NSString stringWithFormat:@"%@/m/", link];
//
//    NSLog(@"clicked=%@",nameStr);
//    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
//    picker.mailComposeDelegate = self;
//    
//    NSString *subject = [NSString stringWithFormat:@"%@ referred you to %@", nameStr, SALON_NAME];
//    [picker setSubject:subject];
//    
//    //	NSArray *toRecipients = [NSArray arrayWithObject:self.strRestEmailId];
//    NSString *body = [NSString stringWithFormat:@"Hi,\n\nYou are referred to %@ by %@.\n\nClick the link below to get an exciting voucher. Fill your information and click Submit to get the referral code to your email.\n\n%@", SALON_NAME, nameStr, link];
//    //	[picker setToRecipients:toRecipients];
//    
//    // Fill out the email body text
//    [picker setMessageBody:body isHTML:NO];
//    
//    [self.navigationController presentViewController:picker animated:YES completion:nil];
//    [picker release];
}

- (NSString *)base64Encode:(NSString *)plainText
{
    NSData *plainTextData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainTextData base64Encoding];
    return base64String;
}



//- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
//{
//    [controller dismissViewControllerAnimated:YES completion:nil];
//    if(result==MFMailComposeResultSent)
//    {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//    FriendDetailsView *friendDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendDetailsView"];    [friendDetails setFriendObj:[data objectAtIndex:indexPath.row]];
//    [self.navigationController pushViewController:friendDetails animated:YES];
}


//-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 100.0f;
//}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
