//
//  LoginVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleObj.h"
@interface LoginVC : UIViewController


@property(weak, nonatomic)IBOutlet UITextField *emailTF,*passwordTF;
-(IBAction)loginClick:(id)sender;
-(IBAction)forgotPasswordClick:(id)sender;
-(IBAction)singUpClick:(id)sender;
@property (retain, nonatomic) NSString *key1;
@property (retain, nonatomic) NSString *key2;
@property (nonatomic, retain) ModuleObj *appointmentModule;

@end
