//
//  PrescriptionTimer.m
//  Template
//
//  Created by Nithin Reddy on 25/11/14.
//
//

#import "PrescriptionTimer.h"
#import "Constants.h"
#import "AppDelegate.h"
@interface PrescriptionTimer ()
{
    AppDelegate *appDelegate;
}
@end

@implementation PrescriptionTimer

@synthesize datePicker;
@synthesize flag;
@synthesize prodObj;
@synthesize prescId;
@synthesize prescViewDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [datePicker setMinimumDate:[NSDate dateWithTimeInterval:(60*60+15) sinceDate:[NSDate date]]];
    self.title = @"Pick up";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)pickup:(id)sender
{
    if(flag==1)
    {
        
        [self showAlert:@"Pick up" MSG:[NSString stringWithFormat:@"You have requested to pick up %@ the next time you visit the Salon/Spa. Click Yes to continue, or No to cancel.", prodObj.productName] TAG:0];
    }
    else
    {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Pick up" message:@"You have requested to pick up all the recommended products the next time you visit the Salon/Spa. Click Yes to continue, or No to cancel." preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self serviceCall];
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

-(void)showAlert : (NSString *) title MSG:(NSString *) message TAG:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[self serviceCall];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self serviceCall];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)serviceCall
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[self loadSharedPreferenceValue:@"slc_id"] forKey:@"slc_id"];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    [params setObject:[df stringFromDate:datePicker.date] forKey:@"datetime"];
    
    
    NSString *url = @"";
    
    if(flag==1)
    {
        [params setObject:prodObj.productId forKey:@"product_id"];
        url = [NSString stringWithFormat:@"%@%@", [appDelegate addSalonIdTo:URL_PRESCRIPTION_PRODUCTS_ORDER], prescId];
    }
    else
        url = [NSString stringWithFormat:@"%@%@", [appDelegate addSalonIdTo:URL_PRESCRIPTION_PRODUCTS_ORDER_ALL], prescId];
    
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:params COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
        }}];
    
    
    
}

-(void) getResponse:(NSMutableDictionary *)dict
{
    
    BOOL status = [[dict objectForKey:@"status"] boolValue];
    if(status)
    {
        [self showAlert:@"Thank you" MSG:@"Your request has been sent to the Salon/Spa" BLOCK:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [prescViewDelegate orderSuccessfulCallback];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
        [self showAlert:@"Error" MSG:@"Error occured, please try again later"];
}


@end
