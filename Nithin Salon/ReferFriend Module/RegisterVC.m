//
//  RegisterVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "RegisterVC.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "OnlineBookindViewCon.h"
#import "ReferFriendVC.h"
#import "MyAccountList.h"
#import <SHSPhoneComponent/SHSPhoneNumberFormatter+UserConfig.h>
#import "OnlineServicesViewController.h"
@interface RegisterVC ()
{
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section1,*section2,*section3,*section4;
    XLFormRowDescriptor *firstNameRow,*lastNameRow,*emailRow,*phoneRow,*submitBtnRow,*passwordRow,*confirmPasswordRow,*DOBRow,*genderRow,*callRow,*messageRow,*emailMeRow,*specialsRow;
    NSArray *genderList;
    AppDelegate *appDelegate;
}
@end

@implementation RegisterVC



-(void)initializeForm
{
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    genderList = [[NSArray alloc] initWithObjects:@"Leave it blank",@"Male",@"Female", nil];
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    formDescriptor.assignFirstResponderOnShow = NO;
    
    // section1---> name row, email Row, phone row
    section1 = [XLFormSectionDescriptor formSectionWithTitle:@"Personal Information"];
    [formDescriptor addFormSection:section1];
    
    firstNameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Name" rowType:XLFormRowDescriptorTypeText title:@"First Name*"];
    firstNameRow.height =44;
    firstNameRow.required =YES;
    [firstNameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [firstNameRow.cellConfigAtConfigure setObject:@"Enter first name" forKey:@"textField.placeholder"];
    [section1 addFormRow:firstNameRow];
    
    
    lastNameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"lName" rowType:XLFormRowDescriptorTypeText title:@"Last Name*"];
    lastNameRow.height =44;
    [lastNameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [lastNameRow.cellConfigAtConfigure setObject:@"Enter last name" forKey:@"textField.placeholder"];
    [section1 addFormRow:lastNameRow];
    
    emailRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"email" rowType:XLFormRowDescriptorTypeText title:@"Email Address*"];
    emailRow.height =44;
    [emailRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [emailRow.cellConfigAtConfigure setObject:@"Enter email" forKey:@"textField.placeholder"];
    [section1 addFormRow:emailRow];
    
    passwordRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"PWD" rowType:XLFormRowDescriptorTypePassword title:@"Create Password*"];
    passwordRow.height =44;
    [passwordRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [passwordRow.cellConfigAtConfigure setObject:@"Enter password" forKey:@"textField.placeholder"];
    [section1 addFormRow:passwordRow];
    
    confirmPasswordRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"COnfirmPWD" rowType:XLFormRowDescriptorTypePassword title:@"Confirm Password*"];
    confirmPasswordRow.height =44;
    [confirmPasswordRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [confirmPasswordRow.cellConfigAtConfigure setObject:@"Enter confirm password" forKey:@"textField.placeholder"];
    [section1 addFormRow:confirmPasswordRow];
    
    SHSPhoneNumberFormatter *formatter1 = [[SHSPhoneNumberFormatter alloc] init];
    [formatter1 setDefaultOutputPattern:@"(###) ###-####" imagePath:nil];
    
    phoneRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Phone" rowType:XLFormRowDescriptorTypeInteger title:@"Mobile No*"];
    phoneRow.height =44;
    phoneRow.valueFormatter = formatter1;
    phoneRow.useValueFormatterDuringInput = YES;
    [phoneRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [phoneRow.cellConfigAtConfigure setObject:@"Enter phone" forKey:@"textField.placeholder"];
    //[phoneRow addValidator:[XLFormRegexValidator formRegexValidatorWithMsg:@"" regex:@"^([5-9][0-9]|100)$"]];
    [section1 addFormRow:phoneRow];
    
    
    DOBRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"DOB" rowType:XLFormRowDescriptorTypeDate title:@"Date Of Birth"];
    DOBRow.height =44;
    //    DOBRow.valueFormatter=df;
    //    DOBRow.value = [NSDate date];
//    [DOBRow.cellConfigAtConfigure setObject:[NSDate dateWithTimeIntervalSinceNow:-15*365*24*60*60] forKey:@"maximumDate"];
    [DOBRow.cellConfigAtConfigure setObject:[NSDate date] forKey:@"maximumDate"];

    NSDateFormatter *inFormat = [[NSDateFormatter alloc] init];
    [inFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *selectedDateofbirth = [inFormat stringFromDate:[NSDate date]];
    DOBRow.value = [NSDate date];
    
    [section1 addFormRow:DOBRow];
    
    genderRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Gemder" rowType:XLFormRowDescriptorTypeSelectorPush title:@"Gender"];
    genderRow.height =44;
    genderRow.selectorOptions = genderList;
    genderRow.value = [genderList objectAtIndex:0];
    [section1 addFormRow:genderRow];
    
    section2 = [XLFormSectionDescriptor formSectionWithTitle:@"Confirmations/Reminder settings"];
    [formDescriptor addFormSection:section2];
    
    NSMutableArray *valArray = [[NSMutableArray alloc]initWithObjects:@"No",@"Yes", nil];
    callRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Call" rowType:XLFormRowDescriptorTypeSelectorPush title:@"Call Me"];
    callRow.height =44;
    callRow.selectorOptions = valArray;
    callRow.value = @"No";
    [section2 addFormRow:callRow];
    
    messageRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"message" rowType:XLFormRowDescriptorTypeSelectorPush title:@"Message Me"];
    messageRow.height =44;
    messageRow.selectorOptions = valArray;
    messageRow.value = @"No";
    [section2 addFormRow:messageRow];
    
    emailMeRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"emailME" rowType:XLFormRowDescriptorTypeSelectorPush title:@"Email Me"];
    emailMeRow.height =44;
    emailMeRow.selectorOptions = valArray;
    emailMeRow.value =@"No";
    [submitBtnRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    [section2 addFormRow:emailMeRow];
    
    section3 = [XLFormSectionDescriptor formSectionWithTitle:@"PROMOTIONS"];
    [formDescriptor addFormSection:section3];
    
    specialsRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Specials" rowType:XLFormRowDescriptorTypeSelectorPush title:@"Special"];
    specialsRow.height =44;
    specialsRow.selectorOptions = valArray;
    specialsRow.value = @"No";;
    [section3 addFormRow:specialsRow];
    
    section4 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section4];
    
    submitBtnRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Register"];
    [submitBtnRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [submitBtnRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    submitBtnRow.action.formSelector = @selector(createAccount);
    [section4 addFormRow:submitBtnRow];
    
    self.form = formDescriptor;
}

#pragma mark create button action
-(void)createAccount{
    
    NSString *fNameStr = [firstNameRow displayTextValue]?:@"";
    NSString *lNameStr = [lastNameRow displayTextValue]?:@"";
    NSString *emailStr = [emailRow displayTextValue]?:@"";
    NSString *phoneStr = [phoneRow displayTextValue]?:@"";
    NSString *passwordStr = [passwordRow displayTextValue];
    NSString *conPasswordStr = [confirmPasswordRow displayTextValue];
    NSString *callStr = [callRow displayTextValue]?:@"";
    NSString *emailMeStr = [emailMeRow displayTextValue]?:@"";
    
    NSString *msgStr =messageRow.value?:@"";
    NSString *specialStr = specialsRow.value?:@"";
    
    if([fNameStr length]==0)
        [self showAlert:@"First Name" MSG:@"Please enter your first name" TAG:0];
    else if([lNameStr length]==0)
        [self showAlert:@"Last Name" MSG:@"Please enter your last name" TAG:0];
    else if(![UIViewController isValidEmail:emailStr])
        [self showAlert:@"Email" MSG:@"Please enter a valid email address" TAG:0];
    else if([passwordStr length]==0)
        [self showAlert:@"Password" MSG:@"Please enter your password" TAG:0];
    else if([conPasswordStr length]==0)
        [self showAlert:@"Confirm Password" MSG:@"Please enter your password" TAG:0];
    else if(![conPasswordStr isEqualToString:passwordStr])
        [self showAlert:@"" MSG:@"Password does not match the confirm password" TAG:0];
    else if ([phoneStr length]==0 || [phoneStr length]<14)
        [self showAlert:@"Mobile" MSG:@"Please enter a valid mobile number" TAG:0];
    else if (callStr.length == 0)
        [self showAlert:@"Call Me" MSG:@"Please select an option" TAG:0];
    else if (msgStr.length == 0)
        [self showAlert:@"Message Me" MSG:@"Please select an option" TAG:0];
    else if (emailMeStr.length == 0)
        [self showAlert:@"Email Me" MSG:@"Please select an option" TAG:0];
    else if (specialStr.length == 0)
        [self showAlert:@"Specials" MSG:@"Please select an option" TAG:0];
    else if([DOBRow value]==Nil)
        [self showAlert:@"Date of Birth" MSG:@"Please select your date of birth" TAG:0];
    
    else{
        
        NSDateFormatter *inFormat = [[NSDateFormatter alloc] init];
        [inFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *selectedDateofbirth = [inFormat stringFromDate:[DOBRow value]];
        
        appDelegate.pushToken = [self loadSharedPreferenceValue:@"token"];
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [paramDic setObject:appDelegate.pushToken?:@"" forKey:@"token"];
        [paramDic setObject:fNameStr forKey:@"firstname"];
        [paramDic setObject:lNameStr forKey:@"lastname"];
        [paramDic setObject:[genderRow value] forKey:@"gender"];
        
        if ([[genderRow value] isEqualToString:@"Leave it blank"]) {
            [paramDic setObject:@"" forKey:@"gender"];
        }
        [paramDic setObject:phoneStr forKey:@"mobile"];
        [paramDic setObject:@"" forKey:@"homephone"];
        [paramDic setObject:emailStr forKey:@"email"];
        [paramDic setObject:selectedDateofbirth forKey:@"birthday"];
        [paramDic setObject:passwordStr forKey:@"password"];
        [paramDic setObject:[NSString stringWithFormat:@"%d",[[callRow value] boolValue]] forKey:@"callme"];
        [paramDic setObject:[NSString stringWithFormat:@"%d",[[messageRow value]boolValue]] forKey:@"textconfirm"];
        [paramDic setObject:[NSString stringWithFormat:@"%d",[[emailRow value]boolValue]] forKey:@"emailconfirm"];
        [paramDic setObject:[NSString stringWithFormat:@"%d",[[specialsRow value]boolValue]] forKey:@"special"];
        
        NSLog(@"Param dict : %@",paramDic);
        
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_ONLINE_BOOK_REG] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSMutableDictionary *dic = (NSMutableDictionary*)responseArray;
                
                int status =[[dic objectForKey:@"status"] intValue];
                if (status==1)
                {
                    [self saveSharedPreferenceValue:[NSString stringWithFormat:@"%@ %@",fNameStr,lNameStr] KEY:@"RegName"];
                    [self saveSharedPreferenceValue:[NSString stringWithFormat:@"%@ ",emailStr] KEY:@"RegEmail"];
                    NSString *integrationType = [dic objectForKey:@"appt_type"];
                    [self saveSharedPreferenceValue:integrationType KEY:@"appt_type"];
                    [self saveSharedPreferenceValue:[dic objectForKey:@"slc_id"] KEY:@"slc_id"];
                    [self saveSharedPreferenceValue:[dic objectForKey:@"clientid"] KEY:@"clientid"];
                    AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
                    [self hideBackButton];
                    if ([objAppdel.flgObj isEqualToString:@"referFrndFlg"]){
                        [self referFriend];
                    }
                    
                    else  if ([objAppdel.flgObj isEqualToString:@"onlineBookFlag"]){
                        
                        
                      /*  OnlineBookindViewCon *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineBookindViewCon"];
                        [self.navigationController pushViewController:onlineBookindViewCon animated:YES];*/
                    
                        OnlineServicesViewController *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineServicesViewController"];
                        
                        [self.navigationController pushViewController:onlineBookindViewCon animated:YES];

                        
                    }
                    
                    
                    else{
                        if(integrationType==nil || [integrationType length]==0){
                            
                            [self showAlert:@"Error" MSG:@"Error occured. Try again later."];
                            return;
                        }
                        else{
                            
                            if([objAppdel.flgObj isEqualToString:@"checkApptFlg"]){
                                MyAccountList *account = [self.storyboard instantiateViewControllerWithIdentifier:@"MyAccountList"];
                                account.appointmentMOdule =appDelegate.appoitmentsModel ;
                                [self.navigationController pushViewController:account animated:YES];
                                
                            }
                            
                        }
                    }
                }
                else if (status==2){
                    NSString *msg=[dic objectForKey:@"msg"];
                    [self showAlert:@"" MSG:msg TAG:1];
                }
                else{
                    [self showAlert:@"Error" MSG:@"Please try again later" TAG:0];
                }
                
                
            }
        }];
        
    }
}
-(void)showAlert : (NSString *) title MSG:(NSString *) message TAG:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)referFriend{
    ReferFriendVC *staffListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReferFriendVC"];
    [self.navigationController pushViewController:staffListVC animated:YES];
    
}

-(void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"Create Account";
    [self initializeForm];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
