//
//  PrescriptionList.m
//  Template
//
//  Created by Nithin Reddy on 24/11/14.
//
//

#import "PrescriptionList.h"
#import "Constants.h"
#import "PrescriptionView.h"
#import "PrescriptionObj.h"
#import "AppDelegate.h"
@interface PrescriptionList ()
{
    AppDelegate *appDelegate;
}
@end

@implementation PrescriptionList

@synthesize data;

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    data = [[NSMutableArray alloc] init];
    
    self.title = @"Prescriptions";
    
    NSString *url = [NSString stringWithFormat:@"%@%@", [appDelegate addSalonIdTo:URL_GET_PRESCRIPTIONS], [self loadSharedPreferenceValue:@"slc_id"]];
    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
        }}];
    
    
}

-(void) getResponse:(NSMutableDictionary *)dict
{
    
    NSArray *prescriptionsArr = [dict objectForKey:@"prescriptions"];
    BOOL status = [[dict objectForKey:@"status"] boolValue];
    if(!status || [prescriptionsArr count]==0)
    {
        [self showAlert:@"Prescriptions" MSG:@"No prescriptions found" TAG:0];
    }
    else
    {
        for(NSDictionary *tempObj in prescriptionsArr)
        {
            PrescriptionObj *obj = [[PrescriptionObj alloc] init];
            [obj setPrescId:[tempObj objectForKey:@"id"]];
            [obj setStylistName:[tempObj objectForKey:@"stylist"]];
            [obj setNote:[tempObj objectForKey:@"note"]];
            [obj setDateTime:[tempObj objectForKey:@"created"]];
            [data addObject:obj];
        }
        [self.tableView reloadData];
    }
}


-(void)showAlert : (NSString *) title MSG:(NSString *) message TAG:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1"];
    if(cell == Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell1"];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
     tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    if(self.data.count){
    PrescriptionObj *obj = [self.data objectAtIndex:indexPath.row];
    
    
    //font align
    
    cell.textLabel.font = [UIFont fontWithName:@"Open Sans" size:16];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Open Sans" size:14];
    [cell.textLabel setText:obj.dateTime];
    [cell.detailTextLabel setText:obj.stylistName];
    
    }
    
    
    return cell;
}




#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    
    PrescriptionObj *obj = [self.data objectAtIndex:indexPath.row];
    
    PrescriptionView *presView = [self.storyboard instantiateViewControllerWithIdentifier:@"PrescriptionView"];
    [presView setObj:obj];
    
    [self.navigationController pushViewController:presView animated:YES];
    
    
}


@end
