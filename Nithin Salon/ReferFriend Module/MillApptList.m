//
//  MyAppointMentsTblViewCon.m
//  Restaurants_Webapp
//
//  Created by Bhalchandra Deogaonkar on 30/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MillApptList.h"
#import "Constants.h"
#import "MacAddressHelper.h"
#import "MilObj.h"
#import "AppDelegate.h"
#import "Globals.h"
#import "GlobalSettings.h"
#import "OnlineCalendarVC.h"
#import "ApptMapView.h"

@implementation MillApptList
{
    AppDelegate *appDelegate;
    NSString *url;
    NSString *segName;
    NSMutableArray *globalArray;
    
}
@synthesize allData;
//@synthesize arrJson;
@synthesize tableView;
@synthesize filteredData;
@synthesize flagStr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.segController.selectedSegmentIndex=0;
    self.searchBar.hidden=YES;
    
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    globalArray = [[NSMutableArray alloc]init];
    
    NSString *titleStr;
    
    
    titleStr=@"My Appointments";
    
    url=[NSString stringWithFormat:@"%@",URL_APPOINTMENTS];
    
    [self serviceCallForAppts];
    
    //    if ([flagStr isEqualToString:@"ConfirmedAppt"]) {
    //        titleStr=@"My Appts";
    //        url=[NSString stringWithFormat:@"%@",URL_APPOINTMENTS];
    //    }
    //    else{
    //        titleStr=@"My Past Appts";
    //        url=[NSString stringWithFormat:@"%@",URL_PAST_APPOINTMENTS];
    //    }
    
    
    self.title= titleStr ;
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    
    
    
    //[self parseJSON];
    
}
-(void)serviceCallForAppts
{
    NSString *slcId = [self loadSharedPreferenceValue:@"slc_id"];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:[self getPushToken] forKey:@"token"];
    [paramDic setObject:slcId forKey:@"slc_id"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
            
            NSLog(@"responseArray : %@",responseArray);
        }}];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)getResponse:(NSDictionary *)dict
{
    
    NSLog(@"REsponse : @",dict);
    
    if ([[dict objectForKey:@"appointments"] count] == 0)
        [self showAlert:SALON_NAME MSG:[dict objectForKey:@"message"] BLOCK:^{
        }];
    else {
        NSMutableArray *arrJson = [dict objectForKey:@"appointments"];
        
        allData = [[NSMutableArray alloc] init];
        filteredData = [[NSMutableArray alloc] init];
        
        
        if(arrJson.count>0){
            for (int i=0; i<[arrJson count]; i++) {
                
                NSDictionary * dictJson=[arrJson objectAtIndex:i];
                
                MilObj *obj = [[MilObj alloc] init];
                
                NSString *integrationType = [self loadSharedPreferenceValue:@"appt_type"];
                if([integrationType isEqualToString:@"IRIS"] && ![[dictJson objectForKey:@"service_type"] isEqualToString:@"S"])
                    continue;
                
                [obj setApptId:[[dictJson objectForKey:@"appt_id"] intValue]];
                [obj setService:[dictJson objectForKey:@"service"]];
                [obj setEmpName:[dictJson objectForKey:@"emp"]];
                [obj setDateStr:[dictJson objectForKey:@"date"]];
                [obj setSalonId:[[dictJson objectForKey:@"salon_id"]intValue]];
                
                [obj setApdId:[[dictJson objectForKey:@"apd_id"] intValue]];
                [obj setApId:[[dictJson objectForKey:@"ap_id"] intValue]];
                [obj setStaffId:[[dictJson objectForKey:@"staff_id"] intValue]];
                [obj setServiceId:[[dictJson objectForKey:@"service_id"] intValue]];
                
                
                
                
                NSString *dateStr = [dictJson objectForKey:@"date"];
                NSString *timeStr = [dictJson objectForKey:@"time"];
                
                NSString *apptDateStr = [NSString stringWithFormat:@"%@ %@", dateStr, timeStr];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
                NSDate *dateFromString = [dateFormatter dateFromString:apptDateStr];
                
                [obj setApptDate:dateFromString];
                [allData addObject:obj];
                [filteredData addObject:obj];
                
            }
            
            
            
            NSLog(@"global Array: %@",globalArray);
            
            [self.tableView setHidden:NO];
            self.searchBar.hidden=NO;
            [self.tableView reloadData];
        }
        else{
            //            [self showAlert:SALON_NAME MSG:[dict objectForKey:@"message"]];
            [self.tableView reloadData];
            
        }
        
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.filteredData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableVieww cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.segController.selectedSegmentIndex==0) {
        static NSString *CellIdentifier = @"IntegrationApptCell";
        UITableViewCell *cell = [tableVieww dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell==Nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        
        MilObj *obj = [self.filteredData objectAtIndex:indexPath.section];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // Configure the cell...
        UIImageView *bgView = (UIImageView *)[cell.contentView viewWithTag:100];
        bgView.layer.cornerRadius = 10;
        bgView.layer.masksToBounds = YES;
        
        
        UILabel *serviceLabel = (UILabel*)[cell.contentView viewWithTag:10];
        UILabel *stylistLabel = (UILabel*)[cell.contentView viewWithTag:20];
        UILabel *dateLabel = (UILabel*)[cell.contentView viewWithTag:30];
        UIButton *addApptToCalButton = (UIButton*)[cell.contentView viewWithTag:40];
        
        UIButton *rebookApptToCalButton = (UIButton*)[cell.contentView viewWithTag:103];
        serviceLabel.text= obj.service;
        dateLabel.text=[self returnAndChangeDate:obj.apptDate];
        stylistLabel.text = [NSString stringWithFormat:@"Stylist: %@", obj.empName];
        [rebookApptToCalButton setTag:indexPath.section];
        
        [addApptToCalButton setTag:indexPath.section];
        
        
        
        
        [rebookApptToCalButton addTarget:self action:@selector(cancelApptToCalButton:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [addApptToCalButton addTarget:self action:@selector(addToCalClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
        
    }
    
    else
    {
        static NSString *CellIdentifier = @"IntegrationCell";
        UITableViewCell *cell = [tableVieww dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell==Nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        
        MilObj *obj = [self.filteredData objectAtIndex:indexPath.section];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // Configure the cell...
        UIImageView *bgView = (UIImageView *)[cell.contentView viewWithTag:100];
        bgView.layer.cornerRadius = 10;
        bgView.layer.masksToBounds = YES;
        
        
        UILabel *serviceLabel = (UILabel*)[cell.contentView viewWithTag:10];
        UILabel *stylistLabel = (UILabel*)[cell.contentView viewWithTag:20];
        UILabel *dateLabel = (UILabel*)[cell.contentView viewWithTag:30];
        UIButton *addApptToCalButton = (UIButton*)[cell.contentView viewWithTag:40];
        
        UIButton *rebookApptToCalButton = (UIButton*)[cell.contentView viewWithTag:103];
        serviceLabel.text= obj.service;
        dateLabel.text=[self returnAndChangeDate:obj.apptDate];
        stylistLabel.text = [NSString stringWithFormat:@"Stylist: %@", obj.empName];
        [rebookApptToCalButton setTag:indexPath.section];
        
        [addApptToCalButton setTag:indexPath.section];
        
        
        
        
        [rebookApptToCalButton addTarget:self action:@selector(rebookApptToCalButton:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [addApptToCalButton addTarget:self action:@selector(addToCalClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
        
    }
    
}


-(void)rebookApptToCalButton:(id)sender{
    UIButton *b=(UIButton*)sender;
    NSLog(@"tag is %li",(long)b.tag);
    
    NSMutableArray *dummyArray = [[NSMutableArray alloc]init];
    
    //dummyArray = [self.filteredData objectAtIndex:b.tag];
    
    MilObj *obj = [self.filteredData objectAtIndex:b.tag];
    
    NSString *clientIDStr=[self loadSharedPreferenceValue:@"clientid"];
    //NSString *slcIdStr= [Globals loadSharedPreferenceValue:[NSString stringWithFormat:@"%@",SALON_ID]];
    
    NSString *staffid=[[NSString stringWithFormat:@"%d",obj.staffId]mutableCopy];
    NSString *serviceid=[[NSString stringWithFormat:@"%d",obj.serviceId] mutableCopy];
    NSString *apptId=[NSString stringWithFormat:@"%d",obj.apptId];
    NSString *salonId=[NSString stringWithFormat:@"%d",obj.salonId];
    
    
    
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:obj.service forKey:@"ServiceNames"];
    [paramDic setObject:serviceid forKey:@"ServiceIds"];
    [paramDic setObject:staffid forKey:@"EmployeeIds"];
    [paramDic setObject:@"anytime" forKey:@"Time"];
    [paramDic setObject:@"custom" forKey:@"Date"];
    //[paramDic setObject:slcIdStr forKey:@"slc_id"];
    [paramDic setObject:clientIDStr forKey:@"ClientId"];
    [paramDic setObject:apptId forKey:@"appt_id"];
    [paramDic setObject:salonId forKey:@"salon_id"];
    
    
    NSMutableDictionary *reBookParamDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    //    [reBookParamDic setObject:obj.service forKey:@"ServiceNames"];
    [reBookParamDic setObject:serviceid forKey:@"ServiceIds"];
    [reBookParamDic setObject:staffid forKey:@"EmployeeIds"];
    [reBookParamDic setObject:@"anytime" forKey:@"Time"];
    [reBookParamDic setObject:@"custom" forKey:@"Date"];
    //[paramDic setObject:slcIdStr forKey:@"slc_id"];
    [reBookParamDic setObject:clientIDStr forKey:@"ClientId"];
    //    [reBookParamDic setObject:apptId forKey:@"appt_id"];
    //    [reBookParamDic setObject:salonId forKey:@"salon_id"];
    
    
    
    
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    OnlineCalendarVC *osl = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineCalendarVC"];
    //    osl.staffID = [[appDelegate.globalCheckAndUncheckArray objectAtIndex:0] objectForKey:@"serviceID"];
    
    [osl setCompareString:@"Rebook"];
    [osl setReBookDict:reBookParamDic];
    [osl setParamDict:paramDic];
    
    
    [self.navigationController pushViewController:osl animated:YES];
}

-(void)cancelApptToCalButton:(id)sender
{
    UIButton *b = (UIButton*)sender;
    MilObj *obj = [self.filteredData objectAtIndex:[b tag]];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Appointment" andMessage:[NSString stringWithFormat:@"%@ with %@ on %@", obj.service, obj.empName, [self returnAndChangeDate:obj.apptDate]]];
    
    [alertView addButtonWithTitle:@"Traffic"
                             type:SIAlertViewButtonTypeGreen
                          handler:^(SIAlertView *alert) {
                              [self showHud];
                              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                  [self hideHud];
                                  self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
                                  ApptMapView *apptMapView = [self.storyboard instantiateViewControllerWithIdentifier:@"ApptMapView"];
                                  [self.navigationController pushViewController:apptMapView animated:YES];
                              });
                          }];
    [alertView addButtonWithTitle:@"Cancel Appt"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              
                              UIButton *b = (UIButton*)sender;
                              
                              MilObj *obj = [self.filteredData objectAtIndex:[b tag]];
                              
                              // NSString *clientId=[self loadSharedPreferenceValue:[NSString stringWithFormat:@"%@",SALON_ID]];
                              NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
                              
                              NSString *clientIDStr = [self loadSharedPreferenceValue:@"clientid"];
                              NSString *slcId = [self loadSharedPreferenceValue:@"slc_id"];
                              
                              NSString *appid=[NSString stringWithFormat:@"%d",obj.apdId];
                              NSString *apid=[NSString stringWithFormat:@"%d",obj.apId];
                              NSString *staffid=[NSString stringWithFormat:@"%d",obj.staffId];
                              
                              [paramDic setObject:appid forKey:@"apd_id"];
                              [paramDic setObject:apid forKey:@"ap_id"];
                              [paramDic setObject:staffid forKey:@"staff_id"];
                              [paramDic setObject:slcId forKey:@"slc_id"];
                              [paramDic setObject:clientIDStr forKey:@"clientid"];
                              
                              
                              
                              [appDelegate showHUD];
                              [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[NSString stringWithFormat:@"%@",URL_CANCEL_APPT] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                                  [appDelegate hideHUD];
                                  if(error)
                                      [self showAlert:@"Error" MSG:error.localizedDescription];
                                  else
                                  {
                                      NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
                                      
                                      BOOL status = [[dict objectForKey:@"status"] boolValue];
                                      NSString *message = [dict objectForKey:@"message"];
                                      if(status){
                                          [self showAlert:@"Success" MSG:message BLOCK:^{
                                              
                                              url=[NSString stringWithFormat:@"%@",URL_APPOINTMENTS];
                                              self.segController.selectedSegmentIndex =0;
                                              [self serviceCallForAppts];
                                              
                                              
                                              
                                          }];
                                      }
                                      else
                                          [self showAlert:@"" MSG:message];
                                      
                                  }}];
                              
                          }];
    
    [alertView addButtonWithTitle:@"Close"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                          }];
    
    
    
    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
    [alertView show];
    
    
    
    
}

-(IBAction) addToCalClicked : (id) sender
{
    UIButton *b = (UIButton*)sender;
    
    MilObj *obj = [filteredData objectAtIndex:[b tag]];
    
    EKEventStore *eventstore = [[EKEventStore alloc] init];
    
    EKEvent *event = [EKEvent eventWithEventStore:eventstore];
    
    event.title= [NSString stringWithFormat:@"%@ Appointment - %@", SALON_NAME, obj.service];
    
    event.startDate = obj.apptDate;
    event.endDate = [[NSDate alloc] initWithTimeInterval:60*60 sinceDate:obj.apptDate];
    
    EKAlarm *remainder = [EKAlarm alarmWithRelativeOffset:-60*30];
    [event addAlarm:remainder];
    
    if([eventstore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        // iOS 6 and later
        [eventstore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            
            // may return on background thread
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (granted){
                    [event setCalendar:[eventstore defaultCalendarForNewEvents]];
                    //---- codes here when user allow your app to access theirs' calendar.
                    NSError *err;
                    BOOL isSuceess=[eventstore saveEvent:event span:EKSpanThisEvent error:&err];
                    
                    if(isSuceess){
                        [self showAlert:SALON_NAME MSG:@"Your appointment has been added to the calendar."];
                    }
                    
                }else
                {
                    [self showAlert:SALON_NAME MSG:@"Please enable access to the calendar in Settings -> Privacy."];
                }
            });
        }];
    }
    else {
        NSError *err;
        BOOL isSuceess=[eventstore saveEvent:event span:EKSpanThisEvent error:&err];
        
        if(isSuceess){
            [self showAlert:SALON_NAME MSG:@"Your appointment has been added to the calendar."];
        }
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}

- (NSString *) returnNewDateFormat : (NSString *) str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:str];
    [dateFormatter setDateFormat:@"dd MMM, yyyy  hh:mm a"];
    return [dateFormatter stringFromDate:date];
}

- (NSString *) returnAndChangeDate : (NSDate *) date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    
    return [dateFormatter stringFromDate:date];
}


-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterResults:searchText];
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

-(void) filterResults : (NSString *) searchStr
{
    [filteredData removeAllObjects];
    if([searchStr length]==0)
    {
        [self.searchBar resignFirstResponder];
        [self refreshFilteredDataWithData];
        return;
    }
    
    searchStr = [searchStr lowercaseString];
    
    for(MilObj *obj in allData)
    {
        if([[obj.service lowercaseString] containsString:searchStr])
            [filteredData addObject:obj];
    }
    [self.tableView reloadData];
}

-(void) refreshFilteredDataWithData
{
    [filteredData removeAllObjects];
    filteredData = [[NSMutableArray alloc] initWithArray:allData];
    [self.tableView reloadData];
}

-(NSString *) getPushToken
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.pushToken = [self loadSharedPreferenceValue:@"token"];
    NSString *pushToken = appDelegate.pushToken;
    if(pushToken==nil || [pushToken length]==0)
        return [MacAddressHelper getMacAddress];
    return pushToken;
}
- (IBAction)timeSegmentAction:(id)sender {
    
    [self.tableView setHidden:YES];
    self.searchBar.hidden=YES;
    [self.filteredData removeAllObjects];
    [self.allData removeAllObjects];
    UISegmentedControl *seg = (UISegmentedControl*)sender;
    if (seg.selectedSegmentIndex==0)
    {
        segName = @"";
        url=[NSString stringWithFormat:@"%@",URL_APPOINTMENTS];
        
    }
    else if (seg.selectedSegmentIndex==1)
    {
        segName =@"Past";
        
        url=[NSString stringWithFormat:@"%@",URL_PAST_APPOINTMENTS];
    }
    
    [self serviceCallForAppts];
    //titleString = [seg titleForSegmentAtIndex:seg.selectedSegmentIndex];
    
}

@end
