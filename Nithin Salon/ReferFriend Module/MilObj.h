//
//  MilObj.h
//  Salon Evolve
//
//  Created by Nithin Reddy on 10/13/13.
//
//

#import <Foundation/Foundation.h>

@interface MilObj : NSObject

@property (nonatomic, assign) int apptId;
@property (nonatomic, retain) NSString *service;
@property (nonatomic, retain) NSDate *apptDate;

@property (nonatomic, assign) int isConfirmed;
@property (nonatomic, retain) NSString *empName;
@property (nonatomic, assign) int apdId;
@property (nonatomic, assign) int apId;
@property (nonatomic, assign) int staffId;
@property (nonatomic, assign) int serviceId;
@property (nonatomic, retain) NSString *dateStr;
@property (nonatomic, assign) int salonId;


@end
