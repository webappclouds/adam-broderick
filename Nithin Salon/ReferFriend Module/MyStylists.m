//
//  MyStylists.m
//  Template
//
//  Created by Nithin Reddy on 06/02/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import "MyStylists.h"
#import "Constants.h"
#import "StaffObj.h"
#import "UIImageView+WebCache.h"
#import "StaffDetails.h"
#import "AppDelegate.h"
@interface MyStylists ()
{
    AppDelegate *appDelegate;
}
@end

@implementation MyStylists

@synthesize data;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"My Service Providers";
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    data = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[self loadSharedPreferenceValue:@"slc_id"], @"slc_id", nil];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_MY_STYLISTS] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
        }}];
    
}


-(void)getResponse:(NSMutableDictionary *)responseDict{
    
    BOOL status = [[responseDict objectForKey:@"status"] boolValue];
    if(!status)
    {
        [self showAlert:@"Error" MSG:@"Error occured. Please try again."];
        return;
    }
    
    NSMutableArray *arrJson=[responseDict objectForKey:@"stylists"];
    
    if ([arrJson count]==0 )
        [self showAlert:SALON_NAME MSG:@"No entries found. Please try again later." BLOCK:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    else
    {
        for (NSDictionary *dictJson in arrJson)
        {
            NSString * strTitles=[dictJson objectForKey:@"name"];
            NSString * strDesignation=[dictJson objectForKey:@"designation"];
            NSString *imageUrl = [dictJson objectForKey:@"image"];
            NSString * strDesc=[dictJson objectForKey:@"description"];
            NSString * strProd1=[dictJson objectForKey:@"product1"];
            NSString * strProd2=[dictJson objectForKey:@"product2"];
            NSString * strProd3=[dictJson objectForKey:@"product3"];
            NSString * strProd4=[dictJson objectForKey:@"product4"];
            
            StaffObj *staffObj = [[StaffObj alloc] init];
            staffObj.staffId = [dictJson objectForKey:@"staff_id"];
            staffObj.name = strTitles;
            staffObj.designation = strDesignation;
            staffObj.imageUrl = imageUrl;
            staffObj.description = strDesc;
            staffObj.product1 = strProd1;
            staffObj.product2 = strProd2;
            staffObj.product3 = strProd3;
            staffObj.product4 = strProd4;
            staffObj.shareLink = [dictJson objectForKey:@"url"];
            staffObj.myWorks = [dictJson objectForKey:@"gallery_url"];
            staffObj.hours = [dictJson objectForKey:@"hours"];
            staffObj.product1Text = [dictJson objectForKey:@"product1_name"];
            staffObj.product2Text = [dictJson objectForKey:@"product2_name"];
            staffObj.product3Text = [dictJson objectForKey:@"product3_name"];
            staffObj.product4Text = [dictJson objectForKey:@"product4_name"];
            
            [data addObject:staffObj];
            
        }
        [self.tableView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [data count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StaffTableViewCell"];
    if(cell == Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"StaffTableViewCell"];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;;

    // Configure the cell...
    
    if(self.data.count){
             StaffObj *staffObj = [data objectAtIndex:indexPath.row];
    
    UIImageView *userImage = (UIImageView *)[cell.contentView viewWithTag:10];
    UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:20];
    UILabel *descLabel = (UILabel *)[cell.contentView viewWithTag:30];
    
    
    //[cell.textLabel setBackgroundColor:[UIColor clearColor]];
    nameLabel.text=[NSString stringWithFormat:@"%@",staffObj.name];
    descLabel.text=[NSString stringWithFormat:@"%@",staffObj.designation];
    [userImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.imageUrl]]];
   // cell.accessoryType=UITableViewCellAcce;
    }
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    StaffDetails *staff = [self.storyboard instantiateViewControllerWithIdentifier:@"StaffDetails"];
    [staff setStaffObj:[data objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:staff animated:YES];
    
}


@end
