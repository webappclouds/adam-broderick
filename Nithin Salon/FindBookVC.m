//
//  FindBookVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 09/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "FindBookVC.h"
#import "Globals.h"
#import "GlobalSettings.h"
@interface FindBookVC ()
{
    NSMutableArray *timesArray;
    int selectedIndex;
}
@end

@implementation FindBookVC
@synthesize compareStr;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"Find My Stylist";
    timesArray  = [[NSMutableArray alloc]initWithObjects:@"11:00 AM",@"12:00 PM",@"1:00 PM",nil];
    
    if([compareStr isEqualToString:@"Gallery"]){
        _descLabel.text = [NSString stringWithFormat:@"Make a appointment with Gavin"];
        _nameLabel.text =@"Gavin";
    }
    else{
        _descLabel.text = [NSString stringWithFormat:@"Make a appointment with Barbara F."];
        _nameLabel.text =@"Barbara";
    }
    
    _timeTV.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - FSCalendarDataSource

- (NSString *)calendar:(FSCalendar *)calendar titleForDate:(NSDate *)date
{
    return [self.gregorianCalendar isDateInToday:date] ? @"" : nil;
}


- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    
    return 0;
}

#pragma mark - FSCalendarDelegate


- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    [self showHud];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hideHud];
    });
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [timesArray count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text =[timesArray objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    if(indexPath.row  == selectedIndex)
    {
      cell.textLabel.textColor = [self colorFromHexString:@"#D93829"];
    }
    else
    {
       cell.textLabel.textColor = [self colorFromHexString:@"#1D1D1D"];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedIndex = (int)indexPath.row;
    [_timeTV reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)bookNowClick:(id)sender {
    
    
    [self showHud];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString *msg;
        if([compareStr isEqualToString:@"Gallery"]){
            msg =@"Thank you, your appointment for hair cut with Gavin has been booked.";
        }
        else
            msg =@"Thank you, your appointment for hair cut with Barbara has been booked.";
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:[NSString stringWithFormat:@"%@", msg] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        [alertController addAction:confirmAction];
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

@end
