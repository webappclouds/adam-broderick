//
//  ListViewController.m
//  Notifii
//
//  Created by Thirupathi on 17/02/14.
//  Copyright (c) 2014 LogicTree. All rights reserved.
//

#import "ListViewController.h"
#import "ViewController.h"
@interface ListViewController ()

@end

@implementation ListViewController
@synthesize listTable,listArray,superController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        listArray = [[NSMutableArray alloc] initWithCapacity:0];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [listTable setBackgroundColor:[UIColor whiteColor]];
//    [listTable setBackgroundColor:[UIColor colorWithRed:193.0/256.0 green:193.0/256.0 blue:193.0/256.0 alpha:0.6]];
    
//    if([listArray count] > 10)
//    {
//        [listTable setScrollEnabled:YES];
//        [listTable setShowsVerticalScrollIndicator:YES];
//    }
//    else{
//        [listTable setScrollEnabled:NO];
//        [listTable setShowsVerticalScrollIndicator:NO];
//        
//    }

    [listTable setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [self.view addSubview:listTable];
}

#pragma mark -
#pragma mark UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 45.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
//        if(indexPath.row != 0 && indexPath.row%2 != 0)
//            [cell setBackgroundColor:[UIColor colorWithRed:224.0/256.0 green:235.0/256.0 blue:245.0/256.0 alpha:0.6]];
//        else
//            [cell setBackgroundColor:[UIColor whiteColor]];
//
        
    }
    [self configureCell:cell forIndexPath:indexPath];
    
    return cell;
    
}

- (void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath{
    [cell.textLabel setNumberOfLines:2];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.text = [listArray objectAtIndex:indexPath.row] ;
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:19]];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [superController optionSelected:[NSNumber numberWithInt:indexPath.row]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
