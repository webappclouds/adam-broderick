//
//  ProfileViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 02/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTF,*lastNameTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *mobileTF;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property(weak,nonatomic) IBOutlet UIView *view1,*view2,*view3,*view4;
@property (weak, nonatomic) IBOutlet UILabel *fullNameTF;
- (IBAction)saveClick:(id)sender;

@end
