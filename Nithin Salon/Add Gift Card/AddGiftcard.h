//
//  AddGiftcard.h
//  Visage
//
//  Created by Nithin Reddy on 15/11/12.
//
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "ZBarReaderView.h"
#import "ZBarReaderViewController.h"


@interface AddGiftcard : UIViewController <UITextFieldDelegate, ZBarReaderDelegate>

@property (nonatomic, retain) IBOutlet UITextField *cardNumber;

-(IBAction)scan:(id)sender;

-(IBAction)add:(id)sender;

-(IBAction)hideKeyboard:(id)sender;

@end
