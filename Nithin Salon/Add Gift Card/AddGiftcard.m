//
//  AddGiftcard.m
//  Visage
//
//  Created by Nithin Reddy on 15/11/12.
//
//

#import "AddGiftcard.h"
#import "Constants.h"
#import "GiftcardGlobals.h"
#import "AppDelegate.h"
@interface AddGiftcard ()

@end

@implementation AddGiftcard
{
    NSString *voucher;
    AppDelegate *appDelegate;
    NSCharacterSet *allowedCharacters;
}

@synthesize cardNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    allowedCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title = @"Add a Gift card";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)scan:(id)sender
{
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    [self presentViewController: reader
                       animated: YES completion:nil];
    
    
}


- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    cardNumber.text = symbol.data;
    
    // EXAMPLE: do something useful with the barcode image
    //    resultImage.image =
    //    [info objectForKey: UIImagePickerControllerOriginalImage];
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    [reader dismissViewControllerAnimated: YES completion:nil];
}


-(IBAction)add:(id)sender
{
    voucher = [cardNumber text];
    if([voucher length]<2)
    {
        [self showAlert:@"Add a Gift card" MSG:@"Please enter a valid gift card number"];
        return;
    }
    voucher = [voucher lowercaseString];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dict setValue:@"nithinredd@gmail.com" forKey:@"customer_email"];
    [dict setValue:voucher forKey:@"voucher_code"];//gU50a3f9bb8acd9
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_CHECK_GIFTCARD] HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *resp = (NSMutableDictionary*)responseArray;
            BOOL status = [[resp objectForKey:@"status"] boolValue];
            if(!status)
                [self showAlert:@"Error" MSG:[resp objectForKey:@"msg"] tag:0];
            else
            {
                int valid = [[resp objectForKey:@"valid"] intValue];
                if(valid==0){
                    [self showAlert:@"Error" MSG:@"Invalid voucher"];
                     cardNumber.text =@"";
                }
                else
                {
                    if([GiftcardGlobals isExistsGiftcard:voucher])
                        [self showAlert:@"Already Exists" MSG:@"This giftcard has already been added." tag:1];
                    else{
                        [GiftcardGlobals addGiftcard:voucher];
                        [self showAlert:@"Success" MSG:@"Congratulations. The card has been added to your gift cards." tag:1];
                    }
                }
            }
            
            
        }
    }];
    
    
}


-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void) textFieldDidBeginEditing:(UITextField *)textView{
   
   // [self animateTextView: textView up: YES];
}
-(void) textFieldDidEndEditing:(UITextField *)textView{
    //[self animateTextView: textView up: NO];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)characters
{
    if(textField == cardNumber) {
        return ([characters rangeOfCharacterFromSet:allowedCharacters].location == NSNotFound);
    }
    return YES;
}

- (void) animateTextView: (UITextField *) textView up: (BOOL) up
{
    const int movementDistance = 90;
    const float movementDuration = 0.3f;
    int movement = (up ? -movementDistance : movementDistance);
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)hideKeyboard:(id)sender
{
    [cardNumber resignFirstResponder];
}


@end
