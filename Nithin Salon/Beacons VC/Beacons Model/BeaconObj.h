//
//  BeaconObj.h
//  Template
//
//  Created by Nithin Reddy on 17/06/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconObj : NSObject

@property (nonatomic, retain) NSString *beaconName;
@property (nonatomic, retain) NSNumber *major;
@property (nonatomic, retain) NSNumber *minor;

@end
