//
//  BeaconNotification.m
//  Template
//
//  Created by Nithin Reddy on 26/06/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import "BeaconNotification.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"
#import "AppDelegate.h"


@interface BeaconNotification ()
{
    AppDelegate *appDelegate;
}
@end

@implementation BeaconNotification

@synthesize dict;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
NSDictionary *beaconsDict = [NSDictionary dictionaryWithObjectsAndKeys:[dict objectForKey:@"major"], @"major",[dict objectForKey:@"minor"], @"minor", nil];
    
    //NSDictionary *beaconsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"1727", @"major",@"47115", @"minor", nil];
    
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_GET_BEACON_INFO] HEADERS:Nil GETPARAMS:Nil POSTPARAMS:beaconsDict COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseDict.count){
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:[responseDict objectForKey:@"special_image"]]];
            [self.label setText:[responseDict objectForKey:@"special_title"]];
        }
        }
    }];
    
          // Do any additional setup after loading the view from its nib.
//    [self.imageView setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]]];
//    [self.label setText:[dict objectForKey:@"title"]];
}



-(IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
