//
//  FindBookVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 09/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
@interface FindBookVC : UIViewController<FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UITableView *timeTV;
@property (weak, nonatomic) IBOutlet UIButton *bookNowClick;
- (IBAction)bookNowClick:(id)sender;
@property(nonatomic, retain)NSString *compareStr;
@property (weak  , nonatomic) IBOutlet FSCalendar *calendar;
@property (strong, nonatomic) NSCalendar *gregorianCalendar;
@end
