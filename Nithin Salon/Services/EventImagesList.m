//
//  EventImagesList.m
//  Salon KM
//
//  Created by Nithin Reddy on 06/04/14.
//
//

#import "EventImagesList.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"

@interface EventImagesList ()
{
    AppDelegate *appDelegate;

}

@end

@implementation EventImagesList

@synthesize data;
@synthesize selectedImageName;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"Select Image"];
    
//    data = [[NSMutableArray alloc] initWithCapacity:0];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

    if(data.count == 0){
      UIAlertController  *alertController = [UIAlertController alertControllerWithTitle:@"Select e-Giftcard Image"
                                                              message: @"No images found."
                                                       preferredStyle: UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Ok", nil)
                                                            style: UIAlertActionStyleDefault
                                                          handler: ^(UIAlertAction * _Nonnull action) {
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                          }]];
         [self presentViewController:alertController animated: true completion: nil];
        return;

    }
    
    
    NSLog(@"Data count :%d",data.count);
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *iden = @"Cell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:iden];
    
//    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:iden];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    NSString *urlStr = [NSString stringWithFormat:@"%@giftcards/%@", URL_IMAGE_UPLOAD, [data objectAtIndex:indexPath.row]];
        
//        NSString *urlStr = [[data objectAtIndex:indexPath.row] objectForKey:@"gallery_image"];

    [image setContentMode:UIViewContentModeScaleAspectFit];
    
        NSLog(@"dict : %@",[data objectAtIndex:indexPath.row]);
        NSLog(@"cell image url :%@",urlStr);
    [image setImageWithURL:[NSURL URLWithString:urlStr]];
    
    [cell addSubview:image];
        image.tag = 100;

    [cell setBackgroundColor:[UIColor whiteColor]];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    if([selectedImageName isEqualToString:[data objectAtIndex:indexPath.row]])
        [cell setBackgroundColor:[UIColor lightGrayColor]];
//        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    return cell;
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *galleryImageView = (UIImage*)[cell viewWithTag:100];
    
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithCapacity:1];
    [userInfo setObject:galleryImageView.image forKey:@"image"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"GiftCardGallery" object:nil userInfo:userInfo];
    [self.navigationController popViewControllerAnimated:YES];
}

//-(void) refreshcheckmark
//{
//    for (id algoPath in [self.tableView indexPathsForVisibleRows]){
//        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:algoPath];
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        
//        if([selectedImageName isEqualToString:[data objectAtIndex:indexPath.row]])
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }
//
//}

@end
