//
//  ServicesViewController.h
//  Nithin Salon
//
//  Created by Nithin Reddy on 23/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "Constants.h"
#import "ServiceObj.h"
#import "StaffObj.h"
@interface ServicesViewController : UIViewController<CAPSPageMenuDelegate,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic) CAPSPageMenu *pagemenu;
@property (nonatomic) IBOutlet UIView *slideView;
@property(nonatomic,assign)id serviceDelegate;
@property (nonatomic) ModuleObj *moduleObj;
@property (nonatomic)StaffObj *StaffModelObj;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHeight;
@property (nonatomic) NSMutableArray *data;
-(void)getServiceString:(NSString *)serviceName type:(NSString *)type  serviceObj:(ServiceObj*)serviceObj;
@end
