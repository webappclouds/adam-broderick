//
//  ServicesTableViewController.m
//  Nithin Salon
//
//  Created by Nithin Reddy on 23/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ServicesTableViewController.h"
#import "ServicesViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "AppDelegate.h"
@interface ServicesTableViewController ()
{
    NSMutableArray *globalResponseArray;
    BOOL loadValue;
    AppDelegate *appDelegate;

}
@end

@implementation ServicesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    appDelegate =(AppDelegate*)[UIApplication sharedApplication].delegate;
    self.data = [NSMutableArray new];
    [self loadDataFromServer];
    self.tableView.estimatedRowHeight = 125;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    [self.tableView reloadData];
   //    self.tableView.estimatedRowHeight = 125;
    
   
}
-(void)viewWillAppear:(BOOL)animated{
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    if(globalResponseArray.count==0)
    globalResponseArray = [[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ServiceSearch" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receive:) name:@"ServiceSearch" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshAllData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAll) name:@"RefreshAllData" object:nil];
    

   
}

-(void)refreshAll{
            if(globalResponseArray.count){
            for(NSDictionary *dict in globalResponseArray)
            {
                ServiceObj *serviceObj = [[ServiceObj alloc] init];
                serviceObj.idStr = [dict objectForKey:@"sub_menu_id"];
                serviceObj.title = [dict objectForKey:@"sub_menu_title"];
                serviceObj.desc = [dict objectForKey:@"sub_menu_description"];
                serviceObj.price = [dict objectForKey:@"sub_menu_price"];
                serviceObj.file = [dict objectForKey:@"sub_menu_file"];
                serviceObj.link = [dict objectForKey:@"sub_menu_externallink"];
                [self.data addObject:serviceObj];
                serviceObj = Nil;
            }
    
            }
   
 [self.tableView reloadData];
}
-(void)receive:(NSNotification * )notification{
    NSLog(@"info: %@",notification.object);
    
   
    NSArray *filtered = [globalResponseArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sub_menu_title CONTAINS[cd] %@",notification.object]];

    NSLog(@"filter:%@",filtered);
    
    if(self.data.count)
        [self.data removeAllObjects];
    NSArray *responseArray = [filtered mutableCopy];
    if([notification.object length]==0)
    {
        if(globalResponseArray.count){
        for(NSDictionary *dict in globalResponseArray)
        {
            ServiceObj *serviceObj = [[ServiceObj alloc] init];
            serviceObj.idStr = [dict objectForKey:@"sub_menu_id"];
            serviceObj.title = [dict objectForKey:@"sub_menu_title"];
            serviceObj.desc = [dict objectForKey:@"sub_menu_description"];
            serviceObj.price = [dict objectForKey:@"sub_menu_price"];
            serviceObj.file = [dict objectForKey:@"sub_menu_file"];
            serviceObj.link = [dict objectForKey:@"sub_menu_externallink"];
            [self.data addObject:serviceObj];
            serviceObj = Nil;
        }

        }
    }
    
    for(NSDictionary *dict in responseArray)
    {
        if(dict.count){
        ServiceObj *serviceObj = [[ServiceObj alloc] init];
        serviceObj.idStr = [dict objectForKey:@"sub_menu_id"];
        serviceObj.title = [dict objectForKey:@"sub_menu_title"];
        serviceObj.desc = [dict objectForKey:@"sub_menu_description"];
        serviceObj.price = [dict objectForKey:@"sub_menu_price"];
        serviceObj.file = [dict objectForKey:@"sub_menu_file"];
        serviceObj.link = [dict objectForKey:@"sub_menu_externallink"];
        [self.data addObject:serviceObj];
        serviceObj = Nil;
        }
    }
    [self.tableView reloadData];

}
-(void) loadDataFromServer
{
   
    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:[NSString stringWithFormat:@"%@%@/%@/", [appDelegate addSalonIdTo:URL_MENU_DETAILS], self.moduleObj.moduleId, self.menuId] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [appDelegate hideHUD];
        if(error)
            [self showAlert:self.title MSG:error.localizedDescription];
        else
        {
            if(self.data.count)
                [self.data removeAllObjects];
            globalResponseArray = [responseArray mutableCopy];
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:self.title MSG:@"No services found"];
                return;
            }
            
            for(NSDictionary *dict in responseArray)
            {
                ServiceObj *serviceObj = [[ServiceObj alloc] init];
                serviceObj.idStr = [dict objectForKey:@"sub_menu_id"];
                serviceObj.title = [dict objectForKey:@"sub_menu_title"];
                serviceObj.desc = [dict objectForKey:@"sub_menu_description"];
                serviceObj.price = [dict objectForKey:@"sub_menu_price"];
                serviceObj.file = [dict objectForKey:@"sub_menu_file"];
                serviceObj.link = [dict objectForKey:@"sub_menu_externallink"];
                [self.data addObject:serviceObj];
                serviceObj = Nil;
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"ServicesCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   

       if(indexPath.row<self.data.count){
    ServiceObj *serviceObj = [self.data objectAtIndex:indexPath.row];
    [(UILabel *) [cell.contentView viewWithTag:1] setText:serviceObj.title];
    [(UILabel *) [cell.contentView viewWithTag:2] setText:serviceObj.desc];
    [(UILabel *) [cell.contentView viewWithTag:3] setText:serviceObj.price];
    UIButton *options = (UIButton *) [cell.contentView viewWithTag:4];
        //options.backgroundColor = [UIColor redColor];
    [options.titleLabel setTag:indexPath.row];
        options.hidden = NO;
        options.userInteractionEnabled =YES;
        
    [options addTarget:self action:@selector(optionsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if([appDelegate.buttonHideValue isEqualToString:@"YES"])
            options.hidden =YES;
    }

    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([appDelegate.buttonHideValue isEqualToString:@"YES"]){
        ServiceObj *serviceObj = [self.data objectAtIndex:indexPath.row];

        appDelegate.globalServiceComparisionStr =@"NO";
    [self.reqAppoitmnetDelegate getServiceString:serviceObj.title type:@"Select" serviceObj:serviceObj];
        

   
    [self.navigationController popViewControllerAnimated:YES];
}
}


-(void) optionsClicked : (id) sender
{
    UIButton *b = (UIButton *)sender;
    ServiceObj *serviceObj = [self.data objectAtIndex:[b.titleLabel tag]];
    [self.reqAppoitmnetDelegate getServiceString:serviceObj.title type:@"Action" serviceObj:serviceObj];
    
   }


@end
