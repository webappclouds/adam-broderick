//
//  ServicesViewController.m
//  Nithin Salon
//
//  Created by Nithin Reddy on 23/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ServicesViewController.h"
#import "ServicesTableViewController.h"
#import "SpecialsObj.h"
#import "AppDelegate.h"
#import "UIViewController+NRFunctions.h"
#import "GoogleReviewVC.h"
#import "LoginVC.h"
#import "OnlineBookindViewCon.h"
@interface ServicesViewController ()
{
    UIToolbar* toolBar;
    AppDelegate *appDelegate;
}
@end

@implementation ServicesViewController
{
    NSMutableArray *controllerArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    _searchBarHeight.constant =0;
   // [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -60)
                                                         //forBarMetrics:UIBarMetricsDefault];
    self.title = self.moduleObj.moduleName;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor], UITextAttributeFont: [UIFont fontWithName:@"OpenSans" size:15.0f]};
    

    
    self.data = [NSMutableArray new];
    
    controllerArray = [NSMutableArray array];
    [UIViewController addBottomBar:self];
    
    
    UIBarButtonItem *search = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search.png"] style:UIBarButtonItemStylePlain target:self action:@selector(search)];
    
     search.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = search;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
     numberToolbar.backgroundColor = UIColorMake(73, 88, 107);
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(remove)],
                           nil];
    [numberToolbar sizeToFit];
    _searchBar.inputAccessoryView = numberToolbar;
    
    [self loadDataFromServer];
}
-(void)remove{
    //_searchBar.text =@"";
           // [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshAllData" object:nil];
    toolBar.hidden = YES;
    [_searchBar resignFirstResponder];
}
-(void)search{
    _searchBarHeight.constant =44;
   
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"ServiceSearch" object:searchBar.text];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
       [[NSNotificationCenter defaultCenter] postNotificationName:@"ServiceSearch" object:searchBar.text];

}
-(void) loadDataFromServer
{
    [self.data removeAllObjects];
    [appDelegate showHUD];
    
    NSLog(@"URL ID : %@",[NSString stringWithFormat:@"%@%@", [appDelegate addSalonIdTo:URL_MENU], self.moduleObj.moduleId]);
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:[NSString stringWithFormat:@"%@%@", [appDelegate addSalonIdTo:URL_MENU], self.moduleObj.moduleId] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                //                [self showAlert:@"Error" MSG:@"No services found"];
                return;
            }
            
            for(NSDictionary *dict in responseArray)
            {
                SpecialsObj *specialObj = [[SpecialsObj alloc] init];
                specialObj.idStr = [dict objectForKey:@"menu_id"];
                specialObj.title = [dict objectForKey:@"menu_title"];
                specialObj.image = [dict objectForKey:@"menu_attachment"];
                
                
                ServicesTableViewController *tableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ServicesTableViewController"];
                tableViewController.reqAppoitmnetDelegate =self;
                tableViewController.title = specialObj.title;
                tableViewController.menuId = specialObj.idStr;
                tableViewController.attachment = specialObj.image;
                tableViewController.moduleObj = self.moduleObj;
                [controllerArray addObject:tableViewController];
                [self.data addObject:specialObj];
                specialObj = Nil;
            }
            
            NSDictionary *parameters = @{CAPSPageMenuOptionMenuItemSeparatorWidth: @(5),
                                         CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(NO),
                                         CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                         CAPSPageMenuOptionAddBottomMenuHairline : @(YES),
                                         CAPSPageMenuOptionCenterMenuItems : @(YES),
                                         CAPSPageMenuOptionMenuHeight : @(56),
                                         CAPSPageMenuOptionMenuItemFont : [UIFont fontWithName:@"OpenSans" size:15.0],
                                         CAPSPageMenuOptionScrollMenuBackgroundColor : [self colorFromHexString:@"#28384D"]
                                         };
            
            // Initialize page menu with controller array, frame, and optional parameters
            _pagemenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0,0, self.slideView.frame.size.width, self.view.frame.size.height-124) options:parameters];
            _pagemenu.delegate = self;
            
                                  // Lastly add page menu as subview of base view controller view
            // or use pageMenu controller in you view hierachy as desired
            [self.slideView addSubview:_pagemenu.view];
            
            
            //            [self.tableView reloadData];
            
        }
    }];
}
/*
-(void)getServiceString:(NSString *)serviceName type:(NSString *)type{
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.serviceNameStr = [serviceName mutableCopy];
    if([type isEqualToString:@"Action"]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Choose one" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Request Appointment" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
            
            
            //code to be executed on the main queue after delay
            RequestAppointment *requestAppointment = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestAppointment"];
            requestAppointment.staffObjModel = _staffModelObj;
            [self.navigationController pushViewController:requestAppointment animated:YES];
                   }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else{
        [self.serviceDelegate getService:serviceName];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

*/

-(void)getServiceString:(NSString *)serviceName type:(NSString *)type serviceObj:(ServiceObj*)shareObj
{
   SIAlertView* alertView = [[SIAlertView alloc] initWithTitle:@"" andMessage:@"Please select one"];
    appDelegate.serviceNameStr = [serviceName mutableCopy];
    NSMutableArray *multipleArray = [[NSMutableArray alloc]init];
    multipleArray =[appDelegate.dynamicApptList mutableCopy];
    for(NSString *name in multipleArray){
        if ([name isEqualToString:@"Last Minute Appts"]) {
            [multipleArray removeObject:name];
        }
    }

    if([type isEqualToString:@"Action"]){
        [multipleArray addObject:@"Share"];
        int count = (int)[multipleArray count];
        if(count==0)
        {
            
            if ([appDelegate.onlineBookingUrl length])
                [alertView addButtonWithTitle:@"Book Online"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          [self bookOnline];
                                      }];
            else{
                [self showAlert:@"Request Appt" MSG:@"There are no Request Appts currently."];
            }
            
           
        }
        else if (count==1) {
            
            for(NSString *name in multipleArray)
            {
                if ([name isEqualToString:@"Book Online"]) {
                    
                    if ([appDelegate.onlineBookingUrl length]==0){
                        
                        NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
                       
                        
                        if ([slcIdStr length]==0) {
                            appDelegate.flgObj=@"onlineBookingFlg";
                            [self loginScreen];
                        }
                        else{
                            [alertView addButtonWithTitle:@"Book Online"
                                                     type:SIAlertViewButtonTypeDefault
                                                  handler:^(SIAlertView *alert) {
                                                      [self bookOnline];
                                                  }];
                        }
                    }
                    else{
                        [alertView addButtonWithTitle:@"Book Online"
                                                 type:SIAlertViewButtonTypeDefault
                                              handler:^(SIAlertView *alert) {
                                                  [self bookOnline];
                                              }];
                    }
                }
                else if ([name isEqualToString:@"Request a new Appt"]){
                    [alertView addButtonWithTitle:@"Request appointment"
                                             type:SIAlertViewButtonTypeDefault
                                          handler:^(SIAlertView *alert) {
                                              [self navigateToRequestAppointmentScreen:shareObj];
                                          }];

                }
            }
            

        }
        
        else{
    
 
            for(NSString *str in multipleArray){
                
                
                if([str isEqualToString:@"Book Online"]){
                    [alertView addButtonWithTitle:@"Book Online"
                                             type:SIAlertViewButtonTypeDefault
                                          handler:^(SIAlertView *alert) {
                                              [self bookOnline];
                                          }];
                }
                
                if([str isEqualToString:@"Request a new Appt"]){
                    
                    [alertView addButtonWithTitle:@"Request appointment"
                                             type:SIAlertViewButtonTypeDefault
                                          handler:^(SIAlertView *alert) {
                                              [self navigateToRequestAppointmentScreen:shareObj];
                                          }];
                }
            }
                 }
        
        
        
        
        if(shareObj.link.length|| shareObj.file.length){
            [alertView addButtonWithTitle:@"Attachment" type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      NSString *urlStr;
                                      if(shareObj.link.length)
                                          urlStr = [NSString stringWithFormat:@"%@",shareObj.link];
                                      else if(shareObj.file.length)
                                          urlStr = [NSString stringWithFormat:@"%@",shareObj.file];
                                      
                                      self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                                      
                                      GoogleReviewVC *grvc = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
                                      [grvc setUrl:urlStr];
                                      [grvc setTitle:shareObj.title];
                                      [self.navigationController pushViewController:grvc animated:YES];
                                      
                                  }];
        }
        
        
        [alertView addButtonWithTitle:@"Share"  type:SIAlertViewButtonTypeBlue
                              handler:^(SIAlertView *alert) {
                                  
                                  NSString * descriptionWithTitle = [NSString stringWithFormat:@"%@\nTitle:%@ \nDescription: %@\nPrice: %@\n %@ ",SHARE_LONG_TEXT_MENU,shareObj.title,shareObj.desc, shareObj.price,[UIViewController returnShareUrl]];
                                   NSString *urlStr;
                                  if(shareObj.link.length)
                                  urlStr = [NSString stringWithFormat:@"%@",shareObj.link];
                                  else if(shareObj.file.length)
                                      urlStr = [NSString stringWithFormat:@"%@",shareObj.file];
                                  
                                  NSArray *itemsToShare;
                                  if(urlStr.length==0)
                                      itemsToShare = @[descriptionWithTitle];
                                  else
                                      itemsToShare = @[descriptionWithTitle,urlStr];

                                  UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
                                  activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
                                  [activityVC setValue:SHARE_SUBJECT forKey:@"subject"];

                                  [self presentViewController:activityVC animated:YES completion:nil];
                                  
                              }];
        
        
        [alertView addButtonWithTitle:@"Cancel" type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        [alertView show];

        
     }
     else{
         
         [self.serviceDelegate getService:serviceName];
         [self.navigationController popViewControllerAnimated:YES];
     }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadWebView : (NSString *) title URL:(NSString *)url
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    GoogleReviewVC *loader = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [loader setUrl:url];
    [loader setTitle:title];
    [self.navigationController pushViewController:loader animated:YES];
    
}

#pragma mark Logi screen method
-(void)loginScreen{
    LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    //login.appointmentModule = self.appointmentsModule;
    [self.navigationController pushViewController:login animated:YES];
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index
{
 _searchBar.text = @"";
    [_searchBar resignFirstResponder];
}

-(void) bookOnline
{
    appDelegate.screenName =@"services";
    if(appDelegate.onlineBookingUrl.length){
        [self loadWebView:@"Book Online" URL:appDelegate.onlineBookingUrl];
    }
    else{
        NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
        if(loginVal.length){
            
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            OnlineBookindViewCon *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineBookindViewCon"];
            
            [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
        }
        else{
            appDelegate.flgObj = @"onlineBookFlag";
            [self loginScreen];
        }
    }
    
    
}
-(void)navigateToRequestAppointmentScreen:(ServiceObj*)shareObj{
    RequestAppointment *requestAppointment = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestAppointment"];
    [requestAppointment setState:1];
    requestAppointment.specialObj = shareObj;
    appDelegate.globalServiceComparisionStr =@"YES";
    [self.navigationController pushViewController:requestAppointment animated:YES];
}

@end
