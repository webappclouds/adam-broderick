//
//  ServiceObj.h
//  Demo Salon
//
//  Created by Nithin Reddy on 25/03/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceObj : NSObject

@property (nonatomic, retain) NSString *idStr;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *file;
@property (nonatomic, retain) NSString *link;
@property (nonatomic, retain) NSString *serviceName;
@property (nonatomic, retain) NSString *cost;

@end
