//
//  StaffDetails.m
//  Nithin Salon
//
//  Created by Webappclouds on 15/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "StaffDetails.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>
#import "UIViewController+NRFunctions.h"
#import "ProductsImage.h"
#import "NSString+HTML.h"
#import "AppDelegate.h"
#import "GoogleReviewVC.h"
#import "OnlineBookindViewCon.h"
#import "LoginVC.h"
@interface StaffDetails ()
{
    AppDelegate *appDelegate;
    NSString *htmlString;
}
@property(nonatomic,retain)NSMutableArray *photos;
@end

@implementation StaffDetails

@synthesize nameLabel, userImage, descTV, designationLabel;
@synthesize staffObj;
@synthesize portfolio;
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden =YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden =NO;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
//    // Clear cache for testing
//    [[SDImageCache sharedImageCache] clearDisk];
//    [[SDImageCache sharedImageCache] clearMemory];
    
    
    self.title=staffObj.name;
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    self.nameLabel.text=[NSString stringWithFormat:@"%@",staffObj.name];
    self.designationLabel.text=[NSString stringWithFormat:@"%@",staffObj.designation];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    htmlString=staffObj.description;
    
    
    [userImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.imageUrl] ] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
    
   }



-(IBAction)ShowTImings:(id)sender{
    
    if(staffObj.hours.length){
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Service provider timings" message:staffObj.hours preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        NSString* urlString =(NSMutableString*) [appDelegate addSalonIdTo:URL_HOURS];
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                [appDelegate hideHUD];
                NSMutableDictionary * arrStringResponse=(NSMutableDictionary *)responseArray;
                [self serviceResponse:arrStringResponse];
                
            }
        }];
        
        //[self showAlert:@"Hours" MSG:@"There is no timings for this staff" tag:0];
    }


}


-(void)serviceResponse:(NSMutableDictionary *)dict{
    
    NSMutableDictionary * arrStringResponse=(NSMutableDictionary *)dict;
    
    if (arrStringResponse==nil||arrStringResponse.count==0)
        [self showAlert:SALON_NAME MSG:@"Information not available. Please try again later."];
    else {
        
        NSString *content = @"";
        
        content = [[arrStringResponse objectForKey:@"config_hours"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        content = [content stringByTrimmingCharactersInSet:
                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Salon timings" message:content preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
    
}


- (IBAction)productClicked : (id) sender
{
    UIButton *btn = (UIButton *)sender;
    
    NSString *prodId = @"";
    NSString *prodNameStr = @"";
    NSString *prodImageUrlStr = @"";
    int whichClicked = (int)[btn tag];
   
    if(whichClicked==50)
    {
        prodId = @"1";
        prodNameStr = staffObj.product1Text;
        prodImageUrlStr = staffObj.product1;
       
    }
    else if(whichClicked==60)
    {
        prodId = @"2";
        prodNameStr = staffObj.product2Text;
        prodImageUrlStr = staffObj.product2;
        

    }
    else if(whichClicked==70)
    {
        prodId = @"3";
        prodNameStr = staffObj.product3Text;
        prodImageUrlStr = staffObj.product3;
        

    }
    else if(whichClicked==80)
    {
        prodId = @"4";
        prodNameStr = staffObj.product4Text;
        prodImageUrlStr = staffObj.product4;
       

    }
    
    //    if([prodNameStr length]==0 || [prodImageUrlStr length]==0)
    //        return;
    
    if(prodImageUrlStr.length){
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil] ;
    ProductsImage *prodImage = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductsImage"];
    [prodImage setStaffId:staffObj.staffId];
    [prodImage setProductId:prodId];
    [prodImage setProductNameStr:prodNameStr];
    [prodImage setProductImageUrlStr:prodImageUrlStr];
    [self.navigationController pushViewController:prodImage animated:YES];
    }
}

-(IBAction)share:(id)sender
{
    NSString *textToShare = [NSString stringWithFormat:@"Hi,\nYou can view the page of %@ at %@.\n\n%@", staffObj.name, SALON_NAME, staffObj.shareLink];
    UIImage *imageToShare = userImage.image;
    NSArray *itemsToShare = @[textToShare, imageToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
    [activityVC setValue:SHARE_SUBJECT forKey:@"subject"];

    [self presentViewController:activityVC animated:YES completion:nil];
    
}

-(IBAction)requestAppt:(id)sender
{
    
    
    NSMutableArray *multipleArray = [[NSMutableArray alloc]init];
    multipleArray =[appDelegate.dynamicApptList mutableCopy];
    for(NSString *name in multipleArray){
        if ([name isEqualToString:@"Last Minute Appts"]) {
            [multipleArray removeObject:name];
        }
    }
    
    
        int count = (int)[multipleArray count];
        if(count==0)
        {
            
            if ([appDelegate.onlineBookingUrl length])
                [self bookOnline];
            else{
                [self showAlert:@"Request Appt" MSG:@"No modes of appointments"];
            }
            
            return;
        }
        else if (count==1) {
            
            for(NSString *name in multipleArray)
            {
                if ([name isEqualToString:@"Book Online"]) {
                    
                    if ([appDelegate.onlineBookingUrl length]==0){
                        
                        NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
                        
                        
                        if ([slcIdStr length]==0) {
                            appDelegate.flgObj=@"onlineBookingFlg";
                            [self loginScreen];
                        }
                        else{
                            [self bookOnline];
                        }
                    }
                    else{
                        [self bookOnline];
                    }
                }
                else if ([name isEqualToString:@"Request a new Appt"]){
                    [self navigateToRequestAppointmentScreen];
                }
            }
            
        }
        
        else{
            
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"" andMessage:@"Please select one"];
            for(NSString *str in multipleArray){
                
                
                if([str isEqualToString:@"Book Online"]){
                    [alertView addButtonWithTitle:@"Book Online"
                                             type:SIAlertViewButtonTypeDefault
                                          handler:^(SIAlertView *alert) {
                                              [self bookOnline];
                                          }];
                }
                
                if([str isEqualToString:@"Request a new Appt"]){
                    
                    [alertView addButtonWithTitle:@"Request appointment"
                                             type:SIAlertViewButtonTypeDefault
                                          handler:^(SIAlertView *alert) {
                                              [self navigateToRequestAppointmentScreen];
                                          }];
                }
            }
            
            [alertView addButtonWithTitle:@"Cancel" type:SIAlertViewButtonTypeDestructive
                                  handler:^(SIAlertView *alert) {
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
            [alertView show];
        }

    
   

}
    -(void)navigateToRequestAppointmentScreen{
        
        RequestAppointment *requestAppointment = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestAppointment"];
        requestAppointment.staffObj = staffObj;
        requestAppointment.state =1;
        appDelegate.globalServiceComparisionStr =@"YES";
        [self.navigationController pushViewController:requestAppointment animated:YES];
    }
-(IBAction)porfolioAction:(id)sender
{
    if(staffObj.myWorks==nil || [staffObj.myWorks length]==0)
    {
        [self showAlert:@"Portfolio" MSG:@"There are no attachments currently."];
        return;
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    
    self.photos = [NSMutableArray array];
    NSMutableArray  *data = [[NSMutableArray alloc] initWithArray:[staffObj.myWorks componentsSeparatedByString:@","]];
    
    // Add photos
    for(int i=0; i<data.count;i++){
        MWPhoto *photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectAtIndex:i]]];
       // photoObj.caption =@"hello";
        [self.photos addObject:photoObj];
        
    }
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    [browser setDelegate:self];
    browser.displayNavArrows = YES;
    browser.startOnGrid = YES;
    browser.displayActionButton = NO;
    [self.navigationController pushViewController:browser animated:YES];
    
    
    
}
#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}


-(void) bookOnline
{
    appDelegate.screenName =@"staffDetails";
    if(appDelegate.onlineBookingUrl.length){
        [self loadWebView:@"Book Online" URL:appDelegate.onlineBookingUrl];
    }
    else{
        NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
        if(loginVal.length){
            
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            OnlineBookindViewCon *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineBookindViewCon"];
            
            [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
        }
        else{
            appDelegate.flgObj = @"onlineBookFlag";
            [self loginScreen];
        }
    }
    
    
}

-(void) loadWebView : (NSString *) title URL:(NSString *)url
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    GoogleReviewVC *loader = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [loader setUrl:url];
    [loader setTitle:title];
    [self.navigationController pushViewController:loader animated:YES];
    
}

-(void)loginScreen{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:login animated:YES];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
   
     if(!(staffObj.product1.length) &&!(staffObj.product2.length)&&!(staffObj.product3.length)&&!(staffObj.product4.length)){
         return 1;
     }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 1)
        self.staffDetailsTV.estimatedRowHeight = 118;
    else
        self.staffDetailsTV.estimatedRowHeight = 150;
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *CellIdentifier = @"cellID";
    NSString *cellIdentifier1 = @"cell2";
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case 0:{
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            [(UILabel *) [cell.contentView viewWithTag:1] setText:@"Description"];
            UITextView *tv = (UITextView *)[cell.contentView viewWithTag:2];
            
           // UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:100];
            cell.layer.masksToBounds = NO;
            cell.layer.cornerRadius  = 6;
            cell.layer.shadowColor   =[UIColor darkGrayColor].CGColor;
            cell.layer.shadowOffset  = CGSizeMake(0, 0);
            cell.layer.shadowRadius  = 1.0;
            cell.layer.borderWidth = 0.00f;
            cell.layer.shadowOpacity = 0.2;
            UIBezierPath *cornersPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds  cornerRadius:cell.layer.cornerRadius];
            cell.layer.shadowPath = (__bridge CGPathRef _Nullable)(cornersPath);
            cell.layer.masksToBounds = NO;
            
            tv.contentInset = UIEdgeInsetsMake(-7.0,0.0,0,0.0);
           [tv setValue:htmlString forKey:@"contentToHTMLString"];
            
           // [(UILabel *) [cell.contentView viewWithTag:3] setText:@""];
            
        }
            //Load data in this prototype cell
            break;
        case 1:{
            
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
            
            cell.layer.cornerRadius  = 6;
            cell.layer.shadowColor   =[UIColor darkGrayColor].CGColor;
            cell.layer.shadowOffset  = CGSizeMake(0, 0);
            cell.layer.shadowRadius  = 1.0;
            cell.layer.borderWidth = 0.0f;
            cell.layer.shadowOpacity = 0.2;
            UIBezierPath *cornersPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds  cornerRadius:cell.layer.cornerRadius];
            cell.layer.shadowPath = (__bridge CGPathRef _Nullable)(cornersPath);
            cell.layer.masksToBounds = NO;
            
            if((staffObj.product1.length)||(staffObj.product2.length)||(staffObj.product3.length)||(staffObj.product4.length)){
                
//                UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:200];
//                imv.layer.shadowColor = [UIColor darkGrayColor].CGColor;
//                imv.layer.shadowOffset = CGSizeMake(0,0);
//                imv.layer.shadowOpacity = 0.2;
//                imv.layer.shadowRadius = 4.0;
//                imv.layer.masksToBounds =NO;
//                
                UIImageView *product1 = (UIImageView *)[cell.contentView viewWithTag:10];
                UIImageView *product2 = (UIImageView *)[cell.contentView viewWithTag:20];
                UIImageView *product3 = (UIImageView *)[cell.contentView viewWithTag:30];
                UIImageView *product4 = (UIImageView *)[cell.contentView viewWithTag:40];
                
                 UIButton *productBtn1 = (UIButton *)[cell.contentView viewWithTag:50];
                
                 UIButton *productBtn2 = (UIButton *)[cell.contentView viewWithTag:60];
                 UIButton *productBtn3 = (UIButton *)[cell.contentView viewWithTag:70];
                 UIButton *productBtn4 = (UIButton *)[cell.contentView viewWithTag:80];
                
                UILabel *productLabel1 = (UILabel *)[cell.contentView viewWithTag:100];
                UILabel *productLabel2 = (UILabel *)[cell.contentView viewWithTag:101];
                UILabel *productLabel3 = (UILabel *)[cell.contentView viewWithTag:102];
                UILabel *productLabel4 = (UILabel *)[cell.contentView viewWithTag:103];

                
                    [productLabel1 setText:staffObj.product1Text];
                    [productLabel2 setText:staffObj.product2Text];
                    [productLabel3 setText:staffObj.product3Text];
                    [productLabel4 setText:staffObj.product4Text];
                
                
                        [product1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.product1]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
                        [product2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.product2]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
                        [product3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.product3]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
                        [product4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",staffObj.product4]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
                       // _productsView.backgroundColor = [UIColor lightGrayColor];
            
                [productBtn1 addTarget:self action:@selector(productClicked:) forControlEvents:UIControlEventTouchUpInside];
                [productBtn2 addTarget:self action:@selector(productClicked:) forControlEvents:UIControlEventTouchUpInside];
                [productBtn3 addTarget:self action:@selector(productClicked:) forControlEvents:UIControlEventTouchUpInside];
                [productBtn4 addTarget:self action:@selector(productClicked:) forControlEvents:UIControlEventTouchUpInside];
            
        }
            //Load data in this prototype cell
            break;
    }
    }
    
   
    
    return cell;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==1)
        return 160;
    else
        return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section==1)
        return 40;
    else
        return 0;

}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.tintColor = [UIColor clearColor];
}
-(IBAction)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc]init];
    header.backgroundColor = [UIColor clearColor];
    return header;
    
}


@end
