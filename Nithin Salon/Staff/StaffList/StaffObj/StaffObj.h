//
//  StaffObj.h
//  5th Avenue
//
//  Created by Sunil on 5/3/14.
//
//
//
#import <Foundation/Foundation.h>

@interface StaffObj : NSObject

@property (nonatomic, retain) NSString *staffId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *designation;
@property (nonatomic, retain) NSString *imageUrl;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *product1;
@property (nonatomic, retain) NSString *product2;
@property (nonatomic, retain) NSString *product3;
@property (nonatomic, retain) NSString *product4;

@property (nonatomic, retain) NSString *product1Text;
@property (nonatomic, retain) NSString *product2Text;
@property (nonatomic, retain) NSString *product3Text;
@property (nonatomic, retain) NSString *product4Text;

@property (nonatomic, retain) NSString *myWorks;
@property (nonatomic, retain) NSString *hours;
@property (nonatomic, retain) NSString *shareLink;

@end
