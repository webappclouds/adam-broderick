//
//  StaffList.m
//  Nithin Salon
//
//  Created by Webappclouds on 07/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "StaffList.h"
#import "Constants.h"
#import "StaffObj.h"
#import "StaffDetails.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
@interface StaffList ()
{
    NSArray *globalArray;
    UIToolbar *toolBar;
    AppDelegate *appDelegate;
}
@end

@implementation StaffList
@synthesize screenNameStr;

-(void)viewWillAppear:(BOOL)animated{
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus) name:@"Reachability" object:nil];
}
-(void)checkNetworkStatus{
    [self showAlert:@"Alert" MSG:@"please check your internet connecion"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Service Providers";
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    //self.staffTableView.layoutMargins =  UIEdgeInsetsZero;
    [UIViewController addBottomBar:self];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.backgroundColor =[UIColor lightGrayColor];
    //numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(remove)],
                           nil];
    [numberToolbar sizeToFit];
    _searchBar.inputAccessoryView = numberToolbar;
    
    [self loadStaffServiceCall];
    
    // Do any additional setup after loading the view.
}


-(void)remove{
    toolBar.hidden = YES;
    [_searchBar resignFirstResponder];
}
-(void) loadStaffServiceCall{
    [appDelegate showHUD];
    self.data = [[NSMutableArray alloc]init];
    NSString *str = [NSString stringWithFormat:@"%@%@",[appDelegate addSalonIdTo:URL_STAFF],appDelegate.staffModel.moduleId];
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:str HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:SALON_NAME MSG:@"No staff found" tag:2];
                return;
            }
            globalArray = [responseArray mutableCopy];
            [self fileteredArray:responseArray];
            
                    }
    }];
    
}

#pragma mark tableview Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.data.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:100];
    imv.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    imv.layer.shadowOffset = CGSizeMake(0,0);
    imv.layer.shadowOpacity = 0.2;
    imv.layer.shadowRadius = 4.0;
    imv.layer.masksToBounds =NO;
    
    if(self.data.count){
        StaffObj *staffObj = [self.data objectAtIndex:indexPath.section];
        UILabel *nameLabel = (UILabel*)[cell viewWithTag:10];
        UILabel *designationLabel = (UILabel*)[cell viewWithTag:20];
        UIImageView *imageView = (UIImageView*)[cell viewWithTag:30];
        
        nameLabel.text=[NSString stringWithFormat:@"%@",staffObj.name];
        designationLabel.text=[NSString stringWithFormat:@"%@",staffObj.designation];
        
                
        if([staffObj.imageUrl length]>0)
        {
            NSString *imageUrl = staffObj.imageUrl;
            if(![imageUrl containsString:IMAGES_URL])
                imageUrl = [NSString stringWithFormat:@"%@%@", IMAGES_URL, staffObj.imageUrl];
            imageView.bounds = CGRectInset(imageView.frame, 20.0f, 20.0f);
            [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
            
        }
        else
            [imageView setImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
        
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    if(![screenNameStr isEqualToString:@"FromReqAppt"]){
        StaffDetails *StaffDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"StaffDetails"];
        StaffDetails.staffObj =[self.data objectAtIndex:indexPath.section];
        
        [self.navigationController pushViewController:StaffDetails animated:YES];
    }
    else{
         appDelegate.globalServiceComparisionStr =@"NO";
        StaffObj *obj = [self.data objectAtIndex:indexPath.section];
        [_reqAppoitmnetDelegate getServiceProvider:obj.name];
       
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    toolBar.hidden =NO;
    if(searchText.length>0&&globalArray.count){
    
    NSArray *filtered = [globalArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@",searchText]];
    
    NSLog(@"filter:%@",filtered);
    
    [self fileteredArray:filtered];
    }
    else
        [self fileteredArray:globalArray];

    
}


-(void)fileteredArray : (NSArray *)responseArray{
    if(self.data.count)
        [self.data removeAllObjects];
    
    if(responseArray.count){
    for(NSDictionary *dictJson in responseArray)
    {
        if(dictJson){
        NSString * strTitles=[dictJson objectForKey:@"name"];
        NSString * strDesignation=[dictJson objectForKey:@"designation"];
        NSString *imageUrl = [dictJson objectForKey:@"image"];
        NSString * strDesc=[dictJson objectForKey:@"description"];
        NSString * strProd1=[dictJson objectForKey:@"product1"];
        NSString * strProd2=[dictJson objectForKey:@"product2"];
        NSString * strProd3=[dictJson objectForKey:@"product3"];
        NSString * strProd4=[dictJson objectForKey:@"product4"];
        
        StaffObj *staffObj = [[StaffObj alloc] init];
        staffObj.staffId = [dictJson objectForKey:@"staff_id"];
        staffObj.name = strTitles;
        staffObj.designation = strDesignation;
        staffObj.imageUrl = imageUrl;
        staffObj.description = strDesc;
        staffObj.product1 = strProd1;
        staffObj.product2 = strProd2;
        staffObj.product3 = strProd3;
        staffObj.product4 = strProd4;
        staffObj.shareLink = [dictJson objectForKey:@"url"];
        staffObj.myWorks = [dictJson objectForKey:@"gallery_url"];
        staffObj.hours = [dictJson objectForKey:@"hours"];
        staffObj.product1Text = [dictJson objectForKey:@"product1_name"];
        staffObj.product2Text = [dictJson objectForKey:@"product2_name"];
        staffObj.product3Text = [dictJson objectForKey:@"product3_name"];
        staffObj.product4Text = [dictJson objectForKey:@"product4_name"];
        
        [self.data addObject:staffObj];
        }
        
    }
    }
    [self.staffTableView reloadData];

}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == [tableView numberOfSections] - 1)
        return 20;
    else
        return 0;
    
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.tintColor = [UIColor clearColor];
}

-(void)viewWillDisappear:(BOOL)animated{
    [_searchBar resignFirstResponder];
}
@end
