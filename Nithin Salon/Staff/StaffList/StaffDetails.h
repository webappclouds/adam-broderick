//
//  StaffDetails.h
//  Nithin Salon
//
//  Created by Webappclouds on 15/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaffObj.h"
#import "MWPhotoBrowser.h"
#import "SDImageCache.h"
#import "MWCommon.h"

@interface StaffDetails : UIViewController<MWPhotoBrowserDelegate,UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,weak)IBOutlet UILabel *nameLabel;
@property(nonatomic,weak)IBOutlet UITextView *descTV;
@property(nonatomic,weak)IBOutlet UILabel *designationLabel;
@property(nonatomic,weak)IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIView *productsView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic)IBOutlet UITableView *staffDetailsTV;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property(nonatomic,weak)IBOutlet UITextView *hoursLabel;
-(IBAction)ShowTImings:(id)sender;

@property(nonatomic,weak)IBOutlet UITableViewCell *portfolio;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descTVHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productsViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeight;
@property(nonatomic,retain) StaffObj *staffObj;
-(IBAction)share:(id)sender;
@property(nonatomic,weak)IBOutlet UIImageView *product1, *product2, *product3, *product4;
@property (nonatomic, weak) IBOutlet UILabel *productLabel1, *productLabel2, *productLabel3, *productLabel4;
-(IBAction)porfolioAction:(id)sender;


@end
