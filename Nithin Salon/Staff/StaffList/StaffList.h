//
//  StaffList.h
//  Nithin Salon
//
//  Created by Webappclouds on 07/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//
//1. staff details screen completed
//2. portfolio screen completed
//3. working on production image description

#import <UIKit/UIKit.h>
#import "ModuleObj.h"
@interface StaffList : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *staffTableView;
@property (nonatomic,retain) NSMutableArray *data;
@property(nonatomic,assign)id reqAppoitmnetDelegate;
@property (nonatomic, retain) ModuleObj *moduleObj;
@property(nonatomic,retain)IBOutlet UIView *tableBGView;
@property(nonatomic,retain)NSString *screenNameStr;
@property (nonatomic, retain)IBOutlet UISearchBar *searchBar;
@end
