//
//  FAQ.m
//  Visage
//
//  Created by Nithin Reddy on 02/12/12.
//
//

#import "FAQ.h"
#import "Constants.h"
#import "AppDelegate.h"
@interface FAQ ()

@end

@implementation FAQ
{
    AppDelegate *appDelegate;
}
@synthesize textView;

@synthesize faq;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
       self.title = @"FAQ";
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.textView.layer.borderWidth = 1.0f;
    self.textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.textView.layer.cornerRadius=5.0f;
    self.textView.editable = NO;
    
    
    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_GIFTCARD_DROPDOWN] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSMutableDictionary * arrStringResponse = (NSMutableDictionary *)responseArray;
            
            if ([arrStringResponse  count]==0) {
                [self showAlert:SALON_NAME MSG:@"FAQ information is not available" tag:0];
            }else {
                [self.textView setText:[[arrStringResponse objectForKey:@"faq"] stringByReplacingOccurrencesOfString:@"\\" withString:@""]];
            }
            
            
        }
    }];
    
    
    
    // Do any additional setup after loading the view from its nib.
    [textView setText:faq];
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
