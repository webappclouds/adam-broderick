//
//  LoyalityRegisterVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 16/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "LoyalityRegisterVC.h"
#import <SHSPhoneComponent/SHSPhoneNumberFormatter+UserConfig.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "MacAddressHelper.h"
#import "LoyaltyMain.h"
NSString *const kName1 = @"name";
NSString *const kEmail1 = @"email";
NSString *const kPhone1 = @"phone";
NSString * const kValidationName1 = @"kName";
NSString *const kButton1 = @"button";
NSString * const kValidationEmail1 = @"email";
NSString *const kInteger = @"integer";

@interface LoyalityRegisterVC ()
{
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section;
    AppDelegate *appDelegate;
    XLFormRowDescriptor *nameRow,*emailRow,*phoneRow,*submitButtonRow,*cancelRow,*zipCodeRow;
}
@end

@implementation LoyalityRegisterVC

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

-(void)initializeForm
{
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@"Register"];
    formDescriptor.assignFirstResponderOnShow = NO;
    
    
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Please enter your Information"];
    
    [formDescriptor addFormSection:section];
    // Name
    nameRow = [XLFormRowDescriptor formRowDescriptorWithTag:kValidationName1 rowType:XLFormRowDescriptorTypeText title:@"Name"];
    [nameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    nameRow.height =44;
    nameRow.required =YES;
    [nameRow.cellConfigAtConfigure setObject:@"Enter name" forKey:@"textField.placeholder"];
    [section addFormRow:nameRow];
    
    
    
    
    
    // Email
    emailRow = [XLFormRowDescriptor formRowDescriptorWithTag:kEmail1 rowType:XLFormRowDescriptorTypeEmail title:@"Email"];
    // validate the email
    [emailRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    emailRow.required = YES;
    [emailRow addValidator:[XLFormValidator emailValidator]];
    emailRow.height=44;
    [emailRow.cellConfigAtConfigure setObject:@"Enter email" forKey:@"textField.placeholder"];
    
    [section addFormRow:emailRow];
    
    
    //Phone Numbers
    SHSPhoneNumberFormatter *formatter = [[SHSPhoneNumberFormatter alloc] init];
    [formatter setDefaultOutputPattern:@"(###) ###-####" imagePath:nil];
    phoneRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLFormRowDescriptorTypeInteger title:@"Phone"];
    phoneRow.valueFormatter = formatter;
    phoneRow.height=44;
    [phoneRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    phoneRow.useValueFormatterDuringInput = YES;
    [phoneRow.cellConfigAtConfigure setObject:@"Enter Phone number" forKey:@"textField.placeholder"];
    [section addFormRow:phoneRow];
    
    
    
    
    
    zipCodeRow = [XLFormRowDescriptor formRowDescriptorWithTag:kInteger rowType:XLFormRowDescriptorTypeInteger title:@"Zip"];
    [zipCodeRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    zipCodeRow.height =44;
    zipCodeRow.required =YES;
    [zipCodeRow.cellConfigAtConfigure setObject:@"Enter Zip" forKey:@"textField.placeholder"];
    [section addFormRow:zipCodeRow];
    
    
    
    //button
    section = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section];
    
    submitButtonRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton1 rowType:XLFormRowDescriptorTypeButton title:@"Submit"];
    [submitButtonRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [submitButtonRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    submitButtonRow.action.formSelector = @selector(submitAllData);
    [section addFormRow:submitButtonRow];
    
    section = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section];
    
    
//    cancelRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton1 rowType:XLFormRowDescriptorTypeButton title:@"Cancel"];
//    [cancelRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
//    [cancelRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
//    cancelRow.action.formSelector = @selector(dismiss);
//    [section addFormRow:cancelRow];
    
    
    self.form = formDescriptor;
    
}
-(void)submitAllData
{
    if([[nameRow displayTextValue] isEqualToString:@""]||[nameRow displayTextValue].length==0)
        [self showAlert:@"Name" MSG:@"Please enter your name" tag:0];
    else if([[emailRow displayTextValue] length]==0)
        [self showAlert:@"Email" MSG:@"Please enter your email" tag:0];
    else if (![UIViewController isValidEmail:[emailRow displayTextValue]])
        [self showAlert:@"Email" MSG:@"Please enter valid  email" tag:0];
    else if(([[phoneRow displayTextValue] length]==0))
        [self showAlert:@"Phone" MSG:@"Please enter your phone number" tag:0];
    else if(([[phoneRow displayTextValue] length]<14))
        [self showAlert:@"Phone" MSG:@"Please enter a valid  phone number" tag:0];
    else if([[zipCodeRow displayTextValue] isEqualToString:@""]||[zipCodeRow displayTextValue].length==0)
        [self showAlert:@"ZIP" MSG:@"Please enter your ZIP Code" tag:0];
    else{
        
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [paramDic setObject:appDelegate.salonId forKey:@"salon_id"];
        [paramDic setObject:[nameRow displayTextValue]?:@"" forKey:@"user_name"];
        [paramDic setObject:[emailRow displayTextValue]?:@"" forKey:@"user_email"];
        [paramDic setObject:[phoneRow displayTextValue]?:@"" forKey:@"user_phone"];
        [paramDic setObject:[zipCodeRow displayTextValue]?:@"" forKey:@"user_zip"];
        
        
        if(appDelegate.pushToken==nil || [appDelegate.pushToken length]==0)
            appDelegate.pushToken=[MacAddressHelper getMacAddress];
        else
         appDelegate.pushToken = [self loadSharedPreferenceValue:@"token"];
        [paramDic setObject:appDelegate.pushToken forKey:@"user_mac"];
        
        [appDelegate showHUD];
        
        NSString *url = [NSString stringWithFormat:@"%@",[appDelegate addSalonIdTo:URL_LOYALTY_REGISTER]];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSDictionary *dict = (NSDictionary*)responseArray;
                BOOL status = [[dict objectForKey:@"status"] boolValue];
                if(status)
                {
                    NSString *userId = [dict objectForKey:@"user_id"];
                    [self saveSharedPreferenceValue:userId KEY:@"loyaltyUserId"];
                    [self showAlert:@"Success" MSG:@"Your registration is successful!" tag:1];
                }
                else
                    [self showAlert:@"Error" MSG:@"Error occured. Please try again." tag:0];
                
            }
            
        }];
    }
    
    
}


-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1 || tag==4 || tag==5||tag==0001){
            LoyaltyMain *loyalityMain = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyaltyMain"];
        
                [self.navigationController pushViewController:loyalityMain animated:YES];
        }
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void) dismiss{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"loyalty"
     object:self];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title =@"Register";
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
