//
//  LoyaltyMain.h
//  Shear Impressions
//
//  Created by Nithin Reddy on 9/21/13.
//
//

#import <UIKit/UIKit.h>

@interface LoyaltyMain : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) IBOutlet UITextView *txtView;
@property (nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

-(IBAction)buttonClicked:(id)sender;

@property (nonatomic,assign) int goBack;

@end
