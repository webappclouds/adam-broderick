//
//  LoyaltyMain.m
//  Shear Impressions
//
//  Created by Nithin Reddy on 9/21/13.
//
//

#import "LoyaltyMain.h"
#import "Constants.h"
#import "LoyalityRegisterVC.h"
#import "LoyalityValidation.h"
#import "ViewController.h"
#import "AppDelegate.h"
@interface LoyaltyMain ()

@end

@implementation LoyaltyMain
{
    int punches;
    int maxPunches;
    AppDelegate *appDelegate;
}

@synthesize label;

@synthesize buttons;

@synthesize goBack;

@synthesize txtView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationItem.hidesBackButton =YES;
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Loyalty Program"];
    maxPunches=0;
    
    // self.screenName = @"Loyalty";
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Info" style:UIBarButtonItemStylePlain target:self action:@selector(moreInfo)];
    self.navigationItem.rightBarButtonItem = right;
    
    
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chevron.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = left;
    
    for(int i=0; i<[buttons count]; i++)
    {
        UIButton *button = [buttons objectAtIndex:i];
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
}

-(void)backAction{
    for(UIViewController *viewnav in self.navigationController.viewControllers){
        
        if([viewnav isKindOfClass:[ViewController class]]){
            [self.navigationController popToViewController:viewnav animated:YES];
        }
    }
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return maxPunches;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"HomeCell";
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *) [cell.contentView viewWithTag:1];
    
    int i =(int)indexPath.item;
    if(i<=punches-1)
        [imageView setImage:[UIImage imageNamed:@"card_icon.png"]];
    else
        [imageView setImage:[UIImage imageNamed:@"card_icon_bg.png"]];
    
    
    return cell;
}




-(void) moreInfo
{
    [self showAlert:@"Loyalty" MSG:@"Please tap the icon, and follow the directions."];
}

-(void) receiveNotif : (NSNotification *) notif
{
    goBack = 1;
}

-(void) viewDidAppear:(BOOL)animated
{
    
    NSString *userId =[self loadSharedPreferenceValue:@"loyaltyUserId"];
    if([userId length]==0)
    {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveNotif:)
                                                     name:@"loyalty"
                                                   object:nil];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
        LoyalityRegisterVC *loyalityRegVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyalityRegisterVC"];
        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:loyalityRegVC];
        
        [self.navigationController presentViewController:navCon animated:YES completion:nil];
    }
    else
    {
        [appDelegate showHUD];
        
        NSString *url = [NSString stringWithFormat:@"%@%@", [appDelegate addSalonIdTo:URL_LOYALTY_GET_POINTS], userId];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSDictionary *dict = (NSDictionary*)responseArray;
                BOOL status = [[dict objectForKey:@"status"] boolValue];
                if(!status)
                {
                    [self showAlert:@"Error" MSG:@"Error occured. Please try again later" tag:1];
                    return;
                }
                
                punches = [[dict objectForKey:@"punch"] intValue];
                
                maxPunches = [[dict objectForKey:@"punch_req"] intValue];
                
                if(maxPunches>20)
                    maxPunches = 20;
                
                //[label setText:[dict objectForKey:@"offer"]];
                
                [txtView setText:[dict objectForKey:@"offer"]];
                
                [self.collectionView reloadData];
                
                
                
            }
            
        }];
    }
    
}


-(void) refreshButtons
{
    
    //    UIButton *button = [self.buttons objectAtIndex:4];
    //    [button setHidden:YES];
    
    //    [[self.buttons objectAtIndex:3] setHidden:YES];
    
    for(int i=0; i<maxPunches ; i++)
    {
        UIButton *button = [self.buttons objectAtIndex:i];
        [button setHidden:NO];
        if(i<=punches-1)
            [button setImage:[UIImage imageNamed:@"card_icon.png"] forState:UIControlStateNormal];
        else
            [button setImage:[UIImage imageNamed:@"card_icon_bg.png"] forState:UIControlStateNormal];
    }
    for(int i=maxPunches; i<20; i++)
    {
        UIButton *button = [self.buttons objectAtIndex:i];
        [button setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    int index = (int)indexPath.item;
    if(index == punches){
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
        LoyalityValidation *loyalityvalid = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyalityValidation"];
        
        [self.navigationController pushViewController:loyalityvalid animated:YES];
    }
}


@end
