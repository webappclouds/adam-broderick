//
//  LoyalityValidation.m
//  Nithin Salon
//
//  Created by Webappclouds on 16/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "LoyalityValidation.h"
#import "Constants.h"
#import "AppDelegate.h"
NSString *const kName2 = @"name";
NSString *const kButton2 = @"button";
@interface LoyalityValidation ()
{
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section,*section1,*section2;
    XLFormRowDescriptor *authorizationRow,*submitButtonRow,*scanRow;
    AppDelegate *appDelegate;
}
@end

@implementation LoyalityValidation


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

-(void)initializeForm
{
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    formDescriptor.assignFirstResponderOnShow = NO;
    
    
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Please enter or scan the authorization code"];
    
    [formDescriptor addFormSection:section];
    
    [self addRows:@""];
    
    section1 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section1];
    
    submitButtonRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton2 rowType:XLFormRowDescriptorTypeButton title:@"Submit"];
    [submitButtonRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [submitButtonRow.cellConfig setObject:[UIColor blueColor] forKey:@"textLabel.textColor"];
    submitButtonRow.action.formSelector = @selector(submitAllData);
    [section1 addFormRow:submitButtonRow];
    
    
    
    
    self.form = formDescriptor;
    
}
-(void)scanningStart{
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    [self.navigationController presentViewController: reader
                                            animated: YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results){
        
        NSLog(@"data: %@",symbol.data);
        
        [formDescriptor removeFormRow:authorizationRow];
        [formDescriptor removeFormRow:scanRow];
        [self addRows:[NSString stringWithFormat:@"%@",symbol.data]];
        
    }
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    [reader dismissViewControllerAnimated: YES completion:nil];
}

-(void)addRows:(NSString *)scanText{
    authorizationRow = [XLFormRowDescriptor formRowDescriptorWithTag:kName2 rowType:XLFormRowDescriptorTypePassword title:@"Authorization #"];
    [authorizationRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    authorizationRow.height =44;
    //authorizationRow.required =YES;
    [authorizationRow setValue:scanText?:@""];
    [authorizationRow.cellConfigAtConfigure setObject:@"" forKey:@"textField.placeholder"];
    [section addFormRow:authorizationRow];
    
    scanRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton2 rowType:XLFormRowDescriptorTypeButton title:@"Click to scan code"];
    [scanRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [scanRow.cellConfig setObject:[UIColor blueColor] forKey:@"textLabel.textColor"];
    scanRow.action.formSelector = @selector(scanningStart);
    [section addFormRow:scanRow];
    
}

-(void)submitAllData{
    
    
    if([[authorizationRow displayTextValue] isEqualToString:@""]||![authorizationRow displayTextValue].length)
        [self showAlert:@"Authorization code" MSG:@"Please enter code" tag:0];
    else{
        
        NSString *userId = [self loadSharedPreferenceValue:@"loyaltyUserId"];
        NSString *url = [NSString stringWithFormat:@"%@%@/%@/", [appDelegate addSalonIdTo:[appDelegate addSalonIdTo:URL_LOYALTY_VALIDATE_CODE]], userId, [authorizationRow displayTextValue]];
        
        [appDelegate showHUD];
        
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSDictionary *dict = (NSDictionary*)responseArray;
                BOOL status = [[dict objectForKey:@"status"] boolValue];
                if(status)
                {
                    //        NSString *userId = [dict objectForKey:@"user_id"];
                    int punch_equal = [[dict objectForKey:@"punch_equal"] intValue];
                    if(punch_equal==1)
                        [self showAlert:@"Congratulations!" MSG:@"You will receive a redemption code to your email address." tag:2];
                    else
                        [self showAlert:@"Success" MSG:@"The punch has been approved!" tag:1];
                    
                }
                else
                    [self showAlert:@"Error" MSG:@"Please check the authorization code and try again." tag:0];
                
            }
            
        }];
    }
    
    
}


-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1){
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if(tag == 2){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    //[[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -60)
    //forBarMetrics:UIBarMetricsDefault];
    self.title=@"Validation";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
