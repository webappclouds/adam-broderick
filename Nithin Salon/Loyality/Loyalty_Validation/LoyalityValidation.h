//
//  LoyalityValidation.h
//  Nithin Salon
//
//  Created by Webappclouds on 16/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormViewController.h"
#import "XLForm.h"
#import "ZBarSDK.h"
#import "ZBarReaderView.h"
#import "ZBarReaderViewController.h"

@interface LoyalityValidation : XLFormViewController<ZBarReaderDelegate>

@end
