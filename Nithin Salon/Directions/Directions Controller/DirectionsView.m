//
//  DirectionsView.m
//  Template
//
//  Created by Nithin Reddy on 30/10/14.
//
//

#import "Constants.h"
#import "DirectionsView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "UIViewController+NRFunctions.h"
@interface DirectionsView ()
{
    AppDelegate *objAppdel;
}
@end

@implementation DirectionsView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.backBarButtonItem.title =@"";
    [self setTitle:@"Directions"];
    
    objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    GMSCameraPosition *camera =
    [GMSCameraPosition cameraWithLatitude:[objAppdel.spaLatitude doubleValue]
                                longitude:[objAppdel.spaLongitude doubleValue]
                                     zoom:14
                                  bearing:30
                             viewingAngle:40];
    
    GMSMapView *mapView = [GMSMapView mapWithFrame:self.mapContainer.frame camera:camera];
    mapView.userInteractionEnabled = YES;
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = camera.target;
    marker.snippet = SALON_NAME;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = mapView;
    
    self.view = mapView;
    
    UIBarButtonItem *directions = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"route.png"] style:UIBarButtonItemStylePlain target:self action:@selector(route)];
    
    
    self.navigationItem.rightBarButtonItem = directions;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)route
{
    [UIViewController loadDirections];
}

@end
