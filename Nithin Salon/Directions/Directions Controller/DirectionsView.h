//
//  DirectionsView.h
//  Template
//
//  Created by Nithin Reddy on 30/10/14.
//
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface DirectionsView : UIViewController

@property (nonatomic, retain) IBOutlet UIView *mapContainer;

@end
