//
//  MacAddressHelper.m
//  Castilian
//
//  Created by Nithin Reddy on 12/05/13.
//
//

#import "MacAddressHelper.h"
#import <sys/socket.h>
#import <sys/sysctl.h>
#import <net/if.h>
#import <net/if_dl.h>

@implementation MacAddressHelper

+ (NSString *)getMacAddress
{

    NSUUID *identifierForVendor =  [NSUUID UUID];
    
    NSString *str = [NSString stringWithFormat:@"%@%@", identifierForVendor.UUIDString, [[NSBundle mainBundle] bundleIdentifier]];
    return str;
    
   }

@end

