//
//  LastMinuteApptVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 26/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "LastMinuteApptVC.h"
#import "Constants.h"
#import "AvailableObj.h"
#import "AvailbleForm.h"
#import "AppDelegate.h"
@interface LastMinuteApptVC ()
{
    AvailableObj *availbledObj;
    AppDelegate *appDelegate;
    
}
@end

@implementation LastMinuteApptVC
@synthesize dataArray;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
}
-(void)viewWillAppear:(BOOL)animated{
    dataArray = [[NSMutableArray alloc] init];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [self setTitle:@"Last Minute Appts"];
    
    [self serviceCall];
}
-(void)serviceCall{
    [appDelegate showHUD];
    NSString *moduleID =    [self loadSharedPreferenceValue:@"Last_min_appointments"];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",[appDelegate addSalonIdTo:URL_LAST_MIN_APPTS],moduleID];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlStr HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSMutableDictionary * arrStringResponse=(NSMutableDictionary *)responseArray;
            [self serviceResponse:arrStringResponse];
            
        }
            }];
}

-(void) serviceResponse:(NSMutableDictionary *)dict
{
    
    BOOL status = [[dict objectForKey:@"msg"] boolValue];
    if(!status)
        [self showAlert:@"Error" MSG:@"Error occured. Please try again later." tag:1];
    else
    {
        NSArray *array = [dict objectForKey:@"appt_list"];
        if([array count]==0)
        {
            [self.lastMinTV reloadData];
            [self showAlert:@"None" MSG:@"There are no last minute appointments at this time." tag:1];
            
            return;
        }
        
        for(NSDictionary *dict in array)
        {
            AvailableObj *obj = [[AvailableObj alloc] init];
            obj.avail_appts_id = [dict objectForKey:@"avail_appts_id"];
            obj.apptDate = [dict objectForKey:@"date"];
            obj.apptTime = [dict objectForKey:@"time"];
            obj.service = [dict objectForKey:@"service"];
            obj.with = [dict objectForKey:@"with"];
            obj.notes = [dict objectForKey:@"notes"];
            [dataArray addObject:obj];
            
        }
    }
    [self.lastMinTV reloadData];

}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view dataArray source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    AvailableObj *obj = [dataArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"Open Sans" size:16];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Open Sans" size:14];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ with %@", obj.service, obj.with];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", obj.apptDate, obj.apptTime];
    
    return cell;
}


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.lastMinTV deselectRowAtIndexPath:indexPath animated:YES];
    availbledObj = [dataArray objectAtIndex:indexPath.row];
    NSString *titleStr = [NSString stringWithFormat:@"%@ with %@", availbledObj.service, availbledObj.with];
    
    NSString *notesStr = @"";
    if(availbledObj.notes!=nil && [availbledObj.notes length]>0)
        notesStr = [NSString stringWithFormat:@"\nNotes: %@", availbledObj.notes];
    
    NSString *dateTimeStr = [NSString stringWithFormat:@"%@ %@", availbledObj.apptDate, availbledObj.apptTime];
    NSString *msgStr = [NSString stringWithFormat:@"Service: %@\nTime: %@%@", availbledObj.service, dateTimeStr, notesStr];
    
    [self showAlert:titleStr message:msgStr];
}
-(void)showAlert:(NSString *)title message:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Book" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        
        AvailbleForm *availForm = [self.storyboard instantiateViewControllerWithIdentifier:@"AvailbleForm"];
        availForm.availObj = availbledObj;
        [self.navigationController pushViewController:availForm animated:YES];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
