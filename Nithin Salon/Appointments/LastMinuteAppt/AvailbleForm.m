//
//  AvailbleForm.m
//  Nithin Salon
//
//  Created by Webappclouds on 27/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "AvailbleForm.h"
#import "Constants.h"
#import "AppDelegate.h"
#import <SHSPhoneComponent/SHSPhoneNumberFormatter+UserConfig.h>
@interface AvailbleForm ()
{
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section1,*section2;
    XLFormRowDescriptor *nameRow,*emailRow,*phoneRow,*submitBtnRow;
    AppDelegate *appDelegate;
    
}
@end

@implementation AvailbleForm

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title =@"Book Appt";
    [self initializeForm];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initializeForm
{
    
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    formDescriptor.assignFirstResponderOnShow = NO;
    
    // section1---> name row, email Row, phone row
    section1 = [XLFormSectionDescriptor formSectionWithTitle:@"Enter your information"];
    [formDescriptor addFormSection:section1];
    
    
    nameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Name" rowType:XLFormRowDescriptorTypeText title:@"Name"];
    nameRow.height =44;
     [nameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [nameRow.cellConfigAtConfigure setObject:@"Enter name" forKey:@"textField.placeholder"];
    [section1 addFormRow:nameRow];
    
    emailRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Amount" rowType:XLFormRowDescriptorTypeText title:@"Email Address"];
    emailRow.height =44;
     [emailRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [emailRow.cellConfigAtConfigure setObject:@"Enter email" forKey:@"textField.placeholder"];
    [section1 addFormRow:emailRow];
    
    SHSPhoneNumberFormatter *formatter = [[SHSPhoneNumberFormatter alloc] init];
    [formatter setDefaultOutputPattern:@"(###) ###-####" imagePath:nil];
    phoneRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Phone" rowType:XLFormRowDescriptorTypeInteger title:@"Phone"];
    phoneRow.height =44;
    phoneRow.valueFormatter = formatter;
    phoneRow.useValueFormatterDuringInput = YES;
     [phoneRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [phoneRow.cellConfigAtConfigure setObject:@"Enter phone" forKey:@"textField.placeholder"];
    [section1 addFormRow:phoneRow];
    
    section2 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section2];
    submitBtnRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Submit"];
    [submitBtnRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [submitBtnRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    submitBtnRow.action.formSelector = @selector(submitAllData);
    [section2 addFormRow:submitBtnRow];
    
     nameRow.value =[self loadSharedPreferenceValue:@"lastMinName"];
     emailRow.value =[self loadSharedPreferenceValue:@"lastMinEmail"];
     phoneRow.value =[self loadSharedPreferenceValue:@"lastMinPhone"];
    

    
    self.form = formDescriptor;
    
}
-(void)submitAllData{
    
    
    
    NSString *nameStr = [nameRow displayTextValue]?:@"";
    NSString *emailStr =[emailRow displayTextValue]?:@"";
    NSString *phoneStr =[phoneRow displayTextValue]?:@"";
    
    if([nameStr length]==0)
        [self showAlert:@"Error" MSG:@"Please enter your name" tag:0];
    else if([emailStr length]==0)
        [self showAlert:@"Error" MSG:@"Please enter your email address" tag:0];
    else if(![UIViewController isValidEmail:emailStr])
        [self showAlert:@"Error" MSG:@"Please enter valid email address" tag:0];
    else if([phoneStr length]==0)
        [self showAlert:@"Error" MSG:@"Please enter your phone number" tag:0];
    else if([phoneStr length]<14)
        [self showAlert:@"Error" MSG:@"Please enter a valid phone number" tag:0];
    else{
//        [self setName:nameStr];
//        [self setEmail:emailStr];
//        [self setPhone:phoneStr];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:nameStr forKey:@"name"];
        [dict setObject:phoneStr forKey:@"phone"];
        [dict setObject:emailStr forKey:@"email"];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@/%@", [appDelegate addSalonIdTo:URL_LAST_MIN_APPT_ADD],appDelegate.lastMinsMOdel.moduleId, _availObj.avail_appts_id];
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlStr HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSDictionary *dict1 = (NSDictionary *)responseArray;
                
                BOOL status = [[dict1 objectForKey:@"status"] boolValue];
                if(status){
                    [self saveSharedPreferenceValue:nameStr KEY:@"lastMinName"];
                     [self saveSharedPreferenceValue:phoneStr KEY:@"lastMinPhone"];
                     [self saveSharedPreferenceValue:emailStr KEY:@"lastMinEmail"];
                    [self showAlert:@"Thank you!" MSG:@"We will contact you to confirm your booking. " tag:2 ];
                }
                else
                    [self showAlert:SALON_NAME MSG:@"Error occured. Please try again later."];
                
            }
        }];
        
    
    }

    
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==2)
            [self.navigationController popToRootViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}



@end
