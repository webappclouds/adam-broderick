//
//  DatePickerController.m
//  Nithin Salon
//
//  Created by Webappclouds on 06/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "DatePickerController.h"
#import "Constants.h"
@interface DatePickerController ()
{
    NSString *changedDateString,*dateString;
}
@end

@implementation DatePickerController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Choose Date and TIme";
    self.datePicker.minuteInterval = 15;
    [self.datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    NSDate *date =[NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd, yyyy hh:mm a"];
    dateString = [dateFormatter stringFromDate:date];
    // Do any additional setup after loading the view.
}

-(void)datePickerValueChanged:(id)sender
{
    NSDateFormatter *dateFormat=[NSDateFormatter new];
    dateFormat.dateFormat=@"dd/MM/yyyy";
    changedDateString=[dateFormat stringFromDate:[sender date]];
    NSDate *date =[sender date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd, yyyy hh:mm a"];
    dateString = [dateFormatter stringFromDate:date];
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)done:(id)sender {
    [self.delegate getDateAndTime:dateString];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
