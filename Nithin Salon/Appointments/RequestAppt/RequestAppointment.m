
//
//  RequestAppointment.m
//  Nithin Salon
//
//  Created by Nithin Reddy on 30/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//


#import "XLForm.h"
#import "RequestAppointment.h"
#import <SHSPhoneComponent/SHSPhoneNumberFormatter+UserConfig.h>
#import "CustomImageCellTableViewCell.h"
#import "StaffList.h"
#import "SpecialsObj.h"
#import "ServicesViewController.h"
#import "UIViewController+NRFunctions.h"
#import "AppDelegate.h"
#import "RecommendedTimingsVC.h"
#import "MacAddressHelper.h"
#import "Globals.h"
#import "NSString+Encoding.h"
NSString *const kName = @"name";
NSString *const kEmail = @"email";
NSString *const kLabelTF =@"labelTF";
NSString *const kPhone = @"phone";
NSString *const kDateTime = @"dateTime";
NSString *const kDateTimeInline = @"dateTimeInline";
static NSString *const kCustomImage =@"kCustomImage";
NSString *const kButtonWithStoryboardId = @"buttonWithStoryboardId";
NSString *const kHobbies = @"hobbies";
NSString * const kValidationName = @"kName";
NSString *const khiderow = @"tag1";
NSString *const kImage = @"image";
NSString *const kButton = @"button";
NSString * const kValidationEmail = @"email";
NSString * kAccessoryViewNotes = @"notes";
NSString *kCustomButton =@"CustomButton";
@interface RequestAppointment ()
{
    XLFormRowDescriptor * nameRow,*emailRow,*phoneRow,*dateTimeRow,*imageRow,*row,*serviceRow,*serviceProviderRow,*notesRow;
    XLFormRowDescriptor* hobbyRow,* submitButtonRow;
    NSString *selectionTypeStr;
    NSDateFormatter *dateFormat;
    XLFormRowDescriptor * buttonWithStoryboardId ;
    
    XLFormSectionDescriptor *imageSection,*dateAndTimingsSection;
    
    XLFormDescriptor * formDescriptor,*formDescriptor1 ;
    XLFormSectionDescriptor * section,*serviceProviderSection,*serviceSection;
    ;
    int i;
    NSMutableArray *controllerArray,*globalServiceName,*globalServiceProviderArray;
    AppDelegate *appDelegate;
    BOOL isBool;
    NSMutableArray *arrData;
}
@end

@implementation RequestAppointment
@synthesize state,serviceNameStr,compareServiceStr;
-(void)selectedString:(NSString*)string{
    buttonWithStoryboardId.title =string;
}
-(void)initializeForm
{
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    i=0;
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd, yyyy"];
    
    NSLog(@"date: %@",[NSDate date]);
    
    NSDateFormatter *displayingFormatter = [NSDateFormatter new];
    [displayingFormatter setDateFormat:@"MMM dd, yyyy HH:mm a"];
    NSString *display = [displayingFormatter stringFromDate:[NSDate date]];


    NSDate *todayDate = (NSDate*)display;
    
    
    
    
    
    
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@"Text Fields"];
    
    
    formDescriptor.assignFirstResponderOnShow = NO;
    
    formDescriptor1 = [XLFormDescriptor formDescriptorWithTitle:@"Text"];
    
    
    formDescriptor1.assignFirstResponderOnShow = NO;
    
    
    
    
    if(!appDelegate.imageLinkURL.length)
        appDelegate.globalButtonNameStr = @"Upload image";
    
    section = [XLFormSectionDescriptor formSectionWithTitle:@""];
    
    [formDescriptor addFormSection:section];
    // Name
    nameRow = [XLFormRowDescriptor formRowDescriptorWithTag:kValidationName rowType:XLFormRowDescriptorTypeText title:@"Name"];
    [nameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    nameRow.height =44;
    nameRow.required =YES;
    [nameRow.cellConfigAtConfigure setObject:@"Enter name" forKey:@"textField.placeholder"];
    
    
    [section addFormRow:nameRow];
    
    
    
    
    
    // Email
    emailRow = [XLFormRowDescriptor formRowDescriptorWithTag:kEmail rowType:XLFormRowDescriptorTypeEmail title:@"Email"];
    // validate the email
    [emailRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    emailRow.required = YES;
    [emailRow addValidator:[XLFormValidator emailValidator]];
    emailRow.height=44;
    [emailRow.cellConfigAtConfigure setObject:@"Enter email" forKey:@"textField.placeholder"];
    
    [section addFormRow:emailRow];
    
    
    //Phone Numbers
    SHSPhoneNumberFormatter *formatter = [[SHSPhoneNumberFormatter alloc] init];
    [formatter setDefaultOutputPattern:@"(###) ###-####" imagePath:nil];
    phoneRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLFormRowDescriptorTypeInteger title:@"Phone"];
    phoneRow.valueFormatter = formatter;
    phoneRow.height=44;
    [phoneRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    phoneRow.useValueFormatterDuringInput = YES;
    [phoneRow.cellConfigAtConfigure setObject:@"Enter Phone number" forKey:@"textField.placeholder"];
    [section addFormRow:phoneRow];
    
    
    // DateTime
    
    if(state == 1 || state == 2){
    dateAndTimingsSection = [XLFormSectionDescriptor formSectionWithTitle:@"SELECT THE DATE AND TIME"];
    [formDescriptor addFormSection:dateAndTimingsSection];
        
        NSDateFormatter *dateAndTimeDateFormatter = [[NSDateFormatter alloc] init];
        [dateAndTimeDateFormatter setDateFormat:@"MM-dd-yyyy hh:mm aa"];

         [dateTimeRow.cellConfigAtConfigure setValue:dateAndTimeDateFormatter forKey:@"dateFormatter"];

    dateTimeRow = [XLFormRowDescriptor formRowDescriptorWithTag:kDateTime rowType:XLFormRowDescriptorTypeDateTime title:@"Date Time"];
    dateTimeRow.height =44;
        NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:[NSDate date]];
    dateTimeRow.value = tomorrow;
    [dateTimeRow.cellConfigAtConfigure setObject:tomorrow forKey:@"minimumDate"];
        [dateTimeRow.cellConfigAtConfigure setObject:[NSNumber numberWithInt:5] forKey:@"minuteInterval"];

//        dateTimeRow.cellConfigAtConfigure.setObject(5, forKey: "minuteInterval");
    //[dateTimeRow.cellConfigAtConfigure setObject:[NSDate dateWithTimeIntervalSinceNow:(60*60*24*3)] forKey:@"maximumDate"];
   // [dateTimeRow.cellConfigAtConfigure setValue:df forKey:@"dateFormatter"];
    [dateAndTimingsSection addFormRow:dateTimeRow];
    }
    else{
        dateAndTimingsSection = [XLFormSectionDescriptor formSectionWithTitle:@"Choose recommended timings"];
        [formDescriptor addFormSection:dateAndTimingsSection];
        
        
        NSString *apptDatesWithProviders = [_fromPendingApptDict objectForKey:@"recommendations"];
        
        
        if(state==6)
            apptDatesWithProviders = [_fromPendingApptDict objectForKey:@"response"];
        
        if([apptDatesWithProviders length]>2)
        {
            if([apptDatesWithProviders characterAtIndex:[apptDatesWithProviders length]-1] == ';')
                apptDatesWithProviders= [apptDatesWithProviders substringToIndex:[apptDatesWithProviders length]-1];
            
            NSArray *datesArr = [apptDatesWithProviders componentsSeparatedByString: @";"];
            
            arrData = [[NSMutableArray alloc] init];
            for(int i=0;i<[datesArr count];i++)
            {
                NSString *str = [datesArr objectAtIndex:i];
               XLFormRowDescriptor *recommendedTimings = [XLFormRowDescriptor formRowDescriptorWithTag:@"checkMark" rowType:XLFormRowDescriptorTypeBooleanCheck title:str];
                [dateAndTimingsSection addFormRow:recommendedTimings];
               
            }
        
//            recommendedTimings.selectorOptions = arrData;
//            recommendedTimings.value = [arrData objectAtIndex:0];
//            [section addFormRow:recommendedTimings];
//            
//            recommendedTimings.action.formSelector = @selector(recommendedTimings);

        
    }
    }
    
    //selection type
    serviceSection = [XLFormSectionDescriptor formSectionWithTitle:@"OTHER INFORMATION"];
    [formDescriptor addFormSection:serviceSection];
    serviceRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton rowType:XLFormRowDescriptorTypeButton title:@"Requested Service"];
    [serviceRow.cellConfig setObject:[UIFont boldSystemFontOfSize:16] forKey:@"textLabel.font"];
    [serviceRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    if(state==1||state==2)
    serviceRow.action.formSelector = @selector(openServices);
    [serviceSection addFormRow:serviceRow];
    
    serviceProviderSection = [XLFormSectionDescriptor formSectionWithTitle:@""];
    
    serviceProviderRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton rowType:XLFormRowDescriptorTypeButton title:@"Requested Service Providers"];
    if(state==1||state==2)
   serviceProviderRow.action.formSelector = @selector(moveToStaffListView);
    [serviceProviderRow.cellConfig setObject:[UIFont boldSystemFontOfSize:16] forKey:@"textLabel.font"];
    [serviceProviderRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    [serviceProviderSection addFormRow:serviceProviderRow];
    [formDescriptor addFormSection:serviceProviderSection];
    if(_specialObj)
       [self getService:_specialObj.title?:@""];
     if(_staffObj)
        [self getServiceProvider:_staffObj.name?:@""];
    
    section = [XLFormSectionDescriptor formSectionWithTitle:@"ADDITIONAL INFORMATION"];
    [formDescriptor addFormSection:section];
    notesRow = [XLFormRowDescriptor formRowDescriptorWithTag:kAccessoryViewNotes rowType:XLFormRowDescriptorTypeTextView title:@""];
    notesRow.height=100;
    [notesRow.cellConfigAtConfigure setObject:@"Enter notes (optional)" forKey:@"textView.placeholder"];
    [section addFormRow:notesRow];
    
    
    
    //image
    
    if(_fromPendingApptDict){
        appDelegate.imageLinkURL = [NSString stringWithFormat:@"%@",[_fromPendingApptDict objectForKey:@"image"]];
    }
    imageSection = [XLFormSectionDescriptor formSectionWithTitle:@"YOU CAN UPLOAD AN IMAGE OF A STYLE YOU LIKE (OPTIONAL) OR YOUR IMAGE"];
    appDelegate.globalButtonNameStr =@"Upload Image";
    [formDescriptor addFormSection:imageSection];
    imageRow = [XLFormRowDescriptor formRowDescriptorWithTag:kCustomImage rowType:XLFormRowDescriptorTypeFullImage title:@"FullImage"];
    imageRow.height = 40;
    [imageRow.cellConfig setObject:[UIFont boldSystemFontOfSize:16] forKey:@"textLabel.font"];
    [imageSection addFormRow:imageRow];
    
    
    //button
    section = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section];
    
   
    
    if(state==1)//noraml
    {

      submitButtonRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton rowType:XLFormRowDescriptorTypeButton title:@"Create Appointment"];
        submitButtonRow.action.formSelector = @selector(didTouchButton:);
        [section addFormRow:submitButtonRow];

    }
    
    else if(state ==2){
        
        
         submitButtonRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton rowType:XLFormRowDescriptorTypeButton title:@"Update appointment"];
        [section addFormRow:submitButtonRow];
        
         submitButtonRow.action.formSelector = @selector(didTouchButton:);

        XLFormRowDescriptor  *cancelButtonRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton rowType:XLFormRowDescriptorTypeButton title:@"Cancel"];
        [cancelButtonRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
        [cancelButtonRow.cellConfig setObject:[UIColor redColor] forKey:@"textLabel.textColor"];
        cancelButtonRow.action.formSelector = @selector(cancelAppt);
        [section addFormRow:cancelButtonRow];
        [submitButtonRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
        [submitButtonRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
        


    }
    
    else if(state == 5){
        
        if(state !=6){
              submitButtonRow = [XLFormRowDescriptor formRowDescriptorWithTag:kButton rowType:XLFormRowDescriptorTypeButton title:@"Respond"];
        [section addFormRow:submitButtonRow];
        submitButtonRow.action.formSelector = @selector(recommendation);
        
        [submitButtonRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
        [submitButtonRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
        }
    }
    
    
    if(state !=1){
        
        nameRow.value =[_fromPendingApptDict objectForKey:@"user_name"];
               emailRow.value = [_fromPendingApptDict objectForKey:@"user_email"];
        phoneRow.value = [_fromPendingApptDict objectForKey:@"user_phone"];
        if([[_fromPendingApptDict objectForKey:@"image"] length]){
            
            [self updateFormRow:imageRow height:100];
            [self reloadFormRow:imageRow];

        }
           NSString *serviceStr = [_fromPendingApptDict objectForKey:@"user_service"];
        if(serviceStr.length){
            NSArray *arr = [serviceStr componentsSeparatedByString:@","];
            for(NSString *serviceStr in arr)
            [self getService:serviceStr];
        }
    
    NSString *serviceProvideStr  = [_fromPendingApptDict objectForKey:@"service_providers"];
        if(serviceProvideStr.length){
            NSArray *arr = [serviceProvideStr componentsSeparatedByString:@","];
            for(NSString *serviceProvideStr in arr)
            [self getServiceProvider:serviceProvideStr];
        }
        
        
        notesRow.value = [_fromPendingApptDict objectForKey:@"user_info"];
        
        NSString *apptDate = [_fromPendingApptDict objectForKey:@"user_appoinment_date"];
        NSDateFormatter *inFormat = [[NSDateFormatter alloc] init];
        [inFormat setDateFormat:@"MM/dd/yyyy hh:mm aa"];

        dateTimeRow.value =[inFormat dateFromString:apptDate];
        
        
    }
   
     [submitButtonRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    
    nameRow.value = [Globals getName];
    phoneRow.value = [Globals getPhone];
    emailRow.value = [Globals getEmail];
    
    
    self.form = formDescriptor;
    
    
    if(state == 5){
        imageRow.disabled =@YES;
        notesRow.disabled = @YES;
    }
    if(state== 6){
        self.form.disabled = YES;
        //[self.tableView endEditing:YES];
        //[self.tableView reloadData];
    }
}

-(void)cancelAppt{
    [appDelegate showHUD];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SALON_NAME message:@"Are you sure you want to cancel your appointment?" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    NSMutableDictionary  *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        
        [paramDic setObject:@"Delete" forKey:@"action"];
        [paramDic setObject:[_fromPendingApptDict objectForKey:@"appointment_id"] forKey:@"appointment_id"];
        
        NSDateFormatter *formatter;
        NSString        *dateString;
        
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        
        dateString = [formatter stringFromDate:[NSDate date]];
        
        [paramDic setObject:dateString forKey:@"currentDate"];
        [paramDic setObject:[_fromPendingApptDict objectForKey:@"user_appoinment_date"] forKey:@"appdate"];
        NSString *str = [NSString stringWithFormat:@"%@%@",[appDelegate addSalonIdTo:URL_RESERVATION],appDelegate.appoitmentsModel.moduleId];
        [self requestAppoitmentServicecall:paramDic urlString:str];

        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
     [appDelegate hideHUD];
}


-(void)recommendedTimings{
    
    RecommendedTimingsVC *recVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RecommendedTimingsVC"];
    recVC.globalArray = arrData;
    [self.navigationController pushViewController:recVC animated:YES];
}
#pragma mark Module request service call
-(void)getModules
{
    self.servicesArray = [NSMutableArray new];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:[appDelegate addSalonIdTo:URL_MODULES_LIST] HEADERS:Nil GETPARAMS:Nil POSTPARAMS:Nil COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
         //srikanth change
            responseDict = [responseDict objectForKey:@"modules"];
            NSArray *servicesArray = [responseDict objectForKey:@"Menu"];
            for(NSDictionary *dict in servicesArray)
            {
                ModuleObj *moduleObj = [[ModuleObj alloc] init];
                moduleObj.moduleId = [dict objectForKey:@"module_id"];
                moduleObj.moduleName = [dict objectForKey:@"module_name"];
                [self.servicesArray addObject:moduleObj];
                moduleObj = nil;
            }
            
            
        }
    }];
}

-(void) openServices
{
    int count = (int)[self.servicesArray count];
    if(count==0)
    {
        [self showAlert:@"Services" MSG:@"There are no services currently."];
        return;
    }
    else if(count==1)
        [self invokeServicesModule:[self.servicesArray objectAtIndex:0]];
    else
    {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Services" andMessage:@"Please select one"];
        for(ModuleObj *moduleObj in self.servicesArray)
            [alertView addButtonWithTitle:moduleObj.moduleName
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [self invokeServicesModule:moduleObj];
                                  }];
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                              }];
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        [alertView show];
    }
}

-(void) invokeServicesModule : (ModuleObj *) moduleObj
{
    appDelegate.buttonHideValue =@"YES";
    ServicesViewController *services = [self.storyboard instantiateViewControllerWithIdentifier:@"ServicesViewController"];
    [services setModuleObj:moduleObj];
    services.serviceDelegate=self;
    [self.navigationController pushViewController:services animated:YES];
}

-(void)moveToStaffListView{
    StaffList *staffListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StaffList"];
    staffListVC.screenNameStr =@"FromReqAppt";
    staffListVC.reqAppoitmnetDelegate=self;
   // staffListVC.moduleObj = _staffObjModel;
    [self.navigationController pushViewController:staffListVC animated:YES];
    
}
-(void)navigateNextView{
    StaffList *staffListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StaffList"];
    staffListVC.screenNameStr =@"FromReqAppt";
    staffListVC.reqAppoitmnetDelegate=self;
    [self.navigationController pushViewController:staffListVC animated:YES];
}
-(void)didTouchButton:(XLFormRowDescriptor *)sender{
    //[self validateForm];
    
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate showHUD];

    NSArray* datesArray = [appDelegate.blockedDays componentsSeparatedByString:@","];
    if(state==1 || state==2)   //Reservation Add/Edit
    {
        
    isBool=YES;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dayName = [dateFormatter stringFromDate:[dateTimeRow value]];
    
    for (NSString *someString in datesArray) {
        if ([someString containsString:dayName ]) {
            NSString *message=[NSString stringWithFormat:@"The Salon is closed on %@. Please select a different day.",appDelegate.blockedDays];
            [self showAlert:SALON_NAME MSG:message tag:0];
            [appDelegate hideHUD];

            isBool=NO;
            break;
        }
    }
    
        if(isBool == NO)
        {
            [appDelegate hideHUD];

            return;
        }
    
    NSString *serviceStr,*serviceProvideStr;
    
    for(XLFormRowDescriptor *serviceRowStr in serviceSection.formRows)
    {
        if(!serviceStr.length){
            if(![serviceRowStr.title isEqualToString:@"Requested Service"])
                serviceStr =[NSString stringWithFormat:@"%@",serviceRowStr.title];
        }
        else{
            serviceStr = [serviceStr stringByAppendingFormat:@",%@",serviceRowStr.title];
        }
    }
        
        serviceStr = [serviceStr stringByReplacingOccurrencesOfString:@"&" withString:@"~"] ;
    
    for(XLFormRowDescriptor *serviceRowStr in serviceProviderSection.formRows)
    {
        if(!serviceProvideStr.length){
            if(![serviceRowStr.title isEqualToString:@"Requested Service Providers"])
                serviceProvideStr =[NSString stringWithFormat:@"%@",serviceRowStr.title];
        }
        else{
            
            serviceProvideStr = [serviceProvideStr stringByAppendingFormat:@",%@",serviceRowStr.title];
        }
    }
    
        serviceProvideStr = [serviceProvideStr stringByReplacingOccurrencesOfString:@"&" withString:@"~"] ;
    
    
    if([[nameRow displayTextValue] length]==0){
        [self showAlert:@"Name" MSG:@"Please enter your name" tag:0];
        [appDelegate hideHUD];

        return;
    }
    else if([[emailRow displayTextValue] length]==0||![UIViewController isValidEmail:[emailRow displayTextValue]]){
        [self showAlert:@"Email" MSG:@"Please enter a valid email address" tag:0];
        [appDelegate hideHUD];

        return;
    }
    
    else if(([[phoneRow displayTextValue] length]<14)){
        [self showAlert:@"Phone number" MSG:@"Please enter a valid phone number" tag:0];
        [appDelegate hideHUD];

        return;
    }
    
   
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [paramDic setObject:[nameRow displayTextValue]?:@"" forKey:@"name"];
    [paramDic setObject:[emailRow displayTextValue]?:@"" forKey:@"email"];
    [paramDic setObject:[phoneRow  displayTextValue]?:@"" forKey:@"phone"];
    [paramDic setObject:serviceStr?:@"" forKey:@"service"];
    
        if(state == 1 || state == 2){
        
    NSDateFormatter *inFormat = [[NSDateFormatter alloc] init];
    [inFormat setDateFormat:@"MM/dd/yyyy hh:mm aa"];
    NSString *strSelectedDate = [inFormat stringFromDate:[dateTimeRow value]];
    [paramDic setObject:strSelectedDate?:@"" forKey:@"appdate"];
        }
        else{
            for(XLFormRowDescriptor *serviceRowStr in dateAndTimingsSection.formRows)
            {
                if(!serviceProvideStr.length){
                    if(![serviceRowStr.title isEqualToString:@"Requested Service Providers"])
                        serviceProvideStr =[NSString stringWithFormat:@"%@",serviceRowStr.title];
                }
                else{
                    serviceProvideStr = [serviceStr stringByAppendingFormat:@",%@",serviceRowStr.title];
                }
            }

        }
        
        
    [paramDic setObject:[notesRow displayTextValue]?:@"" forKey:@"info"];
    [paramDic setObject:serviceProvideStr?:@"" forKey:@"service_providers"];
    
        if(appDelegate.pushToken==nil || [appDelegate.pushToken length]==0){
        appDelegate.pushToken=[MacAddressHelper getMacAddress];
            //[Globals saveSharedPreferenceValue:appDelegate.pushToken?:@"" KEY:@"token"];
        }
    
    [paramDic setObject:appDelegate.pushToken forKey:@"udid"];//appDelegate.pushToken
    [paramDic setObject:@"Apple" forKey:@"user_device_type"];
    
        NSData *userImageData;
        if(appDelegate.imageLinkURL.length)
       userImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",URL_IMAGE_UPLOAD,appDelegate.imageLinkURL]]];
        else
            userImageData =UIImageJPEGRepresentation([imageRow value],1.0);
        if((state == 1 || state == 2)&& userImageData.length!=0)
    {
        NSString *str = [[userImageData base64Encoding ]stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        [paramDic setObject:str?:@"" forKey:@"image"];
    }
    
    if (state == 2) {
        [paramDic setObject:@"Edit" forKey:@"action"];
        [paramDic setObject:[_fromPendingApptDict objectForKey:@"appointment_id"] forKey:@"appointment_id"];
        
    }else{
        [paramDic setObject:@"Add" forKey:@"action"];
        [paramDic setObject:@"" forKey:@"appointment_id"];
    }
        NSString *str = [NSString stringWithFormat:@"%@%@",[appDelegate addSalonIdTo:URL_RESERVATION],appDelegate.appoitmentsModel.moduleId];

        [self requestAppoitmentServicecall:paramDic urlString:str];
        }
}




-(void)requestAppoitmentServicecall:(NSDictionary *)paramDict urlString:(NSString*)urlString{
    
    [Globals setName:[nameRow displayTextValue]?:@""];
    [Globals setEmail:[emailRow displayTextValue]?:@""];
    [Globals setPhone:[phoneRow displayTextValue]?:@""];
    
    
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            
            
            NSDictionary *dict = (NSDictionary*)responseArray;
            NSString *strResponse = [NSString stringWithFormat:@"%@",[dict objectForKey:@"msg"]];
            if([strResponse isEqualToString:@"Data Inserted"])
                [self showAlert:SALON_NAME MSG:@"We will get back to you regarding your appointment on our next business day." tag:0001];
            
            else if([strResponse isEqualToString:@"Data Updated"])
                [self showAlert:SALON_NAME MSG:@"We will get back to you regarding your appointment on our next business day." tag:0001];
            
            else if([strResponse isEqualToString:@"Staus Canceled"])
                [self showAlert:SALON_NAME MSG:@"Your appointment has been cancelled" tag:0001 ];
            
            else if([strResponse isEqualToString:@"Status Can Not Be Cancelled"])
                [self showAlert:SALON_NAME MSG:@"Please call us to cancel or change this appointment" tag:0001 ];
            
            else  if([strResponse isEqualToString:@"true"]||[strResponse isEqualToString:@"1"])
                [self showAlert:SALON_NAME MSG:@"Thank you. All recommended times and dates are not guaranteed unless we send you a confirmation." tag:0001];
            
            else if([strResponse isEqualToString:@"Salon Closed"]){
                //                NSString *message=[NSString stringWithFormat:@"The Salon is closed on %@. Please select a different day.",self.mo.blockedDays];
                //                [self showAlert:SALON_NAME MSG:message];
            }
            else
                [self showAlert:SALON_NAME MSG:@"An error occured during the reservation. Please try again later."];
        }
        
    }
     
     ];
    
}


-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==2 || tag==4 || tag==5||tag==0001)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma marl Add Service Provider Method
-(void)getServiceProvider:(NSString*)ProviderName{
    
     if(![appDelegate.globalServiceComparisionStr isEqualToString:@"YES"]){
    if(!globalServiceProviderArray)
        globalServiceProviderArray = [[NSMutableArray alloc]init];
    
    if(globalServiceProviderArray.count){
        if([globalServiceProviderArray containsObject:ProviderName]){
            
            [self showAlert:@"Alert" MSG:@"You have already selected this staff, Please add an another staff." tag:0];
            return;
            
        }
        else{
            [globalServiceProviderArray addObject:ProviderName];
        }
    }
    else{
        [globalServiceProviderArray addObject:ProviderName];
    }
    
    
     }
     else{
         if(!globalServiceProviderArray)
             globalServiceProviderArray = [[NSMutableArray alloc]init];
            [globalServiceProviderArray addObject:ProviderName];
     }
    
    XLFormRowDescriptor *serviceProviderRow1 = [XLFormRowDescriptor formRowDescriptorWithTag:kButton rowType:XLFormRowDescriptorTypeButton title:ProviderName];
    if(state==1||state==2)
    serviceProviderRow1.action.formSelector = @selector(removeView:);
    [serviceProviderRow1.cellConfig setObject:[UIColor blueColor] forKey:@"textLabel.textColor"];
    [serviceProviderSection addFormRow:serviceProviderRow1];
    
    
    
    for(XLFormRowDescriptor *row1 in serviceProviderSection.formRows)
    {
        
        NSLog(@"row value : %@ %@",row1.title,[row1 displayTextValue]);
    }
}

#pragma mark REmove Service Provider Method
-(void)removeView:(XLFormRowDescriptor*)roww{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Request Service Providers"
                                  message:@"Do you want  delete a service Provider?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Delete"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             NSString *titleStr = [roww title];
                              [globalServiceProviderArray removeObject:titleStr];
                             [formDescriptor removeFormRow:roww];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)formRowDescriptorValueHasChanged:(XLFormRowDescriptor *)formRow oldValue:(id)oldValue newValue:(id)newValue{
    
    
    if([formRow.title isEqualToString:@"Service Providers"]){
       
        selectionTypeStr = newValue;
    }
    if([formRow.title isEqualToString:@"FullImage"]){
        [self updateFormRow:imageRow height:100];
        [self reloadFormRow:imageRow];
    }
    if([formRow.tag isEqualToString:kDateTime])
    {
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSArray* datesArray = [appDelegate.blockedDays componentsSeparatedByString:@","];
        if(state==1 || state==2)   //Reservation Add/Edit
        {
            
            isBool=YES;
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"EEEE"];
            NSString *dayName = [dateFormatter stringFromDate:[dateTimeRow value]];
            
            for (NSString *someString in datesArray) {
                if ([someString containsString:dayName ]) {
                    NSString *message=[NSString stringWithFormat:@"The Salon is closed on %@. Please select a different day.",appDelegate.blockedDays];
                    [self showAlert:SALON_NAME MSG:message tag:0];
                    isBool=NO;
                    break;
                }
            }
        
        }    }
    if([formRow.tag isEqualToString:@"checkMark"]){
    
        NSLog(@"%d", [formRow.value boolValue]);
    }
    
}


#pragma mark - actions

-(void)validateForm{
    NSArray * array = [self formValidationErrors];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XLFormValidationStatus * validationStatus = [[obj userInfo] objectForKey:XLValidationStatusErrorKey];
        if ([validationStatus.rowDescriptor.tag isEqualToString:kValidationEmail]){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert title" message:@"Invalid EMail" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
}


#pragma mark - Helper

-(void)animateCell:(UITableViewCell *)cell
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values =  @[ @0, @20, @-20, @10, @0];
    animation.keyTimes = @[@0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1];
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.additive = YES;
    [cell.layer addAnimation:animation forKey:@"shake"];
}



-(void)viewWillDisappear:(BOOL)animated{
    
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
       
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                   NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                   NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                   };
    self.title=@"Request Appt";
    
     [self initializeForm];
    [self getModules];
    
}

-(void)recommendation{
    
    NSString *str =@"";
    for(XLFormRowDescriptor *serviceRowStr in dateAndTimingsSection.formRows)
    {
        if([serviceRowStr.value boolValue] == 1){
            
            if(!str.length)
                str =[NSString stringWithFormat:@"%@",serviceRowStr.title];
        else{
            str = [str stringByAppendingFormat:@";%@",serviceRowStr.title];
        }
        
    }
    
}
    if(str.length == 0){
        [self showAlert:@"Error" MSG:@"Please select atleast one appointment date time to proceed"  tag:0];
        return;
    }
    else{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
     [paramDic setObject:str forKey:@"response"];
    
        NSString *url = [NSString stringWithFormat:@"%@%@/", URL_RESERVATION_UPDATE_RECOMMENDED, [_fromPendingApptDict objectForKey:@"appointment_id"]?:@""];
        
        [self requestAppoitmentServicecall:paramDic urlString:url];
    }
}
#pragma mark Add services view Method
-(void)getService:(NSString *)serviceName
{
    if(![appDelegate.globalServiceComparisionStr isEqualToString:@"YES"]){
    if(!globalServiceName)
        globalServiceName = [[NSMutableArray alloc]init];
    
    if(globalServiceName.count){
        if([globalServiceName containsObject:serviceName]){
            
            [self showAlert:@"Alert" MSG:@"You have already selected this service, Please add an another service." tag:0];
            return;
            
        }
        else{
             [globalServiceName addObject:serviceName];
        }
    }
    else{
          [globalServiceName addObject:serviceName];
    }
   
    }
    else{
        if(!globalServiceName)
            globalServiceName = [[NSMutableArray alloc]init];
        [globalServiceName addObject:serviceName];

    }
    
    
    XLFormRowDescriptor *serviceProviderRow1 = [XLFormRowDescriptor formRowDescriptorWithTag:kButton rowType:XLFormRowDescriptorTypeButton title:serviceName];
    if(state==1||state==2)
    serviceProviderRow1.action.formSelector = @selector(removeServiceView:);
    [serviceProviderRow1.cellConfig setObject:[UIColor blueColor] forKey:@"textLabel.textColor"];
    [serviceSection addFormRow:serviceProviderRow1];

}


#pragma mark Remove services view Method
-(void)removeServiceView:(XLFormRowDescriptor*)roww{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Request Service"
                                  message:@"Do you want  delete a service?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Delete"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             NSString *titleStr = [roww title];
                             [globalServiceName removeObject:titleStr];
                             [formDescriptor removeFormRow:roww];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)formRowDescriptorPredicateHasChanged:(XLFormRowDescriptor *)formRow
                                   oldValue:(id)oldValue
                                   newValue:(id)newValue
                              predicateType:(XLPredicateType)predicateType
{
    
}


-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
    
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                 
                                                                                 (CFStringRef)self,
                                                                                 
                                                                                 NULL,
                                                                                 
                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                 
                                                                                 CFStringConvertNSStringEncodingToEncoding(encoding)));
    
}
@end



