//
//  CustomImageCellTableViewCell.m
//  XLForm
//
//  Created by Webappclouds on 27/02/17.
//  Copyright © 2017 Xmartlabs. All rights reserved.
//
#import "CustomImageCellTableViewCell.h"
#import "Constants.h"
@interface CustomImageCellTableViewCell() <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIPopoverController *popoverController;
    UIImagePickerController *imagePickerController;
    UIAlertController *alertController;
    BOOL configureVal;
}
@end

@implementation CustomImageCellTableViewCell
@synthesize nameStr;

+(void)load
{
   
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([CustomImageCellTableViewCell class]) forKey:XLFormRowDescriptorTypeFullImage];



}
-(void)configure
{
    [super configure];
   
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
   
      self.lbl.hidden =NO;
    configureVal = NO;
    if(appDelegate.imageLinkURL.length>=7)
        self.lbl.hidden =YES;
       self.lbl.text =appDelegate.globalButtonNameStr;
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",URL_IMAGE_UPLOAD,appDelegate.imageLinkURL];
    //appDelegate.imageLinkURL = urlString;
    if(appDelegate.imageLinkURL.length){
     [self.imgView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
        
    }
   
    self.selectionStyle = UITableViewCellEditingStyleNone;
}



- (void)formDescriptorCellDidSelectedWithFormController:(XLFormViewController *)controller
{
    alertController = [UIAlertController alertControllerWithTitle: self.rowDescriptor.title
                                                          message: nil
                                                   preferredStyle: UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Choose From Library", nil)
                                                        style: UIAlertActionStyleDefault
                                                      handler: ^(UIAlertAction * _Nonnull action) {
                                                          [self openImage:UIImagePickerControllerSourceTypePhotoLibrary];
                                                      }]];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Take Photo", nil)
                                                            style: UIAlertActionStyleDefault
                                                          handler: ^(UIAlertAction * _Nonnull action) {
                                                              [self openImage:UIImagePickerControllerSourceTypeCamera];
                                                          }]];
    }
    
    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel", nil)
                                                        style: UIAlertActionStyleCancel
                                                      handler: nil]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        alertController.modalPresentationStyle = UIModalPresentationPopover;
        alertController.popoverPresentationController.sourceView = self.contentView;
        alertController.popoverPresentationController.sourceRect = self.contentView.bounds;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.formViewController presentViewController:alertController animated: true completion: nil];
    });
}

- (void)openImage:(UIImagePickerControllerSourceType)source
{
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = source;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
        [popoverController presentPopoverFromRect: self.contentView.frame
                                           inView: self.formViewController.view
                         permittedArrowDirections: UIPopoverArrowDirectionAny
                                         animated: YES];
    } else {
        [self.formViewController presentViewController: imagePickerController
                                              animated: YES
                                            completion: nil];
    }
}

#pragma mark -  UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    
    if (editedImage) {
        [self chooseImage:editedImage];
    } else {
        [self chooseImage:originalImage];
    }
       if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if (popoverController && popoverController.isPopoverVisible) {
            [popoverController dismissPopoverAnimated: YES];
        }
    } else {
        [self.formViewController dismissViewControllerAnimated: YES completion: nil];
    }
}

- (void)chooseImage:(UIImage *)image
{
    appDelegate.imageLinkURL =@"";
    if(self.imgView.image)
        self.imgView.image = nil;
    self.imgView.image = image;
    self.rowDescriptor.value = image;
    self.lbl.hidden =YES;
    configureVal=YES;

   }
#pragma mark - Events

//+(CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor
//{
//    return 100;
//}


@end
