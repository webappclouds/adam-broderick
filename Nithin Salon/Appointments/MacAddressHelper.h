//
//  MacAddressHelper.h
//  Castilian
//
//  Created by Nithin Reddy on 12/05/13.
//
//

#import <Foundation/Foundation.h>

@interface MacAddressHelper : NSObject

+ (NSString *)getMacAddress;

@end
