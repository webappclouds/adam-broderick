//
//  ProfileViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 02/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ProfileViewController.h"
#import "Constants.h"
#import "GlobalSettings.h"
#import "AppDelegate.h"
#import "Globals.h"
@interface ProfileViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIBarButtonItem *right;
    AppDelegate *appDelegate;
}
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Profile";
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
   right = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(Edit)];
    self.navigationItem.rightBarButtonItem = right;
    
    self.profileImage.layer.borderWidth=1.0;
    self.profileImage.layer.borderColor=[UIColor colorWithRed:90.0/255.0 green:209.0/255 blue:247.0/255.0 alpha:1].CGColor;
    
    
    
    self.emailTF.layer.borderWidth=1.0;
    self.emailTF.layer.borderColor=[UIColor clearColor].CGColor;
    self.nameTF.layer.borderWidth=1.0;
    self.nameTF.layer.borderColor=[UIColor clearColor].CGColor;
    self.mobileTF.layer.borderWidth=1.0;
    self.mobileTF.layer.borderColor=[UIColor clearColor].CGColor;

    
    [_saveButton setHidden:YES];
    self.view1.backgroundColor = [UIColor clearColor];
    self.view2.backgroundColor = [UIColor clearColor];
    self.view3.backgroundColor = [UIColor clearColor];
    self.view4.backgroundColor = [UIColor clearColor];

    
    self.profileImage.userInteractionEnabled = NO;
    
    //self.profileImage.layer.borderColor = [self colorFromHexString:@"#5AD1F7"];
    
    
    self.nameTF.userInteractionEnabled = NO;
    self.emailTF.userInteractionEnabled = NO;
    self.mobileTF.userInteractionEnabled = NO;
    self.lastNameTF.userInteractionEnabled = NO;
    
    
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.profileImage addGestureRecognizer:tapRecognizer];
    [self getProfileInfo];
    
    // Do any additional setup after loading the view.
}

-(void)getProfileInfo{
    
     NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:0];
     [dict setObject:[self loadSharedPreferenceValue:@"slc_id"] forKey:@"slc_id"];
    [dict setObject:appDelegate.salonId forKey:@"salon_id"];

    [appDelegate showHUD];
    //GET_PROFILE_INFO

[ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:GET_PROFILE_INFO HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
    [appDelegate hideHUD];
    if(error)
        [self showAlert:@"Error" MSG:error.localizedDescription];
    else
    {
       
        NSDictionary *dictJson = (NSDictionary *)responseArray;
        
        NSLog(@"Profile info : %@",dictJson);
        NSString *status = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"status"]];
        if ([status isEqualToString:@"1"]){
            
            self.fullNameTF.text = [NSString stringWithFormat:@"%@ %@",[dictJson objectForKey:@"firstname"],[dictJson objectForKey:@"lastname"]];
            
            self.lastNameTF.text = [dictJson objectForKey:@"lastname"]?:@"";
             self.nameTF.text = [dictJson objectForKey:@"firstname"]?:@"";
            
//            self.lastNameTF.hidden =YES;
//            self.nameTF.hidden = YES;
            self.emailTF.text = [dictJson objectForKey:@"email"]?:@"";
            self.mobileTF.text = [dictJson objectForKey:@"phone"]?:@"";
            NSString *imageUrl = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"image"]];

            [self.profileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"profile_picture.png"]];
                   }
        else
            [self showAlert:@"Error" MSG:[dictJson objectForKey:@"message"]];
        
    }
    
}];


}

-(void)Edit{
    self.fullNameTF.hidden = YES;
    self.nameTF.hidden = NO;
    self.lastNameTF.hidden = NO;
    self.profileImage.userInteractionEnabled = YES;
    self.nameTF.userInteractionEnabled = YES;
    self.emailTF.userInteractionEnabled = YES;
    self.mobileTF.userInteractionEnabled = YES;
    self.lastNameTF.userInteractionEnabled = YES;

    
    self.profileImage.layer.borderWidth=1.0;
    self.profileImage.layer.borderColor=[UIColor colorWithRed:90.0/255.0 green:209.0/255 blue:247.0/255.0 alpha:1].CGColor;
    
    self.emailTF.layer.borderWidth=1.0;
    self.emailTF.layer.borderColor=[UIColor colorWithRed:90.0/255.0 green:209.0/255 blue:247.0/255.0 alpha:1].CGColor;
    self.nameTF.layer.borderWidth=1.0;
    self.nameTF.layer.borderColor=[UIColor colorWithRed:90.0/255.0 green:209.0/255 blue:247.0/255.0 alpha:1].CGColor;
    self.mobileTF.layer.borderWidth=1.0;
    self.mobileTF.layer.borderColor=[UIColor colorWithRed:90.0/255.0 green:209.0/255 blue:247.0/255.0 alpha:1].CGColor;
    self.saveButton.layer.borderWidth=1.0;
    self.saveButton.layer.borderColor=[UIColor colorWithRed:90.0/255.0 green:209.0/255 blue:247.0/255.0 alpha:1].CGColor;
    
    [_saveButton setHidden:NO];
    right.enabled = NO;
    right.title =@"";
    
    _nameTF.placeholder =@"First name";
    _lastNameTF.placeholder = @"Last name";
    _emailTF.placeholder =@"Email";
    _mobileTF.placeholder =@"Mobile number";
    
}
- (void)tapAction:(UITapGestureRecognizer *)tap
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Profile Picture" message:@"Please choose an option" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setEditing:YES];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Capture from Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [self presentViewController:alertController animated:YES completion:Nil];
    
    }

#pragma mark -
#pragma mark <UIImagePickerControllerDelegate> implementation

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    if(!image)
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    UIImage *image1 = [UIImage imageWithData:imageData];
    
    [self.profileImage setImage:image1];
  }

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveClick:(id)sender {
    
    
    
    if([_nameTF.text length]==0)
        [self showAlert:@"First Name" MSG:@"Please enter  first name" TAG:0];
//    else if([_lastNameTF.text length]==0)
//        [self showAlert:@"Last Name" MSG:@"Please enter  last name" TAG:0];
    else if(![UIViewController isValidEmail:_emailTF.text])
        [self showAlert:@"Email" MSG:@"Please enter a valid email address" TAG:0];
    else if ([_mobileTF.text length]==0) {
        [self showAlert:@"Mobile" MSG:@"Please enter mobile number" TAG:0];
    }
    else if ([_mobileTF.text  length] !=10)
        [self showAlert:@"Mobile" MSG:@"Mobile number should be 10 digits long." TAG:0];
    else{
         [_nameTF resignFirstResponder];
         [_lastNameTF resignFirstResponder];
         [_mobileTF resignFirstResponder];
         [_emailTF resignFirstResponder];
        
        
        
NSDictionary *params = @{@"slc_id":[self loadSharedPreferenceValue:@"slc_id"] , @"salon_id" : appDelegate.salonId,@"client_id":[self loadSharedPreferenceValue:@"clientid"],@"firstname":_nameTF.text?:@"",@"lastname":_lastNameTF.text?:@"",@"email":_emailTF.text?:@"",@"phone":_mobileTF.text?:@""};
    [appDelegate showHUD];
        
        NSLog(@"SAVE Params : %@",params);
    [[GlobalSettings sharedManager] uploadImageOverHttp:self.profileImage.image URL:UPDATE_PROFILE_URL HEADERS:Nil PARAMS:params COMPLETIONHANDLER:^(NSData *data, NSDictionary *responseDict, NSError *error) {
        [appDelegate hideHUD];
        
        NSLog(@"Profile update Response :%@",responseDict);
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            BOOL status = [[responseDict objectForKey:@"status"] boolValue];
            if(status)
            {
                
                [self showAlert:@"Success" MSG:@"Your profile has been updated."];
            }
            else
            {
                [self showAlert:@"Error" MSG:[responseDict objectForKey:@"message"]];
            }
        }
    }];
    }
}
-(void)showAlert : (NSString *) title MSG:(NSString *) message TAG:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
