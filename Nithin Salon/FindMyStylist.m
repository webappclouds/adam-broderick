//
//  ViewController.m
//  FindMyStylist
//
//  Created by Nithin Reddy on 09/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "FindMyStylist.h"
#import "FindBookVC.h"
@interface FindMyStylist ()

@end

@implementation FindMyStylist
{
    NSArray *array1, *array2, *array3, *array1Labels, *array2Labels, *array3Labels;
    int whichImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    array1 = @[@"very_short.png", @"medium.png", @"long.png", @"very_long.png"];
    array2 = @[@"straight.png", @"wavy.png", @"loose_curls.png", @"defined_curls.png", @"curls.png"];
    array3 = @[@"little_gray.png", @"mostly_gray.png", @"all_gray.png"];
    
    array1Labels = @[@"Very Short", @"Medium", @"Long", @"Very Long"];
    array2Labels = @[@"Straight", @"Wavy", @"Loose Curls", @"Defined Curls", @"Curls"];
    self.title = @"Find My Stylist";
    array3Labels = @[@"Little Gray", @"Mostly Gray", @"All Gray"];
    
    self.image1.layer.borderWidth = 1.0f;
    self.image1.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.image2.layer.borderWidth = 1.0f;
    self.image2.layer.borderColor = [UIColor lightGrayColor].CGColor;
    whichImage = 0;
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextScreen)];
    [nextButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = nextButton;
}

-(void)nextScreen{
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    FindBookVC *fbc  = [self.storyboard instantiateViewControllerWithIdentifier:@"FindBookVC"];
    [self.navigationController pushViewController:fbc animated:YES];
}


-(IBAction)image1Clicked:(id)sender
{
    whichImage = 0;
    [self imageClicked];
}

-(IBAction)image2Clicked:(id)sender
{
    whichImage = 1;
    [self imageClicked];
}

-(void) imageClicked
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Profile Picture" message:@"Please choose an option" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setEditing:YES];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Capture from Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;

        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [self presentViewController:alertController animated:YES completion:Nil];
}


#pragma mark -
#pragma mark <UIImagePickerControllerDelegate> implementation

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    if(!image)
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    UIImage *img = [UIImage imageWithData:imageData];
    
    if(whichImage==0)
        [self.image1 setImage:img];
    else
        [self.image2 setImage:img];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)sliderChanged:(UISlider *) slider
{
    NSInteger tag = [slider tag];
    int value = ceil(slider.value);
    if(value==-1)
        value = 0;
    if(tag==1)
    {
        [self.hairImage1 setImage:[UIImage imageNamed:[array1 objectAtIndex:value]]];
        [self.hairLabel1 setText:[array1Labels objectAtIndex:value]];
    }
    else if(tag==2)
    {
        [self.hairImage2 setImage:[UIImage imageNamed:[array2 objectAtIndex:value]]];
        [self.hairLabel2 setText:[array2Labels objectAtIndex:value]];
    }
    else
    {
        [self.hairImage3 setImage:[UIImage imageNamed:[array3 objectAtIndex:value]]];
        [self.hairLabel3 setText:[array3Labels objectAtIndex:value]];
    }
}

#pragma mark - FSCalendarDataSource

- (NSString *)calendar:(FSCalendar *)calendar titleForDate:(NSDate *)date
{
    return [self.gregorianCalendar isDateInToday:date] ? @"" : nil;
}


- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
#pragma mark - FSCalendarDelegate


- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
//    
//    selectedIndex=-1;
//    [self.timeTV setHidden:YES];
//    dateString=[dateFormatter2 stringFromDate:date];
//    _dateLabel.text = [NSString stringWithFormat:@"Date : %@",dateString];
//    
//    [self getList:dateString];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
