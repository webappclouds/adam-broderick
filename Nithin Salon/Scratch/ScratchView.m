//
//  ScratchView.m
//  Nithin Salon
//
//  Created by Webappclouds on 10/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ScratchView.h"
#import "MDScratchImageView.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface ScratchView ()<MDScratchImageViewDelegate>
{
    int status;
    MDScratchImageView *scratchImageView;
    AppDelegate *appDelegate;
}
@property (nonatomic, retain) NSString *randomCode;
@end

@implementation ScratchView

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title = @"Win";
    self.middleTextLabel.hidden = YES;
    UIImage *bluredImage = [UIImage imageNamed:@"scratch_off_image.png"];
    scratchImageView = [[MDScratchImageView alloc] initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    scratchImageView.delegate=self;
    [scratchImageView setImage:bluredImage radius:60];
    scratchImageView.image = bluredImage;
    [self.view addSubview:scratchImageView];
    [scratchImageView addSubview:self.claimButton];
    
    [self.middleTextLabel setText:@"Please try again tomorrow!"];
    self.middleTextLabel.hidden = NO;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:[self getPushToken] forKey:@"unique_device_id"];
    
    
    NSString *lotteryDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"lottery_date"];
    
    if(lotteryDate!=nil && [lotteryDate isEqualToString:[self dateFromDate]]){
        [self showAlert:@"Lottery" MSG:@"You have already attempted for the day. Please try again tomorrow." tag:2];
        
    }
    else{
    
        if(appDelegate.netAvailability == YES){
        [self lotteryServiceCall];
        }
        else{
            [self showAlert:@"Network Error" MSG:@"Couldn't connect to the server. Check your network connection." tag:2];

        }
   }
    
    
    
    
}
#pragma mark lottery service call
-(void)lotteryServiceCall{
    [appDelegate showHUD];
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:[appDelegate addSalonIdTo:URL_LOTTERY_CHECK] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(self.randomCode==nil || [self.randomCode length]==0)
            {
                if(responseDict.count){
                    
                    NSLog(@"Scratch :%@",responseDict);
                    
                    status = [[responseDict objectForKey:@"status"] intValue];
                    
                    if(status == -1){
                        self.textLabel.hidden =YES;
                        self.middleTextLabel.hidden =NO;
                        [ self.middleTextLabel setText:@"Please try again tomorrow!"];
                    }
                    else if(status==-6)
                    {
                        [self showAlert:@"Lottery" MSG:@"You have already attempted for the day. Please try again tomorrow." tag:2];
                        self.middleTextLabel.hidden =YES;

                    }
                    else if(status==1)
                    {
                        self.randomCode = [[NSString alloc] initWithString:[responseDict objectForKey:@"random_number"]];
                        [self.textLabel setText:[NSString stringWithFormat:@"Congratulations!! You won \n%@\nPlease click the Claim button below to claim your prize.", [responseDict objectForKey:@"gift"]]];
                        self.middleTextLabel.hidden =YES;

                        NSString *imgUrl = [responseDict objectForKey:@"image"];
                        if(imgUrl!=Nil && [imgUrl length]>0)
                        {
                            //[self.winningImage sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
                            [self.winningImage setImageWithURL:[NSURL URLWithString:imgUrl]];
                            [self.winningImage setContentMode:UIViewContentModeScaleAspectFit];
                        }
                    }
                    else if(status<=-2)
                    {
                        self.middleTextLabel.hidden =YES;

                        [self showAlert:@"ScratchOff" MSG:@"At this time there are no ScratchOff tickets available. Once we have a new contest, we will notify you." tag:2];
                    }
                    
                    
                    
                }
                
            }
            
        }
    }];
    
}

-(IBAction)redeem:(id)sender
{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Lottery"
                                                                              message:@"Please enter your name and email address to claim your gift."
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter your Name";
        textField.textColor = [UIColor blueColor];
        
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email Address";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.secureTextEntry = NO;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        UITextField * emailfiled = textfields[1];
        
        
        if([namefield.text length]==0 || [emailfiled.text length]==0){
            [self showAlert:@"Lottery" MSG:@"Please enter your name and email address" tag:0];
        }
        else
        {
            [self claimServiceCall:namefield.text email:emailfiled.text];
            
        }
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
#pragma mark claim Service call
-(void)claimServiceCall:(NSString*)name email:(NSString*)email{
    [appDelegate showHUD];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    
    [paramDic setObject:name forKey:@"name"];
    [paramDic setObject:email forKey:@"email"];
    [paramDic setObject:self.randomCode forKey:@"random_number"];
    [paramDic setObject:[self getPushToken] forKey:@"unique_device_id"];
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_LOTTERY_SUBMIT_WINNER] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if([[responseDict objectForKey:@"status"] boolValue]==1)
                [self showAlert:@"ScratchOff" MSG:[responseDict objectForKey:@"message"] tag:4];
            
            else
                [self showAlert:@"ScratchOff" MSG:[responseDict objectForKey:@"message"] tag:4];
            
            
            
        }
    }];
}
#pragma mark - MDScratchImageViewDelegate

- (void)mdScratchImageView:(MDScratchImageView *)scratchImageVieww didChangeMaskingProgress:(CGFloat)maskingProgress {
    
    
    [[NSUserDefaults standardUserDefaults] setObject:[self dateFromDate] forKey:@"lottery_date"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if(status==1 && maskingProgress>0.30)
    {
        self.claimButton.hidden = NO;
        
    }
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==2 || tag==4 || tag==5)
            [self.navigationController popToRootViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(NSString *) getPushToken
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     appDelegate.pushToken = [self loadSharedPreferenceValue:@"token"];
    NSString *pushToken = appDelegate.pushToken;
    if(pushToken==nil || [pushToken length]==0)
    {
        NSUUID *uid=[UIDevice currentDevice].identifierForVendor;
        NSString *str = [NSString stringWithFormat:@"%@%@", uid.UUIDString, [[NSBundle mainBundle] bundleIdentifier]];
        return str;
    }
    return pushToken;
}
-(NSString *) dateFromDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    return [dateFormat stringFromDate:[NSDate date]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
