//
//  EventDetailsViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 09/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "EventDetailsViewController.h"
#import <EventKit/EventKit.h>
#import "QuartzCore/QuartzCore.h"
#import "Constants.h"
#import "CustomIOSAlertView.h"
@interface EventDetailsViewController ()
{
    UIImageView*  imageView;
}
@end

@implementation EventDetailsViewController
@synthesize textView;
@synthesize dateLabel, date, desc;
@synthesize eventModel;
@synthesize nameLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-100.f, 0) forBarMetrics:UIBarMetricsDefault];
    
    if (eventModel.imgName.length==0)
    {
       _viewWithImage.hidden=YES;
        textView.hidden=NO;
        
    }
    else
    {
        _viewWithImage.hidden=NO;
        _viewWithImage.backgroundColor = [UIColor clearColor];
        textView.hidden=YES;
    
    }
    [self.eventImage sd_setImageWithURL:[NSURL URLWithString:eventModel.imgName] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(clickProductImage:)];
    [tapRecognizer setNumberOfTouchesRequired:1];
    [tapRecognizer setDelegate:self];
    
    self.eventImage.userInteractionEnabled = YES;
    [self.eventImage addGestureRecognizer:tapRecognizer];
    
    NSDateFormatter *dformatter = [[NSDateFormatter alloc] init];
    [dformatter setDateFormat:@"MMM dd, yyyy"];
    
    [dateLabel setText:[NSString stringWithFormat:@"%@, %@ to %@",[dformatter stringFromDate:eventModel.date],eventModel.sTime,eventModel.eTime]];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",eventModel.name]];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}
     ];
    
    [nameLabel setAttributedText:attributeString];
    
    [textView.layer setCornerRadius:10.0];
    [textView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [textView.layer setBorderWidth:1.0];
    

    [textView setFont:[UIFont systemFontOfSize:16]];
    [textView setText:eventModel.description];
    [textView setEditable:NO];
    
    
    
    [_eventDes.layer setCornerRadius:10.0];
    [_eventDes.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [_eventDes.layer setBorderWidth:1.0];
    
    [_eventDes setEditable:NO];
    [_eventDes setFont:[UIFont systemFontOfSize:16]];
    [_eventDes setText:eventModel.description];
    
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title = [NSString stringWithFormat:@"Event details"];
    // Do any additional setup after loading the view.
}

-(IBAction)addToCalendar:(id)sender
{
    
//    [self addEventOnCalendar];
////    [self checkForCalenders];
//
//    
//    return;
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    
    NSLog(@"Name : %@",eventModel.name);
    NSString *str = [NSString stringWithFormat:@"%@-%@",SALON_NAME,eventModel.name];
    event.title= str;
    NSDateFormatter *dFormatter = [[NSDateFormatter alloc]init];
    [dFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDateFormatter *dtFormatter = [[NSDateFormatter alloc] init];
    [dtFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];
    
    event.startDate = [dtFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", [dFormatter stringFromDate:eventModel.date], eventModel.sTime]];
    
    event.endDate= [dtFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", [dFormatter stringFromDate:eventModel.endDate], eventModel.eTime]];
    
    NSDate *remindDate = [event.startDate dateByAddingTimeInterval:-2*60*60];
    
    NSLog(@"Final date : %@",remindDate);
    NSArray *arrAlarm = [NSArray arrayWithObject:[EKAlarm alarmWithAbsoluteDate:remindDate]];
    event.alarms= arrAlarm;
    //    }
    
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent
                               completion:^(BOOL granted, NSError *error) {
                                   
                                   if (error == nil) {
                                       // Store the returned granted value.
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           if (granted) {
                                               [event setCalendar:[eventStore defaultCalendarForNewEvents]];
                                               [self storeToCalendar:event STORE:eventStore];
                                           } else {
                                               [self showAlert:@"Event" MSG:@"Please provide access to this application in Settings->Privacy ->Calendars" tag:0];
                                           }
                                       });
                                   }
                                   else{
                                       // In case of error, just log its description to the debugger.
                                       NSLog(@"%@", [error localizedDescription]);
                                   }
                                   // may return on background thread
                               }];
    
    
}

-(void) storeToCalendar : (EKEvent *) event STORE:(EKEventStore *)eventStore
{
//    //[self checkForCalenders];
//    return;
    
    
    NSError *err;
    BOOL isSuceess=[eventStore saveEvent:event span:EKSpanThisEvent error:&err];
    
    if(isSuceess){
        NSString *str = [NSString stringWithFormat:@"%@-%@",SALON_NAME,eventModel.name];

        [self showAlert:str MSG:@"This event has been added to your calendar - you will receive an alert two hours before the event" tag:100];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self showAlert:@"Event" MSG:[err description] tag:0];
    }
}

#pragma mark -
#pragma mark Add Event To Calendar
/*
 Adding Event to Calendar
 -----------------
 */

-(void)checkForCalenders
{
    NSMutableArray* cals =  [self getCalendars];
    
    if([cals count] > 0){
        
        UIAlertController *alert = [UIAlertController   alertControllerWithTitle:@"" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        for ( EKCalendar *cal in cals ) {
            UIAlertAction *calAction = [UIAlertAction actionWithTitle: cal.title style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                NSLog(@"You pressed button %@ ", cal.title);
//                [self addEventOnCalendar:cal];
            }];
            
            [alert addAction:calAction];
        }
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Calendar" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        NSLog(@"No hay calendarios");
    }

}

- (NSMutableArray*) getCalendars {
    
    NSMutableArray *res =[[NSMutableArray alloc] init];
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKEntityType type = EKEntityTypeEvent;
    NSArray *calendars = [eventStore calendarsForEntityType: type];
    
    for ( EKCalendar *cal in calendars )
    {
        if (cal.type == EKCalendarTypeCalDAV || cal.type == EKCalendarTypeLocal  ){
            NSLog(@"cal nombre:- %@ ", cal.title);
            [res addObject: cal];
        }
    }
    
    return res;
}


-(void)addEventOnCalendar//: (EKCalendar *) cal{
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            return;
        }
        EKEvent *event = [EKEvent eventWithEventStore:store];
//        event.title = @"Test";
        
//        NSDate *cDate = [NSDate date];
//        NSLog(@"Date :%@",cDate);
//        event.startDate = [cDate dateByAddingTimeInterval:((5*60*60) + (30 * 60) + 15)];
//        event.endDate = [cDate dateByAddingTimeInterval:((5*60*60) + (30 * 60) + 15)];
        
        NSString *str = [NSString stringWithFormat:@"%@-%@",SALON_NAME,eventModel.name];
        
        NSLog(@"Event Title : %@",str);
        event.title= str;
        NSDateFormatter *dFormatter = [[NSDateFormatter alloc]init];
        [dFormatter setDateFormat:@"MM-dd-yyyy"];
        NSDateFormatter *dtFormatter = [[NSDateFormatter alloc] init];
        [dtFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];
        
        NSLog(@" Date : %@",eventModel.date);
        NSLog(@"start Time : %@",eventModel.sTime);
        
        
        event.startDate = [dtFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", [dFormatter stringFromDate:eventModel.date], eventModel.sTime]];
        
        NSLog(@"start Date string  : %@",[NSString stringWithFormat:@"%@ %@", [dFormatter stringFromDate:eventModel.date], eventModel.sTime]);
        NSLog(@"start Date  : %@",[dtFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", [dFormatter stringFromDate:eventModel.date], eventModel.sTime]]);

        NSLog(@"\n");
        NSLog(@"End Time : %@",eventModel.eTime);

        event.endDate= [dtFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", [dFormatter stringFromDate:eventModel.date], eventModel.eTime]];
        
        NSLog(@"END Date string  : %@",[NSString stringWithFormat:@"%@ %@", [dFormatter stringFromDate:eventModel.date], eventModel.eTime]);
        NSLog(@"END Date  : %@",[dtFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", [dFormatter stringFromDate:eventModel.date], eventModel.eTime]]);

        NSDate *remindDate = [event.startDate dateByAddingTimeInterval:-2*60*60];
        

        
        event.calendar = [store defaultCalendarForNewEvents];
//        event.calendar = [store calendarWithIdentifier: cal.calendarIdentifier];
        
        NSError *err = nil;
       BOOL isCommit =  [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        if (err == nil)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:@"Event Successfully added" preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertController animated:YES completion:nil];


        }
        
        NSLog(@"Is Committed : %d",isCommit);
        
    }];
    
}

/*
 -----------------
 */
#pragma mark -
#pragma mark Others


-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==100)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)clickProductImage :(id) sender
{
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createPopupView]];
    
    // Modify the parameters
    //[alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    //    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
    //        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
    //        [alertView close];
    //    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imageView;
}

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}
- (UIView *)createPopupView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, self.view.frame.size.height-100)];
    [demoView setBackgroundColor:[UIColor whiteColor]];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-40, self.view.frame.size.height-120)];
      imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-40, self.view.frame.size.height-120)];
    
    scrollView.contentSize = CGSizeMake(imageView.frame.size.width , imageView.frame.size.height);
    scrollView.maximumZoomScale = 4;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    scrollView.minimumZoomScale = 1;
    scrollView.clipsToBounds = YES;
    scrollView.delegate = self;
    
    
    UIActivityIndicatorView *activityIndicator;
    [imageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
    activityIndicator.center = imageView.center;
    [activityIndicator startAnimating];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        imageView.image =self.eventImage.image;
        [activityIndicator stopAnimating];
        //[activityIndicator removeFromSuperview];
    });
    [scrollView addSubview:imageView];
    [demoView addSubview:scrollView];
    
    return demoView;
}

@end
