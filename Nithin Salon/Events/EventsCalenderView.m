//
//  EventsCalenderView.m
//  Nithin Salon
//
//  Created by Webappclouds on 08/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "EventsCalenderView.h"
#import "Constants.h"
#import "EventsModel.h"
#import "EventDetailsViewController.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
@interface EventsCalenderView() <FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance>{
    BOOL CalendarValue,calenderSelectValue;
    AppDelegate *appDelegate;
}
@property (weak  , nonatomic) IBOutlet FSCalendar *calendar;
@property (strong, nonatomic) NSDateFormatter *dateFormatter1;
@property (strong, nonatomic) NSDateFormatter *dateFormatter2;
@property (strong, nonatomic) NSArray<NSString *> *datesShouldNotBeSelected;
@property (strong, nonatomic) NSMutableArray *datesWithEvent,*tableArray;
@property(strong,nonatomic)NSMutableArray *globalDataArray;
@property (strong, nonatomic) NSCalendar *gregorianCalendar;

@end

@implementation EventsCalenderView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Events";
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSLocale *chinese = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    self.dateFormatter1 = [[NSDateFormatter alloc] init];
    self.dateFormatter1.locale = chinese;
    self.dateFormatter1.dateFormat = @"yyyy/MM/dd";
    
    self.dateFormatter2 = [[NSDateFormatter alloc] init];
    self.dateFormatter2.locale = chinese;
    self.dateFormatter2.dateFormat = @"yyyy-MM-dd";
    
    //adding right bar button item
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"view-list.png"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshPropertyList)];
    [self loadEvents];
    
}
-(void)refreshPropertyList{
    if(CalendarValue==NO){
        CalendarValue = YES;
        self.calenderHeight.constant=0;
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"calendar-1"]];
        
    }
    else{
        CalendarValue = NO;
        self.calenderHeight.constant=300;
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"view-list.png"]];
    }
    [self.eventListTV reloadData];
}
#pragma mark load events
-(void) loadEvents{
    [appDelegate showHUD];
    NSString *eventsModuleid=[self loadSharedPreferenceValue:@"EventsID"];
    self.datesWithEvent = [NSMutableArray new];
    self.globalDataArray = [NSMutableArray new];
    NSString *str = [NSString stringWithFormat:@"%@%@",[appDelegate addSalonIdTo:EVENTS_URL],eventsModuleid];
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:str HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:@"Error" MSG:@"No events found" tag:2];
                return;
            }
            
            NSLog(@"Events list : %@",responseArray);

            for(NSDictionary *dict in responseArray)
            {
                EventsModel *eventObj = [[EventsModel alloc] init];
                
                NSDateFormatter *fmt = [[NSDateFormatter alloc] init] ;
                [fmt setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [fmt dateFromString:[dict objectForKey:@"event_date"]];
                NSDate *endDate = [fmt dateFromString:[dict objectForKey:@"event_end_date"]];

                eventObj.date = date;
                eventObj.endDate = endDate;
                eventObj.name =[dict objectForKey:@"event_name"];
                eventObj.description=[dict objectForKey:@"event_description"];
                eventObj.eventId=[dict objectForKey:@"event_id"];
                eventObj.sTime=[dict objectForKey:@"event_st_time"];
                eventObj.eTime=[dict objectForKey:@"event_end_time"];
                eventObj.imgName = [dict objectForKey:@"event_image"];
                [self.datesWithEvent addObject:[dict objectForKey:@"event_date"]];
                [self.globalDataArray addObject:eventObj];
                
            }
            [self.calendar reloadData];
            CalendarValue =NO;
            [self.eventListTV reloadData];
            
            
        }
    }];
    
}
#pragma mark - FSCalendarDataSource

- (NSString *)calendar:(FSCalendar *)calendar titleForDate:(NSDate *)date
{
    return [self.gregorianCalendar isDateInToday:date] ? @"" : nil;
}


- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    if ([self.datesWithEvent containsObject:[self.dateFormatter2 stringFromDate:date]]) {
        return 1;
    }
    return 0;
}

//- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar
//{
//    return [self.dateFormatter1 dateFromString:@"2016/10/01"];
//}
//
//- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar
//{
//    return [self.dateFormatter1 dateFromString:@"2017/05/31"];
//}
#pragma mark - FSCalendarDelegate


- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    calenderSelectValue = YES;
    
    [self filterEvents:[self.dateFormatter2 stringFromDate:date]];
    
    if (monthPosition == FSCalendarMonthPositionNext || monthPosition == FSCalendarMonthPositionPrevious) {
        [calendar setCurrentPage:date animated:YES];
    }
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    NSLog(@"did change to page %@",[self.dateFormatter1 stringFromDate:calendar.currentPage]);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -

-(void)filterEvents:(NSString*)selectedDate
{
    self.tableArray =[NSMutableArray new];
    for(EventsModel *eventObjModel in self.globalDataArray){
        NSDate *eventDate= eventObjModel.date;
        NSDateFormatter *df =[[NSDateFormatter alloc]init];
        [df setDateFormat:@"yyyy-MM-dd"];
        
        NSString *eventStr = [NSString stringWithFormat:@"%@",[df stringFromDate:eventDate]];
        if([selectedDate isEqualToString:eventStr]){
            [self.tableArray addObject:eventObjModel];
        }
        
    }
    [self.eventListTV reloadData];
}


#pragma mark tableview Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(CalendarValue==YES){
        self.eventListTV.rowHeight=101;
        return  self.globalDataArray.count;
    }
    else{
        self.eventListTV.rowHeight=48;
        
        
        return self.tableArray.count;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier ;
    if(CalendarValue == NO){
        cellIdentifier = @"calendarCell";
    }
    else{
        cellIdentifier=@"listCell";
    }
    
    UITableViewCell *cell = (UITableViewCell *) [self.eventListTV dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableView.separatorStyle= UITableViewCellSeparatorStyleSingleLine;
    if(CalendarValue==NO){
       
            EventsModel *staffObj = [self.tableArray objectAtIndex:indexPath.row];
            cell.textLabel.font=[UIFont fontWithName:@"OpenSans" size:17];
            cell.textLabel.text = staffObj.name?:@"";
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:staffObj.imgName]];
        //}
//        else{
//            EventsModel *staffObj = [self.globalDataArray objectAtIndex:indexPath.row];
//            cell.textLabel.font=[UIFont fontWithName:@"OpenSans" size:17];
//            cell.textLabel.text = staffObj.name?:@"";
//            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:staffObj.imgName]];
//        }
    }
    else{
        if(self.globalDataArray.count){
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            EventsModel *staffObj = [self.globalDataArray objectAtIndex:indexPath.row];
            UILabel *namelabel = (UILabel *)[cell viewWithTag:30];
            namelabel.text = staffObj.name?:@"";
            
            UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:100];
            imv.layer.shadowColor = [UIColor darkGrayColor].CGColor;
            imv.layer.shadowOffset = CGSizeMake(0,0);
            imv.layer.shadowOpacity = 0.2;
            imv.layer.shadowRadius = 4.0;
            imv.layer.masksToBounds =NO;
            
            NSDateFormatter *monthFormatter = [NSDateFormatter new];
            [monthFormatter setDateFormat:@"MMM"];
            NSDateFormatter *dayFormatter = [NSDateFormatter new];
            [dayFormatter setDateFormat:@"dd"];
            UILabel *dateView = (UILabel*)[cell viewWithTag:10];
          
            dateView.text = [NSString stringWithFormat:@"%@",[[monthFormatter stringFromDate:staffObj.date] uppercaseString]];
        
            UILabel *mothView = (UILabel*)[cell viewWithTag:20];
            mothView.text = [NSString stringWithFormat:@"%@",[dayFormatter stringFromDate:staffObj.date]];
           

            //cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",staffObj.date]?:@"";
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    EventDetailsViewController *eventDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailsViewController"];
    if(CalendarValue==NO){
        if(self.tableArray.count>0 && calenderSelectValue == YES)
            eventDetail.eventModel = [self.tableArray objectAtIndex:indexPath.row];
        else
            eventDetail.eventModel = [self.globalDataArray objectAtIndex:indexPath.row];
        
    }
    else{
        eventDetail.eventModel = [self.globalDataArray objectAtIndex:indexPath.row];
    }
    
    [self.navigationController pushViewController:eventDetail animated:YES];
}

@end
