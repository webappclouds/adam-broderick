//
//  EventsModel.h
//  Nithin Salon
//
//  Created by Webappclouds on 08/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventsModel : NSObject

@property (nonatomic, retain) NSDate *date,*endDate;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *eventId;
@property (nonatomic, retain) NSString *sTime;
@property (nonatomic, retain) NSString *eTime;
@property(nonatomic, retain) NSString *imgName;
@end
