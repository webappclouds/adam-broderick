//
//  EventDetailsViewController.h
//  Nithin Salon
//
//  Created by Webappclouds on 09/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsModel.h"
@interface EventDetailsViewController : UIViewController<UIScrollViewDelegate>
@property (nonatomic ,retain) IBOutlet UITextView *textView;
@property (nonatomic ,retain) IBOutlet UILabel *nameLabel;
@property (nonatomic ,retain) IBOutlet UILabel *dateLabel;
-(IBAction)addToCalendar:(id)sender;
@property (nonatomic, retain)EventsModel *eventModel;
@property (nonatomic ,retain) NSString *date;
@property (nonatomic ,retain) NSString *desc;
@property (weak, nonatomic) IBOutlet UIView *eventView;
@property (weak, nonatomic) IBOutlet UIImageView *eventImage;
@property (weak, nonatomic) IBOutlet UIView *viewWithImage;
@property (weak, nonatomic) IBOutlet UITextView *eventDes;
@end
