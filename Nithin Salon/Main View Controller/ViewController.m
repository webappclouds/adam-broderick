//
//  ViewController.m
//  Nithin Salon
//
//  Created by Nithin Reddy on 18/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import "ServicesViewController.h"
#import "EventsCalenderView.h"
#import "ScratchView.h"
#import "LoyaltyMain.h"
#import "ReviewsList.h"
#import "GiftCardsMain.h"
#import "AboutUs.h"
#import "DeviceModel.h"
#import "LastMinuteApptVC.h"
#import "LoginVC.h"
#import "BeforeAfterVc.h"
#import "MessagesListVC.h"
#import "AppDelegate.h"
#import "LoginVC.h"
#import "MyAccountList.h"
#import "DirectionsView.h"
#import "GoogleReviewVC.h"
#import "OnlineBookindViewCon.h"
#import "BeaconNotification.h"
#import "ReferFriendVC.h"
#import "LoyalityRegisterVC.h"
#import "Reachability.h"
#import "MyGalleryViewController.h"
#import "OnlineServicesViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "BeveragesList.h"
#import "FindMyStylist.h"
#import "Globals.h"
#import "CategoriesViewController.h"
#import "ApptMapView.h"
#import "LocationViewController.h"
#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>
#import "ListViewController.h"

@interface ViewController ()<CLLocationManagerDelegate>
{
    AppDelegate *appDelegate;
    NSString *moduleName;
    CLLocationManager *locationManager;
    NSMutableDictionary *dynamicSocialDict;

    ListViewController *list;
    
    NSMutableArray *arrayOfLocations;
}
@end

@implementation ViewController
- (IBAction)rushHours:(id)sender {
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    AboutUs *aboutUS = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUs"];
    aboutUS.flag =CONST_HOURS;
    aboutUS.moduleObj =_staffModule;
    [self.navigationController pushViewController:aboutUS animated:YES];
}
- (IBAction)backToMultilocations:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)locationDropDown:(id)sender {
    [self addMultiLocaitonDropDown];
}

-(void)optionSelected:(NSNumber *)selectedOption{
    [list.view removeFromSuperview];
    
    [_locationButton setTitle:[arrayOfLocations objectAtIndex:selectedOption.intValue] forState:UIControlStateNormal];

    if(selectedOption.intValue == 0)
    {
        appDelegate.salonId = @"691";
    } else {
        appDelegate.salonId = @"692";

    }
    
    [self getModules];
}
-(void)addMultiLocaitonDropDown{
    
    
        NSLog(@"generalWorkShiftsDropDown");
        
    
//        GeneralInfoView *previousView = (GeneralInfoView*)[_scrollBgView viewWithTag:100];
    
        [list.view removeFromSuperview];
        list = [[ListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    
        [[list listArray] addObjectsFromArray:arrayOfLocations];
        [list setSuperController:self];
        
        float finalHeight;
        
        if([arrayOfLocations count] > 10)
        {
            finalHeight = 10.0 * 45.0;
        }
        else{
            finalHeight = [arrayOfLocations count] * 45.0;
        }
        NSLog(@"Final height :%f",finalHeight);
        [list.view setFrame:CGRectMake(_locationButton.frame.origin.x , _locationButton.frame.origin.y + _locationButton.frame.size.height + 10, _locationButton.frame.size.width, finalHeight)];
        
        NSLog(@"Final Frame :%@",NSStringFromCGRect(list.view.frame));
        
        list.view.layer.cornerRadius = 10;
        list.view.layer.masksToBounds = YES;
        
        list.view.layer.borderColor = [UIColor colorWithRed:60.0/256.0 green:29.0/256.0 blue:18.0/256.0 alpha:1.0].CGColor;
        list.view.layer.borderWidth = 2.0f;
    
        [self.view addSubview:list.view];
        [self.view bringSubviewToFront:list.view];
        

}
- (IBAction)rushMap:(id)sender {
    
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    DirectionsView *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"DirectionsView"];
    [self.navigationController pushViewController:scratch animated:YES];

}
- (IBAction)rushCall:(id)sender {
    
    NSString * model=[[UIDevice currentDevice] model];
    if ([model hasPrefix:@"iPhone"]) {
        [self showAlert:@"Call Us" MSG:[UIViewController formatPhone] tag:1];
        
    }else
        [self showAlert:@"Call" MSG:@"Calling feature is not available on this device." tag:0];

}
- (IBAction)rushShare:(id)sender {
    [self newShare];
}

- (IBAction)rushTwitter:(id)sender {
    [self loadWebView:@"TWITTER" URL:@"https://twitter.com/RUSHHairBeauty"];
}
- (IBAction)rushPinterest:(id)sender {
    [self loadWebView:@"PINTEREST" URL:@"https://uk.pinterest.com/rushhairbeauty/"];
}

- (IBAction)rushInstgram:(id)sender {
    [self loadWebView:@"INSTAGRAM" URL:@"https://instagram.com/officialrushhairbeauty/"];
}

- (IBAction)rushGoogle:(id)sender {
    [self loadWebView:@"GOOGLE" URL:@"https://plus.google.com/+rushhair/posts"];
}
- (IBAction)rushFacebook:(id)sender {
    [self loadWebView:@"FACEBOOK" URL:@"https://www.facebook.com/RUSHHairAndBeauty"];
}

- (IBAction)rushYoutube:(id)sender {
    [self loadWebView:@"YOUTUBE" URL:@"https://www.youtube.com/channel/UCAG-3eTOi2h78PWbW_oCSKA"];
}

- (IBAction)rushRSSFeeds:(id)sender {
    [self loadWebView:@"RSS" URL:@"http://careers.rush.co.uk/feed/"];
}

- (IBAction)rushFindMe:(id)sender {
    
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    DirectionsView *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"DirectionsView"];
    [self.navigationController pushViewController:scratch animated:YES];

}
- (IBAction)rushCheckIn:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"You already checked in." preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:Nil];

}

-(void)comingSoon{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Coming soon" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:Nil];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    moduleName=@"";
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    
    arrayOfLocations = [[NSMutableArray alloc] initWithCapacity:0];
    [arrayOfLocations addObject:@"ridgefield"];
    [arrayOfLocations addObject:@"southbury"];
    
    
    [_locationButton setTitle:[arrayOfLocations objectAtIndex:0] forState:UIControlStateNormal];
    
//    [_locationButton setImageEdgeInsets:UIEdgeInsetsMake(0, _locationButton.frame.size.width - 50, 0, 0)];
    
    
    CGFloat spacing = 10; // the amount of spacing to appear between image and title
    _locationButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
    _locationButton.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);


    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reservation" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReservation:) name:@"reservation" object:nil];

    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"service providers" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationServiceProviders:) name:@"service providers" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Services" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationServices:) name:@"Services" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"specials" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationOffers:) name:@"specials" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"about" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAbout:) name:@"about" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"events" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationEvents:) name:@"events" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationRefer:) name:@"refer" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gallery" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationGallery:) name:@"gallery" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gifts" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationGiftCard:) name:@"gifts" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"social" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationSocial:) name:@"social" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reviews" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReviews:) name:@"reviews" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"win" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationWin:) name:@"win" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"contact" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationContact:) name:@"contact" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"share" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationShare:) name:@"share" object:nil];


    //appDelegate.pushToken = [self loadSharedPreferenceValue:@"token"]?:@"12345";
    
    for(UIButton *bottomBarButton in self.bottomBarButtons)
        [[bottomBarButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    for(UIButton *topBarButton in self.topBarButtons)
        [[topBarButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    
    //topbuttons
    [[_beforeButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[_messagesButon imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[_accountButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    
    //bottom buttons
    [[_callUSButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[_directionButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[_hoursButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[_socialButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    
    if(appDelegate.isSideMenuApp){
        _mainBgForSideMenuApp.hidden = NO;
    } else {
        _mainBgForSideMenuApp.hidden = YES;
    }
    
    [self getModules];
    
    
    
    
    //    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    _defaultStack.hidden = NO;

    if (appDelegate.topBarType == 2)
    {
        _defaultStack.hidden = YES;
        [self addTopDynamicView];
    }
    else if (appDelegate.topBarType == 3)
    {
        _defaultStack.hidden = YES;
        [self addTopDynamicViewType3];

    }
    else if (appDelegate.topBarType == 4)
    {
        _defaultStack.hidden = YES;
        [self addTopDynamicViewType4];

        
    }
    
    //    ServicesViewController *services = [self.storyboard instantiateViewControllerWithIdentifier:@"ServicesViewController"];
    //    [self.navigationController pushViewController:services animated:YES];
}


-(void)notificationReservation: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openAppointments];

//    [self bookOnline];

}



-(void)notificationServiceProviders: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];

    [self openStaff];
}
-(void)notificationAbout: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    AboutUs *aboutUS = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUs"];
        aboutUS.flag =CONST_ABOUT_US;
    
    aboutUS.moduleObj =_staffModule;
    [self.navigationController pushViewController:aboutUS animated:YES];

}

-(void)notificationServices: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openServices];

}

-(void)notificationOffers: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openSpecials];

}

-(void)notificationGallery: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openGallery];

}

-(void)notificationEvents: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openEvents];
}
-(void)notificationCareers: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self loadWebView:@"CAREERS" URL:@"http://careers.rush.co.uk/"];

}

-(void)notificationShop: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self loadWebView:@"SHOP" URL:@"http://shoprush.com/"];


}

-(void)notificationBlog: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self loadWebView:@"BLOG" URL:@"https://www.rush.co.uk/blog/"];

}


-(void)notificationAcademy: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openStaff];

}

-(void)notificationSocial: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self showSocail];

}
-(void)notificationFranchise: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self loadWebView:@"FRANCHISE" URL:@"http://careers.rush.co.uk/franchise/"];

}

-(void)notificationGiftCard: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openGiftcards];
}

-(void)notificationRefer:(NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
    if(loginVal.length){
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        ReferFriendVC *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"ReferFriendVC"];
        [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
    }
    else{
        appDelegate.flgObj=@"referFrndFlg";
        [self loginScreen];
    }
}

-(void)notificationContact: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self callNow];

}

-(void)notificationReviews: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openReviews];
}

-(void)notificationShare: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self newShare];
}
-(void)notificationWin: (NSNotification *) notification
{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    [self openScratchOff];
}



-(void)addTopDynamicView
{
    UIView *containerView = [[[NSBundle mainBundle] loadNibNamed:@"TopViewLayOutType1" owner:self options:nil] lastObject];
    [self.view addSubview:containerView];
    
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *parent=self.view;

    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:containerView
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:containerView
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *top =[NSLayoutConstraint
                                 constraintWithItem:containerView
                                 attribute:NSLayoutAttributeTop
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:parent
                                 attribute:NSLayoutAttributeTop
                                 multiplier:1.0f
                                 constant:0.f];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:containerView
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:0
                                  constant:85];
    
    //Add constraints to the Parent
    [parent addConstraint:trailing];
    [parent addConstraint:top];
    [parent addConstraint:leading];

    //Add height constraint to the subview, as subview owns it.
    [containerView addConstraint:height];

}


-(void)addTopDynamicViewType3
{
//    UIView *containerView = [[[NSBundle mainBundle] loadNibNamed:@"TopViewLayOutType1" owner:self options:nil] lastObject];
    
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 85)];
    containerView.backgroundColor = [UIColor clearColor];
    
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:containerView.frame];
    bgImage.image = [UIImage imageNamed:@"logoBgTemp.png"];
    [containerView addSubview:bgImage];
    [self.view addSubview:containerView];


    
    
    
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *parent=self.view;
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:containerView
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:containerView
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *top =[NSLayoutConstraint
                              constraintWithItem:containerView
                              attribute:NSLayoutAttributeTop
                              relatedBy:NSLayoutRelationEqual
                              toItem:parent
                              attribute:NSLayoutAttributeTop
                              multiplier:1.0f
                              constant:0.f];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:containerView
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:0
                                  constant:85];
    
    //Add constraints to the Parent
    [parent addConstraint:trailing];
    [parent addConstraint:top];
    [parent addConstraint:leading];
    
    //Add height constraint to the subview, as subview owns it.
    [containerView addConstraint:height];
    
    //---
    
    bgImage.translatesAutoresizingMaskIntoConstraints = NO;
    
    //Trailing
    NSLayoutConstraint *trailingToBg =[NSLayoutConstraint
                                       constraintWithItem:bgImage
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:containerView
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leadingToBg = [NSLayoutConstraint
                                       constraintWithItem:bgImage
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:containerView
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
    
    //Bottom
    NSLayoutConstraint *topToBg =[NSLayoutConstraint
                                  constraintWithItem:bgImage
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:containerView
                                  attribute:NSLayoutAttributeTop
                                  multiplier:1.0f
                                  constant:0.f];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *bottomToBg = [NSLayoutConstraint
                                      constraintWithItem:bgImage
                                      attribute:NSLayoutAttributeBottom
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:containerView
                                      attribute:NSLayoutAttributeBottom
                                      multiplier:1.0f
                                      constant:0.f];
    
    //Add constraints to the Parent
    [containerView addConstraint:trailingToBg];
    [containerView addConstraint:leadingToBg];
    [containerView addConstraint:topToBg];
    [containerView addConstraint:bottomToBg];

    
}

-(void)addTopDynamicViewType4
{
    UIView *containerView = [[[NSBundle mainBundle] loadNibNamed:@"TopLayOutType4" owner:self options:nil] lastObject];
    [self.view addSubview:containerView];
    
    
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *parent=self.view;
    
    //Trailing
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:containerView
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Leading
    
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:containerView
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    //Bottom
    NSLayoutConstraint *top =[NSLayoutConstraint
                              constraintWithItem:containerView
                              attribute:NSLayoutAttributeTop
                              relatedBy:NSLayoutRelationEqual
                              toItem:parent
                              attribute:NSLayoutAttributeTop
                              multiplier:1.0f
                              constant:0.f];
    
    //Height to be fixed for SubView same as AdHeight
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:containerView
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:0
                                  constant:85];
    
    //Add constraints to the Parent
    [parent addConstraint:trailing];
    [parent addConstraint:top];
    [parent addConstraint:leading];
    
    //Add height constraint to the subview, as subview owns it.
    [containerView addConstraint:height];
    
}


-(void) getModules
{
    [dynamicSocialDict removeAllObjects];
    
    self.specialsArray = [NSMutableArray new];
    self.servicesArray = [NSMutableArray new];
    self.galleryArray = [NSMutableArray new];
    [appDelegate showHUD];
    NSString *_moduleName = moduleName;
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:[appDelegate addSalonIdTo:URL_MODULES_LIST] HEADERS:Nil GETPARAMS:Nil POSTPARAMS:Nil COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        
        NSLog(@"Modules Response : %@",responseDict);
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *globalDict = [responseDict copy];
            
            if ([[responseDict objectForKey:@"Reviews"] isKindOfClass:[NSDictionary class]] || [[responseDict objectForKey:@"Reviews"] isKindOfClass:[NSMutableDictionary class]]){
                [appDelegate.reivewsDictionary addEntriesFromDictionary:[responseDict objectForKey:@"Reviews"]];
            } else {
                NSDictionary *dictEmpty = [NSDictionary new];
                [appDelegate.reivewsDictionary addEntriesFromDictionary:dictEmpty];
            }
            
            dynamicSocialDict = [[responseDict objectForKey:@"social_media"] mutableCopy];

            NSLog(@"Reviews are : %@",appDelegate.reivewsDictionary);
            responseDict = [responseDict objectForKey:@"modules"];
            NSArray *specialsArray = [responseDict objectForKey:@"Specials"];
            for(NSDictionary *dict in specialsArray)
            {
                ModuleObj *moduleObj = [[ModuleObj alloc] init];
                moduleObj.moduleId = [dict objectForKey:@"module_id"];
                moduleObj.moduleName = [dict objectForKey:@"module_name"];
                [self.specialsArray addObject:moduleObj];
                appDelegate.specialsModel = moduleObj;
                moduleObj = nil;
            }
            
            NSArray *servicesArray = [responseDict objectForKey:@"Menu"];
            for(NSDictionary *dict in servicesArray)
            {
                ModuleObj *moduleObj = [[ModuleObj alloc] init];
                moduleObj.moduleId = [dict objectForKey:@"module_id"];
                moduleObj.moduleName = [dict objectForKey:@"module_name"];
                [self.servicesArray addObject:moduleObj];
                
                appDelegate.servicesModel = moduleObj;
                moduleObj = nil;
            }
            
            NSArray *galleryArray = [responseDict objectForKey:@"Gallery"];
            for(NSDictionary *dict in galleryArray)
            {
                ModuleObj *moduleObj = [[ModuleObj alloc] init];
                moduleObj.moduleId = [dict objectForKey:@"module_id"];
                moduleObj.moduleName = [dict objectForKey:@"module_name"];
                [self.galleryArray addObject:moduleObj];
                
                appDelegate.galleryModule = moduleObj;

                moduleObj = nil;
            }
            
            NSArray *bevaragesArray = [responseDict objectForKey:@"Beverages"];
            for(NSDictionary *dict in bevaragesArray)
            {
                self.bevaragesModule = [[ModuleObj alloc] init];
                self.bevaragesModule.moduleId = [dict objectForKey:@"module_id"];
                self.bevaragesModule.moduleName = [dict objectForKey:@"module_name"];
                appDelegate.beveragesModel = self.bevaragesModule;
                [self saveSharedPreferenceValue:self.bevaragesModule.moduleId KEY:@"Bevarages_ID"];
                self.bevaragesModule = nil;
            }
            
        
            
            NSArray *apptArr = [responseDict objectForKey:@"Appointments"];
            if(apptArr!=Nil && [apptArr count]>0)
            {
                self.appointmentsModule = [[ModuleObj alloc] init];
                self.appointmentsModule.moduleId = [[apptArr objectAtIndex:0] objectForKey:@"module_id"];
                self.appointmentsModule.moduleName = [[apptArr objectAtIndex:0] objectForKey:@"module_name"];
                appDelegate.appoitmentsModel = self.appointmentsModule;
                [self saveSharedPreferenceValue:self.appointmentsModule.moduleId KEY:@"PendingAppointments"];
            }
            
            NSArray *staffArr = [responseDict objectForKey:@"Staff2"];
            if(staffArr!=Nil && [staffArr count]>0)
            {
                self.staffModule = [[ModuleObj alloc] init];
                self.staffModule.moduleId = [[staffArr objectAtIndex:0] objectForKey:@"module_id"];
                self.staffModule.moduleName = [[staffArr objectAtIndex:0] objectForKey:@"module_name"];
                appDelegate.staffModel = self.staffModule;
            }
            
            NSArray *lastMinApptArr = [responseDict objectForKey:@"Last_min_appointments"];
            if(lastMinApptArr!=Nil && [lastMinApptArr count]>0)
            {
                self.lastMinModule = [[ModuleObj alloc] init];
                self.lastMinModule.moduleId = [[lastMinApptArr objectAtIndex:0] objectForKey:@"module_id"];
                self.lastMinModule.moduleName = [[lastMinApptArr objectAtIndex:0] objectForKey:@"module_name"];
                appDelegate.lastMinsMOdel =self.lastMinModule;
                [self saveSharedPreferenceValue:self.lastMinModule.moduleId KEY:@"Last_min_appointments"];
            }
            
            NSArray *eventsArr = [responseDict objectForKey:@"Events"];
            if(eventsArr!=Nil && [eventsArr count]>0)
            {
                self.eventsModule = [[ModuleObj alloc] init];
                
                self.eventsModule.moduleId = [[eventsArr objectAtIndex:0] objectForKey:@"module_id"];
                self.eventsModule.moduleName = [[eventsArr objectAtIndex:0] objectForKey:@"module_name"];
                
                
                appDelegate.eventsModel = self.eventsModule;
                [self saveSharedPreferenceValue: self.eventsModule.moduleId  KEY:@"EventsID"];
            }
            
            NSArray *beaconsJsonArr = [globalDict objectForKey:@"beacons"];
            
            [appDelegate.allRegisteredBeacons removeAllObjects];
            
            if ([globalDict objectForKey:@"beacons"] != nil) {
                [appDelegate.allRegisteredBeacons setObject:[globalDict objectForKey:@"beacons"] forKey:@"allBeacons"];
            } else {
                [appDelegate.allRegisteredBeacons setObject:[NSArray new] forKey:@"allBeacons"];
            }

            NSLog(@"beaconsJsonArr = %@",beaconsJsonArr);
            if(beaconsJsonArr !=Nil)
            {
                if([beaconsJsonArr count] ==0)
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"beaconsList"];
                else
                    [[NSUserDefaults standardUserDefaults] setObject:beaconsJsonArr forKey:@"beaconsList"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"Saved beacons = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"beaconsList"]);
            }
            
            
            NSLog(@"GEO lat %@",[globalDict valueForKey:@"salon_latitude"]);
            NSLog(@"GEO long %@",[globalDict valueForKey:@"salon_longitude"]);

            [appDelegate saveSharedPreferenceValue:[globalDict valueForKey:@"salon_latitude"] KEY:@"Latitude"];
            [appDelegate saveSharedPreferenceValue:[globalDict valueForKey:@"salon_longitude"] KEY:@"Longitude"];

            
            NSLog(@"Saving Geo co ordinates");
            
            [appDelegate setGeoFence];

            appDelegate.onlineBookingUrl = [globalDict valueForKey:@"online_booking_url"];
            appDelegate.giftCardUrl = [globalDict valueForKey:@"gift_card_url"];
            appDelegate.dynamicApptList = [globalDict objectForKey:@"appointment_booking_types"];
            
            appDelegate.onlinebookingType = [NSString stringWithFormat:@"%@", [globalDict objectForKey:@"onlinebooking_type"]];

            appDelegate.blockedDays = [globalDict valueForKey:@"blockedDays"];
            NSCharacterSet *charSet= [NSCharacterSet characterSetWithCharactersInString:@"(-)"];
            appDelegate.salonPhone = [[[globalDict valueForKey:@"salon_contact_no"] componentsSeparatedByCharactersInSet: charSet] componentsJoinedByString: @""];
            appDelegate.spaLatitude = [globalDict valueForKey:@"salon_latitude"];
            appDelegate.spaLongitude = [globalDict valueForKey:@"salon_longitude"];
            
            [self saveSharedPreferenceValue:[globalDict valueForKey:@"salon_latitude"] KEY:@"Latitude"];
            [self saveSharedPreferenceValue:[globalDict valueForKey:@"salon_longitude"] KEY:@"Longitude"];

            appDelegate.salonEmail=[globalDict valueForKey:@"salon_email_id"];
            
            [appDelegate setUrls:[globalDict objectForKey:@"apple_app_id"] PLAY:[globalDict objectForKey:@"android_app_id"]];
#if TARGET_IPHONE_SIMULATOR
            NSLog(@"This is simulator mode....");
#else
            [self startRangingBeacons];
            
#endif
            
            if([moduleName isEqualToString:@"Staff"]){
                [self openStaff];
            }
            else if([moduleName isEqualToString:@"Appointments"]){
                [self openAppointments];
            }
            else if([moduleName isEqualToString:@"Services"]){
                [self openServices];
            }
            else if([moduleName isEqualToString:@"Specials"]){
                [self openSpecials];
                
            }
            else if([moduleName isEqualToString:@"Events"]){
                [self openEvents];
                
            }
            else if([moduleName isEqualToString:@"Gallery"]){
                [self openGallery];
                
            }
            else if([moduleName isEqualToString:@"AboutUs"]){
                [self openAboutUsOrHours:1];
                
            }
            
            
            
        }
    }];
    
}

-(void) geoFencelocalNotification : (NSNotification *) notification
{
    [self openSpecials];
}

-(void) localNotification : (NSNotification *) notification
{
    BeaconNotification *beaconNotification = [self.storyboard instantiateViewControllerWithIdentifier:@"BeaconNotification"];
    [beaconNotification setDict:notification.userInfo];
    [self.navigationController presentViewController:beaconNotification animated:YES completion:nil];
}

-(void) startRangingBeacons
{
    self.beaconManager = [ESTBeaconManager new];
    self.beaconManager.delegate = self;
    self.regionArray = [[NSMutableArray alloc] init];
    [self.beaconManager requestAlwaysAuthorization];
    
    NSArray *beaconsList = (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"beaconsList"];
    if(beaconsList!=Nil && [beaconsList count]>0)
    {
        int identifier = 0;
        for (NSDictionary *beacon in beaconsList)
        {
            identifier++;
            
            //            [self.regionArray addObject:];
            
            [self.beaconManager startRangingBeaconsInRegion:[[CLBeaconRegion alloc
                                                              ] initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID major:[[beacon objectForKey:@"major"] unsignedIntValue] minor:[[beacon objectForKey:@"minor"] unsignedIntValue] identifier:[NSString stringWithFormat:@"Region %d", identifier]]];
        }
    }
}

#pragma mark Beacons delegate method

- (void)beaconManager:(id)manager didRangeBeacons:(NSArray *)beacons
             inRegion:(CLBeaconRegion *)region {
    CLBeacon *nearestBeacon = beacons.firstObject;
    if (nearestBeacon)
    {
        NSString * key = [NSString stringWithFormat:@"%@_%@_RANGING",region.major,region.minor];
        NSString * dateStr = [self getBeaconDateTimeFormat];
        
        NSString *storedStr = [self loadSharedPreferenceValue:key];
        //Check if the special has been notified already today
        
        NSDictionary *beaconCheckInDict= [self loadDictionary:@"CheckInBeacon"];
        if(beaconCheckInDict.count == 0)
        {
            
            
            NSArray *beaconsList = (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"beaconsList"];
            
            NSLog(@"beaconsList : %@",beaconsList);
            for (NSDictionary *beacon in beaconsList)
            {
                NSLog(@"beacon : %@",beacon);
                NSLog(@"region.major = %f",[region.major longLongValue]);
                NSLog(@"region.minor = %f",[region.minor longLongValue]);

                if([region.major longLongValue] == [[beacon objectForKey:@"major"] longLongValue] && [region.minor longLongValue] == [[beacon objectForKey:@"minor"] longLongValue])
                {
                    if([[beacon objectForKey:@"isCheckInBeacon"] boolValue]) {
                        NSMutableDictionary *dummyDict = [[NSMutableDictionary alloc]init];
                        [dummyDict setObject:[beacon objectForKey:@"major"]?:@"" forKey:@"major"];
                        [dummyDict setObject:[beacon objectForKey:@"minor"]?:@"" forKey:@"minor"];
                        NSString *slcId = [self loadSharedPreferenceValue:@"slc_id"];
                        if(slcId.length){
                            [self saveDictionary:[dummyDict mutableCopy] key:@"CheckInBeacon"];
                             appDelegate.checInBeaconsDictionary = [dummyDict copy];
                            [self beaconsSlcidServiceCall];
                            return;
                        }
                        
                        
                        
                    }
                }
            }
        }
        
        
        if ([storedStr isEqualToString:dateStr])
            return;
        
        //If not notified already, save the date corresponding to that beacon
        [self saveSharedPreferenceValue:dateStr KEY:key];
        NSArray *beaconsList = (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"beaconsList"];
        for (NSDictionary *beacon in beaconsList)
        {
            
            if([region.major longLongValue] == [[beacon objectForKey:@"major"] longLongValue] && [region.minor longLongValue] == [[beacon objectForKey:@"minor"] longLongValue])
            {
                BOOL ischeckedIn = [[beacon objectForKey:@"isCheckInBeacon"] boolValue];
                
                BeaconNotification *beaconNotification = [[BeaconNotification alloc] initWithNibName:@"BeaconNotification" bundle:nil];
                [beaconNotification setDict:[NSDictionary dictionaryWithObjectsAndKeys:[beacon objectForKey:@"special_title"], @"title", [beacon objectForKey:@"major"], @"major", [beacon objectForKey:@"minor"], @"minor", nil]];
                [self.navigationController presentViewController:beaconNotification animated:YES completion:nil];
                
                
                //                if(ischeckedIn == YES){
                //                    NSMutableDictionary *dummyDict = [[NSMutableDictionary alloc]init];
                //                    [dummyDict setObject:[beacon objectForKey:@"major"]?:@"" forKey:@"major"];
                //                    [dummyDict setObject:[beacon objectForKey:@"minor"]?:@"" forKey:@"minor"];
                //
                //                    appDelegate.checInBeaconsDictionary = [dummyDict copy];
                //                    return;
                
            }
        }
    }
    
}





-(void)beaconsSlcidServiceCall
{
    [appDelegate showHUD];
    
    NSString *slcId = [self loadSharedPreferenceValue:@"slc_id"];
    
    //    https://saloncloudsplus.com/mobile_checkin/checkForUncheckedAppointments
    
    NSString *url=@"https://saloncloudsplus.com/mobile_checkin/checkForUncheckedAppointments";
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:slcId forKey:@"slc_id"];
    [paramDic setObject:appDelegate.salonId forKey:@"salon_id"];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_SALON_DETAILS] HEADERS:Nil GETPARAMS:Nil POSTPARAMS:Nil COMPLETIONHANDLER:^(NSArray* responseDict, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
        {
            //[self showAlert:@"Error" MSG:error.localizedDescription];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Check in" message:@"Do you want to check in?" preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Check in" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self checkInProcess];
                
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertController animated:YES completion:Nil];
            
            
        }}];
    
}

-(NSString *) getBeaconDateTimeFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateNow = [formatter stringFromDate:[NSDate date]];
    return dateNow;
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:@"" forKey:@"ScreenName"];
    [def synchronize];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LOCAL_NOTIFICATION_RECEIVED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotification:) name:@"LOCAL_NOTIFICATION_RECEIVED" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GEO_FENCE_LOCAL_NOTIFICATION_RECEIVED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(geoFencelocalNotification:) name:@"GEO_FENCE_LOCAL_NOTIFICATION_RECEIVED" object:nil];

    [self.navigationController setNavigationBarHidden:YES];
    
}


-(void)internetConnectionCheck{
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Network error" message:@"Couldn't connect to the server. Check your network connection." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self getModules];
    }
    
}

#pragma mark check in Process
-(void)checkInProcess{
    
    return;
    
    if ([CLLocationManager locationServicesEnabled]){
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            [locationManager requestWhenInUseAuthorization];
        
        [locationManager startUpdatingLocation];
        
    }
    else{
        NSDictionary *beaconCheckInDict= [self loadDictionary:@"CheckInBeacon"];
        
        if(beaconCheckInDict.count){
            NSString *major =[beaconCheckInDict objectForKey:@"major"];
            NSString *minor =[beaconCheckInDict objectForKey:@"minor"];
            [self geoFenceServiceCallRequest:major minor:minor];
        }
        else{
            
            UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Alert"  message:@"We were not able to find you within the range of the Salon"  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        
        
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    //NSLog(@"salon latitude and longitude: %f,%f",appDelegate.spaLatitude,appDelegate.spaLongitude);
    locationManager.delegate = nil;
    [locationManager stopUpdatingLocation];
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    
    
    
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:[appDelegate.spaLatitude doubleValue] longitude:[appDelegate.spaLongitude doubleValue]];
    
    CLLocationDistance distanceInMeters = [locA distanceFromLocation:locB];
    
    NSLog(@"distance: %f",distanceInMeters);
    int distancevalue = (int) round(distanceInMeters);
    NSLog(@"distance: %i",distancevalue);
    if(distancevalue<=100)
    {
        
        [self geoFenceServiceCallRequest:@"" minor:@""];
        
        
    }
    
    else{
        
        
        if(appDelegate.checInBeaconsDictionary.count){
            
            NSString *major =[appDelegate.checInBeaconsDictionary objectForKey:@"major"];
            NSString *minor =[appDelegate.checInBeaconsDictionary objectForKey:@"minor"];
            [self geoFenceServiceCallRequest:major minor:minor];
        }
        else{
            
            UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Alert"  message:@"We were not able to find you within the range of the Salon"  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }
    
    
    
}



-(void)geoFenceServiceCallRequest:(NSString *)majorVal minor:(NSString*)minorVal{
    
    
    [appDelegate showHUD];
    
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"HH:mm"];
    
    NSString *currentDate = [df stringFromDate:[NSDate date]];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    [dict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    //[dict setObject:currentDate forKey:@"checkInTime"];
    [dict setObject:appDelegate.salonId forKey:@"salon_id"];
    NSString *urlString;
    if(majorVal.length && minorVal.length){
        [dict setObject:majorVal?:@"" forKey:@"major"];
        [dict setObject:minorVal?:@"" forKey:@"minor"];
        urlString =GEOFENCE_WITHBEACONS_URL;
        
    }
    else{
        urlString =GEOFENCE_WITHLOCATION_URL;
    }
    NSLog(@"server dict: %@",dict);
    
    
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            BOOL status = [[dict objectForKey:@"status"] boolValue];
            
            NSLog(@"Check in Resp :%@",dict);
            if(!status)
                [self showAlert:@"Check-In" MSG:[dict objectForKey:@"message"] BLOCK:^{
                }];
            else
            {
                [self showAlertView:@"Thank you for checking in" message:@"Please take a seat, the stylist will see you in a few minutes. Would you like a free beverage?"];
            }
        }}];
}


-(void)showAlertView:(NSString *)title message:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
        BeveragesList *bvl = [self.storyboard instantiateViewControllerWithIdentifier:@"BeveragesList"];
        [self.navigationController pushViewController:bvl animated:YES];
        
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 12;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"HomeCell";
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *) [cell.contentView viewWithTag:1];
    [imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"cell_%ld.png", (indexPath.item+1)]]];
    return cell;
}

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    float width = (self.collectionView.frame.size.width/3)-10;
    float height = (self.collectionView.frame.size.height)/4-10;
    return CGSizeMake(trunc(width), height);
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    
    switch(indexPath.item)
    {
        case 0:{
            
            if(appDelegate.appoitmentsModel.moduleId.length){
                [self openAppointments];
            }
            else{
                moduleName =@"Appointments";
                [self internetConnectionCheck];
            }
        }
            break;
            
        case 1:{
            if(appDelegate.netAvailability == YES){
                if(appDelegate.staffModel.moduleId.length)
                    [self openStaff];
                else
                    [self showAlert:SALON_NAME MSG:@"There are no staff currently." tag:0];
            }
            else{
                moduleName =@"Staff";
                [self internetConnectionCheck];
            }
        }
            break;
            
        case 4:{
            
            if(appDelegate.salonPhone.length)
                [self openAboutUsOrHours:1];
            else{
                moduleName =@"AboutUs";
                [self internetConnectionCheck];
            }
            
        }
            
            break;
            
        case 3:{
            
            if(appDelegate.specialsModel.moduleId.length)
                [self openSpecials];
            else{
                moduleName =@"Specials";
                [self internetConnectionCheck];
            }
        }
            break;
            
        case 2:{
            if(appDelegate.servicesModel.moduleId.length)
                [self openServices];
            else{
                moduleName =@"Services";
                [self internetConnectionCheck];
            }}
            break;
            
        case 5:{
            if(appDelegate.netAvailability == YES){
                
                if(appDelegate.eventsModel.moduleId.length)
                    [self openEvents];
                else
                    [self showAlert:SALON_NAME MSG:@"There are no events currently." tag:0];
            }
            else{
                moduleName = @"Events";
                [self internetConnectionCheck];
            }
        }
            break;
            
        case 6:
        {
            
            NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
            if(loginVal.length){
                
                self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                ReferFriendVC *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"ReferFriendVC"];
                [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
            }
            else{
                appDelegate.flgObj=@"referFrndFlg";
                [self loginScreen];
            }
            
        }
            break;
            
        case 7:{
            
            if(self.galleryArray.count)
                [self openGallery];
            else{
                moduleName =@"Gallery";
                [self internetConnectionCheck];
            }
            
        }
            break;
            
        case 8:
            [self openGiftcards];
            break;
            
        case 9:
            [self openLoyalty];
            break;
            
        case 10:
            [self openReviews];
            break;
            
        case 11:
            [self openScratchOff];
            break;
    }
}

//Modules

//Specials
- (void) openSpecials
{
    int count = (int)[self.specialsArray count];
    if(count==0)
    {
        [self showAlert:@"Specials" MSG:@"There are no specials currently."];
        return;
    }
    else if(count==1)
        [self invokeSpecialsModule:[self.specialsArray objectAtIndex:0]];
    else
    {
        
        
        
        //        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Specials" message:@"Please select one" preferredStyle:UIAlertControllerStyleAlert];
        //
        //        for(ModuleObj *moduleObj in self.specialsArray){
        //        [alertController addAction:[UIAlertAction actionWithTitle:moduleObj.moduleName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //             [self invokeSpecialsModule:moduleObj];
        //        }]];
        //        }
        //        [self presentViewController:alertController animated:YES completion:nil];
        
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Specials" andMessage:@"Please select one"];
        
        for(ModuleObj *moduleObj in self.specialsArray)
            [alertView addButtonWithTitle:moduleObj.moduleName
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [self invokeSpecialsModule:moduleObj];
                                  }];
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                              }];
        
        
        
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        [alertView show];
    }
}

-(void) invokeSpecialsModule : (ModuleObj *) moduleObj
{
    SpecialsList *specialsList = [self.storyboard instantiateViewControllerWithIdentifier:@"SpecialsList"];
    [specialsList setFlag:SPECIALS_FLAG];
    [specialsList setModuleObj:moduleObj];
    specialsList.specialsId = appDelegate.specialsModel.moduleId;
    [self.navigationController pushViewController:specialsList animated:YES];
}

-(void) openServices
{
    int count = (int)[self.servicesArray count];
    
    NSLog(@"Opent service : %@",self.servicesArray);
    for(ModuleObj *moduleObj in self.servicesArray)
    {
        NSLog(@"Name : %@ & ID : %@",moduleObj.moduleName, moduleObj.moduleId);
    }
    if(count==0)
    {
        [self showAlert:@"Services" MSG:@"There are no services currently."];
        return;
    }
    else if(count==1)
        [self invokeServicesModule:[self.servicesArray objectAtIndex:0]];
    else
    {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Services" andMessage:@"Please select one"];
        for(ModuleObj *moduleObj in self.servicesArray)
            [alertView addButtonWithTitle:moduleObj.moduleName
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [self invokeServicesModule:moduleObj];
                                  }];
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                              }];
        
        
        CGRect frame = alertView.frame;
        frame.size.height = 300;
        alertView.frame = frame;
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        [alertView show];
    }
}

-(void) invokeServicesModule : (ModuleObj *) moduleObj
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.buttonHideValue=@"NO";
    ServicesViewController *services = [self.storyboard instantiateViewControllerWithIdentifier:@"ServicesViewController"];
    [services setModuleObj:moduleObj];
    [services setStaffModelObj:self.staffModule];
    [self.navigationController pushViewController:services animated:YES];
}

-(void) openAppointments
{
    
    
    int count = (int)[appDelegate.dynamicApptList count];
    
    if(count==0)
    {
        
        if ([appDelegate.onlineBookingUrl length])
            [self loadWebView:@"Book Online" URL:appDelegate.onlineBookingUrl];
        else{
            [self showAlert:@"Request Appt" MSG:@"No modes of appointments"];
        }
        
        return;
    }
    
    else if (count==1) {
        
        for(NSString *name in appDelegate.dynamicApptList)
        {
            if ([name isEqualToString:@"Book Online"]) {
                
                if ([appDelegate.onlineBookingUrl length]==0){
                    
                    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
                    //Restaurants_WebappAppDelegate * objAppdel=(Restaurants_WebappAppDelegate*)[[UIApplication sharedApplication]delegate];
                    
                    if ([slcIdStr length]==0) {
                        appDelegate.flgObj=@"onlineBookingFlg";
                        [self loginScreen];
                    }
                    else{
                        [self bookOnline];
                    }
                }
                else{
                    [self loadWebView:@"Book Online" URL:appDelegate.onlineBookingUrl];
                }
            }
            else if ([name isEqualToString:@"Request a new Appt"]){
                [self invokeRequestAppt:self.staffModule];
            }
            else if ([name isEqualToString:@"Last Minute Appts"]){
                [self lastMinAppointments:nil];
            }
        }
    }
    else{
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"BOOK NOW" andMessage:@"Please select one"];
        for(NSString *str in appDelegate.dynamicApptList){
            if([str isEqualToString:@"Book Online"]){
                [alertView addButtonWithTitle:@"Book Online"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          [self bookOnline];
                                      }];
            }
            
            
            
            if([str isEqualToString:@"Request a new Appt"]){
                
                [alertView addButtonWithTitle:@"Request appointment"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          [self invokeRequestAppt:self.staffModule];
                                      }];
            }
            
            if([str isEqualToString:@"Last Minute Appts"]){
                [alertView addButtonWithTitle:@"Last Minute Appointments"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          [self lastMinAppointments:self.lastMinModule];
                                      }];
            }
            
        }
//        [alertView addButtonWithTitle:@"Find My Stylist"
//                                 type:SIAlertViewButtonTypeDefault
//                              handler:^(SIAlertView *alert) {
//                                  
//                                  
//                                  [self OpenFindMyStylist];
//                                  
//                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                              }];
        
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        [alertView show];
    }
    
    
}
-(void)OpenFindMyStylist{
    FindMyStylist *ms = [self.storyboard instantiateViewControllerWithIdentifier:@"FindMyStylist"];
    [self.navigationController pushViewController:ms animated:YES];
}
-(void) bookOnline
{
    appDelegate.isAnna = NO;
    if(appDelegate.onlineBookingUrl.length){
        [self loadWebView:@"Book Online" URL:appDelegate.onlineBookingUrl];
    }
    else{
        NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
        if(loginVal.length){
            
            if(appDelegate.isMultilocation)
            {
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

            LocationViewController *menu=(LocationViewController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LocationViewController"];
            //    ServicesListViewCon *servicesListViewCon = [[ServicesListViewCon alloc]initWithNibName:@"ServicesListViewCon" bundle:nil];
            [self.navigationController pushViewController:menu animated:YES];
            } else {
                self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                OnlineServicesViewController *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineServicesViewController"];
                
                [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
                

            }

        }
        else{
            appDelegate.flgObj = @"onlineBookFlag";
            [self loginScreen];
        }
    }
    /* [self showAlert:@"" MSG:@"At this time, Online Booking through our Mobile App is only available for our Cherry Hill Location. Look for additional locations coming soon! Thank you."  BLOCK:^{
     
     NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
     if(loginVal.length){
     
     self.navigationController.navigationBar.titleTextAttributes = @{
     NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
     NSForegroundColorAttributeName: [UIColor whiteColor]
     };
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
     OnlineServicesViewController *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineServicesViewController"];
     
     [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
     }
     else{
     appDelegate.flgObj = @"onlineBookFlag";
     [self loginScreen];
     }
     }];*/
    
    
    
    
}

-(void) invokeRequestAppt : (ModuleObj *) moduleObj
{
    RequestAppointment *requestAppointment = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestAppointment"];
    appDelegate.imageLinkURL =@"";
    requestAppointment.staffObjModel=self.staffModule;
    requestAppointment.appointmentObjModel = self.appointmentsModule;
    requestAppointment.state=1;
    requestAppointment.serviceFalg = CONST_REQSERVICES;
    requestAppointment.serviceProviderFlag = CONST_REQPROVIDERS;
    [self.navigationController pushViewController:requestAppointment animated:YES];
}

-(void) lastMinAppointments : (ModuleObj *) moduleObj
{
    LastMinuteApptVC *lastMinAppt = [self.storyboard instantiateViewControllerWithIdentifier:@"LastMinuteApptVC"];
    lastMinAppt.moduleObj= self.lastMinModule;
    [self.navigationController pushViewController:lastMinAppt animated:YES];
}

-(void) openStaff
{
    StaffList *StaffList = [self.storyboard instantiateViewControllerWithIdentifier:@"StaffList"];
    StaffList.moduleObj= self.staffModule;
    [self.navigationController pushViewController:StaffList animated:YES];
}

-(void) openAboutUsOrHours : (int) flag
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    AboutUs *aboutUS = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUs"];
    if(flag == 1)
        aboutUS.flag =CONST_ABOUT_US;
    else
        aboutUS.flag =CONST_HOURS;
    
    aboutUS.moduleObj =_staffModule;
    [self.navigationController pushViewController:aboutUS animated:YES];
    
    
    
}

-(void) openEvents
{
    EventsCalenderView *StaffList = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsCalenderView"];
    StaffList.moduleObj= self.eventsModule;
    
    [self.navigationController pushViewController:StaffList animated:YES];
    
}

-(void) openReferAFriend
{
    
    LoginVC *loginvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    loginvc.appointmentModule = self.appointmentsModule;
    [self.navigationController pushViewController:loginvc animated:YES];
}

-(void) openGallery
{
    int count = (int)[self.galleryArray count];
    if(count==0)
    {
        [self showAlert:@"Gallery" MSG:@"There is no gallery. Please check back later."];
        return;
    }
    else if(count==1)
        [self invokeGalleryModule:[self.galleryArray objectAtIndex:0]];
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Gallery" message:@"Please choose one" preferredStyle:UIAlertControllerStyleActionSheet];
        for(ModuleObj *moduleObj in self.galleryArray)
            [alert addAction:[UIAlertAction actionWithTitle:moduleObj.moduleName style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                              {
                                  [self invokeGalleryModule:moduleObj];
                              }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) invokeGalleryModule : (ModuleObj *) moduleObj
{
    self.galleryImagesArray = [[NSMutableArray alloc] init];
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:[NSString stringWithFormat:@"%@%@/", [appDelegate addSalonIdTo:URL_GALLERY], moduleObj.moduleId] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:@"Gallery" MSG:@"There are no images in the Gallery. Please try again later."];
                return;
            }
            
            for(NSDictionary *dict in responseArray)
            {
                MWPhoto *photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[dict objectForKey:@"gallery_image"]]];
                photoObj.caption = [dict objectForKey:@"gallery_title"];
                [self.galleryImagesArray addObject:photoObj];
            }
            
            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
            [browser setDelegate:self];
            browser.displayNavArrows = YES;
            browser.startOnGrid = YES;
            browser.displayActionButton = NO;
            [self.navigationController pushViewController:browser animated:YES];
        }
    }];
}

- (NSUInteger) numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.galleryImagesArray.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return self.galleryImagesArray[index];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index
{
    return [self.galleryImagesArray objectAtIndex:index];
}

-(void) openGiftcards
{
    
//    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Giftcards" andMessage:@"Please select an option"];
//    
//    [alertView addButtonWithTitle:@"Giftcards"
//                             type:SIAlertViewButtonTypeDefault
//                          handler:^(SIAlertView *alert) {
                              NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                              [def setObject:@"GiftCards" forKey:@"ScreenName"];
                              [def synchronize];
                              self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                              
                              GiftCardsMain *loyalityRegVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GiftCardsMain1"];
                              [self.navigationController pushViewController:loyalityRegVC animated:YES];
//                              
//                          }];
//    
//    [alertView addButtonWithTitle:@"Store"
//                             type:SIAlertViewButtonTypeDefault
//                          handler:^(SIAlertView *alert) {
//                              [self ecommerceAction:Nil];
//                          }];
//    [alertView addButtonWithTitle:@"Cancel"
//                             type:SIAlertViewButtonTypeDestructive
//                          handler:^(SIAlertView *alert) {
//                          }];
//    
//    
//    
//    alertView.transitionStyle = SIAlertViewTransitionStyleFade;
//    [alertView show];
    
    
    
    
}

-(void) openLoyalty
{
    
    
//    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
//    NSString *userID =   [self  loadSharedPreferenceValue:@"loyaltyUserId"];
//    
//    if (slcIdStr.length)
//    {
//        LoyaltyMain *loyalityRegVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyaltyMain"];
//        
//        [self.navigationController pushViewController:loyalityRegVC animated:YES];
//        
//    }
//    else
//    {

         NSString *userID =   [self  loadSharedPreferenceValue:@"loyaltyUserId"];
         if(userID.length){
         LoyaltyMain *loyalityRegVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyaltyMain"];
         
         [self.navigationController pushViewController:loyalityRegVC animated:YES];
         
         }
         else{
         self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
         LoyalityRegisterVC *loyalityRegVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyalityRegisterVC"];
         
         [self.navigationController pushViewController:loyalityRegVC animated:YES];
         }
         //        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
         //        LoyalityRegisterVC *loyalityRegVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyalityRegisterVC"];
         //
         //        [self.navigationController pushViewController:loyalityRegVC animated:YES];
//         */
//        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
//        LoyalityRegisterVC *loyalityRegVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyalityRegisterVC"];
//        
//        [self.navigationController pushViewController:loyalityRegVC animated:YES];
//    }

    
    
    
    
}

-(void) openReviews
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    ReviewsList *reviewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewsList"];
    
    [self.navigationController pushViewController:reviewVC animated:YES];
    
}

-(void) openScratchOff
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    ScratchView *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"ScratchView"];
    
    [self.navigationController pushViewController:scratch animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark top bar actions
- (IBAction)beforeAndAfter:(id)sender {
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    
    
    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    AppDelegate  * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if ([slcIdStr length]==0) {
        objAppdel.flgObj=@"beforeAndAfter";
        [self loginScreen];
        return;
    }
    else{
    }

    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    BeforeAfterVc *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"BeforeAfterVc"];
    
    [self.navigationController pushViewController:scratch animated:YES];

    return;
    
    
    
    if(slcIdStr.length){
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Gallery" andMessage:@"Please select one"];
        
        [alertView addButtonWithTitle:@"Before & After"         type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                                  
                                  BeforeAfterVc *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"BeforeAfterVc"];
                                  
                                  [self.navigationController pushViewController:scratch animated:YES];
                              }];
        
        
        
        [alertView addButtonWithTitle:@"My Gallery"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alert) {
                                  
                                  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Gallery" bundle: nil];
                                  
                                  MyGalleryViewController *mgvc = [storyboard instantiateViewControllerWithIdentifier:@"MyGalleryViewController"];
                                  mgvc.flag =0;
                                  [self.navigationController pushViewController:mgvc animated:YES];
                              }];
        
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeDestructive
                              handler:^(SIAlertView *alert) {
                              }];
        
        
        
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        [alertView show];
    }
    else{
        BeforeAfterVc *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"BeforeAfterVc"];
        
        [self.navigationController pushViewController:scratch animated:YES];
    }
    
}
- (IBAction)messagesAction:(id)sender {
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    MessagesListVC *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagesListVC"];
    
    [self.navigationController pushViewController:scratch animated:YES];
}

-(IBAction)accountAction:(id)sender{
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    
    
    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    AppDelegate  * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if ([slcIdStr length]==0) {
        objAppdel.flgObj=@"checkApptFlg";
        [self loginScreen];
    }
    else{
        [self checkApptObj];
    }
}

#pragma mark Logi screen method
-(void)loginScreen{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    login.appointmentModule = self.appointmentsModule;
    [self.navigationController pushViewController:login animated:YES];
}

-(void)checkApptObj{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    MyAccountList *account = [self.storyboard instantiateViewControllerWithIdentifier:@"MyAccountList"];
    account.appointmentMOdule = self.appointmentsModule;
    [self.navigationController pushViewController:account animated:YES];
}

-(IBAction)hoursButtonAction:(id)sender{
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    AboutUs *aboutUS = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUs"];
    aboutUS.flag =CONST_HOURS;
    aboutUS.moduleObj =_staffModule;
    [self.navigationController pushViewController:aboutUS animated:YES];
}
-(IBAction)socialButtonAction:(id)sender{
    [self showSocail];
}

-(void)showSocail
{
    if(appDelegate.netAvailability == YES){
    }
    else{
        [self internetConnectionCheck];
        return;
    }
    
    if (dynamicSocialDict.count)
    {
        NSArray *dynamicSocialArray = [dynamicSocialDict allKeys];
        
        int count = (int)[dynamicSocialArray count];
        if(count==1){
            NSString *titleStr = [dynamicSocialArray objectAtIndex:0];
            //[self invokeSocial:titleStr Url:[dynamicSocialDict objectForKey:titleStr]];
            [self loadWebView:titleStr URL:[dynamicSocialDict objectForKey:titleStr]];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Social" message:@"Please select one" preferredStyle:UIAlertControllerStyleActionSheet];
            for(NSString *title in dynamicSocialArray)
                [alert addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      // [self invokeSocial:title Url:[dynamicSocialDict objectForKey:title]];
                                      [self loadWebView:title URL:[dynamicSocialDict objectForKey:title]];
                                  }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else{
        [self showAlert:@"Social" MSG:@"There are no social currently."];
        return;
    }

}
//-(void) loadSocialWebView : (NSString *) title URL:(NSString *)url
//{
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//
//    WebViewLoad *loader = [[WebViewLoad alloc] initWithNibName:@"WebViewLoad" bundle:nil];
//    [loader setUrl:url];
//    [loader setTitle:title];
//    [self.navigationController pushViewController:loader animated:YES];
//}

-(void) loadWebView : (NSString *) title URL:(NSString *)url
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    GoogleReviewVC *loader = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [loader setUrl:url];
    [loader setTitle:title];
    [self.navigationController pushViewController:loader animated:YES];
}


-(IBAction)directionButtonAction:(id)sender{
    if(appDelegate.netAvailability == NO){
        [appDelegate showNetworkAlert];
        return;
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    DirectionsView *scratch = [self.storyboard instantiateViewControllerWithIdentifier:@"DirectionsView"];
    [self.navigationController pushViewController:scratch animated:YES];
}


#pragma mark bottom bar actions
-(IBAction)callUs:(id)sender
{
    [self callNow];
}

-(void)callNow
{
    
    
    NSString * model=[[UIDevice currentDevice] model];
    if ([model hasPrefix:@"iPhone"]) {
        [self showAlert:@"Call Us" MSG:[UIViewController formatPhone] tag:1];
        
    }else
        [self showAlert:@"Call" MSG:@"Calling feature is not available on this device." tag:0];

}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1){
            AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
            NSString * strPhone=[NSString stringWithFormat:@"tel://%@",objAppdel.salonPhone];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
        }
    }]];
    if(tag == 1){
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
    }
    [self presentViewController:alertController animated:YES completion:nil];
}


- (IBAction)checkProcessAction:(id)sender {
//    [self checkInProcess];
    
    [self.menuContainerViewController setMenuState:MFSideMenuStateLeftMenuOpen completion:^{}];
    


//    [self geoFenceServiceCallRequest:@"" minor:@""];
}
-(IBAction)shareAction:(id)sender;
{
    if(appDelegate.netAvailability == YES){
        [self newShare];
        return;
    }
    [self showAlert:@"Network error" MSG:@"Could not connect to the server. Please check your network connectivity." tag:1];

}
//Share code

-(void) newShare
{
    NSArray *itemsToShare = @[[Globals getShareLongText]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
    [activityVC setValue:SHARE_SUBJECT forKey:@"subject"];

    [self presentViewController:activityVC animated:YES completion:nil];
    
}

-(IBAction)ecommerceAction:(id)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ecommerce" bundle: nil];
    CategoriesViewController *mgvc = [storyboard instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
    [self.navigationController pushViewController:mgvc animated:YES];
}
//
//-(void) openMapPreview
//{
//    
//}

@end
