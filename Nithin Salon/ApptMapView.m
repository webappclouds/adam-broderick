//
//  ApptMapView.m
//  Nithin Salon
//
//  Created by Nithin Reddy on 10/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ApptMapView.h"
#import "AppDelegate.h"

@interface ApptMapView ()

@end

@implementation ApptMapView
//@synthesize mapView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Traffic";
    
    
    
//    self.mapContainer = mapView;
    
    UIBarButtonItem *directions = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"route.png"] style:UIBarButtonItemStylePlain target:self action:@selector(directionsClicked:)];
    
    
    self.navigationItem.rightBarButtonItem = directions;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    AppDelegate *objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    GMSCameraPosition *camera =
    [GMSCameraPosition cameraWithLatitude:[objAppdel.spaLatitude doubleValue]
                                longitude:[objAppdel.spaLongitude doubleValue]
                                     zoom:11
                                  bearing:30
                             viewingAngle:40];
    
    GMSMapView *mapView = [GMSMapView mapWithFrame:self.mapContainer.frame camera:camera];
    mapView.userInteractionEnabled = YES;
    mapView.trafficEnabled = YES;
    mapView.layer.masksToBounds = YES;
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = camera.target;
    marker.snippet = SALON_NAME;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = mapView;
    [self.mapContainer addSubview:mapView];
}

-(IBAction)directionsClicked:(id)sender
{
    [UIViewController loadDirections];
}

-(IBAction) notifySalon : (id) sender
{
    [self showHud];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hideHud];
        [self showAlert:SALON_NAME MSG:@"The Salon has been notified." BLOCK:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
