//
//  AppDelegate.m
//  Nithin Salon
//
//  Created by Nithin Reddy on 18/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <GoogleMaps/GoogleMaps.h>
#import "UIViewController+NRFunctions.h"
#import "Globals.h"
#import "ReviewUsVC.h"
#import "SpecialsList.h"
#import "RequestAppointment.h"
#import "ScratchView.h"
#import "EventsCalenderView.h"
#import "LoginVC.h"
#import "LastMinuteApptVC.h"
#import "UIViewController+NRFunctions.h"
#import "Constants.h"
#import "ViewController.h"
#import "PendingAppoitmentsVC.h"
#import "LoginVC.h"
#import "ApptResponse.h"
#import "Reachability.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <TSMessages/TSMessageView.h>
#import "OnlineBookindViewCon.h"
#import "ChooseDateViewCon.h"
#import "MyGalleryViewController.h"
#import "LeftMenuViewController.h"
#import "LocationViewController.h"
#import "OnlineServicesViewController.h"
#import "ReviewsList.h"
#import "LoginVC.h"
#import "ReviewUsVC.h"
#import "LoyaltyMain.h"
#import "LoyalityRegisterVC.h"
#import "PrescriptionList.h"
#import "ReferFriendVC.h"
#import "MultiLocationViewController.h"
@interface AppDelegate ()
{
    CLLocationManager *locationManager;
}
@end

@implementation AppDelegate

@synthesize topBarType,isMultilocation, latitudeOfSalon, longitudeOfSalon, onlinebookingType, allRegisteredBeacons, salonId;


-(NSString *)addSalonIdTo:(NSString*)urlPart{
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@/",urlPart, self.salonId];
    return finalUrl;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
   // NSLog(@"output is : %@", Identifier);
    
//    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@""];
    
    self.salonId = @"691";
    
    
    NSString *prodName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    NSLog(@"Display Name : %@", prodName);
    
    topBarType = 0 ;
    
    // To show side menu put 1 and else put 0
    _isSideMenuApp = 1;
    
    isMultilocation = YES;
    _globalCheckAndUncheckArray=[NSMutableArray new];
    _walletArray = [NSMutableArray new];
    _reivewsDictionary = [[NSMutableDictionary alloc] initWithCapacity:0];
    _onlineAppointmentBookDetails = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    allRegisteredBeacons = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [self internetIntegration];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        [Fabric with:@[[Crashlytics class]]];
    [IQKeyboardManager sharedManager].enable = TRUE;
    [GMSServices provideAPIKey:GOOGLE_MAPS_API_KEY];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPopup:) name:@"SHOWPOPUP" object:nil];
    [ESTConfig setupAppID:@"demosalonclouds" andAppToken:@"66e02cf077747fd02ae31252bb2fbbc3"];
    
    
    //Register for Push Notifications
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    else if([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    
    
    [GMSServices provideAPIKey:GOOGLE_MAPS_API_KEY];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = 500; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    
    //    //Location
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        
        [locationManager requestAlwaysAuthorization];
        
        [locationManager startUpdatingLocation];
        
    }else{
        
        [locationManager startUpdatingLocation];
        
    }
    
//    [self performSelector:@selector(moveToReferFriend) withObject:nil afterDelay:10];
    [self setGeoFence];
    
    
    //[ESTConfig ena];
    
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"This is simulator mode....");
#else
    [self initBeacons];
#endif
    

    if (_isSideMenuApp) {
        [self addMainNavigationWithSideMenu];
    } else {
        [self addMainNavigationWithOutSideMenu];
    }
    
    
    UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    NSDictionary *remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (localNotification || remoteNotification)
        application.applicationIconBadgeNumber = 0;
    
    // [self setAllTabsWithNavigationControllers];
    
    
    if(localNotification)
    {
        
        [self localNotificationClicked:localNotification.userInfo];
    }
    else if(remoteNotification && [remoteNotification objectForKey:@"type"] !=nil)
    {
        int type = [[remoteNotification objectForKey:@"type"] intValue];
        
        [self pushNotifications:type dict:remoteNotification];
        
    }
    
        return YES;
}
    
-(void)addMainNavigationWithOutSideMenu
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    self.navigationController = [[UINavigationController alloc]initWithRootViewController:lvc];
    
    //    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:(34.0/255.0) green:(46.0/255.0) blue:(64.0/255.0) alpha:1.0]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };
    
    
    
    //self.navigationController.navigationBar.backgroundColor = ;
    self.window.rootViewController = self.navigationController;
    // [self.navigationController pushViewController:lvc animated:YES];

}



//-(void)addMainNavigationWithSideMenu
//{
//    // Single location with Side Menu
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    MultiLocationViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:@"MultiLocationViewController"];
//    self.navigationController = [[UINavigationController alloc]initWithRootViewController:lvc];
//    
//    //    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:(34.0/255.0) green:(46.0/255.0) blue:(64.0/255.0) alpha:1.0]];
//    [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
//    
//    self.navigationController.navigationBar.titleTextAttributes = @{
//                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
//                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
//                                                                    };
//    
//    
//    
//    //self.navigationController.navigationBar.backgroundColor = ;
//    self.window.rootViewController = self.navigationController;
//    // [self.navigationController pushViewController:lvc animated:YES];
//    
//}

-(void)addMainNavigationWithSideMenu
{
    // Single location with Side Menu
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    
    self.navigationController = [[UINavigationController alloc]initWithRootViewController:lvc];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };
    
    
    
    LeftMenuViewController *leftMenu = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"];
    _container = [MFSideMenuContainerViewController
                  containerWithCenterViewController:self.navigationController
                  leftMenuViewController:leftMenu
                  rightMenuViewController:nil];
    
    _container.leftMenuWidth = self.window.frame.size.width - 100;
    self.window.rootViewController = _container;
    

    [self.window makeKeyAndVisible];
    
}


-(void) setUrls : (NSString *) itunesId PLAY:(NSString *)playPackage
{
    if(itunesId!=Nil && [itunesId length]>0)
        self.itunesUrl = [[NSString alloc] initWithString:[NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", itunesId]];
    
    if(playPackage!=Nil && [playPackage length]>0)
        self.playUrl = [[NSString alloc] initWithString:[NSString stringWithFormat:@"https://play.google.com/store/apps/details?id=%@", playPackage]];
    
}

#pragma mark Push Delegates
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];
    
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.pushToken = str;
    NSLog(@"Token : %@",str);
    if(![str isEqualToString:@""])
        [self sendPushToken:str];
}


-(void) sendPushToken : (NSString *) token
{
    NSLog(@"Push token : %@",token);
    self.pushToken = token?:@"9800XXX";
    
    [self saveSharedPreferenceValue:self.pushToken?:@"" KEY:@"token"];
   
    NSString *url = [NSString stringWithFormat:@"%@%@/1/", [self addSalonIdTo:URL_PUSH_TOKEN], token];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString *)url HEADERS:Nil GETPARAMS:Nil POSTPARAMS:Nil COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        
        if(error){
            //[self showAlert:@"Error" MSG:error.localizedDescription];
            
        }
        else
        {
            
        }
    }];
    
}

#pragma mark - REACHABILITY METHODS
- (BOOL)reachabilityChanged
{
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        self.netAvailability = NO;
        
    }
    else{
        self.netAvailability = YES;
    }
    
    return self.netAvailability;
}


- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    self.pushToken = @"";
}

-(void)moveToPriscriptions
{
    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    
    if ([slcIdStr length]==0) {
        self.flgObj=@"checkApptFlg";
        [self loginScreen];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        PrescriptionList *applist = [storyboard instantiateViewControllerWithIdentifier:@"PrescriptionList"];
        [self.navigationController pushViewController:applist animated:YES];
    }

}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSLog(@"PayLoad :%@",userInfo);
    if([userInfo objectForKey:@"type"]!=nil){
        int type = [[userInfo objectForKey:@"type"] intValue];
        [self pushNotifications:type dict:userInfo];
        
    }
}

-(void)moveToLoyalty
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    NSString *userID =   [self  loadSharedPreferenceValue:@"loyaltyUserId"];
    if(userID.length){
        LoyaltyMain *loyalityRegVC = [storyboard instantiateViewControllerWithIdentifier:@"LoyaltyMain"];
        
        [self.navigationController pushViewController:loyalityRegVC animated:YES];
        
    }
    else{
        self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
        LoyalityRegisterVC *loyalityRegVC = [storyboard instantiateViewControllerWithIdentifier:@"LoyalityRegisterVC"];
        
        [self.navigationController pushViewController:loyalityRegVC animated:YES];
    }

}

-(void)moveToReferFriend
{
    NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
    if(loginVal.length){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

        self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        ReferFriendVC *onlineBookindViewCon = [storyboard instantiateViewControllerWithIdentifier:@"ReferFriendVC"];
        [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
    }
    else{
        self.flgObj=@"referFrndFlg";
        [self loginScreen];
    }

}
-(void)pushNotifications:(int)type dict:(NSDictionary *)userInfo{
      _moduleID = [userInfo objectForKey:@"module_id"];
    
    if(type==1)
        [self loadReviews];
    else if(type==2)
        [self loadSpecials];
    else if(type==3)
        [self pendingAppointments];
    else if(type==4){
        [self loadApptResponse:userInfo];
    }
    else if(type==5){
        
        NSString *msg =  [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        [self showAlert:@"Customer Alert" MSG:msg];
    }
    
    else if(type==6)
        [self scracthView];
    else if(type==7)
        [self events];
    else if(type==10)
        [self lastMinuteAppointment];
    else if(type == 17)
        [self bookOnline];
    else if (type == 13)
    {
        [self moveToLoyalty];
    }
    else if (type == 8)
    {
        [self moveToPriscriptions];
    }
    else if (type == 11)
    {
        [self moveToReferFriend];
    }

    else if(type == 18){
   
        // check in
        return;
        NSArray *arr =[userInfo objectForKey:@"s_ids"];
         NSArray *nameArray =[userInfo objectForKey:@"s_names"];
        NSString *nameStr,*idStr;
        for(int i=0;i<arr.count;i++)
        {
            NSString *id_str = [arr objectAtIndex:i];
                    if(idStr.length==0)
        idStr = [[NSString alloc]initWithString:id_str];
            else
            idStr = [idStr stringByAppendingFormat:@",%@",id_str];
    }
        
        for(int i=0;i<nameArray.count;i++)
        {
             NSString *name_str = [nameArray objectAtIndex:i];
           
            if(nameStr.length==0)
                nameStr = [[NSString alloc]initWithString:name_str];
            else
                nameStr = [nameStr stringByAppendingFormat:@",%@",name_str];
        }
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setObject:idStr forKey:@"s_ids"];
        [dict setObject:nameStr forKey:@"s_names"];
        [self navigateToChooseDateClass:dict];
}
}

-(void) loadApptResponse : (NSDictionary *) resp
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ApptResponse *apptResponse = [storyboard instantiateViewControllerWithIdentifier:@"ApptResponse"];
    [apptResponse setLabelStr:[[resp objectForKey:@"aps"] objectForKey:@"alert"]];
    [apptResponse setStr:[resp objectForKey:@"content"]];
    [self.navigationController pushViewController:apptResponse animated:YES];
}

-(void)pendingAppointments{
    
    
    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    AppDelegate  * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if ([slcIdStr length]==0) {
        objAppdel.flgObj=@"checkApptFlg";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        LoginVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:lvc animated:YES];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        PendingAppoitmentsVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"PendingAppoitmentsVC"];
        [self.navigationController pushViewController:lvc animated:YES];

    }

    
    
}

-(void)loadReviews{
    
    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    AppDelegate  * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if ([slcIdStr length]==0) {
        objAppdel.flgObj=@"FromPush";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        LoginVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:lvc animated:YES];
    }

    else{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ReviewUsVC *reviewVC = [storyboard instantiateViewControllerWithIdentifier:@"ReviewUsVC"];
//    reviewVC.comparisonFlag =@"FromPush";
    [self.navigationController pushViewController:reviewVC animated:YES];
    }
}
-(void)loadSpecials{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    SpecialsList *lvc = [storyboard instantiateViewControllerWithIdentifier:@"SpecialsList"];
//    lvc.specialsId = _moduleID;
//    [self.navigationController pushViewController:lvc animated:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    SpecialsList *specialsList = [storyboard instantiateViewControllerWithIdentifier:@"SpecialsList"];
    [specialsList setFlag:SPECIALS_FLAG];
//    [specialsList setModuleObj:moduleObj];
    specialsList.specialsId = _moduleID;
    [self.navigationController pushViewController:specialsList animated:YES];

}
-(void)scracthView{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ScratchView *scratch = [storyboard instantiateViewControllerWithIdentifier:@"ScratchView"];
    [self.navigationController pushViewController:scratch animated:YES];
    
}
-(void)events{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    EventsCalenderView *lvc = [storyboard instantiateViewControllerWithIdentifier:@"EventsCalenderView"];
    [self.navigationController pushViewController:lvc animated:YES];
    
}

-(void)login{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    LoginVC *lvc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:lvc animated:YES];
}

-(void)lastMinuteAppointment{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    LastMinuteApptVC *lastMinAppt = [storyboard instantiateViewControllerWithIdentifier:@"LastMinuteApptVC"];
    [self.navigationController pushViewController:lastMinAppt animated:YES];
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [self localNotificationClicked:notification.userInfo];
}

-(void) localNotificationClicked : (NSDictionary *) userInfo
{
    //    UIApplicationState state = [application applicationState];
    //    if (state == UIApplicationStateActive || state==UIApplicationStateBackground) {
    if(userInfo==nil)
        return;
    if([[userInfo objectForKey:@"title"] isEqualToString:@""])
        return;
    if([[userInfo objectForKey:@"image"] isEqualToString:@""]){
        //[self showAlert:SALON_NAME msg:[userInfo objectForKey:@"title"] ];
    }
    else{
            if ([[userInfo objectForKey:@"type"] isEqualToString:@"geoFence"]){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GEO_FENCE_LOCAL_NOTIFICATION_RECEIVED" object:self userInfo:userInfo];
            }
            else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCAL_NOTIFICATION_RECEIVED" object:self userInfo:userInfo];
            }
        }
    
    // Set icon badge number to zero
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
}


-(void) initBeacons
{
    ESTBeaconManager *beaconManager;
    NSArray *beaconsList = (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"beaconsList"];
    if(beaconsList!=Nil && [beaconsList count]>0)
    {
        for (NSDictionary *beacon in beaconsList)
        {
            beaconManager = [[ESTBeaconManager alloc] init];
            beaconManager.delegate = self;
            
            CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID
                                                                                   major:[[beacon objectForKey:@"major"] unsignedIntValue]
                                                                                   minor:[[beacon objectForKey:@"minor"] unsignedIntValue]
                                                                              identifier:[beacon objectForKey:@"beacon_name"]];
            
            beaconRegion.notifyOnEntry = YES;
            beaconRegion.notifyOnExit = NO;
            
            [beaconManager requestAlwaysAuthorization];
            [beaconManager startMonitoringForRegion:beaconRegion];
            
        }
    }
}
- (void)beaconManager:(id)manager didEnterRegion:(CLBeaconRegion *)region
{
    NSString * key = [NSString stringWithFormat:@"%@_%@",region.major,region.minor];
    NSString * dateStr = [self getBeaconDateTimeFormat];
    
    
    NSString *storedStr = [self loadSharedPreferenceValue:key];
    //Check if the special has been notified already today
    if ([storedStr isEqualToString:dateStr])
        return;
    
    //If not notified already, save the date corresponding to that beacon
    [self saveSharedPreferenceValue:dateStr KEY:key];
    
    NSArray *beaconsList = (NSArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"beaconsList"];
    for (NSDictionary *beacon in beaconsList)
    {
        if([region.major longLongValue] == [[beacon objectForKey:@"major"] longLongValue] && [region.minor longLongValue] == [[beacon objectForKey:@"minor"] longLongValue])
        {
            UILocalNotification *notification = [UILocalNotification new];
            notification.alertBody = [beacon objectForKey:@"special_title"];
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[beacon objectForKey:@"special_title"], @"title", [beacon objectForKey:@"major"], @"major", [beacon objectForKey:@"minor"], @"minor", nil];
            [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
            break;
        }
    }
    
    
    /*
     //Send it to Server
     NSDictionary *beaconsDict = [NSDictionary dictionaryWithObjectsAndKeys:region.major, @"major", region.minor, @"minor", nil];
     
     SendRequest *sendRequest = [[SendRequest alloc] init];
     [sendRequest setDelegate:self];
     [sendRequest url:(NSMutableString*)[appDelegate addSalonIdTo:URL_GET_BEACON_INFO]
     withParameter:beaconsDict
     withRequestType:@"POST" SHOWPROGRESS:NO];
     */
}

-(NSString *) getBeaconDateTimeFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateNow = [formatter stringFromDate:[NSDate date]];
    return dateNow;
}




- (void)beaconManager:(id)manager didExitRegion:(CLBeaconRegion *)region
{
    //    UILocalNotification *notification = [UILocalNotification new];
    //    notification.alertBody = [NSString stringWithFormat:@"Exited beacon %@", region.identifier];
    //    notification.userInfo = nil;
    //    notification.soundName = UILocalNotificationDefaultSoundName;
    //    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

//- (void)beaconManager:(id)manager monitoringDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
//{
//    UIAlertView* errorView = [[UIAlertView alloc] initWithTitle:@"Monitoring error"
//                                                        message:error.localizedDescription
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//
//    [errorView show];
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) showPopup : (NSNotification *) notification
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SALON_NAME message:[notification.userInfo objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [self.window.rootViewController presentViewController:alertController animated:YES completion:Nil];
}


-(void) saveSharedPreferenceValue : (NSString *)val KEY:(NSString *)key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:val forKey:key];
    [prefs synchronize];
}

-(NSString *) loadSharedPreferenceValue : (NSString *)key
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [self checkNull:[prefs valueForKey:key]];
}
-(NSString *) checkNull:(NSObject *) val
{
    NSString *value = [NSString stringWithFormat:@"%@",val];
    
    if(value == nil || [val isEqual:[NSNull null]] || [value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"]||[value length] == 0)
        value = @"";
    ///sr
    return value;
}



#pragma mark Geo Fencing

-(void) setGeoFence
{

    float currentLatitude,currentLongitude;
    
    NSString *stingLatitude = [self loadSharedPreferenceValue:@"Latitude"];
    NSString *stingLongitude = [self loadSharedPreferenceValue:@"Longitude"];

    if([stingLatitude length]) {
        currentLatitude = stingLatitude.doubleValue;
    } else {
        currentLatitude = SALON_LATITUDE;
    }
    
    if([stingLongitude length]) {
        currentLongitude = stingLongitude.doubleValue;
    } else {
        currentLongitude = SALON_LONGITUDE;
    }
    NSLog(@"Geo coordinates : %f & %f",currentLatitude,currentLongitude);
    
    CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake(currentLatitude,currentLongitude);
    CLCircularRegion *grRegion = [[CLCircularRegion alloc] initWithCenter:coordinates radius:1500 identifier:[NSString stringWithFormat:SALON_NAME]];
    
    if([Globals isGeoFencingOn]){
        NSLog(@"............. Start Monitoring");
        [locationManager startMonitoringForRegion:(CLRegion *)grRegion];
    }
    else
    {
        NSLog(@"............. Stop Monitoring");

        [locationManager stopMonitoringForRegion:(CLRegion *)grRegion];
    }
}

- (void)addLocalNotification{
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    
    NSDate *fireTime = [NSDate date];
    localNotif.fireDate = fireTime;
    
    NSString *text = [NSString stringWithFormat:@"You are close to %@. Check out our Specials!", SALON_NAME];
    localNotif.alertBody = text;
    localNotif.applicationIconBadgeNumber = 1;
    
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"geoFence",@"type",text,@"alertBody", nil];
    
    [localNotif setUserInfo:dict];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    
}

- (void) locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    if([self isWithinBusinesHours]==0)
        return;
    if([self checkIfAlreadyNotifiedToday] == 1)
        [self addLocalNotification];
}

-(int) isWithinBusinesHours
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit) fromDate:[NSDate date]];
    NSInteger hour = [components hour];
    if(hour>=8 && hour<=20)
        return 1;
    return 0;
}


//Returns 1 if not notified today, else 0
-(int) checkIfAlreadyNotifiedToday
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"EN"]];
    [dateFormatter setDateFormat:@"d MMM yyyy"];
    NSString *str = [dateFormatter stringFromDate:[NSDate date]];
    if(![str isEqualToString:[Globals getLastNotifiedDate]])
    {
        [Globals setLastNotifiedDate:str];
        return 1;
    }
    return 0;
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self.window.rootViewController presentViewController:alertController animated:YES completion:Nil];
}

-(void)showHUD{
    
    //[MBProgressHUD showHUDAddedTo:self.window animated:YES];
   [MBProgressHUD showHUDAddedTo:self.window animated:YES];
    
}

-(void)internetIntegration{
NSString *remoteHostName = @"www.apple.com";
NSString *remoteHostLabelFormatString = NSLocalizedString(@"Remote Host: %@", @"Remote host label format string");

Reachability *internetReachability = [Reachability reachabilityForInternetConnection];
[internetReachability startNotifier];

}

#pragma mark -----HUD Methods-----
-(void)hideHUD{
    
    [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
}

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        self.netAvailability = NO;

//        [TSMessage showNotificationWithTitle:@"Network error"
//                                    subtitle:@"Couldn't connect to the server. Check your network connection."
//                                        type:TSMessageNotificationTypeError];
    }
    else{
        
                   self.netAvailability = YES;
//                        [TSMessage showNotificationWithTitle:@"Success"
//                                    subtitle:@"Network connection is  availble now"
//                                        type:TSMessageNotificationTypeSuccess];

    }

   
   
}

-(void)bookOnline{
    
//        NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
//        if(loginVal.length){
//            
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//            OnlineBookindViewCon *lvc = [storyboard instantiateViewControllerWithIdentifier:@"OnlineBookindViewCon"];
//            [self.navigationController pushViewController:lvc animated:YES];
//
//                   }
//        else{
//            self.flgObj = @"onlineBookFlag";
//            [self login];
//        }
    
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

        NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
        if(loginVal.length){
            
            if(self.isMultilocation)
            {
                self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                
                LocationViewController *menu=(LocationViewController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LocationViewController"];
                [self.navigationController pushViewController:menu animated:YES];
            } else {
                self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                OnlineServicesViewController *onlineBookindViewCon = [storyboard instantiateViewControllerWithIdentifier:@"OnlineServicesViewController"];
                
                [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
                
                
            }
            
        }
        else{
            self.flgObj = @"onlineBookFlag";
            [self loginScreen];
        }

}

-(void)loginScreen{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    LoginVC *login = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:login animated:YES];
}

-(void)navigateToChooseDateClass:(NSMutableDictionary*)paramDict{
    NSString *loginVal = [self loadSharedPreferenceValue:@"slc_id"];
    if(loginVal.length){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        OnlineBookindViewCon *lvc = [storyboard instantiateViewControllerWithIdentifier:@"OnlineBookindViewCon"];
        lvc.paramDict = paramDict;
        [self.navigationController pushViewController:lvc animated:YES];
        
    }
    else{
        self.flgObj = @"onlineBookFlag";
        [self login];
    }
}

-(void)showNetworkAlert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Network error" message:@"Couldn't connect to the server. Check your network connection." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

@end
