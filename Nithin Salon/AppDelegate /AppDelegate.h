//
//  AppDelegate.h
//  Nithin Salon
//
//  Created by Nithin Reddy on 18/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import "Constants.h"
#import "UIViewController+NRFunctions.h"
#import <UserNotifications/UserNotifications.h>
#import "ModuleObj.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "MFSideMenu.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

//commit for text
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSString *pushToken, *salonId;
@property(nonatomic,assign)int giftcardType;
@property (nonatomic, retain) NSString *itunesUrl;
@property (nonatomic, retain) NSString *playUrl;
@property (nonatomic, retain) NSString *flgObj,*imageLinkURL;
@property (nonatomic, retain) NSMutableArray *dynamicApptList,*globalCheckAndUncheckArray,*walletArray,*globaldummyArray,*globalStaffDummyArray,*globalStaffListkArray;
@property (nonatomic, retain) NSString *salonEmail,*screenName;
@property (nonatomic, retain) NSString *salonPhone;
@property (nonatomic, retain) NSString *salonHours;
@property (nonatomic, retain) NSDate * globalImgData;
@property (nonatomic, retain) NSString *spaLatitude;
@property (nonatomic, retain) NSString *spaLongitude;
@property (nonatomic, retain) NSString *blockedDays,*moduleType;
@property (nonatomic, retain) NSString *onlineBookingUrl;
@property (nonatomic, retain) NSString *giftCardUrl;
@property (nonatomic, retain) UIImage *giftCardImage;
@property (nonatomic, retain) NSString *globalServiceComparisionStr;
@property (nonatomic, retain) NSString *buttonHideValue,*serviceNameStr;
@property (nonatomic, retain) ModuleObj *staffModel,*appoitmentsModel,*lastMinsMOdel,*servicesModel,*specialsModel,*eventsModel,*beveragesModel, *galleryModule;
-(BOOL)reachabilityChanged;
@property (nonatomic, retain)NSMutableDictionary *checInBeaconsDictionary, *reivewsDictionary, *onlineAppointmentBookDetails, *allRegisteredBeacons;
@property(nonatomic,retain)NSString *moduleID;
@property(nonatomic) BOOL netAvailability;
@property (nonatomic, assign)int totalCount;

@property (nonatomic,retain)  NSString *globalButtonNameStr;
@property (nonatomic, retain) NSString *loyaltyRegisterStr;
@property (nonatomic, retain) UINavigationController *navigationController;
@property (strong, nonatomic) MFSideMenuContainerViewController *container;

@property (nonatomic,assign) int topBarType;
@property (nonatomic,assign) float latitudeOfSalon, longitudeOfSalon;

@property (nonatomic,assign) BOOL isMultilocation, isSideMenuApp , isAnna;

@property (nonatomic, retain) NSString *onlinebookingType;

-(void) setUrls : (NSString *) itunesId PLAY:(NSString *)playPackage;
-(void)showNetworkAlert;
-(void)showHUD;
-(void)hideHUD;
-(void) setGeoFence;

-(void)loadReviews;
-(void)loadSpecials;
-(void)pendingAppointments;
-(void) loadApptResponse;
-(void) showAlert : (NSString *) title MSG:(NSString *) message;
-(void)scracthView;
-(void)events;
-(void)lastMinuteAppointment;
-(void) bookOnline;
-(NSString *)addSalonIdTo:(NSString*)urlPart;

-(void) saveSharedPreferenceValue : (NSString *)val KEY:(NSString *)key;
-(NSString *) loadSharedPreferenceValue : (NSString *)key;

@end

