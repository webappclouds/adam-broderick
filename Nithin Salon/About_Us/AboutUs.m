//
//  AboutUsViewCon.m
//  Restaurants_Webapp
//
//  Created by Nithin Reddy on 07/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AboutUs.h"
#import "Constants.h"
#import "QuartzCore/QuartzCore.h"
#import "StaffList.h"
#import "AppDelegate.h"
@implementation AboutUs
{
    int isRequestOrMailing;
    AppDelegate *appDelegate;
}

@synthesize flag;
@synthesize textView;

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
        // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.textView = Nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    isRequestOrMailing = 1;
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

    
    NSMutableString *urlString;
    
    if (self.flag==CONST_ABOUT_US) {
        self.title=@"About Us";
        
        urlString = (NSMutableString*)[appDelegate addSalonIdTo:URL_ABOUT_US];
        
    }
    else if (self.flag==CONST_HOURS) {
        self.title=@"Hours";
        urlString =(NSMutableString*) [appDelegate addSalonIdTo:URL_HOURS];
    }
    
    
    
    [textView.layer setCornerRadius:10.0];
    [textView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [textView.layer setBorderWidth:1.0];
    
    [appDelegate showHUD];
   [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSMutableDictionary * arrStringResponse=(NSMutableDictionary *)responseArray;
            [self serviceResponse:arrStringResponse];
            
        }
    }];
    
    [UIViewController addBottomBar:self];
}


#pragma mark -

-(IBAction)staff:(id)sender
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    StaffList *staffListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StaffList"];
    staffListVC.screenNameStr =@"";
    staffListVC.reqAppoitmnetDelegate=self;
    staffListVC.moduleObj = _moduleObj;
    [self.navigationController pushViewController:staffListVC animated:YES];
}

-(IBAction)mailingList:(id)sender
{
    isRequestOrMailing = 2;
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Email Address" message:@"Please enter your email address\n*By entering your email address, your email address is secure with us, and will not be shared to any third party." preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter email";
       // textField.secureTextEntry = YES;
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Canelled");
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Current password %@", [[alertController textFields][0] text]);
        
        NSString *enteredEmailText = [[alertController textFields][0] text];
        if([enteredEmailText length]==0||![UIViewController isValidEmail:enteredEmailText])
        {
            [self showAlert:@"Email Address" MSG:@"Please enter valid email address"];
            return ;
        }
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setObject:enteredEmailText forKey:@"email"];
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:URL_NEWSLETTER_SIGNUP] HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSMutableDictionary * arrStringResponse=(NSMutableDictionary *)responseArray;
                [self serviceResponse:arrStringResponse];
                
                
            }
        }];
        
        //compare the current password and do action here
        
    }];
   [alertController addAction:confirmAction];
    [alertController addAction:cancelAction];
    
   
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)serviceResponse:(NSMutableDictionary *)dict{
    
    NSMutableDictionary * arrStringResponse=(NSMutableDictionary *)dict;
    
    if (arrStringResponse==nil||arrStringResponse.count==0)
        [self showAlert:SALON_NAME MSG:@"Information not available. Please try again later."];
    else {
        
        if(isRequestOrMailing==1)
        {
            NSString *content = @"";
            if (self.flag==CONST_ABOUT_US)
                content = [[arrStringResponse objectForKey:@"config_aboutus"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
            else
                content = [[arrStringResponse objectForKey:@"config_hours"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
            
            [textView setEditable:YES];
            [textView setFont:[UIFont systemFontOfSize:16]];
            [textView setText:content];
            [textView setEditable:NO];
        }
        else
        {
            BOOL status = [[arrStringResponse objectForKey:@"msg"] boolValue];
            if(status)
                [self showAlert:@"Newsletter" MSG:@"You have been successfully added to our newsletter."];
            else
                [self showAlert:@"Mailing List" MSG:@"Error occured. Please try again later."];
        }
    }
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

}

@end
