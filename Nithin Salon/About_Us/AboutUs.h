//
//  AboutUsViewCon.h
//  Restaurants_Webapp
//
//  Created by Nithin Reddy on 07/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleObj.h"
@interface AboutUs : UIViewController 

@property (nonatomic, assign) int flag;

@property (nonatomic ,retain) IBOutlet UITextView *textView;
@property (nonatomic, retain) ModuleObj *moduleObj;
-(IBAction)staff:(id)sender;
-(IBAction)mailingList:(id)sender;

@end
