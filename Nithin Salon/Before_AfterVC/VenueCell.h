//
//  VenueCell.h
//  Wildcard
//
//  Created by Nithin Reddy on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueCell : UITableViewCell

@property(nonatomic,retain) IBOutlet UIButton *removeBefore;
@property(nonatomic,retain) IBOutlet UIButton *removeAfter;

@property(nonatomic,retain) IBOutlet UIImageView *before;
@property(nonatomic,retain) IBOutlet UIImageView *after;

@property (nonatomic, retain) id delegate;

@property (nonatomic, assign) SEL addBeforeImg;
@property (nonatomic, assign) SEL addAfterImg;
@property (nonatomic, assign) SEL removeBeforeImg;
@property (nonatomic, assign) SEL removeAfterImg;


-(IBAction)beforeClicked:(id)sender;
-(IBAction)afterClicked:(id)sender;

-(IBAction)removeBeforeClicked:(id)sender;
-(IBAction)removeAfterClicked:(id)sender;


@end
