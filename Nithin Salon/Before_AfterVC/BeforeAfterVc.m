//
//  BeforeAfterVc.m
//  Nithin Salon
//
//  Created by Webappclouds on 28/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "BeforeAfterVc.h"
#import "Constants.h"
#import "GoogleReviewVC.h"
#import "Data.h"
#import "VenueCell.h"
#import "SDImageCache.h"
#import "Match.h"
@interface BeforeAfterVc ()

@end

@implementation BeforeAfterVc


@synthesize tableView;

@synthesize webSheet;

@synthesize isBeforeImg;
@synthesize selectedIndex;
@synthesize arrList;


BOOL camera,swipe;


NSString *c1 = @"Take picture";
NSString *g1 = @"Choose from Library";

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title = @"Before & After";
    
    [super viewDidLoad];
   
    
    // Do any additional setup after loading the view, typically from a nib.
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Help" style:UIBarButtonItemStylePlain target:self action:@selector(help)];
    [nextButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = nextButton;
}
-(void)viewWillAppear:(BOOL)animated{
    swipe = NO;
     [self initView];
}
-(void) help
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    GoogleReviewVC *grvc = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [grvc setUrl:URL_BEFORE_AFTER];
    [grvc setTitle:@"Help"];
    [self.navigationController pushViewController:grvc animated:YES];
    
}

-(void) initView
{
    arrList = [[NSMutableArray alloc] init];
    
    NSMutableArray *loadSaveImg = [self loadSaveStandardUserDefaults];
    
    for(NSMutableArray *arr in loadSaveImg)
    {
        if(arr != NULL && [arr count] > 0)
        {
            Data *d = [[Data alloc] init];
            d.beforeImgName = [arr objectAtIndex:0];
            d.afterImgName = [arr objectAtIndex:1];
            [arrList addObject:d];
        
        }
    }
    
    [self reloadAllData];
}
-(void) reloadAllData
{
    self.arrList = [[self.arrList reverseObjectEnumerator] allObjects];
    
    if([arrList count]>0){
        
        if([arrList count]-2 != -1)
        {
            Data *oldData = [arrList objectAtIndex:[arrList count]-2];
            if(oldData.afterImgName == nil || oldData.afterImgName.length == 0)
            {
                [arrList removeObjectAtIndex:[arrList count]-1];
            }
        }
        Data *d = [arrList objectAtIndex:[arrList count]-1];
        if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
        {
            Data *data = [[Data alloc] init];
            data.beforeImgName = @"";
            data.afterImgName = @"";
            [arrList addObject:data];
           
        }
        
    }else{
        Data *data = [[Data alloc] init];
        data.beforeImgName = @"";
        data.afterImgName = @"";
        [arrList addObject:data];
       
    }
    
    self.arrList = [[self.arrList reverseObjectEnumerator] allObjects];
    
    NSMutableArray *saveArr = [[NSMutableArray alloc] init];
    for(Data *d in arrList)
    {
        if((d.beforeImgName != nil && d.beforeImgName.length > 0) || (d.afterImgName != nil  && d.afterImgName.length >0))
        {
            NSMutableArray *imgArr = [[NSMutableArray alloc] init];
            [imgArr addObject:d.beforeImgName];
            
            [imgArr addObject:d.afterImgName];
            
            [saveArr addObject:imgArr];
        }
    }
    if([saveArr count] > 0)
        [self saveStandardUserDefaults:saveArr];
    else
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"array"];
    }
    
    
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrList count];
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    VenueCell *cell = (VenueCell *)[self.tableView dequeueReusableCellWithIdentifier:@"VenueCell"];
    if (cell == nil) {
        cell = [[VenueCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VenueCell"];
    }
    
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VenueCell" owner:self options:nil];
    
    for (id currentObject in topLevelObjects){
        if ([currentObject isKindOfClass:[UITableViewCell class]]){
            cell =  (VenueCell *) currentObject;
            break;
        }
    }
    
    [cell setDelegate:self];
    [cell setAddAfterImg:@selector(addAfterImg:atIndexPath:)];
    [cell setAddBeforeImg:@selector(addBeforeImg:atIndexPath:)];
    
    Data *d = [arrList objectAtIndex:indexPath.row];
    
    //    UIImage *beforeImg = [self getImage:d.beforeImgName];
    //    UIImage *afterImg = [self getImage:d.afterImgName];
    
    
    NSString *beforeImgName = d.beforeImgName;
    NSString *afterImgName = d.afterImgName;
    
    
    cell.removeBefore.hidden = YES;
    cell.removeAfter.hidden = YES;
    
    if(indexPath.row == 0)
    {
        if(beforeImgName == nil || [beforeImgName isEqualToString:@""])
        {
            beforeImgName = @"before_icon.jpg";
            
        }else
        {
            cell.removeBefore.hidden = NO;
            [cell setRemoveBeforeImg:@selector(removeBeforeImg:atIndexPath:)];
        }
        if(afterImgName == nil ||[afterImgName isEqualToString:@""])
        {
            //            afterImg = [UIImage imageNamed:@"after_icon.png"];
            afterImgName = @"after_icon.jpg";
            
        }else
        {
            cell.removeAfter.hidden = NO;
            [cell setRemoveAfterImg:@selector(removeAfterImg:atIndexPath:)];
        }
        
    }
    
    if( indexPath.row == 1)
    {
        if((d.beforeImgName == nil || d.beforeImgName.length == 0) || (d.afterImgName == nil  || d.afterImgName.length == 0))
            
        {
            if(afterImgName != nil && ![afterImgName isEqualToString:@""])
            {
                cell.removeAfter.hidden = NO;
                [cell setRemoveAfterImg:@selector(removeAfterImg:atIndexPath:)];
            }
        }
    }
    
    UIImage *beforeImg, *afterImg;
    
    
    
    if(![beforeImgName isEqualToString:@"before_icon.jpg"])
    {
        [cell.before sd_setImageWithURL:[NSURL URLWithString:beforeImgName] placeholderImage:[UIImage imageNamed:@"Placeholder.png"]];
          [cell.before setContentMode:UIViewContentModeScaleAspectFit];
        
    } else
    {
        beforeImg = [UIImage imageNamed:beforeImgName];
        cell.before.image = beforeImg;
        [cell.before setContentMode:UIViewContentModeScaleAspectFit];
    }
    if(![afterImgName isEqualToString:@"after_icon.jpg"]){
        [cell.after sd_setImageWithURL:[NSURL URLWithString:afterImgName] placeholderImage:[UIImage imageNamed:@"Placeholder.png"]];
        [cell.after setContentMode:UIViewContentModeScaleAspectFit];
    }else{
        afterImg = [UIImage imageNamed:afterImgName];
        
        cell.after.image = afterImg;
        [cell.after setContentMode:UIViewContentModeScaleAspectFit];
    }
    
    return cell;
}

- (void)addAfterImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    Data *d = [arrList objectAtIndex:indexPath.row];
    if(d.afterImgName == nil || [d.afterImgName isEqualToString:@""])
    {
        selectedIndex = (int)indexPath.row;
        isBeforeImg = NO;
        [self takePhoto];
    }else
    {
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }
    
}
- (void)addBeforeImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Data *d = [arrList objectAtIndex:indexPath.row];
    if(d.beforeImgName == nil || [d.beforeImgName isEqualToString:@""])
    {
        
        selectedIndex = (int)indexPath.row;
        isBeforeImg = YES;
        
        [self takePhoto];
    }else
    {
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }
}


- (void)removeAfterImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Data *d = [arrList objectAtIndex:indexPath.row];
    [[SDImageCache sharedImageCache] removeImageForKey:d.afterImgName];
    d.afterImgName = @"";
    [self reloadAllData];
}
- (void)removeBeforeImg:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Data *d = [arrList objectAtIndex:indexPath.row];
    [[SDImageCache sharedImageCache] removeImageForKey:d.beforeImgName];
    d.beforeImgName = @"";
    [self reloadAllData];
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(swipe == NO){
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    Data *d = [self.arrList objectAtIndex:indexPath.row];
    if((d.beforeImgName != nil && d.beforeImgName.length > 0) && (d.afterImgName != nil  && d.afterImgName.length >0))
    {
        Match *mView = [self.storyboard instantiateViewControllerWithIdentifier:@"Match"];
        [mView setTitle:@"Match"];
        [mView setMatchData:d];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        [self.navigationController pushViewController:mView animated:YES];
    }
    }
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    
    if(indexPath.row == 0)
        return NO;
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Data *d = [self.arrList objectAtIndex:indexPath.row];
        
        [[SDImageCache sharedImageCache] removeImageForKey:d.beforeImgName];
        [[SDImageCache sharedImageCache] removeImageForKey:d.afterImgName];
        [self.arrList removeObjectAtIndex:indexPath.row];
        [self reloadAllData];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}



- (void) takePhoto
{
    NSString *title = @"Add Photo";
    NSString *btnTitle = nil;
    if(NO)
    {
        title = @"";
        btnTitle = @"Remove Current picture";
    }
    
    
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:g1 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
        [picker setDelegate:(id)self];
        picker.allowsEditing = YES;
        camera = NO;
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypePhotoLibrary])
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:nil];
    }]];
    
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:c1 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        camera = YES;
        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = YES;
        [picker setDelegate:(id)self];
        picker.sourceType = hasCamera ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }]];


        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];

    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
       return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    swipe = YES;
};
- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(nullable NSIndexPath *)indexPath {
    swipe =NO;
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

#pragma mark -
#pragma mark <UIImagePickerControllerDelegate> implementation

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    NSString *fileName = [self getNewFileName];
    [self saveImage:image FILE:fileName];
    Data *d = [self.arrList objectAtIndex:selectedIndex];
    if(isBeforeImg)
        d.beforeImgName = fileName;
    else
        d.afterImgName = fileName;
    [self reloadAllData];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}


- (void) saveStandardUserDefaults : (NSMutableArray *) arr
{
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:@"array"];
}



- (NSMutableArray *) loadSaveStandardUserDefaults
{
    NSMutableArray *arr = [[NSUserDefaults standardUserDefaults]objectForKey:@"array"];
    return arr;
}


- (void)saveImage : (UIImage *) image FILE : (NSString *) fileName
{
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",fileName,@".jpg"]];
    //    //    UIImage *image = imageView.image; // imageView is my image from camera
    //    NSData *imageData = UIImageJPEGRepresentation(image,1.0f);
    //    [imageData writeToFile:savedImagePath atomically:NO];
    
    
    [[SDImageCache sharedImageCache] storeImage:image forKey:fileName];
    
}

- (UIImage *)getImage  : (NSString *) fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",fileName,@".jpg"]];
    return [UIImage imageWithContentsOfFile:getImagePath];
}

-(NSString *) getNewFileName
{
    double d = [[NSDate date] timeIntervalSince1970];
    
    NSString * fileName = [NSString stringWithFormat:@"%lf", d];
    
    fileName = [fileName stringByReplacingOccurrencesOfString:@"." withString:@""];
    return  fileName;
}

@end
