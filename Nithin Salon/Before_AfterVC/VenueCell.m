//
//  VenueCell.m
//  Wildcard
//
//  Created by Nithin Reddy on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VenueCell.h"

@implementation VenueCell

@synthesize before, after;
@synthesize delegate;
@synthesize addAfterImg,addBeforeImg;
@synthesize removeAfter,removeBefore;
@synthesize removeAfterImg,removeBeforeImg;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}


-(IBAction)beforeClicked:(id)sender
{
    if (delegate) {
        NSIndexPath *indexPath = [(UITableView *)self.superview.superview indexPathForCell: self];
        [delegate performSelector:addBeforeImg withObject:self withObject:indexPath];
    }
}

-(IBAction)afterClicked:(id)sender
{
    if (delegate) {
        NSIndexPath *indexPath = [(UITableView *)self.superview.superview indexPathForCell: self];
        [delegate performSelector:addAfterImg withObject:self withObject:indexPath];
    }
}

-(IBAction)removeBeforeClicked:(id)sender
{
    if (delegate) {
        NSIndexPath *indexPath = [(UITableView *)self.superview.superview indexPathForCell: self];
        [delegate performSelector:removeBeforeImg withObject:self withObject:indexPath];
    }
}

-(IBAction)removeAfterClicked:(id)sender
{
    if (delegate) {
        NSIndexPath *indexPath = [(UITableView *)self.superview.superview indexPathForCell: self];
        [delegate performSelector:removeAfterImg withObject:self withObject:indexPath];
    }
}


@end
