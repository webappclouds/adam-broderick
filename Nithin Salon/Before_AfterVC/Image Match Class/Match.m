//
//  Match.m
//  ImageAlbum
//
//  Created by Ashish on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Match.h"
#import "SDImageCache.h"
#import <MessageUI/MessageUI.h>
#import "Constants.h"
#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>
#import <Twitter/TWTweetComposeViewController.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import "UIViewController+NRFunctions.h"
#import "AppDelegate.h"

@implementation Match
{
    NSData *beforeImage;
    NSData *afterImage;
    AppDelegate * objAppdel;
}
@synthesize beforeView,afterView,matchData;

@synthesize before, after;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    before = [[ImageZoomingViewController alloc] initWithNibName:@"ImageZoomingViewController" bundle:nil];
    [before setZoomImage:[self getImage:matchData.beforeImgName]];
    [self.beforeView addSubview:before.view];
    
    after = [[ImageZoomingViewController alloc] initWithNibName:@"ImageZoomingViewController" bundle:nil];
    [after setZoomImage:[self getImage:matchData.afterImgName]];
    [self.afterView addSubview:after.view];
    
    
    beforeImage = UIImageJPEGRepresentation([self getImage:matchData.beforeImgName], 0.5) ;
    afterImage = UIImageJPEGRepresentation([self getImage:matchData.afterImgName], 0.5);

    
    [self.beforeView addSubview:self.beforeLabel];
    [self.afterView addSubview:self.afterLabel];
    [self.beforeLabel bringSubviewToFront:self.beforeView];
    [self.afterLabel bringSubviewToFront:self.afterView];
    
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title = @"My Gallery";
    
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(shared)];
    nextButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = nextButton;
    
    
}
-(void)viewWillAppear:(BOOL)animated{
   
}
-(void) shared
{
    NSString *str = @"BEFORE & AFTER";
    
   
    
    NSString *other1 = @"Other Button 1";
    NSString *other2 = @"Other Button 2";
    NSString *other3 = @"Other Button 3";
    NSString *other4 = @"Other Button4";
    
    if([self isSocialAvailable])
    {
        other1 = @"Facebook";
        other2 = @"Twitter";
        other3 = @"Mail";
        other4 = @"Message";
    }
    else
    {
        other1 = @"Mail";
        other2 = Nil;
        other3 = Nil;
    }
    
    NSString *actionSheetTitle = @"Share via";
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:actionSheetTitle message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Message" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self callSocialView:@"Message"];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Send to Salon" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        [self displayComposerSheet:objAppdel.salonEmail];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:other1 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self callSocialView:other1];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:other2 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self callSocialView:other2];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:other3 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self callSocialView:other3];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(BOOL)isTwitterAvailable {
    return NSClassFromString(@"TWTweetComposeViewController") != nil;
}

-(BOOL)isSocialAvailable {
    return NSClassFromString(@"SLComposeViewController") != nil;
}

-(void)callSocialView:(NSString *)choice
{
    
    NSString *emailBody = [NSString stringWithFormat:@"Check out my before and after images from the %@ App\n%@", SALON_NAME, [UIViewController returnShareUrl]];
    if ([choice isEqualToString:@"Facebook"])
    {
        UIImage * bfImage = [self drawText:@"Before" inImage:[UIImage imageWithData:beforeImage ]];
        UIImage * afImage = [self drawText:@"After" inImage:[UIImage imageWithData:afterImage ]];
        
        SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [composeViewController setInitialText:emailBody];
        [composeViewController addImage:bfImage];
        [composeViewController addImage:afImage];
        [self.navigationController presentViewController:composeViewController animated:YES completion:nil];
    }
    else if([choice isEqualToString:@"Twitter"])
    {
        UIImage * bfImage = [self drawText:@"Before" inImage:[UIImage imageWithData:beforeImage ]];
        UIImage * afImage = [self drawText:@"After" inImage:[UIImage imageWithData:afterImage ]];
        
        SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [composeViewController setInitialText:emailBody];
        [composeViewController addImage:bfImage];
        [composeViewController addImage:afImage];
        
        [self.navigationController presentViewController:composeViewController animated:YES completion:nil];
        
    }
    else if([choice isEqualToString:@"Mail"])
    {
        [self displayComposerSheet:objAppdel.salonEmail];
    }
    else if([choice isEqualToString:@"Send to Salon"])
    {
       
        [self displayComposerSheet:objAppdel.salonEmail];
    }
    else if([choice isEqualToString:@"Message"])
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = emailBody;
            
            UIImage * bfImage = [self drawText:@"Before" inImage:[UIImage imageWithData:beforeImage ]];
            UIImage * afImage = [self drawText:@"After" inImage:[UIImage imageWithData:afterImage ]];

            [controller addAttachmentData: UIImagePNGRepresentation(bfImage) typeIdentifier:@"public.data" filename:@"image.png"];
            [controller addAttachmentData:UIImagePNGRepresentation(afImage) typeIdentifier:@"public.data" filename:@"image1.png"];
            
            controller.recipients = nil;
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}



-(void)displayComposerSheet : (NSString *) email
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        NSString *str = [NSString stringWithFormat:@"Images from %@ App", SALON_NAME];
        
        if(email!=nil && [email length]>0)
        {
            NSArray *toRecipents = [NSArray arrayWithObject:email];
            [mailer setToRecipients:toRecipents];
        }
        
        [mailer setSubject:str];
        
        
        
       
       
        if(beforeImage.length){
             NSData *beforeImage = UIImageJPEGRepresentation([self drawText:@"Before" inImage:[self getImage:self.matchData.beforeImgName]] , 0.5) ;
            [mailer addAttachmentData:beforeImage mimeType:@"image/jpeg" fileName:@"before"];
        }
        if(afterImage.length){
             NSData *afterImage = UIImageJPEGRepresentation([self drawText:@"After" inImage:[self getImage:self.matchData.afterImgName]] , 0.5) ;
            [mailer addAttachmentData:afterImage mimeType:@"image/jpeg" fileName:@"after"];
        }
        NSString *emailBody = [NSString stringWithFormat:@"Check out my before and after images from the %@ App\n%@", SALON_NAME, [UIViewController returnShareUrl]];
        
        AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        if([email isEqualToString:objAppdel.salonEmail])
            emailBody = [NSString stringWithFormat:@"Check out my before and after images from the %@ App\n", SALON_NAME];
        
        [mailer setMessageBody:emailBody isHTML:NO];
        [self presentViewController:mailer animated:YES completion:nil];
        
    }
    
    else
    {
        
        [self showAlert:@"Failure" MSG:@"Email not configured. Please try again." tag:0];
        
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //    beforeView = nil;
    //    afterView = nil;
    //    after = nil;
    //    before = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown));
    
}
//- (UIImage *)getImage  : (NSString *) fileName
//{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",fileName,@".jpg"]];
//    return [UIImage imageWithContentsOfFile:getImagePath];
//}


- (UIImage *)getImage  : (NSString *) fileName
{
    NSLog(@"file Nmae: %@",fileName);
    return [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:fileName];
}


-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
{
    UIFont *font = [UIFont boldSystemFontOfSize:20];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(100, 10, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys: font, NSFontAttributeName,
                                nil];
    [text drawInRect:CGRectIntegral(rect) withAttributes:dictionary];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    return;
}


@end
