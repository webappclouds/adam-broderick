//
//  ReviewUsVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 21/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormViewController.h"
#import "XLForm.h"
@interface ReviewUsVC : XLFormViewController
+(void) setRate:(int)rate;
@property(nonatomic, retain)NSString *comparisonFlag;
@end
