//
//  ReviewUsVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ReviewUsVC.h"
#import "DYRateView.h"
#import "Constants.h"
#import "GoogleReviewVC.h"
#import "AppDelegate.h"
#import "MyGalleryViewController.h"
#import "ViewController.h"
NSString *const kRateView = @"CustomRateView";
@interface ReviewUsVC ()
{
    NSString *ratingVal;
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section,*section1,*section2,*section3,*section4;
    XLFormRowDescriptor *rateViewRow,*nameRow,*emailRow,*titleRow,*notesRow,*submitRow,*termsRow;
    AppDelegate *appDelegate;
    UIButton *transparentButton ;
}
@end

@implementation ReviewUsVC

@synthesize comparisonFlag;
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

-(void)initializeForm
{
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    formDescriptor.assignFirstResponderOnShow = NO;
    
    //section ( rate view)
    section = [XLFormSectionDescriptor formSectionWithTitle:@"HOW DO YOU RATE US? TAP ON THE STARS TO RATE US."];
    [formDescriptor addFormSection:section];
    rateViewRow = [XLFormRowDescriptor formRowDescriptorWithTag:kRateView rowType:XLFormRowDescriptorTypeRateView title:@""];
   
    rateViewRow.height=60;
    
    [section addFormRow:rateViewRow];
    
    
    // section1 (Email & Footer & name)
    
    //    section1 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    //    [formDescriptor addFormSection:section1];
    nameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"name" rowType:XLFormRowDescriptorTypeText title:@"Nickname"];
    [nameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    nameRow.height =44;
    nameRow.required =YES;
    [nameRow.cellConfigAtConfigure setObject:@"Enter Nickname" forKey:@"textField.placeholder"];
    [section addFormRow:nameRow];
    
    emailRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"email" rowType:XLFormRowDescriptorTypeEmail title:@"Email"];
    // validate the email
    [emailRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    emailRow.required = YES;
    [emailRow addValidator:[XLFormValidator emailValidator]];
    emailRow.height=44;
    [emailRow.cellConfigAtConfigure setObject:@"Enter email" forKey:@"textField.placeholder"];
    [section addFormRow:emailRow];
    section.footerTitle =@"Your mail will not appear in the review.";
    
    
    //Section2 (textfiled & Textview)
    section2 = [XLFormSectionDescriptor formSectionWithTitle:@"Review Information"];
    [formDescriptor addFormSection:section2];
    titleRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"title" rowType:XLFormRowDescriptorTypeText title:@"Your Title"];
    [titleRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    titleRow.height =44;
    titleRow.required =YES;
    [titleRow.cellConfigAtConfigure setObject:@"Enter title" forKey:@"textField.placeholder"];
    [section2 addFormRow:titleRow];
    
    notesRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Your review" rowType:XLFormRowDescriptorTypeTextView title:@""];
    notesRow.height=100;
    [notesRow.cellConfigAtConfigure setObject:@"Enter review text" forKey:@"textView.placeholder"];
    [section2 addFormRow:notesRow];
    
    
    
    //section 3 (button)
    section3 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section3];
    submitRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Submit"];
    [submitRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [submitRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    submitRow.action.formSelector = @selector(submitAllData);
    [section3 addFormRow:submitRow];
    
    
    //section 4 (button)
    section4 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section4];
    termsRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Terms and conditions"];
    [termsRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [termsRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    termsRow.action.formSelector = @selector(termsAndConditions);
    [section4 addFormRow:termsRow];
    
    
    if([self loadSharedPreferenceValue:@"RegName"]){
        nameRow.value = [self loadSharedPreferenceValue:@"RegName"]?:@"";
    }
    if([self loadSharedPreferenceValue:@"RegEmail"]){
        emailRow.value = [self loadSharedPreferenceValue:@"RegEmail"]?:@"";
    }
    
    self.form = formDescriptor;
    
    
    
}


-(void)termsAndConditions{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    GoogleReviewVC *googlePlus = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [googlePlus setUrl:URL_REVIEWS_TNC];
    [googlePlus setTitle:@"Terms"];
    [self.navigationController pushViewController:googlePlus animated:YES];
}
-(void)submitAllData{
    
    NSString *emailStr = [emailRow displayTextValue];
    NSString *trimmedEmailString = [emailStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    if([[nameRow displayTextValue] isEqualToString:@""] ||[nameRow displayTextValue].length==0)
        [self showAlert:@"Nickname" MSG:@"Please enter your nickname" tag:0];
    else if([emailStr length]==0)
        [self showAlert:@"Email" MSG:@"Please enter your email" tag:0];
    else if(![UIViewController isValidEmail:trimmedEmailString]){
      
          [self showAlert:@"Email" MSG:@"Please enter valid email" tag:0];
    }
    else if([[titleRow displayTextValue] isEqualToString:@""]||[titleRow displayTextValue].length==0)
        [self showAlert:@"Review title" MSG:@"Please enter the review title" tag:0];
    else if([[notesRow displayTextValue] isEqualToString:@""]||[notesRow displayTextValue].length==0)
        [self showAlert:@"Review text" MSG:@"Please enter the review text" tag:0];
    
    else{
        [appDelegate showHUD];
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [paramDic setObject:appDelegate.salonId forKey:@"salon_id"];
        [paramDic setObject:[titleRow displayTextValue]?:@"" forKey:@"title"];
        [paramDic setObject:[nameRow displayTextValue]?:[self loadSharedPreferenceValue:@"RegName"] forKey:@"name"];
        [paramDic setObject:[notesRow displayTextValue] forKey:@"description"];
        [paramDic setObject:ratingVal forKey:@"rating"];
        [paramDic setObject:[emailRow displayTextValue]?: [self loadSharedPreferenceValue:@"RegEmail"] forKey:@"email"];
        
        

        
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)URL_REVIEW_SAVE HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSDictionary *dict = (NSDictionary *)responseArray;
                BOOL status = [[dict objectForKey:@"status"] boolValue];
                if(status)
                {
                    if([comparisonFlag isEqualToString:@"FromPush"]&&[ratingVal intValue]>3){
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Gallery" bundle: nil];
                        
                        
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Would you like to share your after picture" preferredStyle:UIAlertControllerStyleAlert];
                        [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                             self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
                            MyGalleryViewController *mgvc = [storyboard instantiateViewControllerWithIdentifier:@"MyGalleryViewController"];
                            mgvc.flag=1;
                            [self.navigationController pushViewController:mgvc animated:YES];

                        }]];
                        
                        [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                           //[self.navigationController popto];
                            
                        }]];

                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    else{
                        [self showAlert:@"Success" MSG:@"Your review has been sent to the salon. Thank you for taking the time to review us." tag:1];
                        NSString *reviewId = [ NSString stringWithFormat:@"%@",[dict objectForKey:@"review_id"]];
                        [self loadSharedPreferenceValue:reviewId];
                    }


                        
                    
                    }
                                }}];
    
    }
}
-(void)viewWillAppear:(BOOL)animated{
    transparentButton = [[UIButton alloc] init];
    [transparentButton setFrame:CGRectMake(0,0, 50, 40)];
    [transparentButton setBackgroundColor:[UIColor clearColor]];
    [transparentButton addTarget:self action:@selector(backk) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:transparentButton];

    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(ratingVal:) name:@"RatingStatus" object:nil];
    ratingVal =@"5";
}
- (void) backk{
    
    [transparentButton removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)ratingVal:(NSNotification*)notification
{
    ratingVal = [NSString stringWithFormat:@"%@",notification.object];
    ratingVal = [ratingVal stringByReplacingOccurrencesOfString:@"Rate: " withString:@""];
    NSLog(@"Rate: %@",ratingVal);
    
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Review us";
   
    // Do any additional setup after loading the view.
}

- (void)backNavigate{
    
    [transparentButton removeFromSuperview];
    if([comparisonFlag isEqualToString:@"FromPush"]){
        for(UIViewController *controller in self.navigationController.viewControllers){
            if([controller isKindOfClass:[ViewController class]]){
                
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [transparentButton removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
