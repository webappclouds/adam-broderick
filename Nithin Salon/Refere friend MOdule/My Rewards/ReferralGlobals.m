//
//  ReferralGlobals.m
//  DJW
//
//  Created by Nithin Reddy on 30/03/13.
//
//

#import "ReferralGlobals.h"

@implementation ReferralGlobals

+(NSMutableArray *)getVoucherCodes
{
    NSArray *giftcardsList;
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath2 = [documentsDirectory stringByAppendingPathComponent:@"Referrals.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath2]){
		giftcardsList = [NSArray arrayWithContentsOfFile: plistPath2];
	} else
		giftcardsList = [[NSArray alloc] init];
    
    NSMutableArray *mast = [[NSMutableArray alloc] initWithArray:giftcardsList];
    return mast;
}

+(BOOL) isExistsVoucher: (NSString *) voucher
{
    if([[self getVoucherCodes] containsObject:voucher])
        return TRUE;
    return FALSE;
}

+(void) addVoucher:(NSString *) word
{
    NSMutableArray *arr = [self getVoucherCodes];
    if(![arr containsObject:word])
        [arr addObject:word];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"Referrals.plist"];
    [arr writeToFile:plistPath atomically:YES];
}

+(void) removeVoucher:(NSString *) word
{
    NSMutableArray *arr = [self getVoucherCodes];
    if([arr containsObject:word])
        [arr removeObject:word];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"Referrals.plist"];
    [arr writeToFile:plistPath atomically:YES];
}

@end
