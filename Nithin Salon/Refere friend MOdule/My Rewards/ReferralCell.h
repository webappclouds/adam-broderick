//
//  ReferralCell.h
//  DJW
//
//  Created by Nithin Reddy on 31/03/13.
//
//

#import <UIKit/UIKit.h>

@interface ReferralCell : UITableViewCell
{
    UILabel *label1;
    UILabel *label2;
    UILabel *label3;


    UIImageView *img;
    UIButton *moreInfo;
}

@property(nonatomic,retain)	IBOutlet UILabel *label1;
@property(nonatomic,retain)	IBOutlet UILabel *label2;
@property(nonatomic,retain)	IBOutlet UILabel *label3;
@property(nonatomic,retain) IBOutlet UIImageView *img;
@property(nonatomic,retain) IBOutlet UIButton *moreInfo;

@end
