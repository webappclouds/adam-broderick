//
//  StaffListViewCon.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnlineBookindViewCon.h"

@interface StaffListViewCon : UIViewController

@property (nonatomic,retain) NSMutableArray *selectSubServicesArray;

@property (nonatomic,retain) NSMutableArray *staffListData;

@property (nonatomic,retain) NSString *selectedIdStr;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic,retain) IBOutlet UITableView *staffListTableView;

@property (nonatomic,retain)OnlineBookindViewCon *onlineView;


@end
