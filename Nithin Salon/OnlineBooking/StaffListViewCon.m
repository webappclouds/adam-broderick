//
//  StaffListViewCon.m
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import "StaffListViewCon.h"
#import "ChooseDateViewCon.h"
#import "Constants.h"
#import "Staff.h"
#import "AppDelegate.h"
@interface StaffListViewCon ()
{
    AppDelegate *appDelegate;
    //NSString *selectedIdStr;
}

@end

@implementation StaffListViewCon
@synthesize selectSubServicesArray,staffListData;
@synthesize staffListTableView,selectedIdStr;

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title = @"Staff";
     self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    staffListTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
    right.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = right;
    
    staffListData = [[NSMutableArray alloc] init];
    
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:selectedIdStr forKey:@"service_iid"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",[appDelegate addSalonIdTo:SERVICES_STAFF_LIST]];
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSDictionary *dict = (NSDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
    
    // Do any additional setup after loading the view from its nib.
}


-(void) showAlert : (NSString *) title MSG:(NSString *) message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)closeView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark -
#pragma mark SendRequest delegate
-(void)getResponse:(NSDictionary *)dict
{
    
    NSLog(@"Staff list :%@",dict);
    int status =[[dict objectForKey:@"status"] intValue];
    
    if (dict.count==0|| status == 0 )
        [self showAlert:SALON_NAME MSG:@"No staff found. Please try again later."];
    
    else {
        NSMutableArray *arrJson = [dict objectForKey:@"serviceproviders"];
        
        for (int i=0; i<[arrJson count]; i++) {
            NSDictionary *empDict = [arrJson objectAtIndex:i];
            Staff *staffObj = [[Staff alloc] init];
            [staffObj setEmployeeId:[empDict objectForKey:@"employee_iid"]];
            [staffObj setEmployeeFName:[empDict objectForKey:@"firstname"]];
            [staffObj setEmployeeLName:[empDict objectForKey:@"lastname"]];
            [staffListData addObject:staffObj];
        }
        [staffListTableView reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [staffListData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *iden = @"Cell";
    
    UITableViewCell *cell = [staffListTableView dequeueReusableCellWithIdentifier:iden];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:iden];
    }
    
    Staff *staffObj = [staffListData objectAtIndex:indexPath.row];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.font=[UIFont systemFontOfSize:14.0];
    cell.textLabel.text=[NSString stringWithFormat:@"%@ %@",staffObj.employeeFName,staffObj.employeeLName];
    [cell setBackgroundColor:[UIColor clearColor]];
    //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.staffListTableView deselectRowAtIndexPath:indexPath animated:YES];
    Staff *obj = [staffListData objectAtIndex:indexPath.row];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:obj.employeeId forKey:@"EmpId"];
    [paramDic setObject:selectedIdStr forKey:@"SerId"];
    [paramDic setObject:obj.employeeFName forKey:@"EmpFName"];
    [paramDic setObject:obj.employeeLName forKey:@"EmpLName"];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.onlineView callBackStaffList:paramDic];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
