//
//  ServicesListViewCon.m
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import "ServicesListViewCon.h"
#import "SubServicesListViewCon.h"
#import "Services.h"
#import "Constants.h"
#import "AppDelegate.h"
@interface ServicesListViewCon ()
{
    AppDelegate *appDelegate;
}
@end

@implementation ServicesListViewCon
@synthesize  servicesListData;
@synthesize tableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title=@"Categories";
     self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
    right.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = right;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    servicesListData = [[NSMutableArray alloc] init];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@",[appDelegate addSalonIdTo:SERVICES_LIST]];
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)closeView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark SendRequest delegate

-(void)getResponse:(NSMutableDictionary *)dict
{
    
    int status =[[dict objectForKey:@"status"] intValue];
    
    if (dict==nil||dict.count==0 || status == 0)
        [self showAlert:SALON_NAME MSG:@"No categories found. Please try again later." tag:2];
    
    else
    {
        NSMutableArray *catArray = [dict objectForKey:@"servicetypes"];
        for(int i=0; i<[catArray count]; i++)
        {
            Services *serObj = [[Services alloc] init];
            [serObj setCategoryId:[[catArray objectAtIndex:i] objectForKey:@"category_iid"]];
            [serObj setCategoryName:[[catArray objectAtIndex:i] objectForKey:@"category_cclassname"]];
            [servicesListData addObject:serObj];
        }
        [self.tableView reloadData];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [servicesListData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *iden = @"Cell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:iden];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:iden];
    }
    
    Services *serObj = [servicesListData objectAtIndex:indexPath.row];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.font = [UIFont fontWithName:@"Open Sans" size:16];
    cell.textLabel.text=[NSString stringWithFormat:@"%@",serObj.categoryName];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Services *obj = [servicesListData objectAtIndex:indexPath.row];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:obj.categoryName forKey:@"CatName"];
    [paramDic setObject:obj.categoryId forKey:@"CatId"];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.onlineView callBackMethode:paramDic];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
