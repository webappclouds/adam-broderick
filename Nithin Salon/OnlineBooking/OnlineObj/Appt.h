//
//  ApptObj.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 20/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Appt : NSObject
@property (nonatomic, retain) NSString *serviceId;
@property (nonatomic, retain) NSString *employeeId;

@property (nonatomic, retain) NSString *startLength;
@property (nonatomic, retain) NSString *gapLength;
@property (nonatomic, retain) NSString *finishLength;
@property (nonatomic, retain) NSString *apptType;
@property (nonatomic, retain) NSString *resourceId;
@property (nonatomic, retain) NSString *genderId;

@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *service;
@property (nonatomic, retain) NSString *employee;
@property (nonatomic, retain) NSString *fromTime;
@property (nonatomic, retain) NSString *toTime;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *mDisplayPrice;



@end
