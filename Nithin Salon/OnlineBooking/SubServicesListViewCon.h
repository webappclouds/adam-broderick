//
//  SubServicesListViewCon.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnlineBookindViewCon.h"

@interface SubServicesListViewCon : UIViewController <UISearchBarDelegate>

@property (nonatomic,retain) IBOutlet UITableView *aTableView;
@property (nonatomic,retain) IBOutlet UISearchBar *searchBar;

@property (nonatomic,retain) NSMutableArray *subServicesdata;
@property (nonatomic,retain) NSMutableArray *data;

@property (nonatomic,retain) NSString *categoryIdStr;

@property (nonatomic,retain)OnlineBookindViewCon *onlineView;



@end
