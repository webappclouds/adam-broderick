//
//  ChooseDateViewCon.m
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 19/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import "ChooseDateViewCon.h"
#import "Appt.h"
#import "Constants.h"
#import "SelectAppt.h"
#import "Constants.h"
#import "UIViewController+NRFunctions.h"
#import "AppDelegate.h"
#import <EventKit/EventKit.h>
@interface ChooseDateViewCon ()
{
    NSArray *dateOptionsArray;
    NSArray *timeOptionsArray;
    int servicecall;
    NSMutableArray *serIdArray;
    NSMutableArray *empIdArray;
    
    
    UIActionSheet *actionSheetView;
    UIDatePicker *pickerView;
    UIToolbar *pickerToolbar;
    
    int datecall;
    int timecall;
    
    int customDate;
    
    int dividerCount;
    
    BOOL timeValue,dateValue;
    AppDelegate *appDelegate;
    NSString *bookedServiceName,*bookedProvider,*bookedTtime,*bookedFromTIme,*bookedToTime;
    NSDate *bookedDate;
}

@end

@implementation ChooseDateViewCon
//@synthesize serviceIdStr, empIdStr;
@synthesize dateButton,timeButton,apptTableView;
@synthesize apptData,selectedApptData;
@synthesize fromDate,toDate,dateOnLable;
@synthesize fromTime,toTime,timeOnLable;
@synthesize fromDateStr, twoDateStr, fromTimeStr, toTimeStr;
@synthesize numberArray,fromPushStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    serIdArray=[[NSMutableArray alloc] init];
    empIdArray=[[NSMutableArray alloc] init];
    numberArray=[[NSMutableArray alloc] init];
    // self.apptTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    fromDate.hidden=YES;
    toDate.hidden=YES;
    dateOnLable.hidden=YES;
    
    fromTime.hidden=YES;
    toTime.hidden=YES;
    timeOnLable.hidden=YES;
    
    //self.apptTableView.estimatedRowHeight = 100;
    // self.apptTableView.rowHeight=UITableViewAutomaticDimension;
    
    for (SelectAppt *obj in selectedApptData) {
        [serIdArray addObject:obj.serId];
        [empIdArray addObject:obj.empId];
        NSLog(@"%@",serIdArray);
        NSLog(@"%@",empIdArray);
    }
    
    self.title=@"Book Online";
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    apptTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self dateAndTimeOptions];
    
    apptData = [[NSMutableArray alloc] init];
    
    actionSheetView = [[UIActionSheet alloc]init];
    pickerToolbar = [[UIToolbar alloc] init];
    pickerView = [[UIDatePicker alloc] init];
    
    
    _dateViewHeight.constant  = 10;
    _dateView.hidden =YES;
    _timeViewHeight.constant = 10;
    _timeView.hidden = YES;
    
    _upViewHeight.constant = _dateButtonHeight.constant+_timeButtonHeight.constant+_checkAvailabilityHeight.constant+_dateViewHeight.constant+_timeViewHeight.constant+50;
    
    if(fromPushStr.length){
        [dateButton setTitle:@"Today" forState:UIControlStateNormal];
        [timeButton setTitle:@"Anytime" forState:UIControlStateNormal];
        [appDelegate showHUD];
        [self clickAvailability:nil];
    }
    
    // Custom
}

-(void)dateAndTimeOptions
{
    dateOptionsArray=[[NSArray alloc] initWithObjects:@"Today", @"First Available", @"Tomorrow", @"This Week",@"Pick a Date Range",nil];
    timeOptionsArray=[[NSArray alloc] initWithObjects:@"Anytime", @"Morning (Before 12PM)", @"Afternoon (12PM to 5PM)", @"Evening (After 5PM)",@"Pick a Time Range",nil];
}

-(void)closeView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)selectDate
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Choose one" preferredStyle:UIAlertControllerStyleAlert];
    for(NSString *dateStr in dateOptionsArray)
    {
        [alertController addAction:[UIAlertAction actionWithTitle:dateStr style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if ([dateStr isEqualToString:@"Pick a Date Range"]) {
                [dateButton setTitle:dateStr forState:UIControlStateNormal];
                fromDate.hidden=NO;
                toDate.hidden=NO;
                dateOnLable.hidden=NO;
                
                
                _dateViewHeight.constant  = 40;
                _dateView.hidden =NO;
                dateValue = YES;
                _upViewHeight.constant = _dateButtonHeight.constant+_timeButtonHeight.constant+_checkAvailabilityHeight.constant+_dateViewHeight.constant+_timeViewHeight.constant+50;
                
            }
            else{
                [dateButton setTitle:dateStr forState:UIControlStateNormal];
                fromDate.hidden=YES;
                toDate.hidden=YES;
                dateOnLable.hidden=YES;
                
                _dateViewHeight.constant  = 10;
                _dateView.hidden =YES;
                dateValue = NO;
                _upViewHeight.constant = _dateButtonHeight.constant+_timeButtonHeight.constant+_checkAvailabilityHeight.constant+_dateViewHeight.constant+_timeViewHeight.constant+50;
                
            }
        }]];
    }
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
    }]];
    [self presentViewController:alertController animated:YES completion:Nil];
}

-(void)selectTime
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Choose one" preferredStyle:UIAlertControllerStyleAlert];
    for( NSString *timeStr in timeOptionsArray)
    {
        [alertController addAction:[UIAlertAction actionWithTitle:timeStr style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if ([timeStr isEqualToString:@"Pick a Time Range"]) {
                [timeButton setTitle:timeStr forState:UIControlStateNormal];
                fromTime.hidden=NO;
                toTime.hidden=NO;
                _timeViewHeight.constant = 40;
                _timeView.hidden = NO;
                _timeONLabel.hidden =NO;
                
                timeValue = YES;
                _upViewHeight.constant = _dateButtonHeight.constant+_timeButtonHeight.constant+_checkAvailabilityHeight.constant+_dateViewHeight.constant+_timeViewHeight.constant+50;
                
            }
            else{
                [timeButton setTitle:timeStr forState:UIControlStateNormal];
                fromTime.hidden=YES;
                toTime.hidden=YES;
                timeOnLable.hidden=YES;
                timeValue = NO;
                _timeViewHeight.constant = 10;
                _timeONLabel.hidden =YES;
                
                _upViewHeight.constant = _dateButtonHeight.constant+_timeButtonHeight.constant+_checkAvailabilityHeight.constant+_dateViewHeight.constant+_timeViewHeight.constant+50;
            }
        }]];
    }
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
    }]];
    [self presentViewController:alertController animated:YES completion:Nil];
}


-(IBAction)clickDateBut:(id)sender
{
    [self selectDate];
}
-(IBAction)clickTimeBut:(id)sender
{
    [self selectTime];
}

-(IBAction)fromDate:(id)sender{
    apptTableView.hidden=YES;
    customDate=1;
    datecall=1;
    NSDate *todaydate=[NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    fromDateStr = [[NSString alloc] initWithString:[dateFormatter stringFromDate:todaydate]];
    [pickerView setDate:[NSDate date]];
    [pickerView setMinimumDate:[NSDate date]];
    [self datePickerView];
}

-(IBAction)toDate:(id)sender{
    apptTableView.hidden=YES;
    customDate=1;
    datecall=0;
    NSDate *todaydate=[NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    twoDateStr = [[NSString alloc] initWithString:[dateFormatter stringFromDate:todaydate]];
    [pickerView setDate:[NSDate date]];
    [pickerView setMinimumDate:[NSDate date]];
    
    //twoDateStr= [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:todaydate]];
    [self datePickerView];
}


-(IBAction)fromTimeClick:(id)sender{
    apptTableView.hidden=YES;
    customDate=2;
    timecall=1;
    NSDate *todaydate=[NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    fromTimeStr = [[NSString alloc] initWithString:[dateFormatter stringFromDate:todaydate]];
    //fromTimeStr= [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:todaydate]];
    [pickerView setDate:[NSDate date]];
    [self datePickerView];
}

-(IBAction)toTimeClick:(id)sender{
    apptTableView.hidden=YES;
    //datecall=2;
    customDate=2;
    timecall=0;
    NSDate *todaydate=[NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    toTimeStr = [[NSString alloc] initWithString:[dateFormatter stringFromDate:todaydate]];
    // twoTimeStr= [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:todaydate]];
    [pickerView setDate:[NSDate date]];
    [self datePickerView];
}



-(void)datePickerView{
    
    
    [actionSheetView setDelegate:self];
    [actionSheetView setFrame:CGRectMake(0,self.view.frame.size.height-250,self.view.frame.size.width, 250)];
    
    if (customDate==1) {
        pickerView.datePickerMode = UIDatePickerModeDate;
    }
    else{
        pickerView.datePickerMode = UIDatePickerModeTime;
    }
    
    [pickerView setFrame:CGRectMake(0, 50, self.view.frame.size.width, 200)];
    
    [actionSheetView addSubview:pickerView];
    [pickerView addTarget:self action:@selector(changeDate) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:actionSheetView];
    
    //UIToolbar *pickerToolbar = [[UIToolbar alloc] init];
    pickerToolbar.barStyle = UIBarStyleDefault;
    pickerToolbar.tintColor = [UIColor grayColor];
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    //[barItems addObject:doneBtn];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain  target:self action:@selector(pickerCancel)];
    //cancelBtn.tag = 1;
    [barItems addObject:cancelBtn];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UILabel *lbl_tit = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    UIBarButtonItem *title = [[UIBarButtonItem alloc] initWithCustomView:lbl_tit];
    if (customDate==1) {
        lbl_tit.text =@"Select date";
    }
    else{
        lbl_tit.text =@"Select Time";
    }
    
    
    lbl_tit.textAlignment=NSTextAlignmentCenter;
    [lbl_tit setBackgroundColor:[UIColor clearColor]];
    [lbl_tit setFont:[UIFont boldSystemFontOfSize:16.0f]];
    lbl_tit.textColor = [UIColor blackColor];
    [barItems addObject:title];
    
    UIBarButtonItem *flexSpace1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace1];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain  target:self action:@selector(pickerDone)];
    //doneBtn.tag = 2;
    [barItems addObject:doneBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    [actionSheetView addSubview:pickerToolbar];
    [actionSheetView addSubview:pickerView];
}

- (void)changeDate{
    if (customDate==1) {
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        if (datecall==1) {
            fromDateStr = [[NSString alloc] initWithString:[dateFormatter stringFromDate:pickerView.date]];
        }
        else {
            twoDateStr = [[NSString alloc] initWithString:[dateFormatter stringFromDate:pickerView.date]];
            
        }
    }
    else{
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        if (timecall==1) {
            //timecall=2;
            fromTimeStr = [[NSString alloc] initWithString:[dateFormatter stringFromDate:pickerView.date]];
            
        }
        else {
            toTimeStr = [[NSString alloc] initWithString:[dateFormatter stringFromDate:pickerView.date]];
            
        }
        
    }
}
-(void)pickerDone
{
    if (customDate==1){
        if (datecall==1){
            NSString *formate=[NSString stringWithFormat:@"%@",fromDateStr];
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [df dateFromString:formate];
            [df setDateFormat:@"MM-dd-yyyy"];
            //[cell.desLable setText:[df stringFromDate:date]];
            [fromDate setTitle:[df stringFromDate:date] forState:UIControlStateNormal];
        }
        else{
            NSString *formate=[NSString stringWithFormat:@"%@",twoDateStr];
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [df dateFromString:formate];
            [df setDateFormat:@"MM-dd-yyyy"];
            //[cell.desLable setText:[df stringFromDate:date]];
            
            [toDate setTitle:[df stringFromDate:date] forState:UIControlStateNormal];
        }
    }
    else{
        if (timecall==1){
            [fromTime setTitle:fromTimeStr forState:UIControlStateNormal];
        }
        else{
            [toTime setTitle:toTimeStr forState:UIControlStateNormal];
        }
        
    }
    
    [actionSheetView setFrame:CGRectMake(0,self.view.frame.size.height,self.view.frame.size.width, 250)];
    [actionSheetView dismissWithClickedButtonIndex:0 animated:YES];
    
    
}
-(void)pickerCancel
{
    [actionSheetView setFrame:CGRectMake(0,self.view.frame.size.height,self.view.frame.size.width, 250)];
    [actionSheetView dismissWithClickedButtonIndex:0 animated:YES];
}


-(IBAction)clickAvailability:(id)sender
{
    servicecall =0;
    [apptData removeAllObjects];
    [apptTableView reloadData];
    apptTableView.hidden=NO;
    [actionSheetView dismissWithClickedButtonIndex:0 animated:YES];
    NSString *getDate = dateButton.titleLabel.text;
    NSString *lowerGetDate;
    if ([getDate isEqualToString:@"Today"]) {
        lowerGetDate=@"today";
    }
    else if ([getDate isEqualToString:@"First Available"]) {
        lowerGetDate=@"first";
    }
    else if ([getDate isEqualToString:@"Tomorrow"]) {
        lowerGetDate=@"tomorrow";
    }
    else if ([getDate isEqualToString:@"This Week"]) {
        lowerGetDate=@"thisweek";
    }
    else if ([getDate isEqualToString:@"Pick a Date Range"]) {
        lowerGetDate=@"custom";
    }
    
    NSString *gettime = timeButton.titleLabel.text;
    NSString *lowerGetTime;
    
    if ([gettime isEqualToString:@"Anytime"]) {
        lowerGetTime=@"anytime";
    }
    else if ([gettime isEqualToString:@"Morning (Before 12PM)"]) {
        lowerGetTime=@"morning";
    }
    else if ([gettime isEqualToString:@"Afternoon (12PM to 5PM)"]) {
        lowerGetTime=@"afternoon";
    }
    else if ([gettime isEqualToString:@"Evening (After 5PM)"]) {
        lowerGetTime=@"evening";
    }
    else if ([gettime isEqualToString:@"Pick a Time Range"]) {
        lowerGetTime=@"custom";
    }
    
    
    NSString *fDateStr = [self convertDate:fromDate.titleLabel.text];
    NSString *tDateStr = [self convertDate:toDate.titleLabel.text];
    
    NSString *fTimeStr = fromTime.titleLabel.text;
    NSString *tTimeStr = toTime.titleLabel.text;
    
    NSLog(@"%@%@",fDateStr,tDateStr);
    
    
    
    
    //NSString *lowerGetDate = [getDate lowercaseString];
    //NSString *lowerGetTime = [gettime lowercaseString];
    
    
    if ([getDate isEqualToString:@"Select a Date"]) {
        [self showAlert:@"Date" MSG:@"Please select date" tag:0];
    }
    else if ([fromDate.titleLabel.text isEqualToString:@"From"] && dateValue == YES)
    {
        [self showAlert:@"Date" MSG:@"Please choose from date" tag:0];
    }
    else if ([toDate.titleLabel.text isEqualToString:@"To"] && dateValue == YES)
    {
        [self showAlert:@"Date" MSG:@"Please choose to date" tag:0];
    }
    
    else if ([gettime isEqualToString:@"Select a Time"])
    {
        [self showAlert:@"Time" MSG:@"Please select time" tag:0];
    }
    else if ([fromTime.titleLabel.text isEqualToString:@"From"] && timeValue == YES)
    {
        [self showAlert:@"Date" MSG:@"Please choose from time" tag:0];
    }
    else if ([toTime.titleLabel.text isEqualToString:@"To"] && timeValue == YES)
    {
        [self showAlert:@"Date" MSG:@"Please choose from time" tag:0];
    }
    else{
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        NSString *fromTimes=[[self convertTime:fTimeStr] stringByReplacingOccurrencesOfString:@":" withString:@""];
        NSString *toTimes=[[self convertTime:tTimeStr] stringByReplacingOccurrencesOfString:@":" withString:@""];
        
        
        if ([getDate isEqualToString:@"Pick a Date Range"] && [gettime isEqualToString:@"Pick a Time Range"] ) {
            [paramDic setObject:fDateStr forKey:@"FromDate"];
            [paramDic setObject:tDateStr forKey:@"ToDate"];
            
            [paramDic setObject:fromTimes forKey:@"FromTime"];
            [paramDic setObject:toTimes forKey:@"ToTime"];
        }
        else if ([getDate isEqualToString:@"Pick a Date Range"]) {
            [paramDic setObject:fDateStr forKey:@"FromDate"];
            [paramDic setObject:tDateStr forKey:@"ToDate"];
        }
        else if ([gettime isEqualToString:@"Pick a Time Range"]){
            [paramDic setObject:fromTimes forKey:@"FromTime"];
            [paramDic setObject:toTimes forKey:@"ToTime"];
        }
        [paramDic setObject:lowerGetDate forKey:@"Date"];
        [paramDic setObject:lowerGetTime forKey:@"Time"];
        
        NSString *clientIDStr = [self loadSharedPreferenceValue:@"clientid"];
        NSString *empIdStr = [empIdArray componentsJoinedByString:@","];
        NSString *serviceIdStr = [serIdArray componentsJoinedByString:@","];
        [paramDic setObject:empIdStr forKey:@"EmployeeIds"];
        [paramDic setObject:serviceIdStr forKey:@"ServiceIds"];
        
        [paramDic setObject:clientIDStr forKey:@"ClientId"];
        
        [self slotServiveCall:paramDic];
        
    }
}

-(void)slotServiveCall :(NSMutableDictionary *)paramDic
{
    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:CHECK_APPT_LIST] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription tag:0];
        else
        {
            NSDictionary *dict = (NSDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
}

-(NSString*)convertTime:(NSString *)value
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    NSDate *date = [dateFormatter dateFromString:value];
    dateFormatter.dateFormat = @"HH:mm";
    NSString *convertTime = [dateFormatter stringFromDate:date];
    NSLog(@"%@",convertTime);
    return convertTime;
}


-(NSString*)convertDate:(NSString *)value
{
    NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
    [dateFormatte setDateFormat:@"MM-dd-yyyy"];
    NSDate *todate = [dateFormatte dateFromString:value];
    [dateFormatte setDateFormat:@"yyyy-MM-dd"];
    NSString *convertdate = [dateFormatte stringFromDate:todate];
    NSLog(@"%@",convertdate);
    
    
    
    //    // strValue = [NSMutableString stringWithString:@"1015"];
    //    [value insertString:@":" atIndex:2];
    //    NSString *conStr = [NSString stringWithString:value];
    //    NSLog(@"%@",conStr);
    return convertdate;
    
    //    NSString *formate=[NSString stringWithFormat:@"%@",fromDateStr];
    //    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //    [df setDateFormat:@"yyyy-MM-dd"];
    //    NSDate *date = [df dateFromString:formate];
    //    [df setDateFormat:@"MM-dd-yyyy"];
    //    //[cell.desLable setText:[df stringFromDate:date]];
    //    [fromDate setTitle:[df stringFromDate:date] forState:UIControlStateNormal];
    
}


-(void)getResponse:(NSDictionary *)dict
{
    if(dict.count == 0){
        [self showAlert:@"Book online" MSG:@"No appointments are available"];
        return;
    }
    
    
    int status =[[dict objectForKey:@"status"] intValue];
    
    if (dict==nil||dict.count==0 || status == 0 ){
        
        NSString *msgStr = [dict objectForKey:@"message"];
        
        if (servicecall==1 && dict.count>0) {
            [self showAlert:@"Error" MSG:msgStr?:@"" tag:0];
        }
        else{
            [self showAlert:@"" MSG:msgStr?:@"" tag:0];
        }
        
    }
    else {
        
        if (servicecall==1) {
            NSLog(@"");
            int apptMess=[[dict objectForKey:@"AppointmentId"] intValue];
            
            NSString *apptIdStr=[NSString stringWithFormat:@"Thank you. Your appointment has been booked. Your booking Id# %d",apptMess];
            
            
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Appointment Created" andMessage:apptIdStr];
            
            [alertView addButtonWithTitle:@"Add to Calendar"                                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [self addToCalendar];
                                  }];
            
            [alertView addButtonWithTitle:@"Close"                                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alert) {
                                      [self.navigationController popToRootViewControllerAnimated:YES];
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
            [alertView show];
            
            
            //            [self showAlert:@"" MSG:apptIdStr tag:2];
        }
        else{
            NSMutableArray *arrJson = [dict objectForKey:@"openings"];
            if(arrJson.count == 0){
                [self showAlert:@"Book online" MSG:@"No appointments are available"];
                return;
            }
            
            for (int i=0; i<[arrJson count]; i++) {
                NSDictionary *empDict = [arrJson objectAtIndex:i];
                dividerCount =[[dict objectForKey:@"num_services"]intValue];
                Appt *apptObj = [[Appt alloc] init];
                [apptObj setServiceId:[empDict objectForKey:@"iservid"]];
                [apptObj setEmployeeId:[empDict objectForKey:@"iempid"]];
                
                [apptObj setStartLength:[empDict objectForKey:@"nstartlen"]];
                [apptObj setGapLength:[empDict objectForKey:@"ngaplen"]];
                [apptObj setFinishLength:[empDict objectForKey:@"nfinishlen"]];
                [apptObj setApptType:[empDict objectForKey:@"iappttype"]];
                [apptObj setResourceId:[empDict objectForKey:@"iresourceid"]];
                [apptObj setGenderId:[empDict objectForKey:@"igenderid"]];
                
                
                [apptObj setDate:[empDict objectForKey:@"ddate"]];
                [apptObj setService:[empDict objectForKey:@"service"]];
                [apptObj setEmployee:[empDict objectForKey:@"employee"]];
                //NSString *fromTime =[self convert:[empDict objectForKey:@"cmstarttime"]];
                // NSString *toTime =[self convert:[empDict objectForKey:@"cmfinishtime"]];
                
                [apptObj setFromTime:[empDict objectForKey:@"cmstarttime"]];
                [apptObj setToTime:[empDict objectForKey:@"cmfinishtime"]];
                [apptObj setPrice:[empDict objectForKey:@"nprice"]];
                [apptObj setMDisplayPrice:[empDict objectForKey:@"m_display_price"]];
                [apptData addObject:apptObj];
            }
            [apptTableView reloadData];
        }
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [apptData count];
}


// Customize the appearance of table view cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static  NSString *CellIdentifier = @"ApptCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    UILabel *nameLable= (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *desLable= (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *timeLable= (UILabel *)[cell.contentView viewWithTag:3];
    UIButton *bookApptButton= (UIButton *)[cell.contentView viewWithTag:5];
    UILabel *price= (UILabel *)[cell.contentView viewWithTag:10];
    UIView *lineView= (UIView *)[cell.contentView viewWithTag:6];
    
    
    Appt *apptObj = [apptData objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [nameLable setBackgroundColor:[UIColor clearColor]];
    nameLable.font=[UIFont systemFontOfSize:14.0];
    NSString *nameStr = [NSString stringWithFormat:@"%@ with %@",apptObj.service,apptObj.employee];
    nameLable.text=[NSString stringWithFormat:@"%@",nameStr];
    
    [desLable setBackgroundColor:[UIColor clearColor]];
    desLable.font=[UIFont systemFontOfSize:12.0];
    
    NSString *fromTimes =[self convert:[NSMutableString stringWithFormat:@"%@",apptObj.fromTime]];
    NSString *toTimes =[self convert:[NSMutableString stringWithFormat:@"%@",apptObj.toTime]];
    
    NSString *formate=[NSString stringWithFormat:@"%@",apptObj.date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [df dateFromString:formate];
    [df setDateFormat:@"MMM dd, yyyy"];
    [desLable setText:[df stringFromDate:date]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *dates = [dateFormatter dateFromString:fromTimes];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedTime = [dateFormatter stringFromDate:dates];
    NSLog(@"%@",formattedTime);
    
    NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
    [dateFormatte setDateFormat:@"HH:mm"];
    NSDate *todate = [dateFormatte dateFromString:toTimes];
    [dateFormatte setDateFormat:@"hh:mm a"];
    NSString *formattedtoTime = [dateFormatte stringFromDate:todate];
    NSLog(@"%@",formattedtoTime);
    
    
    
    // NSString *timeStr = [NSString stringWithFormat:@"%@ - %@" ,fromTimes, toTimes];
    // cell.timeLable.text=[NSString stringWithFormat:@"%@",timeStr];
    timeLable.text=formattedTime;
    NSString *priceStr =[NSString stringWithFormat:@"%@",apptObj.mDisplayPrice];
    if (![priceStr isEqualToString:@""]) {
        price.text=priceStr;
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    
    bookApptButton.hidden=YES;
    lineView.hidden=YES;
    //self.apptTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (dividerCount > 1 && ((indexPath.row + 1) % (dividerCount) == 0)){
        bookApptButton.hidden=NO;
        lineView.hidden=NO;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
        self.apptTableView.separatorColor = [UIColor grayColor];
    }
    else if(dividerCount ==1){
        bookApptButton.hidden=NO;
        lineView.hidden=NO;
    }
    [bookApptButton setTag:indexPath.row];
    [bookApptButton addTarget:self action:@selector(bookApptClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

-(NSString*)convert:(NSMutableString *)strValue
{
    // strValue = [NSMutableString stringWithString:@"1015"];
    [strValue insertString:@":" atIndex:2];
    NSString *conStr = [NSString stringWithString:strValue];
    NSLog(@"%@",conStr);
    return conStr;
}

#pragma marl Book button action
-(void) bookApptClicked : (id) sender
{
    servicecall =1;
    UIButton *btn = (UIButton *)sender;
    Appt *obj = [apptData objectAtIndex:[btn tag]];
    NSMutableArray *servicesIdArray=[[NSMutableArray alloc] init];
    NSMutableArray *empIdsArray=[[NSMutableArray alloc] init];
    NSMutableArray *startLengthArray=[[NSMutableArray alloc] init];
    NSMutableArray *gapLengthArray=[[NSMutableArray alloc] init];
    NSMutableArray *finishLengthArray=[[NSMutableArray alloc] init];
    NSMutableArray *apptTypeArray=[[NSMutableArray alloc] init];
    NSMutableArray *fromTimeArray=[[NSMutableArray alloc] init];
    NSMutableArray *toTimeArray=[[NSMutableArray alloc] init];
    NSMutableArray *resourceIdArray=[[NSMutableArray alloc] init];
    NSMutableArray *genderIdArray=[[NSMutableArray alloc] init];
    //NSMutableArray *dateArray=[[NSMutableArray alloc] init];
    
    
    //bookedServiceName,*bookedProvider,*bookedTtime;
    
    //new srikanth code
    bookedToTime =[self convert:[NSMutableString stringWithFormat:@"%@",obj.toTime]];
    bookedFromTIme =[self convert:[NSMutableString stringWithFormat:@"%@",obj.fromTime]];
    
    NSString *formate=[NSString stringWithFormat:@"%@ %@",obj.date,bookedFromTIme];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd hh:mm"];
    NSDate *date = [df dateFromString:formate];
    
    
    
    bookedServiceName = [NSString stringWithFormat:@"%@",obj.service];
    bookedProvider = [NSString stringWithFormat:@"%@",obj.employee];
    //bookedTtime = formattedtoTime;
    bookedDate = date;
    
    //end
    for (int i=0; i<dividerCount; i++) {
        NSLog(@"%d",[btn tag]-i);
        Appt *apptObj = [apptData objectAtIndex:[btn tag]-i];
        [servicesIdArray addObject:apptObj.serviceId];
        [empIdsArray addObject:apptObj.employeeId];
        [startLengthArray addObject:apptObj.startLength];
        [gapLengthArray addObject:apptObj.gapLength];
        [finishLengthArray addObject:apptObj.finishLength];
        [apptTypeArray addObject:apptObj.apptType];
        [fromTimeArray addObject:apptObj.fromTime];
        [toTimeArray addObject:apptObj.toTime];
        [resourceIdArray addObject:apptObj.resourceId];
        [genderIdArray addObject:apptObj.genderId];
        //[dateArray addObject:apptObj.date];
    }
    
    NSString *servicesIdsStr = [servicesIdArray componentsJoinedByString:@","];
    NSString *empIdsStr = [empIdsArray componentsJoinedByString:@","];
    NSString *startLengthStr = [startLengthArray componentsJoinedByString:@","];
    NSString *gapLengthStr = [gapLengthArray componentsJoinedByString:@","];
    NSString *finishLengthStr = [finishLengthArray componentsJoinedByString:@","];
    NSString *apptTypeStr = [apptTypeArray componentsJoinedByString:@","];
    NSString *fromTimesStr = [fromTimeArray componentsJoinedByString:@","];
    NSString *toTimesStr = [toTimeArray componentsJoinedByString:@","];
    NSString *resourceIdStr = [resourceIdArray componentsJoinedByString:@","];
    NSString *genderIdStr = [genderIdArray componentsJoinedByString:@","];
    // NSString *dateStr = [dateArray componentsJoinedByString:@","];
    
    //Appt *apptObj = [apptData objectAtIndex:[sender tag]];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    NSString *clientIDStr = [self loadSharedPreferenceValue:@"clientid"];
    [paramDic setObject:servicesIdsStr forKey:@"ServiceIds"];
    [paramDic setObject:empIdsStr forKey:@"EmployeeIds"];
    [paramDic setObject:startLengthStr forKey:@"StartLength"];
    [paramDic setObject:gapLengthStr forKey:@"GapLength"];
    [paramDic setObject:finishLengthStr forKey:@"FinishLength"];
    [paramDic setObject:apptTypeStr forKey:@"AppointmentType"];
    [paramDic setObject:fromTimesStr forKey:@"CheckInTime"];
    [paramDic setObject:toTimesStr forKey:@"CheckOutTime"];
    [paramDic setObject:resourceIdStr forKey:@"ResourceId"];
    [paramDic setObject:genderIdStr forKey:@"Genderid"];
    [paramDic setObject:obj.date forKey:@"AppointmentDate"];//2016-09-22
    [paramDic setObject:clientIDStr forKey:@"ClientId"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:BOOKING_CONFIRMATION] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription tag:0];
        else
        {
            NSDictionary *dict = (NSDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==2)
            [self.navigationController popToRootViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Add claendar
-(void)addToCalendar
{
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    
    EKEventStore *eventstore = [[EKEventStore alloc] init];
    
    EKEvent *event = [EKEvent eventWithEventStore:eventstore];
    
    event.title= [NSString stringWithFormat:@"Thank you. Your appointment has been booked - %@\n %@\n%@",bookedServiceName,bookedProvider,bookedDate];
    
    event.startDate = bookedDate;
    event.endDate = [[NSDate alloc] initWithTimeInterval:60*60 sinceDate:bookedDate];
    
    EKAlarm *remainder = [EKAlarm alarmWithRelativeOffset:-60*30];
    [event addAlarm:remainder];
    
    if([eventstore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        // iOS 6 and later
        [eventstore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            
            // may return on background thread
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (granted){
                    [event setCalendar:[eventstore defaultCalendarForNewEvents]];
                    //---- codes here when user allow your app to access theirs' calendar.
                    NSError *err;
                    BOOL isSuceess=[eventstore saveEvent:event span:EKSpanThisEvent error:&err];
                    
                    if(isSuceess){
                        [self showAlert:SALON_NAME MSG:@"Your appointment has been added to the calendar." BLOCK:^{
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }];                    }
                    
                }else
                {
                    [self showAlert:SALON_NAME MSG:@"Please enable access to the calendar in Settings -> Privacy."];
                }
            });
        }];
    }
    else {
        NSError *err;
        BOOL isSuceess=[eventstore saveEvent:event span:EKSpanThisEvent error:&err];
        
        if(isSuceess){
            [self showAlert:SALON_NAME MSG:@"Your appointment has been added to the calendar." BLOCK:^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }
        
    }
    
    
}


@end
