//
//  CartGlobals.m
//  Store
//
//  Created by Webappclouds on 04/03/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import "CartGlobals.h"

@implementation CartGlobals

static NSMutableArray *productsArray;
static NSMutableArray *favoriteProductsArray;


+(NSMutableArray *) getProducts
{
    return productsArray;
}

+(void) addProduct : (CartObj *) obj
{
    if(productsArray==Nil)
        productsArray = [[NSMutableArray alloc] init];
        //NSLog(@"%d",[productsArray count]);
    
    int isAdded = 0;
    
    for(int i=0; i< [productsArray count]; i++)
    {
        CartObj *addedObj = [productsArray objectAtIndex:i];
       // NSLog(@"%d %d",addedObj.productId,obj.productId);
        if((addedObj.productId == obj.productId))
        {
            isAdded = 1;
            //addedObj.productQuantity+=obj.productQuantity;
           // NSLog(@"%d %f",obj.productQuantity,obj.productPrice);
            NSInteger price =obj.productQuantity*obj.productPrice;
            obj.totalPrice=price;
            //NSLog(@"%f",obj.totalPrice);
            [productsArray replaceObjectAtIndex:i withObject:addedObj];
            break;
        }
    }
    if(isAdded==0)
        [productsArray addObject:obj];
}

+(float) getTotalPrice
{
    float totalPrice = 0.0;
    if(productsArray==Nil || [productsArray count]==0)
        return totalPrice;
    
    for(CartObj *obj in productsArray)
    {
        //if (obj.productQuantity ==1) {
            float temp = obj.productQuantity*obj.productPrice;
            totalPrice+=temp;
       // }
       // else{
//        float temp = obj.productQuantity*obj.totalPrice;
//        totalPrice+=temp;
       // }
    }
    
    return totalPrice;
}


//
//+(void) deleteProductAtIndex : (int) index
//{
//    [productsArray removeObjectAtIndex:index];
//}
//
+(void) deleteAllProducts
{
    [productsArray removeAllObjects];
}






+(NSMutableArray *) getFavoriteProducts
{
    return favoriteProductsArray;
}

+(void) addFavoriteProduct : (Favorite *) favObj
{
    if(favoriteProductsArray==Nil)
        favoriteProductsArray = [[NSMutableArray alloc] init];
    NSLog(@"%lu",(unsigned long)[favoriteProductsArray count]);
    
    int isAdded = 0;
    
//    for(int i=0; i< [favoriteProductsArray count]; i++)
//    {
//        Favorite *addedObj = [favoriteProductsArray objectAtIndex:i];
//        // NSLog(@"%d %d",addedObj.productId,obj.productId);
//        if((addedObj.favProdId == favObj.favProdId))
//        {
//            isAdded = 1;
//            [favoriteProductsArray replaceObjectAtIndex:i withObject:addedObj];
//            break;
//        }
//    }
    if(isAdded==0)
        [favoriteProductsArray addObject:favObj];
}



@end
