//
//  CateInfoViewController.m
//  Store
//
//  Created by Webappclouds on 25/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import "CateInfoViewController.h"
#import "UIViewController+NRFunctions.h"
#import "GlobalSettings.h"
#import "Constants.h"
#import "CategoriesInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CartObj.h"
#import "CartGlobals.h"
#import "CartViewController.h"
#import "Favorite.h"

@interface CateInfoViewController ()
{
    CategoriesInfo *cateInfo;
    float price;
    NSMutableArray *checkProductArray;
    UIImageView *imageView;
    BOOL isClick;
}

@end

@implementation CateInfoViewController
@synthesize infoImageView, infoScrollView, firstView, secondView, descriptionView;
@synthesize cateListObj, favoriteObj, cateInfoArray, favoriteFlag, selectedIndex;
@synthesize cateNameLable, catePriceLable, cateInfoTextView, favoriteButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    isClick = YES;
    self.title = @"Details";
    checkProductArray = [[NSMutableArray alloc] init];
    [self loadDataFromServer];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(clickProductImage:)];
    [tapRecognizer setNumberOfTouchesRequired:1];
    [tapRecognizer setDelegate:self];
    
    infoImageView.userInteractionEnabled = YES;
    [infoImageView addGestureRecognizer:tapRecognizer];

    // Do any additional setup after loading the view.
    
}

-(void) loadDataFromServer
{
    [cateInfoArray removeAllObjects];
    [self showHud];
    NSString *cateListUrl;
    if ([favoriteFlag isEqualToString:@"FAVORITE"]) {
        cateListUrl = [NSString stringWithFormat:@"%@%d&product_id=%d", CATEGORIES_INFO_URL, favoriteObj.favCateId,favoriteObj.favCateListId];
    }else{
    cateListUrl = [NSString stringWithFormat:@"%@%d&product_id=%d", CATEGORIES_INFO_URL, cateListObj.cateId,cateListObj.cateListId];
    }
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:cateListUrl HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(id responseObject, NSURLResponse *urlResponse, NSError *error) {
        [self hideHud];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseObject==Nil || [responseObject count]==0)
            {
                [self showAlert:@"Error" MSG:@"No Categories found"];
                return;
            }
                cateInfo = [[CategoriesInfo alloc] init];
                cateInfo.productName = [responseObject objectForKey:@"heading_title"];
                cateInfo.productId = [[responseObject objectForKey:@"product_id"] intValue];
                cateInfo.productImage = [responseObject objectForKey:@"thumb"];
                cateInfo.productPopupImage = [responseObject objectForKey:@"popup"];
                cateInfo.productPrice = [responseObject objectForKey:@"price"];
                price =[[cateInfo.productPrice stringByReplacingOccurrencesOfString:@"$" withString:@""]floatValue];
                cateInfo.productDesc = [responseObject objectForKey:@"description"];
                cateNameLable.text = cateInfo.productName;
                catePriceLable.text = cateInfo.productPrice;
                cateInfoTextView.text = cateInfo.productDesc;
                [self chekFavorite];
                [self loadImage];
            
            }
    }];
}

-(void)chekFavorite
{
    for (Favorite *obj in [CartGlobals getFavoriteProducts]){
        
        if (obj.favCateId == favoriteObj.favCateId) {
            [favoriteButton setImage:[UIImage imageNamed:@"Like"] forState:UIControlStateNormal];
            favoriteButton.contentMode = UIViewContentModeScaleToFill;
            return;
        }
    }
    
    [favoriteButton setImage:[UIImage imageNamed:@"UnLike"] forState:UIControlStateNormal];
    favoriteButton.contentMode = UIViewContentModeScaleToFill;
}

-(void)loadImage
{
    UIActivityIndicatorView *activityIndicator;
    [infoImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
    activityIndicator.center = infoImageView.center;
    [activityIndicator startAnimating];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [infoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",cateInfo.productImage]]];
        [activityIndicator stopAnimating];
        //[activityIndicator removeFromSuperview];

    });
}




///////////////////////////////////////// Custom Popup View ///////////////////////////////////////

-(IBAction)clickProductImage :(id) sender
{
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createPopupView]];
    
    // Modify the parameters
    //[alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
//    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
//        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
//        [alertView close];
//    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}

- (UIView *)createPopupView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, self.view.frame.size.height-100)];
    [demoView setBackgroundColor:[UIColor whiteColor]];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-40, self.view.frame.size.height-120)];
   imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-40, self.view.frame.size.height-120)];
    
    scrollView.contentSize = CGSizeMake(imageView.frame.size.width , imageView.frame.size.height);
    scrollView.maximumZoomScale = 4;
    scrollView.minimumZoomScale = 1;
    scrollView.clipsToBounds = YES;
    scrollView.delegate = self;
    
    
    UIActivityIndicatorView *activityIndicator;
    [imageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
    activityIndicator.center = imageView.center;
    [activityIndicator startAnimating];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",cateInfo.productPopupImage]]];
        [activityIndicator stopAnimating];
        //[activityIndicator removeFromSuperview];
    });
    [scrollView addSubview:imageView];
    [demoView addSubview:scrollView];
    
    return demoView;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imageView;
}

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}


-(IBAction)addToCart:(id)sender
{
    for (CartObj *obj in [CartGlobals getProducts]){
        
        if (obj.productId == cateInfo.productId) {
            [self showAlert:@"Error" MSG:@"This product already add to cart."];
            return;
        }
    }
    CartObj *cartObj = [[CartObj alloc] init];
    [cartObj setProductId:cateInfo.productId];
    [cartObj setProductName:cateInfo.productName];
    [cartObj setProductDesc:cateInfo.productDesc];
    [cartObj setProductPrice:price];
    [cartObj setProductQuantity:1];
    [cartObj setI:1];
    [cartObj setProductImage:cateInfo.productImage];
    [CartGlobals addProduct:cartObj];
    [self removeNavgationBackButtonTitle];
    CartViewController *cartView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:cartView animated:YES];
    [[super.tabBarController.viewControllers objectAtIndex:2] tabBarItem].badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[[CartGlobals getProducts] count]];
}

-(IBAction)favoriteClick:(id)sender
{
    for (Favorite *obj in [CartGlobals getFavoriteProducts]){
        
        if (obj.favCateId == favoriteObj.favCateId) {
            [favoriteButton setImage:[UIImage imageNamed:@"UnLike"] forState:UIControlStateNormal];
            favoriteButton.contentMode = UIViewContentModeScaleToFill;
            [self removeToFavorite];
            return;
        }
    }
    
    [favoriteButton setImage:[UIImage imageNamed:@"Like"] forState:UIControlStateNormal];
    favoriteButton.contentMode = UIViewContentModeScaleToFill;
    [self addToFavorite];

//    if ( favoriteButton.selected ==NO) {
//        favoriteButton.selected=YES;
//        [favoriteButton setImage:[UIImage imageNamed:@"Like"] forState:UIControlStateNormal];
//        favoriteButton.contentMode = UIViewContentModeScaleToFill;
//        [self addToFavorite];
//        }
//    else{
//        favoriteButton.selected=NO;
//        [favoriteButton setImage:[UIImage imageNamed:@"UnLike"] forState:UIControlStateNormal];
//        favoriteButton.contentMode = UIViewContentModeScaleToFill;
//        //[self removeToFavorite];
//    }
}

-(void)addToFavorite
{
    Favorite *favObj = [[Favorite alloc] init];
    [favObj setFavCateId:cateListObj.cateId];
    [favObj setFavCateListId:cateListObj.cateListId];
    [favObj setFavCateListName:cateListObj.cateListName];
    [favObj setFavCatePrice:cateListObj.catePrice];
    [favObj setFavCateImage:cateListObj.cateImage];
    [CartGlobals addFavoriteProduct:favObj];
}

-(void)removeToFavorite
{
//    for (int i=0; i<[[CartGlobals getFavoriteProducts] count]; i++){
//        Favorite *obj = [[CartGlobals getFavoriteProducts] objectAtIndex:i];
//        if (obj.favCateId == favoriteObj.favCateId) {
            [[CartGlobals getFavoriteProducts] removeObjectAtIndex:selectedIndex];
//            return;
//        }
//    }
//  
}


-(IBAction)shareClick:(id)sender
{
    UIImageView *aImage= [[UIImageView alloc] init];
    [aImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",cateInfo.productImage]]];
    NSString * descriptionWithTitle = [NSString stringWithFormat:@"Title: %@ \n\nDescription: %@\n\nPrice: %@",cateInfo.productName,cateInfo.productDesc,cateInfo.productPrice];
    
    NSArray *itemsToShare = @[descriptionWithTitle, aImage.image];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
    [self presentViewController:activityVC animated:YES completion:nil];
    

}

-(IBAction)viewDetailsClick:(id)sender
{
    if (isClick == YES) {
        [infoScrollView setContentOffset:
         CGPointMake(0, 110) animated:YES];
        isClick=NO;
    }
    else{
        [infoScrollView setContentOffset:
         CGPointMake(0, 0) animated:YES];
        isClick=YES;
    }
    
   
}


//UIButton * button = [[UIButton alloc] init];
//button.backgroundColor = [UIColor redColor];
//[demoView addSubview:button];
//
//NSDictionary * buttonDic = NSDictionaryOfVariableBindings(button);
//button.translatesAutoresizingMaskIntoConstraints = NO;
//
//NSArray * hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[button]-10-|"
//                                                                 options:0
//                                                                 metrics:nil
//                                                                   views:buttonDic];
//[demoView addConstraints:hConstraints];
//
//NSArray * vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[button(30)]"
//                                                                 options:0
//                                                                 metrics:nil
//                                                                   views:buttonDic];
//[demoView addConstraints:vConstraints];
//[demoView setBackgroundColor:[UIColor whiteColor]];
//

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [infoScrollView setContentSize:CGSizeMake(320, 860)];
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    [self chekFavorite];
//
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
