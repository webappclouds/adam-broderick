//
//  CategoriesViewController.m
//  Store
//
//  Created by Webappclouds on 18/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import "CategoriesViewController.h"
#import "GlobalSettings.h"
#import "UIViewController+NRFunctions.h"
#import "Constants.h"
#import "Categories.h"
#import "CateListViewController.h"
#import "CartGlobals.h"
@interface CategoriesViewController ()

@end

@implementation CategoriesViewController
@synthesize categoriesArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Categories";
    self.categoriesTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

     [[super.tabBarController.viewControllers objectAtIndex:2] tabBarItem].badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[[CartGlobals getProducts] count]];
    
    categoriesArray = [[NSMutableArray alloc] init];
    [self loadDataFromServer];
    // Do any additional setup after loading the view.
}

-(void) loadDataFromServer
{
    [categoriesArray removeAllObjects];
    [self showHud];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:CATEGORIES_URL HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(id responseObject, NSURLResponse *urlResponse, NSError *error) {
        [self hideHud];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseObject==Nil || [responseObject count]==0)
            {
                [self showAlert:@"Error" MSG:@"No Categories found"];
                return;
            }
            
            NSDictionary *dict = responseObject;
            NSMutableArray *catArray = [dict objectForKey:@"category"];
            
            for(NSDictionary *dict in catArray)
            {
                Categories *catObj = [[Categories alloc] init];
                catObj.categoryName = [dict objectForKey:@"name"];
                catObj.categoryId = [[dict objectForKey:@"category_id"] intValue];
                [categoriesArray addObject:catObj];
            }
            
            [self.categoriesTable reloadData];
        }
    }];
}


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [categoriesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CategoriesCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    Categories *obj = [categoriesArray objectAtIndex:indexPath.row];
    UIFont * font1 =[UIFont fontWithName:@"Open Sans" size:16.0f];
    cell.textLabel.font=font1;
    cell.textLabel.text =[NSString stringWithFormat:@"%@",obj.categoryName];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.categoriesTable deselectRowAtIndexPath:indexPath animated:YES];
    [self removeNavgationBackButtonTitle];
    Categories *obj = [categoriesArray objectAtIndex:indexPath.row];
    CateListViewController *cateListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CateListViewController"];
    cateListViewController.categoriesObj=obj;
    [self.navigationController pushViewController:cateListViewController animated:YES];

 
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    
//    [self.categoriesTable deselectRowAtIndexPath:sender animated:YES];
//    if ([[segue identifier] isEqualToString:@"DetailsView"]) {
//            Categories *obj= [categoriesArray objectAtIndex:[[self.categoriesTable indexPathForSelectedRow] row]];
//           CatDetailsviewViewController *destViewController = segue.destinationViewController;
//        }
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}

@end
