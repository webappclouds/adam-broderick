//
//  CatDetailsviewViewController.m
//  Store
//
//  Created by Webappclouds on 19/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import "CateListViewController.h"
#import "UIViewController+NRFunctions.h"
#import "GlobalSettings.h"
#import "Constants.h"
#import "CategoriesList.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CateInfoViewController.h"
#import "GridViewCell.h"
#import "ListViewCell.h"
#import "PictureViewCell.h"
#import "DeviceModel.h"

@interface CateListViewController ()
{
    int whichCell;
}

@end

@implementation CateListViewController
@synthesize firstView, secondView, thirdView;
@synthesize cateListArray, filteredArray,categoriesObj;
@synthesize cateCollectionView;
@synthesize sortLable, gridButtion, segControlShortList;


- (void)viewDidLoad {
    [super viewDidLoad];
    whichCell =1;
    self.title = categoriesObj.categoryName;
    cateListArray = [[NSMutableArray alloc] init];
    filteredArray = [[NSMutableArray alloc] init];

    [self loadDataFromServer];
}

-(void) loadDataFromServer
{
    [cateListArray removeAllObjects];
    [self showHud];
    
    NSString *cateListUrl = [NSString stringWithFormat:@"%@%d", CATEGORIES_LIST_URL, categoriesObj.categoryId];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:cateListUrl HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [self hideHud];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:@"Error" MSG:@"No Categories found"];
                return;
            }
            
            for(NSDictionary *dict in responseArray)
            {
                CategoriesList *cateList = [[CategoriesList alloc] init];
                cateList.cateListName = [dict objectForKey:@"name"];
                cateList.cateId = [[dict objectForKey:@"subcategory_id"] intValue];
                cateList.cateListId = [[dict objectForKey:@"product_id"] intValue];
                cateList.catePrice = [dict objectForKey:@"price"];
                cateList.cateImage = [dict objectForKey:@"thumb"];
                [filteredArray addObject:cateList];
                [cateListArray addObject:cateList];
                
            }
            [sortLable setText:@"Default"];
            [cateCollectionView reloadData];
        }
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)sortByClick:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Sort By" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Default" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [sortLable setText:@"Default"];
        
        [self loadDataFromServer];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Name (A - Z)" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [sortLable setText:@"Name (A - Z)"];
        
        NSArray *sortedArray = [cateListArray sortedArrayUsingComparator:^NSComparisonResult(CategoriesList *p1, CategoriesList *p2){
            
            return [p1.cateListName compare:p2.cateListName];
        }];
        [cateListArray removeAllObjects];
        [cateListArray setArray:sortedArray];
        [cateCollectionView reloadData];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Name (Z - A)" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [sortLable setText:@"Name (Z - A)"];
        
        NSArray *sortedArray = [cateListArray sortedArrayUsingComparator:^NSComparisonResult(CategoriesList *p1, CategoriesList *p2){
            
            return [p2.cateListName compare:p1.cateListName];
        }];
        [cateListArray removeAllObjects];
        [cateListArray setArray:sortedArray];
        [cateCollectionView reloadData];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Price (Low > High)" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [sortLable setText:@"Price (Low > High)"];
        
        NSArray *sortedArray = [cateListArray sortedArrayUsingComparator:^NSComparisonResult(CategoriesList *p1, CategoriesList *p2){
            
            return [p1.catePrice compare:p2.catePrice];
        }];
        [cateListArray removeAllObjects];
        [cateListArray setArray:sortedArray];
        [cateCollectionView reloadData];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Price (High < Low)" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [sortLable setText:@"Price (High < Low)"];
        
        NSArray *sortedArray = [cateListArray sortedArrayUsingComparator:^NSComparisonResult(CategoriesList *p1, CategoriesList *p2){
            
            return [p2.catePrice compare:p1.catePrice];
        }];
        [cateListArray removeAllObjects];
        [cateListArray setArray:sortedArray];
        [cateCollectionView reloadData];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(IBAction)searchClick:(id)sender
{
    [UIView animateWithDuration:0.6f animations:^{
         self.title=@"Search";
        firstView.frame =CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width,60);
        secondView.frame =CGRectMake(0, 125, [[UIScreen mainScreen] bounds].size.width,60);
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(searchCancelClick)];
        self.navigationItem.rightBarButtonItem = anotherButton;
    }];
  
}

-(void)searchCancelClick
{
    
    [UIView animateWithDuration:0.6f animations:^{
        self.title = categoriesObj.categoryName;
        firstView.frame =CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,60);
        secondView.frame =CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width,60);
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:nil];
        self.navigationItem.rightBarButtonItem = anotherButton;
        [self.searchBar resignFirstResponder];
    }];
   
}

-(IBAction)gridClick:(id)sender
{
    [UIView animateWithDuration:0.6f animations:^{
        self.title=@"Select View";
         [[segControlShortList.subviews objectAtIndex:0] setTintColor:[UIColor blackColor]];
         [[segControlShortList.subviews objectAtIndex:1] setTintColor:[UIColor blackColor]];
         [[segControlShortList.subviews objectAtIndex:2] setTintColor:[UIColor blackColor]];

        thirdView.frame =CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width,60);
        secondView.frame =CGRectMake(0, 125, [[UIScreen mainScreen] bounds].size.width,60);
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(gridCancelClick)];
        self.navigationItem.rightBarButtonItem = anotherButton;
        [self.searchBar resignFirstResponder];
    }];
  
}

-(void)gridCancelClick
{
    [UIView animateWithDuration:0.6f animations:^{
        self.title = categoriesObj.categoryName;
        [[segControlShortList.subviews objectAtIndex:0] setTintColor:[UIColor clearColor]];
        [[segControlShortList.subviews objectAtIndex:1] setTintColor:[UIColor clearColor]];
        [[segControlShortList.subviews objectAtIndex:2] setTintColor:[UIColor clearColor]];
        thirdView.frame =CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,60);
        secondView.frame =CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width,60);
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:nil];
        self.navigationItem.rightBarButtonItem = anotherButton;
    }];
    
}

-(void)normalPostion
{
    [UIView animateWithDuration:0.6f animations:^{
        self.title = categoriesObj.categoryName;
        [[segControlShortList.subviews objectAtIndex:0] setTintColor:[UIColor clearColor]];
        [[segControlShortList.subviews objectAtIndex:1] setTintColor:[UIColor clearColor]];
        [[segControlShortList.subviews objectAtIndex:2] setTintColor:[UIColor clearColor]];
        thirdView.frame =CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,60);
        secondView.frame =CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width,60);
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:nil];
        self.navigationItem.rightBarButtonItem = anotherButton;
    }];
  
}
- (IBAction)segShortListBtnTapped:(id)sender {
    
    if(segControlShortList.selectedSegmentIndex==0){
        [self normalPostion];
        whichCell = 1;
        [cateCollectionView reloadData];
        NSLog(@"1");
    }
    else if(segControlShortList.selectedSegmentIndex==1){
        NSLog(@"2");
        [self normalPostion];
        whichCell=2;
        [cateCollectionView reloadData];
        
    }
    else{
        [self normalPostion];
        whichCell=3;
        [cateCollectionView reloadData];
        NSLog(@"3");
    }
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [cateListArray count];
}

//-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    float width = (self.cateCollectionView.frame.size.width/3)-10;
//    float height = (self.cateCollectionView.frame.size.height)/4-10;
//    return CGSizeMake(trunc(width), height);
//}

- (CGSize)collectionView:(UICollectionView * )collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath * )indexPath
{
//    CGSize size;
//    if([[DeviceModel iPhoneVersion] isEqualToString:@"iPhone 6"] || [[DeviceModel iPhoneVersion] isEqualToString:@"iPhone 6s"] || [[DeviceModel iPhoneVersion] isEqualToString:@"iPhone 7"])
//        size=CGSizeMake(120, 125);
//    else if([[DeviceModel iPhoneVersion] isEqualToString:@"iPhone 5"] || [[DeviceModel iPhoneVersion] isEqualToString:@"iPhone 5s"] || [[DeviceModel iPhoneVersion] isEqualToString:@"Simulator"])
//        size=CGSizeMake(159, 210);
//    else if([[DeviceModel iPhoneVersion] isEqualToString:@"iPhone 7 Plus"] || [[DeviceModel iPhoneVersion] isEqualToString:@"iPhone 6 Plus"]|| [[DeviceModel iPhoneVersion] isEqualToString:@"Simulator"] || [[DeviceModel iPhoneVersion] isEqualToString:@"iPhone 6s Plus"])
//        size=CGSizeMake(186.20, 246);
//    
    
//         iPhone 6s &iPhone 6 & iPhone 7
//          size=CGSizeMake(120, 125);
//    
//     iPhone 5 &iPhone 5s
//     size=CGSizeMake(95, 100);
//     iPhone 6 Plus && iPhone 7 Plus
//     size=CGSizeMake(134, 140);
    
    
//    return size;

    CGSize size;
    if(whichCell == 1)
        size=CGSizeMake(186.20, 246);
    else if(whichCell == 2)
        size=CGSizeMake(self.view.frame.size.width, 86);
    else
        size=CGSizeMake(self.view.frame.size.width, 300);
    return size;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CategoriesList *categoriesList=[cateListArray objectAtIndex:indexPath.row];
    if(whichCell == 1){
    static NSString *gridIdentifier = @"GrideCell";

    GridViewCell *gridViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:gridIdentifier forIndexPath:indexPath];
    
    
    [gridViewCell.recipeImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:IMAGE_BASE_URL,categoriesList.cateImage]]];
        [gridViewCell.nameLable setText:categoriesList.cateListName];
    [gridViewCell.priseLable setText:categoriesList.catePrice];
    //cell.backgroundColor=[UIColor clearColor];
    return gridViewCell;
    }
    else if(whichCell == 2){
        static NSString * const listIdentifier = @"ListCell";
        
        ListViewCell *listViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:listIdentifier forIndexPath:indexPath];
        [listViewCell.recipeImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:IMAGE_BASE_URL,categoriesList.cateImage]]];
        [listViewCell.nameLable setText:categoriesList.cateListName];
        [listViewCell.priseLable setText:categoriesList.catePrice];

        return listViewCell;
    }
    else{
        static NSString * const PictureIdentifier = @"PictureCell";
        
        PictureViewCell *pictureViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:PictureIdentifier forIndexPath:indexPath];
        [pictureViewCell.recipeImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:IMAGE_BASE_URL,categoriesList.cateImage]]];
        [pictureViewCell.nameLable setText:categoriesList.cateListName];
        [pictureViewCell.priseLable setText:categoriesList.catePrice];
        
        return pictureViewCell;
  
    }
}
#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self removeNavgationBackButtonTitle];
    CategoriesList *categoriesList=[cateListArray objectAtIndex:indexPath.row];
    CateInfoViewController *infoView = [self.storyboard instantiateViewControllerWithIdentifier:@"CateInfoViewController"];
    infoView.cateListObj=categoriesList;
    [self.navigationController pushViewController:infoView animated:YES];
    
}


////////////Search

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterResults:searchText];
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

-(void) filterResults : (NSString *) searchStr
{
    [cateListArray removeAllObjects];
    if([searchStr length]==0)
    {
        [self.searchBar resignFirstResponder];
        [self refreshFilteredDataWithData];
        return;
    }
    
    searchStr = [searchStr lowercaseString];
    
    for(CategoriesList *obj in filteredArray)
    {
        if([[obj.cateListName lowercaseString] containsString:searchStr])
            [cateListArray addObject:obj];
    }
    [self.cateCollectionView reloadData];
}

-(void) refreshFilteredDataWithData
{
    [cateListArray removeAllObjects];
    cateListArray = [[NSMutableArray alloc] initWithArray:filteredArray];
    [self.cateCollectionView reloadData];
}



/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//__block UIActivityIndicatorView *activityIndicator;
//__weak UIImageView *weakImageView = recipeImageView;
//SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
//[downloader downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://dev3.saloncloudsplus.com/voga/image/%@",categoriesList.cateImage]]
//                         options:0
//                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                            // progression tracking code
//                            if (!activityIndicator) {
//                                [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
//                                activityIndicator.center = weakImageView.center;
//                                [activityIndicator startAnimating];
//                            }
//                            
//                        }
//                       completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
//                           if (image && finished) {
//                               // do something with image
//                               [recipeImageView setImage:image];
//                               [activityIndicator removeFromSuperview];
//                               activityIndicator = nil;
//                               
//                               
//                           }
//  }];


@end
