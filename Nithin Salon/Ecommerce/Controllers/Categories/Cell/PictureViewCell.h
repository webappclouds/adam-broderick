//
//  PictureViewCell.h
//  Store
//
//  Created by Webappclouds on 06/04/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureViewCell : UICollectionViewCell
@property (nonatomic, retain) IBOutlet UIImageView *recipeImageView;
@property (nonatomic, retain) IBOutlet UILabel *nameLable;
@property (nonatomic, retain) IBOutlet UILabel *priseLable;

@end
