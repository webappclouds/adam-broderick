//
//  FavoritesViewController.h
//  Store
//
//  Created by Webappclouds on 18/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesViewController : UIViewController
@property (nonatomic, retain) IBOutlet UITableView *favoriteTable;

@end
