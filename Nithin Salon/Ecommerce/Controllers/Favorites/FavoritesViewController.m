//
//  FavoritesViewController.m
//  Store
//
//  Created by Webappclouds on 18/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import "FavoritesViewController.h"
#import "UIViewController+NRFunctions.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CateInfoViewController.h"
#import "CartGlobals.h"
#import "Favorite.h"
#import "Constants.h"

@interface FavoritesViewController ()

@end

@implementation FavoritesViewController
@synthesize favoriteTable;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Favorites";
    NSLog(@"%@",[CartGlobals getFavoriteProducts]);
//    [self showAlert:@"" MSG:@"Coming soon" BLOCK:^{
//        [self.navigationController popViewControllerAnimated:YES];
//    }];
    // Do any additional setup after loading the view.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[CartGlobals getFavoriteProducts] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UILabel *nameLabel = (UILabel*)[cell viewWithTag:1];
    UILabel *priceLabel = (UILabel*)[cell viewWithTag:2];
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:3];

   
    Favorite *obj = [[CartGlobals getFavoriteProducts] objectAtIndex:indexPath.row];
    nameLabel.text = [NSString stringWithFormat:@"%@",obj.favCateListName];
    priceLabel.text = [NSString stringWithFormat:@"%@", obj.favCatePrice];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:IMAGE_BASE_URL,obj.favCateImage]]];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.favoriteTable deselectRowAtIndexPath:indexPath animated:YES];
    [self removeNavgationBackButtonTitle];
    Favorite *obj = [[CartGlobals getFavoriteProducts] objectAtIndex:indexPath.row];
    CateInfoViewController *cateInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"CateInfoViewController"];
    cateInfo.favoriteObj=obj;
    cateInfo.favoriteFlag=@"FAVORITE";
    [cateInfo setSelectedIndex:indexPath.row];
    [self.navigationController pushViewController:cateInfo animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([[CartGlobals getFavoriteProducts] count]==0) {
        [self showAlert:@"Favorite" MSG:@"No favorites found"];
    }
    [favoriteTable reloadData];
    NSLog(@"%d", [CartGlobals getFavoriteProducts]);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
