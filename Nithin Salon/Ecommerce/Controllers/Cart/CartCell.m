//
//  CartCell.m
//  Store
//
//  Created by Webappclouds on 05/03/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import "CartCell.h"

@implementation CartCell
@synthesize prodImageView, prodNameLable, prodPriceLable, prodDeleteButton, prodQuantityView,prodMinusButton, prodPluseButton, prodQuantityLable;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
