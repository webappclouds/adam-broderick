//
//  CartViewController.h
//  Store
//
//  Created by Webappclouds on 18/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartViewController : UIViewController

@property (nonatomic, retain) IBOutlet UITableView *cartTableView;

@property (nonatomic, retain) IBOutlet UILabel *totalPriceLabel;

@property (nonatomic, retain) IBOutlet UIView *continueView;

@property (nonatomic, retain) IBOutlet UIImageView *noCartImageView;

@property (nonatomic, retain) IBOutlet UIButton *continueButton;


@end
