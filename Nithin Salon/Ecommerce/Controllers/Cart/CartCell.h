//
//  CartCell.h
//  Store
//
//  Created by Webappclouds on 05/03/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *prodImageView;
@property (nonatomic, retain) IBOutlet UILabel *prodNameLable;
@property (nonatomic, retain) IBOutlet UIButton *prodDeleteButton;
@property (nonatomic, retain) IBOutlet UILabel *prodPriceLable;
@property (nonatomic, retain) IBOutlet UIView *prodQuantityView;
@property (nonatomic, retain) IBOutlet UIButton *prodPluseButton;
@property (nonatomic, retain) IBOutlet UIButton *prodMinusButton;
@property (nonatomic, retain) IBOutlet UILabel *prodQuantityLable;

@end
