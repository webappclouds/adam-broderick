//
//  CartViewController.m
//  Store
//
//  Created by Webappclouds on 18/02/17.
//  Copyright © 2017 SyamPrasad. All rights reserved.
//

#import "CartViewController.h"
#import "CartGlobals.h"
#import "CartCell.h"
#import "CartObj.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LoginVC.h"
#import "UIViewController+NRFunctions.h"

@interface CartViewController ()
{
    //int i;
    CartCell *cell;
    CartObj *obj;

}

@end

@implementation CartViewController
@synthesize cartTableView, totalPriceLabel, continueView, noCartImageView, continueButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Shopping Cart";
    [self refreshCheckoutAmount];

//    if ([[CartGlobals getProducts] count] ==0) {
//        continueView.hidden = YES;
//        cartTableView.hidden =YES;
//        noCartImageView.hidden =NO;
//    }
//    else{
//    continueView.hidden = NO;
//    cartTableView.hidden =NO;
//    noCartImageView.hidden =YES;
//    }
    
    //i = 1;
    
    // Do any additional setup after loading the view.
    NSLog(@"1");
}

-(void)viewWillAppear:(BOOL)animated
{
    
    continueButton.layer.cornerRadius = 2;
    continueButton.clipsToBounds = YES;
//    continueButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    continueButton.layer.borderWidth=1.0f;


//    NSInteger selectedIndx=self.tabBarController.selectedIndex;
//    
//    
//    if (selectedIndx==3) {
//        
//    if ([[CartGlobals getProducts] count] ==0) {
//        continueView.hidden = YES;
//        cartTableView.hidden =YES;
//        noCartImageView.hidden =NO;
//    }
//    else{
//        continueView.hidden = NO;
//        cartTableView.hidden =NO;
//        noCartImageView.hidden =YES;
//    }
//    }
    NSLog(@"2");
    [cartTableView reloadData];
    [self refreshCheckoutAmount];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"3");
  
}

-(void)viewWillDisappear:(BOOL)animated
{

    NSLog(@"4");
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"5");
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[CartGlobals getProducts] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CartCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CartCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    obj = [[CartGlobals getProducts] objectAtIndex:indexPath.row];
    [cell.prodImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",obj.productImage]]];
    cell.prodNameLable.text = [NSString stringWithFormat:@"%@",obj.productName];
    
    if (obj.productQuantity == 1) {
        cell.prodPriceLable.text = [NSString stringWithFormat:@"$ %0.2f",obj.productPrice];
    }
    else
    {
       cell.prodPriceLable.text = [NSString stringWithFormat:@"$ %0.2f",obj.totalPrice];
    }
    cell.prodQuantityLable.text = [NSString stringWithFormat:@"%d",obj.productQuantity];

    
    [cell.prodPluseButton setTag:indexPath.row];
    [cell.prodPluseButton addTarget:self action:@selector(pluseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.prodMinusButton setTag:indexPath.row];
    [cell.prodMinusButton addTarget:self action:@selector(minusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.prodDeleteButton setTag:indexPath.row];
    [cell.prodDeleteButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [self.categoriesTable deselectRowAtIndexPath:indexPath animated:YES];
//    [self removeNavgationBackButtonTitle];
//    Categories *obj = [categoriesArray objectAtIndex:indexPath.row];
//    CateListViewController *cateListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CateListViewController"];
//    cateListViewController.categoriesObj=obj;
//    [self.navigationController pushViewController:cateListViewController animated:YES];
//    
//    
//}
//


-(IBAction)pluseButtonClicked:(id)sender
{
    
   //    [cell.prodQuantityLable setTag:[sender tag ]];
//    [cell.prodQuantityLable setText:[NSString stringWithFormat:@"%d",i]];
//     CartObj *obj = [[CartGlobals getProducts]objectAtIndex:[sender tag]];
//    [cell.prodPriceLable setText:[NSString stringWithFormat:@"$ %0.2f",obj.productPrice*i]];
    NSLog(@"%d",[sender tag]);
    CartObj *cartObj = [[CartGlobals getProducts]objectAtIndex:[sender tag]];
    
    NSLog(@"%d",cartObj.i);
    if (cartObj.i==4){
        [self showAlert:@"" MSG:@"Sorry, you can't add more of these items"];
        return;
    }
    //else{
    cartObj.i++;

    
    [cartObj setProductQuantity:cartObj.i];
    [cartObj setProductId:obj.productId];
    [CartGlobals addProduct:cartObj];
    [cartTableView reloadData];
    [self refreshCheckoutAmount];
}

-(IBAction)minusButtonClicked:(id)sender
{
   //    [cell.prodQuantityLable setText:[NSString stringWithFormat:@"%d",i]];
//    CartObj *obj = [[CartGlobals getProducts]objectAtIndex:[sender tag]];
//    [cell.prodPriceLable setText:[NSString stringWithFormat:@"$ %0.2f",obj.productPrice*i]];
    CartObj *cartObj = [[CartGlobals getProducts]objectAtIndex:[sender tag]];
    if (cartObj.i==1)
        return;
    cartObj.i--;
    NSLog(@"%d",cartObj.i);
    

    [cartObj setProductQuantity:cartObj.i];
    [cartObj setProductId:obj.productId];
    [CartGlobals addProduct:cartObj];
    [cartTableView reloadData];
    [self refreshCheckoutAmount];
}

-(IBAction)deleteButtonClicked:(id)sender
{
    NSLog(@"%f",[CartGlobals getTotalPrice]);
    [[CartGlobals getProducts] removeObjectAtIndex:[sender tag]];
    [cartTableView reloadData];
    [self refreshCheckoutAmount];
    [[super.tabBarController.viewControllers objectAtIndex:2] tabBarItem].badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[[CartGlobals getProducts] count]];
    
    
}

-(void) refreshCheckoutAmount
{
    NSString *amountStr = [NSString stringWithFormat:@"$%0.2f",[CartGlobals getTotalPrice]];
    [totalPriceLabel setText:amountStr];
}

-(IBAction)continueButtonClicked:(id)sender
{
    if ([[CartGlobals getProducts] count]==0) {
        [self showAlert:@"" MSG:@"Please added one product"];
    }
    else{
    [self removeNavgationBackButtonTitle];
    LoginVC *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    loginViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:loginViewController animated:YES];
    }
    
    //self.tabBarController.tabBar.hidden=YES;
    //[self.tabBarController setTabBarItem:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
