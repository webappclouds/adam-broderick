//
//  BeveragesList.m
//  Nithin Salon
//
//  Created by Webappclouds on 25/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "BeveragesList.h"
#import "Globals.h"
#import "AppDelegate.h"
#import "BeverageObj.h"
@interface BeveragesList ()
{
    AppDelegate *appDelegate;
    NSString *beverageIDString;
    NSMutableArray *beveragesListArray;
}
@end

@implementation BeveragesList

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"Beverage Menu";
    
    self.beveragesTV.estimatedRowHeight =91;
    self.beveragesTV.rowHeight = UITableViewAutomaticDimension;
    appDelegate= (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self oldOrderCheckServiceCall];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
}


-(void)oldOrderCheckServiceCall{
    
    NSMutableDictionary *paramDict = [[NSMutableDictionary alloc]init];
    [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:LAST_ORDER_SERVICECALL HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            BOOL status = [[dict objectForKey:@"status"] boolValue];
            //beverageIDString  =[dict objectForKey:@"beverage_id"];
            
            if(!status)
                [self beveragesListServiceCall];
            else
            {
                beverageIDString = [[dict objectForKey:@"last_ordered"] objectForKey:@"beverage_id"];
                //[self showAlert:@"Check-In" MSG:[dict objectForKey:@"message"] tag:0];
                
                NSString *str = [NSString stringWithFormat:@"Would you like to have a %@",[[dict objectForKey:@"last_ordered"] objectForKey:@"beverage_name"]];
//                NSString *str = @"Would you like to have the same beverage you had last time?\nCucumber-Infused Water";
                
                [self showAlertView:str message:@"" tag:10];
            }
            
        }}];
}

-(void)showAlertView:(NSString *)title message:(NSString *)message tag:(int)tag{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if(tag == 10){
        [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            [self placeOrderServiceCall];
            
            
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            [self beveragesListServiceCall];
            
            
        }]];
    }
    
    if(tag == 0||tag==20){
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            
            if(tag==20){
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            
        }]];
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)placeOrderServiceCall{
    
    NSMutableDictionary *paramDict = [[NSMutableDictionary alloc]init];
    [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    [paramDict setObject:beverageIDString?:@"" forKey:@"beverage_id"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:PLACE_ORDER_URL HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            BOOL status = [[dict objectForKey:@"status"] boolValue];
            
            if(!status)
                [self showAlertView:@"Order" message:[dict objectForKey:@"message"] tag:0];
            else
            {
                //[self showAlert:@"Check-In" MSG:[dict objectForKey:@"message"] tag:0];
                
                [self showAlertView:@"Thank you" message:[dict objectForKey:@"message"] tag:20];
            }
        }}];
}


-(void)beveragesListServiceCall{
    NSMutableDictionary *paramDict = [[NSMutableDictionary alloc]init];
    [paramDict setObject:appDelegate.salonId forKey:@"param1"];
    NSString *moduleID = [self loadSharedPreferenceValue:@"Bevarages_ID"];
    [paramDict setObject:moduleID?:@"" forKey:@"param2"];
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",BEVEREGS_LIST_URL,appDelegate.salonId,moduleID];
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            BOOL status = [[dict objectForKey:@"status"] boolValue];
            
            if(!status)
                [self showAlertView:@"Beverages_List" message:[dict objectForKey:@"message"] tag:0];
            else
            {
                
                NSMutableArray *beveragesarray =  [dict objectForKey:@"beverages"];
                beveragesListArray = [[NSMutableArray alloc]init];
                for(NSDictionary *dict in beveragesarray){
                    BeverageObj *obj = [[BeverageObj alloc]init];
                    obj.name = [dict objectForKey:@"beverage_name"];
                    obj.description =[dict objectForKey:@"description"];
                    obj.imageURL = [dict objectForKey:@"image"];
                    obj.beverege_ID = [dict objectForKey:@"beverage_id"];
                    [beveragesListArray addObject:obj];
                    
                    
                }
                [self.beveragesTV reloadData];
            }
            
        }}];
    
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [beveragesListArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:100];
    imv.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    imv.layer.shadowOffset = CGSizeMake(0,0);
    imv.layer.shadowOpacity = 0.2;
    imv.layer.shadowRadius = 4.0;
    imv.layer.masksToBounds =NO;
    
    if(beveragesListArray.count){
        BeverageObj *staffObj = [beveragesListArray objectAtIndex:indexPath.row];
        UILabel *nameLabel = (UILabel*)[cell viewWithTag:10];
        UILabel *designationLabel = (UILabel*)[cell viewWithTag:20];
        UIImageView *imageView = (UIImageView*)[cell viewWithTag:30];
        UIButton *orderButton = (UIButton *)[cell viewWithTag:50];
        orderButton.tag = indexPath.section;
        [orderButton addTarget:self action:@selector(placeOrder:) forControlEvents:UIControlEventTouchUpInside];
        nameLabel.text=[NSString stringWithFormat:@"%@",staffObj.name];
        designationLabel.text=[NSString stringWithFormat:@"%@",staffObj.description];
        
        
        if([staffObj.imageURL length]>0)
        {
            NSString *imageUrl = staffObj.imageURL;
            if(![imageUrl containsString:IMAGES_URL])
                imageUrl = [NSString stringWithFormat:@"%@", staffObj.imageURL];
            imageView.bounds = CGRectInset(imageView.frame, 20.0f, 20.0f);
            [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
            
        }
        else
            [imageView setImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
        
    }
    
    return cell;
}

-(void)placeOrder:(UIButton*)sender{
    
    int tag = sender.tag;
    BeverageObj *staffObj = [beveragesListArray objectAtIndex:tag];
    NSMutableDictionary *paramDict = [[NSMutableDictionary alloc]init];
    [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
    [paramDict setObject:staffObj.beverege_ID?:@"" forKey:@"beverage_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:BEVEREGE_PLACE_ORDER_URL HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            BOOL status = [[dict objectForKey:@"status"] boolValue];
            
            if(!status)
                [self showAlertView:@"Error" message:[dict objectForKey:@"message"] tag:0];
            else
            {
                
                [self showAlertView:@"Thank you" message:[dict objectForKey:@"message"] tag:20];
                
            }
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
