//
//  BeveragesList.h
//  Nithin Salon
//
//  Created by Webappclouds on 25/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeveragesList : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(weak,nonatomic)IBOutlet UITableView *beveragesTV;
@end
