//
//  ViewController.m
//  MyGallery
//
//  Created by Nithin Reddy on 22/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "MyGalleryViewController.h"
#import "DYRateView.h"
#import "GalleryCell.h"
#import "SelectedViewController.h"
#import "AddGalleryImage.h"
#import "GlobalSettings.h"
#import "UIViewController+NRFunctions.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"

@interface MyGalleryViewController ()

@end

@implementation MyGalleryViewController

@synthesize data,flag;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.data = [NSMutableArray new];
    
    self.title = @"My Gallery";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addClicked)];
    
        [self loadImages];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadImages) name:@"IMAGE_ADDED" object:Nil];
    
    if(flag != 0)
    [self addClicked];
    
    /*
    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample1.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample2.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample3.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample4.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample5.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample6.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample7.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
     */
//    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample8.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
//    [self.data addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sample9.jpg", @"image", [NSNumber numberWithFloat:4.5], @"rating", nil]];
}

-(void) loadImages
{
    [self.data removeAllObjects];
    NSMutableDictionary *clientDict =[NSMutableDictionary new];
    [clientDict setObject:[self loadSharedPreferenceValue:@"clientid"] forKey:@"client_id"];//[self loadSharedPreferenceValue:@"clientid"]?:@""
    [self showHud];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:LOAD_ALL_IMAGES_URL HEADERS:Nil GETPARAMS:Nil POSTPARAMS:clientDict COMPLETIONHANDLER:^(NSDictionary *responseDict, NSURLResponse *urlResponse, NSError *error) {
        [self hideHud];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            BOOL status = [[responseDict objectForKey:@"status"] boolValue];
            if(status)
            {
                [self.data addObjectsFromArray:[responseDict objectForKey:@"images"]];
            }
//            else
//                [self showAlert:@"Error" MSG:[responseDict objectForKey:@"msg"]];
        }
        [self.collectionView reloadData];
    }];
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([self.data count]==0)
        return 0;
    if([self.data count]%2==1 && section==[self.data count]/2)
        return 1;
    return 2;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if([self.data count]==0)
        return 0;
    return (ceil)([self.data count]/2.0);
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = @"GalleryCell";
    GalleryCell *cell = (GalleryCell *) [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
//    if(cell==Nil)
//        cell = [[GalleryCell alloc] init];
    
    NSDictionary *dict = [self.data objectAtIndex:indexPath.section*2+indexPath.item];
    [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"url"]] placeholderImage:[UIImage imageNamed:@"profile_picture.png"]];
    [cell.profileImageView setContentMode:UIViewContentModeScaleAspectFill];
    [cell.profileImageView setClipsToBounds:YES];
    [cell.profileImageView setBackgroundColor:[UIColor lightGrayColor]];
    cell.profileImageView.layer.masksToBounds = YES;
    [cell.stylistLabel setText:[NSString stringWithFormat:@"Stylist: %@", [dict objectForKey:@"stylist"]]];
    [cell.dateLabel setText:[dict objectForKey:@"date"]];
    [cell.totalReviewsLabel setText:[NSString stringWithFormat:@"Total Reviews %@", [dict objectForKey:@"totalreviews"]]];
    
    DYRateView *rateView = [[DYRateView alloc] initWithFrame:CGRectMake(0, 0, cell.rateView.frame.size.width, 20) fullStar:[UIImage imageNamed:@"StarFull"] emptyStar:[UIImage imageNamed:@"StarEmpty"]];
//    rateView.padding = 20;
    rateView.rate = [[dict objectForKey:@"avg_rating"] floatValue];
    rateView.alignment = RateViewAlignmentCenter;
    rateView.editable = NO;
//    rateView.delegate = self;
    
    for (UIView *v in [cell.rateView subviews])
        [v removeFromSuperview];

    [cell.rateView addSubview:rateView];
    
//    [cell.rateView setRate:4.3];
//    [(DYRateView *)cell.rateView setRate:[[dict objectForKey:@"rating"] floatValue]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectionView.frame.size.width/2-20, self.collectionView.frame.size.height*0.40);
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    SelectedViewController *selected = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedViewController"];
    [selected setData:[self.data objectAtIndex:indexPath.section*2+indexPath.item]];
    [self.navigationController pushViewController:selected animated:YES];
}

-(void) addClicked
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    AddGalleryImage *addGalleryImage = [self.storyboard instantiateViewControllerWithIdentifier:@"AddGalleryImage"];
    [self.navigationController pushViewController:addGalleryImage animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
