//
//  ApptResponse.m
//  Nithin Salon
//
//  Created by Webappclouds on 04/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ApptResponse.h"
#import "Constants.h"
#import "AppDelegate.h"
@interface ApptResponse ()
{
    AppDelegate *appDelegate;
}
@end

@implementation ApptResponse

@synthesize label;
@synthesize str;
@synthesize labelStr;
@synthesize type, salonId, apptId;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    label.layer.borderColor = [UIColor lightGrayColor].CGColor;
    label.layer.borderWidth = 1.0f;
    label.layer.cornerRadius = 5.0f;
    
    
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSArray *arr = [str componentsSeparatedByString:@","];
    
    salonId = [[NSString alloc] initWithString:(NSString *)[arr objectAtIndex:0]];
    apptId = [[NSString alloc] initWithString:(NSString *)[arr objectAtIndex:1]];
    type = [[NSString alloc] initWithString:(NSString *)[arr objectAtIndex:2]];
    
    labelStr = [labelStr stringByReplacingOccurrencesOfString:@"Tap here for more details" withString:@"Click Yes if you are coming, or No if you can't."];
    [label.layer setCornerRadius:10.0];
    [label.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [label.layer setBorderWidth:1.0];
    
    
    [label setFont:[UIFont systemFontOfSize:16]];
    [label setEditable:NO];
    [label setText:labelStr];
    
    [self setTitle:@"Confirmation"];
    

    // Do any additional setup after loading the view.
}

-(IBAction)yes:(id)sender
{
    [self sendResp:@"1"];
}

-(IBAction)no:(id)sender
{
    [self sendResp:@"0"];
}
-(void) sendResp : (NSString *) response
{
    [appDelegate showHUD];
    NSString *url = [NSString stringWithFormat:@"%@%@/%@/%@", [appDelegate addSalonIdTo:URL_RESERVATION_RESPONSE_UPDATE], apptId, type, response];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:url HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
    }];
    
  

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
