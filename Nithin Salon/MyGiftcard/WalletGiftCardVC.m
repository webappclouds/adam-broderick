//
//  WalletGiftCardVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 24/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "WalletGiftCardVC.h"
#import "SSStackedPageView.h"
#import "UIColor+CatColors.h"
#import "ColoredCardView.h"
#import "AppDelegate.h"
#import "GiftcardGlobals.h"
@interface WalletGiftCardVC ()<SSStackedViewDelegate>{
    AppDelegate *appDelegate;
    NSMutableArray *giftCardsList;
    NSMutableArray *giftCardsObjList;
}
@property (nonatomic) SSStackedPageView *stackView;
@property (nonatomic) NSMutableArray *views;
@end

@implementation WalletGiftCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
       self.views = [[NSMutableArray alloc]init];
    /*for(int i=0;i<500;i++)
    {
        ColoredCardView *thisView = [[[NSBundle mainBundle] loadNibNamed:@"ColoredCardView" owner:self options:nil] objectAtIndex:0];
        thisView.indexLabel.text = [NSString stringWithFormat:@"%ld",(long)i];
        
        /*indexLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,10, 100, 30)];
         //indexLabel.backgroundColor = [UIColor redColor];
         indexLabel.text = [NSString stringWithFormat:@"%ld",(long)i];
         indexLabel.textColor = [UIColor whiteColor];
         [thisView addSubview:indexLabel];
     
        
        [self.views addObject:thisView];
    }*/
    
  
    [self callWebservice];
}

-(void)callWebservice{
    giftCardsList = [GiftcardGlobals getGiftCards];
    
    giftCardsObjList = [[NSMutableArray alloc] init];
    
    
    
    self.title = @"My Giftcards";
    if(appDelegate.netAvailability == YES){
        
        if([giftCardsList count]==0)
            [self showAlert:@"Gift Cards" MSG:@"No Giftcards available." tag:1];
        else
        {
            
            NSString *cardsList = @"";
            for(int i=0;i<[giftCardsList count];i++)
                cardsList = [NSString stringWithFormat:@"%@\"%@\",", cardsList, [giftCardsList objectAtIndex:i]];
            
            cardsList = [cardsList substringToIndex:[cardsList length]-1];
            cardsList = [NSString stringWithFormat:@"[%@]", cardsList];
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:cardsList forKey:@"voucher_codes"];
            
            [appDelegate showHUD];
            [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_GIFTCARD_DETAILS] HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                [appDelegate hideHUD];
                if(error)
                    [self showAlert:@"Error" MSG:error.localizedDescription];
                else
                {
                    
                    NSDictionary *dict = (NSDictionary *)responseArray;
                    
                    NSLog(@"Gift Cards LIst ; %@",dict);
                    if([[dict objectForKey:@"status"] boolValue])
                    {
                        
                       
                        
                        NSMutableArray *vouchers = [dict objectForKey:@"vouchers"];
                        for(int i=0;i<[vouchers count];i++)
                        {
                            NSDictionary *subDict = [vouchers objectAtIndex:i];
                            ColoredCardView *thisView = [[[NSBundle mainBundle] loadNibNamed:@"ColoredCardView" owner:self options:nil] objectAtIndex:0];
                            //thisView.frame =CGRectMake(0, 0, 10, 10);
                            thisView.valueLabel.text = [NSString stringWithFormat:@"$ %@",[subDict objectForKey:@"voucher_value"]]?:@"";
                            thisView.giftCardNumberLabel.text =[subDict objectForKey:@"voucherCode"]?:@"";
                            thisView.fromLabel.text = [subDict objectForKey:@"customer_name"]?:@"";
                            thisView.statusLabel.text =@"Active";
                            thisView.messageLabel.text =[subDict objectForKey:@"message"]?:@"";
                            [self.views addObject:thisView];
                        }
                        
                        /*GiftcardObject *obj = [[GiftcardObject alloc] init];
                         NSDictionary *subDict = [vouchers objectAtIndex:i];
                         [obj setVoucherCode:[subDict objectForKey:@"voucherCode"]];
                         [obj setVoucherValue:[subDict objectForKey:@"voucher_value"]];
                         [obj setFromName:[subDict objectForKey:@"customer_name"]];
                         [obj setFromEmail:[subDict objectForKey:@"customer_email"]];
                         [obj setToEmail:[subDict objectForKey:@"receiver_email"]];
                         [obj setToName:[subDict objectForKey:@"receiver_name"]];
                         [obj setMessage:[subDict objectForKey:@"message"]];
                         [giftCardsObjList addObject:obj];
                         }*/
                        
                        if([self.views count]>0){
                            
                            _stackView = [[SSStackedPageView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height-75)];
                            _stackView.backgroundColor = [UIColor whiteColor];
                            _stackView.delegate = self;
                            _stackView.pagesHaveShadows =YES;
                            [self.view addSubview:_stackView];
                            //[[NSNotificationCenter defaultCenter]postNotificationName:@"GiftCardsWallet" object:nil];
                            
                        }
                        else
                            [self showAlert:@"Error" MSG:@"No vouchers found." tag:1];
                    }
                    else
                    {
                        [self showAlert:@"Error" MSG:@"Error occured. Please try again later."];
                    }
                    
                }
            }];
            
        }
    }
    else{
        [self showAlert:@"Network error" MSG:@"Could not connect to the server. Please check your network connectivity." tag:1];
    }
    

}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GiftCardsWallet" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(process) name:@"GiftCardsWallet" object:nil];
}

-(void)process{
    _stackView.delegate = self;
    _stackView.pagesHaveShadows = YES;
    
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIView *)stackView:(SSStackedPageView *)stackView pageForIndex:(NSInteger)index
{
   
    UIView *thisView = [stackView dequeueReusablePage];
    if (!thisView) {
        thisView = [self.views objectAtIndex:index];
        if(index/4==0){
                   }
        else {
            index =0;
        }
        NSLog(@"Index: %d",index);
        thisView.backgroundColor = [UIColor getRandomColor:index];
         index++;
        thisView.layer.cornerRadius = 5;
        thisView.layer.masksToBounds = YES;
    }
    return thisView;
}




- (NSInteger)numberOfPagesForStackView:(SSStackedPageView *)stackView
{
    
    
    return [self.views count];
}

- (void)stackView:(SSStackedPageView *)stackView selectedPageAtIndex:(NSInteger) index
{
    NSLog(@"selected page: %i",(int)index);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
