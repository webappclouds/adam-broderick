//
//  MyGiftcards.m
//  Visage
//
//  Created by Nithin Reddy on 15/11/12.
//
//

#import "MyGiftcards.h"
#import "MyGiftCardDetail.h"
#import "GiftcardGlobals.h"
#import "GiftcardObject.h"
#import "Constants.h"
#import "AppDelegate.h"
@interface MyGiftcards ()

@end

@implementation MyGiftcards
{
    NSMutableArray *giftCardsList;
    NSMutableArray *giftCardsObjList;
    AppDelegate *appDelegate;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    giftCardsList = [GiftcardGlobals getGiftCards];
    
    giftCardsObjList = [[NSMutableArray alloc] init];
    
    
    
    self.title = @"My Giftcards";
    if(appDelegate.netAvailability == YES){
    
    if([giftCardsList count]==0)
        [self showAlert:@"Gift Cards" MSG:@"No Giftcards available." tag:1];
    else
    {
        
        NSString *cardsList = @"";
        for(int i=0;i<[giftCardsList count];i++)
            cardsList = [NSString stringWithFormat:@"%@\"%@\",", cardsList, [giftCardsList objectAtIndex:i]];
        
        cardsList = [cardsList substringToIndex:[cardsList length]-1];
        cardsList = [NSString stringWithFormat:@"[%@]", cardsList];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:cardsList forKey:@"voucher_codes"];
        
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_GIFTCARD_DETAILS] HEADERS:nil GETPARAMS:nil POSTPARAMS:dict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                
                NSDictionary *dict = (NSDictionary *)responseArray;
                if([[dict objectForKey:@"status"] boolValue])
                {
                    NSMutableArray *vouchers = [dict objectForKey:@"vouchers"];
                    for(int i=0;i<[vouchers count];i++)
                    {
                        GiftcardObject *obj = [[GiftcardObject alloc] init];
                        NSDictionary *subDict = [vouchers objectAtIndex:i];
                        [obj setVoucherCode:[subDict objectForKey:@"voucherCode"]];
                        [obj setVoucherValue:[subDict objectForKey:@"voucher_value"]];
                        [obj setFromName:[subDict objectForKey:@"customer_name"]];
                        [obj setFromEmail:[subDict objectForKey:@"customer_email"]];
                        [obj setToEmail:[subDict objectForKey:@"receiver_email"]];
                        [obj setToName:[subDict objectForKey:@"receiver_name"]];
                        [obj setMessage:[subDict objectForKey:@"message"]];
                        [giftCardsObjList addObject:obj];
                    }
                    
                    if([vouchers count]>0)
                        [self.tableView reloadData];
                    else
                        [self showAlert:@"Error" MSG:@"No vouchers found." tag:1];
                }
                else
                {
                    [self showAlert:@"Error" MSG:@"Error occured. Please try again later."];
                }
                
            }
        }];
        
    }
    }
    else{
        [self showAlert:@"Network error" MSG:@"Could not connect to the server. Please check your network connectivity." tag:1];
    }
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [giftCardsObjList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GiftCardCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(giftCardsList.count){
         tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    UILabel *label1 = (UILabel*)[cell.contentView viewWithTag:20];
    UIButton *moreInfo = (UIButton*)[cell.contentView viewWithTag:30];
    
    label1.text = [NSString stringWithFormat:@"$ %@", [[giftCardsObjList objectAtIndex:indexPath.row] voucherValue]];
    
    [moreInfo setTag:indexPath.row];
    
    [moreInfo addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    }
    return cell;
}

-(IBAction)buttonTapped:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    int tag = (int)btn.tag;
    GiftcardObject *obj = [giftCardsObjList objectAtIndex:tag];
    MyGiftCardDetail *myGiftCardDetail = [[MyGiftCardDetail alloc] initWithNibName:@"MyGiftCardDetail" bundle:nil];
    [myGiftCardDetail setGiftCardObj:obj];
    [self pushController:myGiftCardDetail withTransition:UIViewAnimationTransitionFlipFromLeft];
    
}



-(void)pushController:(UIViewController *)controller withTransition:(UIViewAnimationTransition)transition{
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:
     UIViewAnimationTransitionFlipFromRight
                           forView:self.navigationController.view cache:NO];
    
    
    [self.navigationController pushViewController:controller animated:YES];
    [UIView commitAnimations];
    
}



@end
