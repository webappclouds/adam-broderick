//
//  ReviewsList.m
//  Webappclouds
//
//  Created by Nithin Reddy on 07/03/13.
//
//

#import "ReviewsList.h"
#import "Constants.h"
#import "ReviewObj.h"
#import "DYRateView.h"
#import "ReviewUsVC.h"
#import "GoogleReviewVC.h"
#import "AppDelegate.h"
@interface ReviewsList ()
{
    AppDelegate *appDelegate;
}
@end

@implementation ReviewsList

@synthesize reviewsList;

//
//- (id)initWithStyle:(UITableViewStyle)style
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    _midSpaceOfReviews.constant = 200;
    
    AppDelegate *appDelegateObje = (AppDelegate *)[UIApplication sharedApplication].delegate;

    NSArray *arrayOfReviews = appDelegateObje.reivewsDictionary.allKeys;
    
    NSLog(@"here reviews = %@",appDelegateObje.reivewsDictionary);
    NSLog(@"here reviews keys = %@",appDelegateObje.reivewsDictionary.allKeys);

    NSLog(@"count = %d",arrayOfReviews.count);
        if (arrayOfReviews.count >0) {
            _widthOfReviewUs.constant = 0;
            
        } else {
            _widthOfReviewUs.constant = self.view.frame.size.width - 10;
        }
    
    if([arrayOfReviews count] == 1)
    {
        [_otherReviewsButton setTitle:[arrayOfReviews objectAtIndex:0] forState:UIControlStateNormal];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate showHUD];
    reviewsList = [[NSMutableArray alloc] init];
    
    self.title = @"Reviews";
    self.tableView.estimatedRowHeight = 75;
    self.tableView.rowHeight=UITableViewAutomaticDimension;
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_GET_REVIEWS] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSDictionary *wholeJson = (NSDictionary*)responseArray;
            NSString *response;
            if ([response isEqualToString:@"dfsdf"]||[wholeJson count]==0) {
                [self showAlert:SALON_NAME MSG:@"Reviews not available. Please try again later." tag:2];
                
            }else {
                
                NSMutableArray *reviewArray = [wholeJson objectForKey:@"ratings"];
                for(int i=0;i<[reviewArray count];i++)
                {
                    NSDictionary *review = [reviewArray objectAtIndex:i];
                    ReviewObj *obj = [[ReviewObj alloc] init];
                    [obj setTitle:[review objectForKey:@"title"]];
                    NSString *name = [review objectForKey:@"name"];
                    NSString *date = [review objectForKey:@"date"];
                    NSString *dateName = [NSString stringWithFormat:@"By %@ on %@", name, date];
                    [obj setTimeName:dateName];
                    [obj setDescription:[review objectForKey:@"description"]];
                    [obj setRating:[[review objectForKey:@"rating"] intValue]];
                    [reviewsList addObject:obj];
                    
                }
                if([reviewsList count]>0)
                    [self.tableView reloadData];
            }
        }
        
    }];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [reviewsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ReviewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        
    }
    [cell setLayoutMargins:UIEdgeInsetsZero];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the cell...
    
    if ([self.reviewsList count] > 0)
    {
         tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        ReviewObj *obj = [[self reviewsList] objectAtIndex:indexPath.row];
        
        UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:10];
        UILabel *timeLabel = (UILabel *)[cell.contentView viewWithTag:11];
        UIView *ratingHolder = (UIView *)[cell.contentView viewWithTag:12];
        UILabel *descriptionLabel = (UILabel *)[cell.contentView viewWithTag:13];
        
        titleLabel.text = obj.title;
        timeLabel.text = obj.timeName;
        descriptionLabel.text = obj.description;
        
        DYRateView *rateView = [[DYRateView alloc] initWithFrame:CGRectMake(0, 2, 160, 18)];
        [rateView setEditable:NO];
        [rateView setAlignment:RateViewAlignmentLeft];
        [rateView setRate:obj.rating];
        [rateView setEmptyStarImage:[UIImage imageNamed:@"StarEmpty.png"]];
        [rateView setFullStarImage:[UIImage imageNamed:@"StarFull.png"]];
        [ratingHolder addSubview:rateView];
        
    }
    
    
    return cell;
}


#pragma mark - Table view delegate

-(IBAction)googlePlus:(id)sender
{
    AppDelegate *appDelegateObje = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSArray *arrayOfReviews = appDelegateObje.reivewsDictionary.allKeys;

    if([arrayOfReviews count] == 1)
    {
        
        GoogleReviewVC *googlePlus = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
        [googlePlus setUrl:[appDelegateObje.reivewsDictionary objectForKey:[arrayOfReviews objectAtIndex:0]]];
        [googlePlus setTitle:[arrayOfReviews objectAtIndex:0]];
        [self.navigationController pushViewController:googlePlus animated:YES];
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Reviews" message:@"Please choose one" preferredStyle:UIAlertControllerStyleActionSheet];
        
        for(NSString *reviewType in arrayOfReviews)
            [alert addAction:[UIAlertAction actionWithTitle:reviewType style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                GoogleReviewVC *googlePlus = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
                [googlePlus setUrl:[appDelegateObje.reivewsDictionary objectForKey:reviewType]];
                [googlePlus setTitle:reviewType];
                [self.navigationController pushViewController:googlePlus animated:YES];
            }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
        [self presentViewController:alert animated:YES completion:nil];
    }

    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
}

-(IBAction)reviewsUs:(id)sender
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    ReviewUsVC *loyalityRegVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewUsVC"];
    
    [self.navigationController pushViewController:loyalityRegVC animated:YES];
}


@end
