//
//  OnlineCalendarVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 08/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "OnlineCalendarVC.h"
#import "BBBadgeBarButtonItem.h"
#import "AppDelegate.h"
#import "Globals.h"
#import "GlobalSettings.h"
#import "Appt.h"
#import "Constants.h"
#import "SelectAppt.h"
#import "Globals.h"
#import "GlobalSettings.h"
#import <EventKit/EventKit.h>
#import "CartListVC.h"
#import "ApptCell.h"
#import "ApptRest.h"
@interface OnlineCalendarVC ()
{
    AppDelegate *appDelegate;
    BBBadgeBarButtonItem *barButton;
    NSDateFormatter *dateFormatter2;
    NSString *dateString, *titleString;
    int servicecall;
    NSMutableArray *apptData;
    NSMutableDictionary *paramServiceDict;
    int selectedIndex;
    NSMutableArray *serIdArray;
    NSMutableArray *empIdArray;
    
    int dividerCount;
    NSString *bookedServiceName,*bookedProvider,*bookedTtime,*bookedFromTIme,*bookedToTime;
    NSDate *bookedDate;
    
    NSArray *timeDemoArray;
    
    NSMutableArray *serviceIDArray,*staffIdArray,*dummyStaffArrayValues;
    NSString *staffString;
    NSString *servicesIdsStr,*selecteddEmpstr;
    NSString *convertedDate;
    
    
}
@property (strong, nonatomic) NSCalendar *gregorian;

@end

@implementation OnlineCalendarVC

@synthesize selectedApptData,selectedEmp,compareString,reBookDict;

- (IBAction)morningAction:(id)sender {
    [_morningBtn setTitleColor:[self colorFromHexString:@"#F92E2E"] forState:UIControlStateNormal];
    [_eveningBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_anyTimeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    titleString =@"morning";
    [self availableSlatsTimings];
    
}

- (IBAction)eveningAction:(id)sender {
    [_eveningBtn setTitleColor:[self colorFromHexString:@"#F92E2E"] forState:UIControlStateNormal];
    [_anyTimeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_morningBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    titleString =@"evening";
    [self availableSlatsTimings];
    
}

- (IBAction)anytimeAction:(id)sender {
    [_anyTimeBtn setTitleColor:[self colorFromHexString:@"#F92E2E"] forState:UIControlStateNormal];
    [_morningBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_eveningBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    titleString =@"anytime";
    [self availableSlatsTimings];
    
    
}
- (IBAction)prevMonth:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
    
}
- (IBAction)nextMonth:(id)sender {
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex =-1;
    servicecall = 0;
    _timeTV.hidden =YES;
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    timeDemoArray = @[@"12:00 PM", @"12:30 PM", @"1:00 PM", @"1:30 PM", @"2:00 PM", @"2:30 PM", @"3:30 PM", @"5:00 PM"];
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    paramServiceDict=[[NSMutableDictionary alloc]init];
    titleString =@"anytime";
    [_anyTimeBtn setTitleColor:[self colorFromHexString:@"#F92E2E"] forState:UIControlStateNormal];
    
    //_calendar.minimumDate =[NSDate date];
    serviceIDArray = [NSMutableArray new];
    staffIdArray=[NSMutableArray new];
    dummyStaffArrayValues=[NSMutableArray new];
    
    
    serIdArray=[[NSMutableArray alloc]init];
    empIdArray=[[NSMutableArray alloc]init];
    
    
    
    
    //    for (NSMutableDictionary *obj in selectedApptData)
    //    {
    //        [serIdArray addObject:[obj objectForKey:@"serviceID"]];
    //        if ([selectedEmp isEqualToString:@"No"])
    //        {
    //            [empIdArray addObject:@"0"];
    //        }
    //        else {
    //            [empIdArray addObject:[_paramDict objectForKey:@"EmpId"]];
    //        }
    //        NSLog(@"%@",serIdArray);
    //    }
    
    //    for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++) {
    //
    //        staffString=[[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"StaffDetails"] objectForKey:@"employeeId"];
    //        [staffIdArray addObject:staffString];
    //
    //        [serviceIDArray addObject:[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"]];
    //        NSArray *addonArray=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"AddOn"];
    //
    //        if (addonArray.count) {
    //            for (int i=0; i<addonArray.count; i++)
    //            {
    //                NSString *addonServiceName=[[addonArray objectAtIndex:i] objectForKey:@"addon_service_iid"];
    //                [serviceIDArray addObject:addonServiceName];
    //                [staffIdArray addObject:staffString];
    //            }
    //        }
    //    }
    //
    //    servicesIdsStr = [NSString stringWithFormat:@"%@", [serviceIDArray componentsJoinedByString:@","]];
    //    selecteddEmpstr = [NSString stringWithFormat:@"%@", [staffIdArray componentsJoinedByString:@","]];
    //
    
    NSLog(@"Cal : %@",appDelegate.globalCheckAndUncheckArray);
    for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++) {
        
        staffString=[[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"StaffDetails"] objectForKey:@"employeeId"];
        [staffIdArray addObject:staffString];
        
        [serviceIDArray addObject:[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"]];
        NSArray *addonArray=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"AddOn"];
        
        if (addonArray.count) {
            for (int i=0; i<addonArray.count; i++)
            {
                NSString *addonServiceName=[[addonArray objectAtIndex:i] objectForKey:@"addon_service_iid"];
                [serviceIDArray addObject:addonServiceName];
                [staffIdArray addObject:staffString];
                
            }
        }
    }
    
    servicesIdsStr = [NSString stringWithFormat:@"%@", [serviceIDArray componentsJoinedByString:@","]];
    selecteddEmpstr = [NSString stringWithFormat:@"%@", [staffIdArray componentsJoinedByString:@","]];
    
    
    _nextButton.layer.borderColor=[UIColor whiteColor].CGColor;
    _nextButton.layer.borderWidth=2.0;
    _nextButton.layer.cornerRadius=4.0;
    
    
    
    self.title = @"Date & Time";
    NSLocale *chinese = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    dateFormatter2 = [[NSDateFormatter alloc] init];
    dateFormatter2.locale = chinese;
    dateFormatter2.dateFormat = @"MMM dd, yyyy";
    
    dateString=[dateFormatter2 stringFromDate:[NSDate date]];
    _dateLabel.text = [NSString stringWithFormat:@"Date : %@",dateString];
    //        [self getList:dateString];
    
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [customButton addTarget:self action:@selector(barButtonItemPressed) forControlEvents:UIControlEventTouchUpInside];
    [customButton setImage:[UIImage imageNamed:@"cart_icon"] forState:UIControlStateNormal];
    
    // Then create and add our custom BBBadgeBarButtonItem
    barButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
    // Set a value for the badge
    barButton.badgeValue = [NSString stringWithFormat:@"%d", appDelegate.globalCheckAndUncheckArray.count];
    barButton.badgeBGColor = [self colorFromHexString:@"#3BC651"];
    
    barButton.badgeOriginX = 0;
    barButton.badgeOriginY = 0;
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    convertedDate = [df stringFromDate:[NSDate date]];
    
    // Add it as the leftBarButtonItem of the navigation bar
    self.navigationItem.rightBarButtonItem = barButton;
    
    self.timeTV.allowsMultipleSelection =NO;
    [self.timeTV reloadData];
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self RefreshCount];
}
-(void)RefreshCount{
    [self setTopBadgeValue:nil];
    
    [staffIdArray removeAllObjects];
    [serviceIDArray removeAllObjects];
    
    NSLog(@"Cal : %@",appDelegate.globalCheckAndUncheckArray);
    for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++) {
        
        staffString=[[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"StaffDetails"] objectForKey:@"employeeId"];
        [staffIdArray addObject:staffString];
        
        [serviceIDArray addObject:[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"]];
        NSArray *addonArray=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"AddOn"];
        
        if (addonArray.count) {
            for (int i=0; i<addonArray.count; i++)
            {
                NSString *addonServiceName=[[addonArray objectAtIndex:i] objectForKey:@"addon_service_iid"];
                [serviceIDArray addObject:addonServiceName];
                [staffIdArray addObject:staffString];
                
            }
        }
    }
    
    servicesIdsStr = [NSString stringWithFormat:@"%@", [serviceIDArray componentsJoinedByString:@","]];
    selecteddEmpstr = [NSString stringWithFormat:@"%@", [staffIdArray componentsJoinedByString:@","]];
    
    
}

-(void)setTopBadgeValue:(int)sectionVal
{
    
    int totalCount = 0;
    
    for(int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
    {
        NSMutableDictionary *dict = [appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
        
        if(dict.count){
            
            NSArray *array = [[dict objectForKey:@"AddOn"] mutableCopy];
            int k = (int)array.count;
            totalCount = totalCount+k;
        }
        
    }
    
    int globalCount = (int)appDelegate.globalCheckAndUncheckArray.count;
    totalCount =  totalCount + globalCount;
    
    appDelegate.totalCount = totalCount;
    barButton.badgeValue =[NSString stringWithFormat:@"%i", totalCount];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) barButtonItemPressed
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    if(appDelegate.globalCheckAndUncheckArray.count){
        CartListVC *clv = [self.storyboard instantiateViewControllerWithIdentifier:@"CartListVC"];
        clv.selectedApptData = [appDelegate.globalCheckAndUncheckArray mutableCopy];
        [self.navigationController pushViewController:clv animated:YES];
    }
}
#pragma mark - FSCalendarDataSource

- (NSString *)calendar:(FSCalendar *)calendar titleForDate:(NSDate *)date
{
    return [self.gregorianCalendar isDateInToday:date] ? @"" : nil;
}


- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


-(void)getList:(NSString *)datestring{
    
    if([compareString isEqualToString:@"Rebook"]){
        [reBookDict setObject:dateString forKey:@"FromDate"];
        [reBookDict setObject:@"custom" forKey:@"Date"];
        [reBookDict setObject:dateString forKey:@"ToDate"];
        [reBookDict setObject:[_paramDict objectForKey:@"Time"] forKey:@"Time"];
        
        [self checkForAvailability:reBookDict];
    }
    else{
        NSString *empIdStr = [empIdArray componentsJoinedByString:@","];
        NSString *serviceIdStr = [serIdArray componentsJoinedByString:@","];
        
        [paramServiceDict setObject:serviceIdStr?:@"" forKey:@"ServiceIds"];
        [paramServiceDict setObject:empIdStr?:@"" forKey:@"EmployeeIds"];
        [paramServiceDict setObject:[_paramDict objectForKey:@"Time"] forKey:@"Time"];
        NSString *clientIDStr = [self loadSharedPreferenceValue:@"clientid"];
        [paramServiceDict setObject:clientIDStr forKey:@"ClientId"];
        [paramServiceDict setObject:dateString forKey:@"FromDate"];
        [paramServiceDict setObject:@"custom" forKey:@"Date"];
        [paramServiceDict setObject:dateString forKey:@"ToDate"];
        [self checkForAvailability:paramServiceDict];
    }
    
}
-(void)checkForAvailability :(NSMutableDictionary *)paramDic
{
    NSLog(@"checkForAvailability Prarams : %@",paramDic);
    [appDelegate showHUD];
    
    NSString *stringUrl;
    if ([appDelegate.onlinebookingType isEqualToString:@"1"]) {
        stringUrl = CHECK_APPT_LIST_REST;
    }
    else {
        stringUrl = CHECK_APPT_LIST;
    }
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:stringUrl HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription tag:0];
        else
        {
            servicecall =0;
            NSDictionary *dict = (NSDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
}

#pragma mark - FSCalendarDelegate
- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar
{
    return [NSDate date];
}
- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    
    
    selectedIndex=-1;
    [self.timeTV setHidden:YES];
    dateString=[dateFormatter2 stringFromDate:date];
    
    
    _dateLabel.text = [NSString stringWithFormat:@"Date : %@",dateString];
    
    
    //    NSString *formate = [NSString stringWithFormat:@"%@",apptObj.date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    //    NSDate *date = [df dateFromString:formate];
    //    [df setDateFormat:@"MMM dd, yyyy"];
    
    convertedDate = [df stringFromDate:date];
    
    NSLog(@"Converted DAte : %@",convertedDate);
    //    //    [self getList:dateString];
    //    [self showHud];
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        [self hideHud];
    //        [_timeTV setHidden:NO];
    //        [_timeTV reloadData];
    //    });
    
    NSLog(@"Date : %@",[NSString stringWithFormat:@"Date : %@",dateString]);
    
    //    [appDelegate.onlineAppointmentBookDetails setObject:appDelegate.globalCheckAndUncheckArray forKey:@"services"];
    //    [appDelegate.onlineAppointmentBookDetails setObject:paramDic forKey:@"staff"];
    //    [appDelegate.onlineAppointmentBookDetails setObject:timeStr forKey:@"time"];
    
    NSLog(@"Online Booking detaisl :%@",appDelegate.onlineAppointmentBookDetails);
    
    /*
     NSMutableDictionary *dictOfParams = [[NSMutableDictionary alloc] initWithCapacity:0];
     NSString *clientIDStr = [Globals loadSharedPreferenceValue:@"clientid"];
     [dictOfParams setObject:clientIDStr forKey:@"ClientId"];
     
     NSMutableArray *arrayOfEmployeeIds = [[NSMutableArray alloc] initWithCapacity:0];
     NSMutableArray *arrayOfServices = [[NSMutableArray alloc] initWithCapacity:0];
     
     for (NSDictionary *dict in [appDelegate.onlineAppointmentBookDetails objectForKey:@"services"])
     {
     [arrayOfServices  addObject:[dict objectForKey:@"serviceID"]];
     }
     
     NSString *serviceIdStr = [arrayOfServices componentsJoinedByString:@","];
     
     
     [arrayOfEmployeeIds  addObject:[[appDelegate.onlineAppointmentBookDetails objectForKey:@"staff"] objectForKey:@"EmpId"]];
     
     NSString *empIdStr = [arrayOfEmployeeIds componentsJoinedByString:@","];
     
     [dictOfParams setObject:empIdStr forKey:@"EmployeeIds"];
     [dictOfParams setObject:serviceIdStr forKey:@"ServiceIds"];
     
     [dictOfParams setObject:@"Tomorrow" forKey:@"Date"];
     [dictOfParams setObject:[appDelegate.onlineAppointmentBookDetails objectForKey:@"time"] forKey:@"Time"];
     
     
     NSLog(@"Check Params .... %@",dictOfParams);
     [self checkForAvailability:dictOfParams];
     
     */
    
    [self availableSlatsTimings];
}


-(void)availableSlatsTimings
{
    NSLog(@"availableSlatsTimings");
    selectedIndex=-1;
    _timeTV.hidden=YES;
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    NSString *clientIDStr = [Globals loadSharedPreferenceValue:@"clientid"];
    
    [paramDic setObject:selecteddEmpstr forKey:@"EmployeeIds"];
    [paramDic setObject:servicesIdsStr forKey:@"ServiceIds"];
    
    if (dateString.length)
    {
        [paramDic setObject:convertedDate forKey:@"FromDate"];
        [paramDic setObject:convertedDate forKey:@"ToDate"];
        
    }
    
    [paramDic setObject:titleString forKey:@"Time"];
    [paramDic setObject:@"custom" forKey:@"Date"];
    
    
    
    [paramDic setObject:clientIDStr forKey:@"ClientId"];
    
    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    
    if ([appDelegate.onlinebookingType isEqualToString:@"1"]) {
        [paramDic setObject:slcIdStr forKey:@"slc_id"];
        [paramDic setObject:SALON_ID forKey:@"salon_id"];
        
    }
    
    
    if([compareString isEqualToString:@"Rebook"]){
        [reBookDict setObject:convertedDate forKey:@"FromDate"];
        [reBookDict setObject:@"custom" forKey:@"Date"];
        [reBookDict setObject:convertedDate forKey:@"ToDate"];
        [reBookDict setObject:[_paramDict objectForKey:@"Time"] forKey:@"Time"];
        
        [self checkForAvailability:reBookDict];
    }
    else{
        [self checkForAvailability:paramDic];
        
    }
    
    
    
}


//-(IBAction)clickAvailability:(id)sender
//{
//
//
//
//
//    [apptData removeAllObjects];
//    [apptTableView reloadData];
//    apptTableView.hidden=NO;
//    [actionSheetView dismissWithClickedButtonIndex:0 animated:YES];
//    NSString *getDate = dateButton.titleLabel.text;
//    NSString *lowerGetDate;
//    if ([getDate isEqualToString:@"Today"]) {
//        lowerGetDate=@"today";
//    }
//    else if ([getDate isEqualToString:@"First Available"]) {
//        lowerGetDate=@"first";
//    }
//    else if ([getDate isEqualToString:@"Tomorrow"]) {
//        lowerGetDate=@"tomorrow";
//    }
//    else if ([getDate isEqualToString:@"This Week"]) {
//        lowerGetDate=@"thisweek";
//    }
//    else if ([getDate isEqualToString:@"Pick a Date Range"]) {
//        lowerGetDate=@"custom";
//    }
//
//    NSString *gettime = timeButton.titleLabel.text;
//    NSString *lowerGetTime;
//
//    if ([gettime isEqualToString:@"Anytime"]) {
//        lowerGetTime=@"anytime";
//    }
//    else if ([gettime isEqualToString:@"Morning (Before 12PM)"]) {
//        lowerGetTime=@"morning";
//    }
//    else if ([gettime isEqualToString:@"Afternoon (12PM to 5PM)"]) {
//        lowerGetTime=@"afternoon";
//    }
//    else if ([gettime isEqualToString:@"Evening (After 5PM)"]) {
//        lowerGetTime=@"evening";
//    }
//    else if ([gettime isEqualToString:@"Pick a Time Range"]) {
//        lowerGetTime=@"custom";
//    }
//
//
//    //    NSString *lowerGetDate = [[getDate stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
//    //     NSString *lowerGetTime = [[gettime stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
//    //
//    NSString *fDateStr = [self convertDate:fromDate.titleLabel.text];
//    NSString *tDateStr = [self convertDate:toDate.titleLabel.text];
//
//    NSString *fTimeStr = fromTime.titleLabel.text;
//    NSString *tTimeStr = toTime.titleLabel.text;
//
//    NSLog(@"%@%@",fDateStr,tDateStr);
//
//
//
//
//    //NSString *lowerGetDate = [getDate lowercaseString];
//    //NSString *lowerGetTime = [gettime lowercaseString];
//
//
//    if ([getDate isEqualToString:@"Select a Date"]) {
//        [NRUtils showAlert:@"Date" MSG:@"Please select date"];
//    }
//    else if ([fromDate.titleLabel.text isEqualToString:@"From"] && dateValue == YES)
//    {
//        [NRUtils showAlert:@"Date" MSG:@"Please choose from date"];
//    }
//    else if ([toDate.titleLabel.text isEqualToString:@"To"] && dateValue == YES)
//    {
//        [NRUtils showAlert:@"Date" MSG:@"Please choose to date"];
//    }
//
//    else if ([gettime isEqualToString:@"Select a Time"])
//    {
//        [NRUtils showAlert:@"Time" MSG:@"Please select time"];
//    }
//    else if ([fromTime.titleLabel.text isEqualToString:@"From"] && timeValue == YES)
//    {
//        [NRUtils showAlert:@"Date" MSG:@"Please choose from time"];
//    }
//    else if ([toTime.titleLabel.text isEqualToString:@"To"] && timeValue == YES)
//    {
//        [NRUtils showAlert:@"Date" MSG:@"Please choose from time"];
//    }
//
//    else{
//        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
//
//        NSString *fromTimes=[[self convertTime:fTimeStr] stringByReplacingOccurrencesOfString:@":" withString:@""];
//        NSString *toTimes=[[self convertTime:tTimeStr] stringByReplacingOccurrencesOfString:@":" withString:@""];
//
//
//        if ([getDate isEqualToString:@"Pick a Date Range"] && [gettime isEqualToString:@"Pick a Time Range"] ) {
//            [paramDic setObject:fDateStr forKey:@"FromDate"];
//            [paramDic setObject:tDateStr forKey:@"ToDate"];
//
//            [paramDic setObject:fromTimes forKey:@"FromTime"];
//            [paramDic setObject:toTimes forKey:@"ToTime"];
//        }
//        else if ([getDate isEqualToString:@"Pick a Date Range"]) {
//            [paramDic setObject:fDateStr forKey:@"FromDate"];
//            [paramDic setObject:tDateStr forKey:@"ToDate"];
//        }
//        else if ([gettime isEqualToString:@"Pick a Time Range"]){
//            [paramDic setObject:fromTimes forKey:@"FromTime"];
//            [paramDic setObject:toTimes forKey:@"ToTime"];
//        }
//        [paramDic setObject:lowerGetDate forKey:@"Date"];
//        [paramDic setObject:lowerGetTime forKey:@"Time"];
//
//        NSString *clientIDStr = [Globals loadSharedPreferenceValue:@"clientid"];
//        NSString *empIdStr = [empIdArray componentsJoinedByString:@","];
//        NSString *serviceIdStr = [serIdArray componentsJoinedByString:@","];
//        [paramDic setObject:empIdStr forKey:@"EmployeeIds"];
//        [paramDic setObject:serviceIdStr forKey:@"ServiceIds"];
//
//        [paramDic setObject:clientIDStr forKey:@"ClientId"];
//
//        SendRequest *objRequest = [[SendRequest alloc] init];
//        objRequest.delegate = self;
//        [objRequest url:(NSMutableString *)CHECK_APPT_LIST
//          withParameter:paramDic
//        withRequestType:@"POST"];
//    }
//}
//


-(void)getResponse:(NSDictionary *)dict
{
    if(dict.count == 0){
        [self showAlert:@"Book online" MSG:@"No appointments are available"];
        return;
    }
    
    NSLog(@"Response :%@",dict);
    
    int status =[[dict objectForKey:@"status"] intValue];
    
    if (dict==nil||dict.count==0 || status == 0 ){
        NSString *msgStr = @"";
        
        if([dict objectForKey:@"message"] != nil)
            msgStr = [dict objectForKey:@"message"];
        
        
        if (servicecall==1 && dict.count>0) {
            [self showAlert:@"Error" MSG:msgStr?:@"" tag:0];
        }
        else{
            [self showAlert:@"" MSG:msgStr?:@"" tag:0];
        }
        
    }
    else {
        
        if (servicecall==1) {
            NSLog(@"");
            int apptMess=[[dict objectForKey:@"AppointmentId"] intValue];
            
            NSString *apptIdStr=[NSString stringWithFormat:@" Thank you, your appointment for %@ with %@ has been booked.",bookedServiceName,bookedProvider];
            
            
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Appointment Created" andMessage:[dict objectForKey:@"message"]];
            
            [alertView addButtonWithTitle:@"Add to Calendar"                                                     type:SIAlertViewButtonTypeDestructive
                                  handler:^(SIAlertView *alert) {
                                      [self addToCalendar];
                                  }];
            
            [alertView addButtonWithTitle:@"Close"                                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alert) {
                                      [self.navigationController popToRootViewControllerAnimated:YES];
                                  }];
            
            alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
            [alertView show];
            
            
            //            [self showAlert:@"" MSG:apptIdStr tag:2];
        }
        else{
            NSMutableArray *arrJson = [dict objectForKey:@"openings"];
            if(arrJson.count == 0){
                [self showAlert:@"Book online" MSG:@"No appointments are available"];
                return;
            }
            apptData = [NSMutableArray new];
            
            for (int i=0; i<[arrJson count]; i++) {
                NSDictionary *empDict = [arrJson objectAtIndex:i];
                dividerCount =[[dict objectForKey:@"num_services"]intValue];
                if ([appDelegate.onlinebookingType isEqualToString:@"1"]) {
                    ApptRest *apptRestObj = [[ApptRest alloc] init];
                    [apptRestObj setChoiceNumber:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"choiceNumber"]]];
                    [apptRestObj setOrderNumber:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"orderNumber"]]];
                    [apptRestObj setClientName:[empDict objectForKey:@"clientName"]];
                    [apptRestObj setEmployeeName:[empDict objectForKey:@"employeeName"]];
                    [apptRestObj setResourceName:[empDict objectForKey:@"resourceName"]];
                    [apptRestObj setServiceName:[empDict objectForKey:@"serviceName"]];
                    [apptRestObj setPrice:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"price"]]];
                    [apptRestObj setServiceIds:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"serviceId"]]];
                    [apptRestObj setEmployeeIds:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"employeeId"]]];
                    [apptRestObj setStartDateTime:[empDict objectForKey:@"startDateTime"]];
                    [apptRestObj setEndDateTime:[empDict objectForKey:@"endDateTime"]];
                    [apptRestObj setStartLength:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"startLength"]]];
                    [apptRestObj setGapLength:[empDict objectForKey:@"gapLength"]];
                    [apptRestObj setFinishLength:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"finishLength"]]];
                    [apptRestObj setAppointmentTypeId:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"appointmentTypeId"]]];
                    [apptRestObj setResourceId:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"resourceId"]]];
                    [apptRestObj setGenderId:[NSString stringWithFormat:@"%@",[empDict objectForKey:@"genderId"]]];
                    [apptRestObj setAptdate:[empDict objectForKey:@"aptdate"]];
                    [apptRestObj setApttime:[empDict objectForKey:@"apttime"]];
                    [apptRestObj setDisplayPrice:[empDict objectForKey:@"display_price"]];
                    [apptData addObject:apptRestObj];
                }
                else{
                    
                    Appt *apptObj = [[Appt alloc] init];
                    [apptObj setServiceId:[empDict objectForKey:@"iservid"]];
                    [apptObj setEmployeeId:[empDict objectForKey:@"iempid"]];
                    
                    [apptObj setStartLength:[empDict objectForKey:@"nstartlen"]];
                    [apptObj setGapLength:[empDict objectForKey:@"ngaplen"]];
                    [apptObj setFinishLength:[empDict objectForKey:@"nfinishlen"]];
                    [apptObj setApptType:[empDict objectForKey:@"iappttype"]];
                    [apptObj setResourceId:[empDict objectForKey:@"iresourceid"]];
                    [apptObj setGenderId:[empDict objectForKey:@"igenderid"]];
                    
                    
                    [apptObj setDate:[empDict objectForKey:@"ddate"]];
                    [apptObj setService:[empDict objectForKey:@"service"]];
                    [apptObj setEmployee:[empDict objectForKey:@"employee"]];
                    //NSString *fromTime =[self convert:[empDict objectForKey:@"cmstarttime"]];
                    // NSString *toTime =[self convert:[empDict objectForKey:@"cmfinishtime"]];
                    
                    [apptObj setFromTime:[empDict objectForKey:@"cmstarttime"]];
                    [apptObj setToTime:[empDict objectForKey:@"cmfinishtime"]];
                    [apptObj setPrice:[empDict objectForKey:@"nprice"]];
                    [apptObj setMDisplayPrice:[empDict objectForKey:@"m_display_price"]];
                    [apptData addObject:apptObj];
                }
            }
            _timeTV.hidden = NO;
            [_timeTV reloadData];
        }
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return apptData.count;
    //    return timeDemoArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 86.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = @"ApptCell";
    ApptCell *cell = (ApptCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ApptCell" owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[ApptCell class]])
            {
                cell = (ApptCell *)currentObject;
                break;
            }
        }
    }
    
    if ([appDelegate.onlinebookingType isEqualToString:@"1"]) {
        ApptRest *apptRestObj = [apptData objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.nameLable setBackgroundColor:[UIColor clearColor]];
        cell.nameLable.font=[UIFont systemFontOfSize:14.0];
        NSString *nameStr = [NSString stringWithFormat:@"%@ with %@",apptRestObj.serviceName,apptRestObj.employeeName];
        cell.nameLable.text=[NSString stringWithFormat:@"%@",nameStr];
        
        [cell.desLable setBackgroundColor:[UIColor clearColor]];
        cell.desLable.font=[UIFont systemFontOfSize:12.0];
        
        NSString *formate=[NSString stringWithFormat:@"%@",apptRestObj.aptdate];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd-MM-yyyy"];
        NSDate *date = [df dateFromString:formate];
        [df setDateFormat:@"MMM dd, yyyy"];
        [cell.desLable setText:[df stringFromDate:date]];
        
        cell.timeLable.text=apptRestObj.apttime;
        NSString *priceStr =[NSString stringWithFormat:@"%@",apptRestObj.displayPrice];
        if (![priceStr isEqualToString:@""]) {
            cell.priceLable.text=priceStr;
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.bookApptButton.hidden=YES;
        cell.lineView.hidden=YES;
        //self.apptTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        if (dividerCount > 1 && ((indexPath.row + 1) % (dividerCount) == 0)){
            cell.bookApptButton.hidden=NO;
            cell.lineView.hidden=NO;
        }
        else if(dividerCount ==1){
            cell.bookApptButton.hidden=NO;
            cell.lineView.hidden=NO;
        }
        [cell.bookApptButton setTag:indexPath.row];
        [cell.bookApptButton addTarget:self action:@selector(bookNow:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    else {
        Appt *apptObj = [apptData objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.nameLable setBackgroundColor:[UIColor clearColor]];
        cell.nameLable.font=[UIFont systemFontOfSize:14.0];
        NSString *nameStr = [NSString stringWithFormat:@"%@ with %@",apptObj.service,apptObj.employee];
        cell.nameLable.text=[NSString stringWithFormat:@"%@",nameStr];
        
        [cell.desLable setBackgroundColor:[UIColor clearColor]];
        cell.desLable.font=[UIFont systemFontOfSize:12.0];
        
        NSString *fromTimes =[self convert:[NSMutableString stringWithFormat:@"%@",apptObj.fromTime]];
        NSString *toTimes =[self convert:[NSMutableString stringWithFormat:@"%@",apptObj.toTime]];
        
        NSString *formate=[NSString stringWithFormat:@"%@",apptObj.date];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [df dateFromString:formate];
        [df setDateFormat:@"MMM dd, yyyy"];
        [cell.desLable setText:[df stringFromDate:date]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm"];
        NSDate *dates = [dateFormatter dateFromString:fromTimes];
        [dateFormatter setDateFormat:@"hh:mm a"];
        NSString *formattedTime = [dateFormatter stringFromDate:dates];
        NSLog(@"%@",formattedTime);
        
        NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
        [dateFormatte setDateFormat:@"HH:mm"];
        NSDate *todate = [dateFormatte dateFromString:toTimes];
        [dateFormatte setDateFormat:@"hh:mm a"];
        NSString *formattedtoTime = [dateFormatte stringFromDate:todate];
        NSLog(@"%@",formattedtoTime);
        
        
        
        // NSString *timeStr = [NSString stringWithFormat:@"%@ - %@" ,fromTimes, toTimes];
        // cell.timeLable.text=[NSString stringWithFormat:@"%@",timeStr];
        cell.timeLable.text=formattedTime;
        NSString *priceStr =[NSString stringWithFormat:@"%@",apptObj.mDisplayPrice];
        if (![priceStr isEqualToString:@""]) {
            cell.priceLable.text=priceStr;
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.bookApptButton.hidden=YES;
        cell.lineView.hidden=YES;
        //self.apptTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        if (dividerCount > 1 && ((indexPath.row + 1) % (dividerCount) == 0)){
            cell.bookApptButton.hidden=NO;
            cell.lineView.hidden=NO;
        }
        else if(dividerCount ==1){
            cell.bookApptButton.hidden=NO;
            cell.lineView.hidden=NO;
        }
        cell.bookApptButton.tag = indexPath.row;
        [cell.bookApptButton addTarget:self action:@selector(bookNow:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        //    if (([apptData count] %dividerCount)-1==0) {
        //    }
        //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        return cell;
        
    }
    
}


-(NSString*)convert:(NSMutableString *)strValue
{
    // strValue = [NSMutableString stringWithString:@"1015"];
    [strValue insertString:@":" atIndex:2];
    NSString *conStr = [NSString stringWithString:strValue];
    NSLog(@"%@",conStr);
    return conStr;
}
#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //tableView.accessrory
    selectedIndex = indexPath.row;
    [_timeTV reloadData];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)bookNow:(UIButton*)sender
{
    
    //    int value = [sender tag];
    selectedIndex = [sender tag];
    [appDelegate showHUD];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        /*
         NSString *apptIdStr=[NSString stringWithFormat:@"Thank you, your appointment has been booked and you will be sent an email with the appointment details.",bookedServiceName,bookedProvider];
         
         SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Appointment Created" andMessage:apptIdStr];
         
         [alertView addButtonWithTitle:@"Add to Calendar"                                                     type:SIAlertViewButtonTypeDestructive
         handler:^(SIAlertView *alert) {
         [self addToCalendar];
         }];
         
         [alertView addButtonWithTitle:@"Close"                                                     type:SIAlertViewButtonTypeCancel
         handler:^(SIAlertView *alert) {
         [self.navigationController popToRootViewControllerAnimated:YES];
         }];
         
         alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
         [alertView show];
         
         
         [self hideHud];
         
         */
        
        //    return;
        servicecall=1;
        NSLog(@"Appdata = %@",apptData);
        
        if ([appDelegate.onlinebookingType isEqualToString:@"1"]) {
            NSMutableArray *choiceNumberArray=[[NSMutableArray alloc] init];
            NSMutableArray *orderNumberArray=[[NSMutableArray alloc] init];
            NSMutableArray *clientNameArray=[[NSMutableArray alloc] init];
            NSMutableArray *employeeNameArray=[[NSMutableArray alloc] init];
            NSMutableArray *resourceNameArray=[[NSMutableArray alloc] init];
            NSMutableArray *serviceNameArray=[[NSMutableArray alloc] init];
            NSMutableArray *priceArray=[[NSMutableArray alloc] init];
            NSMutableArray *serviceIdsArray=[[NSMutableArray alloc] init];
            NSMutableArray *employeeIdsArray=[[NSMutableArray alloc] init];
            NSMutableArray *startDateTimeArray=[[NSMutableArray alloc] init];
            NSMutableArray *endDateTimeArray=[[NSMutableArray alloc] init];
            NSMutableArray *startLengthArray=[[NSMutableArray alloc] init];
            NSMutableArray *gapLengthArray=[[NSMutableArray alloc] init];
            NSMutableArray *finishLengthArray=[[NSMutableArray alloc] init];
            NSMutableArray *appointmentTypeIdArray=[[NSMutableArray alloc] init];
            NSMutableArray *resourceIdArray=[[NSMutableArray alloc] init];
            NSMutableArray *genderIdArray=[[NSMutableArray alloc] init];
            
            //NSMutableArray *dateArray=[[NSMutableArray alloc] init];
            
            NSString *apptDate, *apptTime;
            for (int i=0; i<dividerCount; i++) {
                NSLog(@"%d",[sender tag]-i);
                ApptRest *apptRestObj = [apptData objectAtIndex:selectedIndex];
                [choiceNumberArray addObject:apptRestObj.choiceNumber];
                [orderNumberArray addObject:apptRestObj.orderNumber];
                [clientNameArray addObject:apptRestObj.clientName];
                [employeeNameArray addObject:apptRestObj.employeeName];
                [resourceNameArray addObject:apptRestObj.resourceName];
                [serviceNameArray addObject:apptRestObj.serviceName];
                [priceArray addObject:apptRestObj.price];
                [serviceIdsArray addObject:apptRestObj.serviceIds];
                [employeeIdsArray addObject:apptRestObj.employeeIds];
                [startDateTimeArray addObject:apptRestObj.startDateTime];
                [endDateTimeArray addObject:apptRestObj.endDateTime];
                [startLengthArray addObject:apptRestObj.startLength];
                [gapLengthArray addObject:apptRestObj.gapLength];
                [finishLengthArray addObject:apptRestObj.finishLength];
                [appointmentTypeIdArray addObject:apptRestObj.appointmentTypeId];
                [resourceIdArray addObject:apptRestObj.resourceId];
                [genderIdArray addObject:apptRestObj.genderId];
                
                apptDate = apptRestObj.aptdate;
                apptTime = apptRestObj.apttime;
                
            }
            
            NSString *choiceNumberStr = [choiceNumberArray componentsJoinedByString:@","];
            NSString *orderNumberStr = [orderNumberArray componentsJoinedByString:@","];
            NSString *clientNameStr = [clientNameArray componentsJoinedByString:@","];
            NSString *employeeNameStr = [employeeNameArray componentsJoinedByString:@","];
            NSString *resourceNameStr = [resourceNameArray componentsJoinedByString:@","];
            NSString *serviceNameStr = [serviceNameArray componentsJoinedByString:@","];
            NSString *priceStr = [priceArray componentsJoinedByString:@","];
            NSString *serviceIdsStr = [serviceIdsArray componentsJoinedByString:@","];
            NSString *employeeIdsStr = [employeeIdsArray componentsJoinedByString:@","];
            NSString *startDateTimeStr = [startDateTimeArray componentsJoinedByString:@","];
            NSString *endDateTimeStr = [endDateTimeArray componentsJoinedByString:@","];
            NSString *startLengthStr = [startLengthArray componentsJoinedByString:@","];
            NSString *gapLengthStr = [gapLengthArray componentsJoinedByString:@","];
            NSString *finishLengthStr = [finishLengthArray componentsJoinedByString:@","];
            NSString *appointmentTypeIdStr = [appointmentTypeIdArray componentsJoinedByString:@","];
            NSString *resourceIdStr = [resourceIdArray componentsJoinedByString:@","];
            NSString *genderIdStr = [genderIdArray componentsJoinedByString:@","];
            NSString *empIdStr = [empIdArray componentsJoinedByString:@","];
            
            //Appt *apptObj = [apptData objectAtIndex:[sender tag]];
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
            NSString *clientIDStr = [Globals loadSharedPreferenceValue:@"clientid"];
            NSString *slcIDStr = [Globals loadSharedPreferenceValue:@"slc_id"];
            
            
            [paramDic setObject:choiceNumberStr forKey:@"choiceNumber"];
            [paramDic setObject:orderNumberStr forKey:@"orderNumber"];
            [paramDic setObject:clientNameStr forKey:@"clientName"];
            [paramDic setObject:employeeNameStr forKey:@"employeeName"];
            [paramDic setObject:resourceNameStr forKey:@"resourceName"];
            [paramDic setObject:serviceNameStr forKey:@"serviceName"];
            [paramDic setObject:priceStr forKey:@"price"];
            [paramDic setObject:serviceIdsStr forKey:@"serviceIds"];
            [paramDic setObject:employeeIdsStr forKey:@"employeeIds"];
            [paramDic setObject:startDateTimeStr forKey:@"startDateTime"];
            [paramDic setObject:endDateTimeStr forKey:@"endDateTime"];//2016-09-22
            [paramDic setObject:startLengthStr forKey:@"startLength"];
            [paramDic setObject:gapLengthStr forKey:@"gapLength"];
            [paramDic setObject:finishLengthStr forKey:@"finishLength"];
            [paramDic setObject:appointmentTypeIdStr forKey:@"appointmentTypeId"];
            [paramDic setObject:resourceIdStr forKey:@"resourceId"];
            [paramDic setObject:genderIdStr forKey:@"genderId"];
            [paramDic setObject:empIdStr forKey:@"SelectedEmployees"];
            [paramDic setObject:clientIDStr forKey:@"ClientId"];
            [paramDic setObject:slcIDStr forKey:@"slc_id"];
            [paramDic setObject:SALON_ID forKey:@"salon_id"];
            
            
            NSString *formate=[NSString stringWithFormat:@"%@ %@",apptDate,apptTime];
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"dd-MM-yyyy hh:mm a"];
            NSDate *date = [df dateFromString:formate];
            
            
            bookedServiceName = [NSString stringWithFormat:@"%@",serviceNameStr];
            bookedProvider = [NSString stringWithFormat:@"%@",employeeNameStr];
            
            NSLog(@"Selected Date : %@",date);
            
            bookedDate = date;
            
            
            servicecall=1;
            
            [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:BOOKING_CONFIRMATION_REST HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                [appDelegate hideHUD];
                
                NSLog(@"Error :%@",error);
                if(error)
                    [self showAlert:@"Error" MSG:error.localizedDescription tag:0];
                else
                {
                    [appDelegate hideHUD];
                    
                    NSDictionary *dict = (NSDictionary *)responseArray;
                    [self getResponse:dict];
                }
            }];
            
        } else {
            Appt *obj = [apptData objectAtIndex:selectedIndex];
            
            NSLog(@"date = %@",obj.date);
            
            NSMutableArray *servicesIdArray=[[NSMutableArray alloc] init];
            NSMutableArray *empIdsArray=[[NSMutableArray alloc] init];
            NSMutableArray *startLengthArray=[[NSMutableArray alloc] init];
            NSMutableArray *gapLengthArray=[[NSMutableArray alloc] init];
            NSMutableArray *finishLengthArray=[[NSMutableArray alloc] init];
            NSMutableArray *apptTypeArray=[[NSMutableArray alloc] init];
            NSMutableArray *fromTimeArray=[[NSMutableArray alloc] init];
            NSMutableArray *toTimeArray=[[NSMutableArray alloc] init];
            NSMutableArray *resourceIdArray=[[NSMutableArray alloc] init];
            NSMutableArray *genderIdArray=[[NSMutableArray alloc] init];
            //NSMutableArray *dateArray=[[NSMutableArray alloc] init];
            
            
            //bookedServiceName,*bookedProvider,*bookedTtime;
            
            //new srikanth code
            bookedToTime =[self convert:[NSMutableString stringWithFormat:@"%@",obj.toTime]];
            bookedFromTIme =[self convert:[NSMutableString stringWithFormat:@"%@",obj.fromTime]];
            
            NSString *formate=[NSString stringWithFormat:@"%@ %@",obj.date,bookedFromTIme];
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd hh:mm"];
            NSDate *date = [df dateFromString:formate];
            
            
            
            bookedServiceName = [NSString stringWithFormat:@"%@",obj.service];
            bookedProvider = [NSString stringWithFormat:@"%@",obj.employee];
            //bookedTtime = formattedtoTime;
            bookedDate = date;
            
            //end
            for (int i=0; i<dividerCount; i++) {
                
                Appt *apptObj = [apptData objectAtIndex:selectedIndex -i];
                [servicesIdArray addObject:apptObj.serviceId];
                [empIdsArray addObject:apptObj.employeeId];
                [startLengthArray addObject:apptObj.startLength];
                [gapLengthArray addObject:apptObj.gapLength];
                [finishLengthArray addObject:apptObj.finishLength];
                [apptTypeArray addObject:apptObj.apptType];
                [fromTimeArray addObject:apptObj.fromTime];
                
                NSLog(@"Time : %@",apptObj.fromTime);
                [toTimeArray addObject:apptObj.toTime];
                [resourceIdArray addObject:apptObj.resourceId];
                [genderIdArray addObject:apptObj.genderId];
                selectedIndex++;
                //[dateArray addObject:apptObj.date];
            }
            
            NSString *servicesIdsStr = [servicesIdArray componentsJoinedByString:@","];
            NSString *empIdsStr = [empIdsArray componentsJoinedByString:@","];
            NSString *startLengthStr = [startLengthArray componentsJoinedByString:@","];
            NSString *gapLengthStr = [gapLengthArray componentsJoinedByString:@","];
            NSString *finishLengthStr = [finishLengthArray componentsJoinedByString:@","];
            NSString *apptTypeStr = [apptTypeArray componentsJoinedByString:@","];
            NSString *fromTimesStr = [fromTimeArray componentsJoinedByString:@","];
            NSString *toTimesStr = [toTimeArray componentsJoinedByString:@","];
            NSString *resourceIdStr = [resourceIdArray componentsJoinedByString:@","];
            NSString *genderIdStr = [genderIdArray componentsJoinedByString:@","];
            // NSString *dateStr = [dateArray componentsJoinedByString:@","];
            
            //Appt *apptObj = [apptData objectAtIndex:[sender tag]];
            NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
            NSString *clientIDStr = [self loadSharedPreferenceValue:@"clientid"];
            [paramDic setObject:servicesIdsStr forKey:@"ServiceIds"];
            [paramDic setObject:empIdsStr forKey:@"EmployeeIds"];
            [paramDic setObject:startLengthStr forKey:@"StartLength"];
            [paramDic setObject:gapLengthStr forKey:@"GapLength"];
            [paramDic setObject:finishLengthStr forKey:@"FinishLength"];
            [paramDic setObject:apptTypeStr forKey:@"AppointmentType"];
            [paramDic setObject:fromTimesStr forKey:@"CheckInTime"];
            [paramDic setObject:toTimesStr forKey:@"CheckOutTime"];
            [paramDic setObject:resourceIdStr forKey:@"ResourceId"];
            [paramDic setObject:genderIdStr forKey:@"Genderid"];
            [paramDic setObject:obj.date forKey:@"AppointmentDate"];//2016-09-22
            [paramDic setObject:clientIDStr forKey:@"ClientId"];
            
            NSLog(@"Book Slot params : %@",paramDic);
            [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:BOOKING_CONFIRMATION HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                [appDelegate hideHUD];
                if(error)
                    [self showAlert:@"Error" MSG:error.localizedDescription tag:0];
                else
                {
                    [appDelegate hideHUD];
                    
                    NSDictionary *dict = (NSDictionary *)responseArray;
                    [self getResponse:dict];
                }
            }];
            
        }
        
        
        
    });
    
}
- (IBAction)nextAction:(id)sender {
    
    if (selectedIndex==-1)
    {
        [self showAlert:@"Timings" MSG:@"Please select at least one timings"];
    }
    else
    {
        //        [self bookNow];
    }
}
#pragma mark Add claendar
-(void)addToCalendar
{
    
    EKEventStore *eventstore = [[EKEventStore alloc] init];
    
    EKEvent *event = [EKEvent eventWithEventStore:eventstore];
    
    
    event.title= [NSString stringWithFormat:@" Thank you, your appointment for %@ with %@ has been booked\n %@",bookedServiceName,bookedProvider,bookedDate];
    
    event.startDate = bookedDate;
    event.endDate = [[NSDate alloc] initWithTimeInterval:60*60 sinceDate:bookedDate];
    
    
    NSLog(@"Calendar start :%@",event.startDate);
    NSLog(@"Calendar End :%@",event.endDate);
    
    EKAlarm *remainder = [EKAlarm alarmWithRelativeOffset:-60*30];
    [event addAlarm:remainder];
    
    if([eventstore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        // iOS 6 and later
        [eventstore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            
            
            if (error == nil) {
                // Store the returned granted value.
                // may return on background thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (granted){
                        [event setCalendar:[eventstore defaultCalendarForNewEvents]];
                        //---- codes here when user allow your app to access theirs' calendar.
                        NSError *err;
                        BOOL isSuceess=[eventstore saveEvent:event span:EKSpanThisEvent error:&err];
                        
                        if(isSuceess){
                            [self showAlert:SALON_NAME MSG:@"Your appointment has been added to the calendar." BLOCK:^{
                                [self.navigationController popToRootViewControllerAnimated:YES];
                            }];                    }
                        
                    }else
                    {
                        [self showAlert:SALON_NAME MSG:@"Please enable access to the calendar in Settings -> Privacy."];
                    }
                });
            }
            else{
                // In case of error, just log its description to the debugger.
                NSLog(@"%@", [error localizedDescription]);
            }
            
            
            
            
            
        }];
    }
    else {
        NSError *err;
        BOOL isSuceess=[eventstore saveEvent:event span:EKSpanThisEvent error:&err];
        
        if(isSuceess){
            [self showAlert:SALON_NAME MSG:@"Your appointment has been added to the calendar." BLOCK:^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }
        
    }
    
    
}

@end
