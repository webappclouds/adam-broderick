//
//  ServicesViewController.h
//  Nithin Salon
//
//  Created by Nithin Reddy on 23/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "Constants.h"
#import "ServiceObj.h"
#import "StaffObj.h"


@interface OnlineServicesViewController : UIViewController<CAPSPageMenuDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
- (IBAction)nextAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *nextClick;
@property (nonatomic) CAPSPageMenu *pagemenu;
@property (nonatomic) IBOutlet UIView *slideView;
@property(nonatomic,assign)id serviceDelegate;
@property (nonatomic)StaffObj *StaffModelObj;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHeight;
- (IBAction)nextBtnAction:(id)sender;
@property (nonatomic) NSMutableArray *data;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
-(void)getcheckeddata:(NSDictionary*)dictionaary type:(NSString *)typeString;
-(void)getAddondata:(NSMutableDictionary*)dictionaary type:(NSString *)typeString;
-(void)setTopBadgeValue:(int)sectionVal;
-(void)getcheckeddata:(NSMutableDictionary*)dictionaary type:(NSString *)typeString index:(int)indexVal;

@end
