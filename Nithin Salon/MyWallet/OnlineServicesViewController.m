//
//  ServicesViewController.m
//  Nithin Salon
//
//  Created by Nithin Reddy on 23/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "OnlineServicesViewController.h"
#import "ServicesTableViewController.h"
#import "SpecialsObj.h"
#import "OnlineBookindViewCon.h"
#import "Services.h"
//#import "ChooseCalendar.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "OnlineServicesTableViewController.h"
#import "Globals.h"
#import "GlobalSettings.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "BBBadgeBarButtonItem.h"
#import "OnlineStaffList.h"
#import "OnlineCalendarVC.h"
#import "CartListVC.h"
@interface OnlineServicesViewController ()
{
    AppDelegate *appDelegate;
     NSMutableDictionary *globalCheckDictionary;
    UIButton *transparentButton;
    BBBadgeBarButtonItem *barButton;
}

@end

@implementation OnlineServicesViewController
{
    NSMutableArray *controllerArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if(appDelegate.globalCheckAndUncheckArray.count){
        [appDelegate.globalCheckAndUncheckArray removeAllObjects];
    }
    
    _nextButton.layer.borderColor=[UIColor whiteColor].CGColor;
    _nextButton.layer.borderWidth=2.0;
    _nextButton.layer.cornerRadius=4.0;

    
    
       _searchBarHeight.constant =0;
    //[[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -60)
                                                        // forBarMetrics:UIBarMetricsDefault];
    
    
    self.title = @"Services";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor], UITextAttributeFont: [UIFont fontWithName:@"OpenSans" size:15.0f]};
    

    

    
    controllerArray = [NSMutableArray array];

    self.data = [NSMutableArray new];
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];

    NSString *slc_id=[self loadSharedPreferenceValue:@"slc_id"];
    [paramDic setObject:slc_id forKey:@"slc_id"];
    
    NSString *clientIDStr = [self loadSharedPreferenceValue:@"clientid"];
    [paramDic setObject:clientIDStr forKey:@"ClientId"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",[appDelegate addSalonIdTo:SERVICES_LIST]];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
   // [UIViewController addBottomBar:self];
    
    
//    UIBarButtonItem *search = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search.png"] style:UIBarButtonItemStylePlain target:self action:@selector(search)];
//    
//     search.tintColor=[UIColor whiteColor];
//
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    // Add your action to your button
    [customButton addTarget:self action:@selector(barButtonItemPressed) forControlEvents:UIControlEventTouchUpInside];
    // Customize your button as you want, with an image if you have a pictogram to display for example
    [customButton setImage:[UIImage imageNamed:@"cart_icon"] forState:UIControlStateNormal];
    
    // Then create and add our custom BBBadgeBarButtonItem
    barButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
    // Set a value for the badge
    barButton.badgeValue = @"0";
    barButton.badgeBGColor = [self colorFromHexString:@"#3BC651"];
    
    barButton.badgeOriginX = 0;
    barButton.badgeOriginY = 0;

    // Add it as the leftBarButtonItem of the navigation bar
    self.navigationItem.rightBarButtonItem = barButton;
    
    
    //self.navigationItem.rightBarButtonItem = cartButton;
   
}

-(void) barButtonItemPressed
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    if(appDelegate.globalCheckAndUncheckArray.count){
    CartListVC *clv = [self.storyboard instantiateViewControllerWithIdentifier:@"CartListVC"];
    clv.selectedApptData = [appDelegate.globalCheckAndUncheckArray mutableCopy];
    [self.navigationController pushViewController:clv animated:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self RefreshCount];
    self.navigationItem.hidesBackButton=NO;
    transparentButton = [[UIButton alloc] init];
    [transparentButton setFrame:CGRectMake(0,0, 50, 40)];
    [transparentButton setBackgroundColor:[UIColor clearColor]];
    [transparentButton addTarget:self action:@selector(goBackToHome) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:transparentButton];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [transparentButton removeFromSuperview];
}
-(void)goBackToHome{
    
    [transparentButton removeFromSuperview];
    for(UIViewController *controller in self.navigationController.viewControllers){
        if([controller isKindOfClass:[ViewController class]]){
            
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    
    //  [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark SendRequest delegate

-(void)getResponse:(NSDictionary *)dict
{
    
    NSMutableArray *catArray = [dict objectForKey:@"servicetypes"];
    
    for(NSDictionary *dict in catArray)
    {
        Services *serObj = [[Services alloc] init];
        [serObj setCategoryId:[dict objectForKey:@"category_iid"]];
        [serObj setCategoryName:[dict objectForKey:@"category_cclassname"]];
        
        
        
       // ServicesTableViewController *tableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ServicesTableViewController"];
        
            OnlineServicesTableViewController *tableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineServicesTableViewController"];
        tableViewController.reqAppoitmnetDelegate =self;
        tableViewController.title = serObj.categoryName;
        tableViewController.menuId = serObj.categoryId;
        // tableViewController.attachment = specialObj.image;
        //tableViewController.moduleObj = self.moduleObj;
        [controllerArray addObject:tableViewController];
        [self.data addObject:serObj];
        serObj = Nil;
    }
    
    NSDictionary *parameters = @{CAPSPageMenuOptionMenuItemSeparatorWidth: @(5),
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(NO),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 CAPSPageMenuOptionAddBottomMenuHairline : @(YES),
                                 CAPSPageMenuOptionCenterMenuItems : @(YES),
                                 CAPSPageMenuOptionMenuHeight : @(44),
                                 CAPSPageMenuOptionMenuItemFont : [UIFont systemFontOfSize:14],
                                 CAPSPageMenuOptionScrollMenuBackgroundColor :[self colorFromHexString:@"#1D1D1D"],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor whiteColor],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor : [self colorFromHexString:@"#D93829"]
                                 };
    
    // Initialize page menu with controller array, frame, and optional parameters
    _pagemenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0,0, self.slideView.frame.size.width, self.view.frame.size.height-120) options:parameters];
    _pagemenu.delegate = self;
    
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    [self.slideView addSubview:_pagemenu.view];
    
    
    //            [self.tableView reloadData];
    
    
}

/*
-(void)getServiceString:(NSString *)serviceName type:(NSString *)type{
 
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.serviceNameStr = [serviceName mutableCopy];
    if([type isEqualToString:@"Action"]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Choose one" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Request Appointment" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
            
            
            //code to be executed on the main queue after delay
            RequestAppointment *requestAppointment = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestAppointment"];
            requestAppointment.staffObjModel = _staffModelObj;
            [self.navigationController pushViewController:requestAppointment animated:YES];
                   }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else{
        [self.serviceDelegate getService:serviceName];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

*/

-(void)getcheckeddata:(NSMutableDictionary*)dictionaary type:(NSString *)typeString{
    
    if([typeString isEqualToString:@"ADD"])
    [appDelegate.globalCheckAndUncheckArray addObject:dictionaary];
    else{
        NSString *str = [dictionaary objectForKey:@"serviceID"];
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(serviceID != %@)", str];
       NSArray* filteredArray = [appDelegate.globalCheckAndUncheckArray filteredArrayUsingPredicate:bPredicate];
        NSLog(@"HERE %@",filteredArray);
        if(appDelegate.globalCheckAndUncheckArray.count)
        [appDelegate.globalCheckAndUncheckArray removeAllObjects];
        
        appDelegate.globalCheckAndUncheckArray = [filteredArray mutableCopy];
        
    }
    NSLog(@"Gloabal categories :%@",appDelegate.globalCheckAndUncheckArray);

    [appDelegate.onlineAppointmentBookDetails setObject:appDelegate.globalCheckAndUncheckArray forKey:@"services"];
    
    barButton.badgeValue =[NSString stringWithFormat:@"%i", appDelegate.globalCheckAndUncheckArray.count];
    
}


-(void)getcheckeddata:(NSMutableDictionary*)dictionaary type:(NSString *)typeString index:(int)indexVal{
    
    if([typeString isEqualToString:@"ADD"])
        [appDelegate.globalCheckAndUncheckArray addObject:dictionaary];
    else{
        NSString *str = [dictionaary objectForKey:@"service_iid"];
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(service_iid != %@)", str];
        NSArray* filteredArray = [appDelegate.globalCheckAndUncheckArray filteredArrayUsingPredicate:bPredicate];
        NSLog(@"HERE %@",filteredArray);
        if(appDelegate.globalCheckAndUncheckArray.count)
            [appDelegate.globalCheckAndUncheckArray removeAllObjects];
        
        appDelegate.globalCheckAndUncheckArray = [filteredArray mutableCopy];
        
        
    }
    
    NSLog(@"Global:%@",appDelegate.globalCheckAndUncheckArray);
    [self setTopBadgeValue:indexVal];
    
    
    //barButton.badgeValue =[NSString stringWithFormat:@"%lu", (unsigned long)appDelegate.globalCheckAndUncheckArray.count];
    
}

-(void)RefreshCount{
    [self setTopBadgeValue:nil];
}

-(void)setTopBadgeValue:(int)sectionVal
{
    
    int totalCount = 0;
    
    for(int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
    {
        NSMutableDictionary *dict = [appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
        
        if(dict.count){
            
            NSArray *array = [[dict objectForKey:@"AddOn"] mutableCopy];
            int k = (int)array.count;
            totalCount = totalCount+k;
        }
        
    }
    
    int globalCount = (int)appDelegate.globalCheckAndUncheckArray.count;
    totalCount =  totalCount + globalCount;
    
    appDelegate.totalCount = totalCount;
    barButton.badgeValue =[NSString stringWithFormat:@"%i", totalCount];
}

-(void)getAddondata:(NSMutableDictionary*)dictionaary type:(NSString *)typeString
{
    
    if([typeString isEqualToString:@"ADD"]){
        [appDelegate.globaldummyArray addObject:dictionaary];
        
    }
    else{
        NSString *str = [dictionaary objectForKey:@"addon_service_iid"];
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(addon_service_iid != %@)", str];
        NSArray* filteredArray = [appDelegate.globaldummyArray filteredArrayUsingPredicate:bPredicate];
        NSLog(@"HERE %@",filteredArray);
        if(appDelegate.globaldummyArray.count)
            [appDelegate.globaldummyArray removeAllObjects];
        appDelegate.globaldummyArray = [filteredArray mutableCopy];
        
        
        
    }
    
    NSLog(@"appDelegate.globalAddonsListkArray %@",appDelegate.globaldummyArray);
    
    
}

- (void)nextAction:(id)sender {
    
    
}
- (IBAction)nextBtnAction:(id)sender {
    
    if (appDelegate.globalCheckAndUncheckArray.count==0)
    {
        [self showAlert:@"Service" MSG:@"Please select at least one service"];
    }
    else
    {
        if (appDelegate.globalCheckAndUncheckArray.count == 1)
        {
            
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
            OnlineStaffList *osl = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineStaffList"];
            osl.staffID = [[appDelegate.globalCheckAndUncheckArray objectAtIndex:0] objectForKey:@"serviceID"];
            [self.navigationController pushViewController:osl animated:YES];
            
        }
        else{
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
            OnlineStaffList *osl = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineStaffList"];
            [self.navigationController pushViewController:osl animated:YES];
        }
    }
    
}
@end
