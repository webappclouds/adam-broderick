//
//  LocationViewController.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/08/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "LocationViewController.h"
#import "AppDelegate.h"
#import "OnlineServicesViewController.h"

@interface LocationViewController ()
{
    AppDelegate *appDelegate;
    
    NSMutableArray *locationNames;
    NSString *desCriptionString;
    UIButton *transparentButton;
    int selectedIndex;

}
- (IBAction)findMyLocationAction:(id)sender;

@end

@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.title = @"Location";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    appDelegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    desCriptionString=@"At this time, Online Booking through our Mobile App is only available for our Cherry Hill Location. Look for additional locations coming soon! Thanks";
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:desCriptionString];
    
    //Fing range of the string you want to change colour
    //If you need to change colour in more that one place just repeat it
    NSRange range = [desCriptionString rangeOfString:@"Cherry Hill"];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:(217/255.0) green:(56/255.0) blue:(41/255.0) alpha:1.0] range:range];
    
    //Add it to the label - notice its not text property but it's attributeText
    _descriptionLabel.attributedText = attString;
    
    _nextButton.layer.borderColor=[UIColor whiteColor].CGColor;
    _nextButton.layer.borderWidth=2.0;
    _nextButton.layer.cornerRadius=4.0;
    
    _findMyLocation.layer.borderColor=[UIColor clearColor].CGColor;
    _findMyLocation.layer.borderWidth=4.0;
    _findMyLocation.layer.cornerRadius=8.0;
    
    selectedIndex=-1;
    locationNames=[[NSMutableArray alloc]initWithObjects:@"Cherry Hill",nil];
    
    
    _locationTv.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _locationTv.allowsMultipleSelection = NO;
    
    //  _descriptionLabel.hidden=YES;
    
    [_locationTv setHidden:YES];
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton=NO;
    transparentButton = [[UIButton alloc] init];
    [transparentButton setFrame:CGRectMake(0,0, 50, 40)];
    [transparentButton setBackgroundColor:[UIColor clearColor]];
    [transparentButton addTarget:self action:@selector(goBackToHome) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:transparentButton];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [transparentButton removeFromSuperview];
}
-(void)goBackToHome{
    
    [transparentButton removeFromSuperview];
    for(UIViewController *controller in self.navigationController.viewControllers){
//        if([controller isKindOfClass:[ViewController class]]){
//
//
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        }
    }
    
    //  [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table view Datesource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [locationNames count];
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    cell.textLabel.text=[locationNames objectAtIndex:indexPath.row];
    
    cell.textLabel.textColor = [self colorFromHexString:@"#D93829"];
    
    
    return cell;
    
    
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = indexPath.row;
    
    _locationName.text=[locationNames objectAtIndex:indexPath.row];
    
    [self saveSharedPreferenceValue:_locationName.text KEY:@"locationName"];
    
    
    if ([_locationName.text isEqualToString:@"Cherry Hill"])
    {
        _descriptionLabel.hidden=NO;
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:desCriptionString];
        
        //Fing range of the string you want to change colour
        //If you need to change colour in more that one place just repeat it
        NSRange range = [desCriptionString rangeOfString:@"Cherry Hill"];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:(217/255.0) green:(56/255.0) blue:(41/255.0) alpha:1.0] range:range];
        
        //Add it to the label - notice its not text property but it's attributeText
        _descriptionLabel.attributedText = attString;
        
    }
    else
        _descriptionLabel.hidden=YES;
    
    
    
    [self.locationTv setHidden:YES];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



- (IBAction)dropDownAction:(id)sender {
    
    [_locationTv setHidden:NO];
    [_locationTv reloadData];
    
}
- (IBAction)nextAction:(id)sender {
    
    if (selectedIndex==-1)
    {
        if ([_locationName.text isEqualToString:@"Cherry Hill"]) {
            [self onlineServiceScreen];
        }
        else
            [self showAlert:@"" MSG:@"Please select the location"];
        
    }
    
    else{
        
        if ([_locationName.text isEqualToString:@"Cherry Hill"]) {
            
            [self onlineServiceScreen];
        }
        else
        {
            [self showAlert:@"" MSG:@"Online booking is now available only for Cherry Hill location."];
        }
        
    }
    
    
}

-(void)onlineServiceScreen
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    OnlineServicesViewController *onlineBookindViewCon = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineServicesViewController"];
    
    [self.navigationController pushViewController:onlineBookindViewCon animated:YES];
}
- (IBAction)findMyLocationAction:(id)sender {
    
    _locationName.text=@"Cherry Hill";
    
    [self saveSharedPreferenceValue:_locationName.text KEY:@"locationName"];
    
    _findMyLocation.hidden=YES;
    
    
}
- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
