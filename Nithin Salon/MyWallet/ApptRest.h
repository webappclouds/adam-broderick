//
//  ApptObj.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 20/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApptRest : NSObject

@property (nonatomic, retain) NSString *choiceNumber;
@property (nonatomic, retain) NSString *orderNumber;
@property (nonatomic, retain) NSString *clientName;
@property (nonatomic, retain) NSString *employeeName;
@property (nonatomic, retain) NSString *resourceName;
@property (nonatomic, retain) NSString *serviceName;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *serviceIds;
@property (nonatomic, retain) NSString *employeeIds;
@property (nonatomic, retain) NSString *startDateTime;
@property (nonatomic, retain) NSString *endDateTime;
@property (nonatomic, retain) NSString *startLength;
@property (nonatomic, retain) NSString *gapLength;
@property (nonatomic, retain) NSString *finishLength;
@property (nonatomic, retain) NSString *appointmentTypeId;
@property (nonatomic, retain) NSString *resourceId;
@property (nonatomic, retain) NSString *genderId;

@property (nonatomic, retain) NSString *aptdate;
@property (nonatomic, retain) NSString *apttime;
@property (nonatomic, retain) NSString *displayPrice;










@end
