//
//  OnlineStaffList.h
//  Nithin Salon
//
//  Created by Webappclouds on 08/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineStaffList : UIViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
- (IBAction)segmentAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *staffTAV;
@property(nonatomic, retain)NSString *staffID;
- (IBAction)nextAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end
