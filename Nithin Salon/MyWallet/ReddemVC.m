//
//  ReddemVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 05/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ReddemVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AvailbleCoupons.h"
@interface ReddemVC ()
{
    int value;
    NSMutableArray *couponsArray;
    AppDelegate *appDelegate;
}
@end

@implementation ReddemVC
@synthesize pointsStr;
- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate =(AppDelegate*)[UIApplication sharedApplication].delegate;
    self.title =@"Coupons";
    
    _pointsLabel.text = [NSString stringWithFormat:@" Available Points : %@",pointsStr];
    
    [self getAvailbleCoupons];
}



-(void)getAvailbleCoupons{
    [appDelegate showHUD];
    //GET_PROFILE_INFO
    NSMutableDictionary *paramDict = [NSMutableDictionary new];
    [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
    [paramDict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"Coupon_ID"]?:@"" forKey:@"module_id"];
    
    
    NSLog(@"Params : %@",paramDict);
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:GET_REEDEM_AVAILBLE_COUPONS HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *dictJson = (NSDictionary *)responseArray;
            NSLog(@"Response : %@",dictJson);
            NSString *status = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"status"]];
            if ([status isEqualToString:@"1"]){
                
                couponsArray = [NSMutableArray new];
                couponsArray = [dictJson objectForKey:@"availableCoupons"];
                if(couponsArray.count){
                    [self.pointsTV reloadData];
                }
                
                
            }
            else{
                [self showAlert:@"Error" MSG:[dictJson objectForKey:@"message"] BLOCK:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            
        }
        
    }];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return couponsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PointsCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:10];
    UIButton *pointsBtn = (UIButton *)[cell.contentView viewWithTag:20];
    
    NSDictionary *pointsDict =[couponsArray objectAtIndex:indexPath.row];
    
    if(pointsDict.count){
        nameLabel.text = [pointsDict objectForKey:@"coupon_name"]?:@"";
    }
   
    int couponVal = [[pointsDict objectForKey:@"coupon_points"] intValue];
    int mainPoints = [pointsStr intValue];
    
    pointsBtn.layer.cornerRadius = 5.0f;
   
    [pointsBtn setTitle:[pointsDict objectForKey:@"coupon_points"]?:@"" forState:UIControlStateNormal];
   
    pointsBtn.userInteractionEnabled = YES;

    
    if(mainPoints>=couponVal)
    {
       pointsBtn.tintColor = [UIColor greenColor];
       pointsBtn.layer.borderColor = [UIColor greenColor].CGColor;
         pointsBtn.userInteractionEnabled = YES;
    }
    else{
        pointsBtn.tintColor = [UIColor redColor];
        pointsBtn.layer.borderColor = [UIColor redColor].CGColor;
        pointsBtn.userInteractionEnabled = NO;
    }
        
    
    pointsBtn.layer.borderWidth = 1.0f;
    pointsBtn.titleLabel.tag = indexPath.row;
    
    [pointsBtn addTarget:self action:@selector(Add:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)Add:(UIButton *)sender{
       NSDictionary *dict =[couponsArray objectAtIndex:sender.titleLabel.tag];
    [self redeemServiceCall:[dict objectForKey:@"coupon_id"] couponValue:[[dict objectForKey:@"coupon_points"] intValue]];
}


-(void)redeemServiceCall:(NSString *)coupon_ID couponValue:(int)couponVal{
    [appDelegate showHUD];
    //GET_PROFILE_INFO
      NSMutableDictionary *paramDict = [NSMutableDictionary new];
     [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
     [paramDict setObject:coupon_ID?:@"" forKey:@"coupon_id"];
     [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
     [paramDict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    
 

    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:GET_REDEEM_COUPON_POINTS HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *dictJson = (NSDictionary *)responseArray;
            
            NSString *status = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"status"]];
            if ([status isEqualToString:@"1"]){
                
                int value = [pointsStr intValue];
                int finalVal = value-couponVal;
                
                _pointsLabel.text = [NSString stringWithFormat:@" Available Points : %i",finalVal];
                
               self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                    // UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Gallery" bundle:nil];
                    AvailbleCoupons *avc = [self.storyboard instantiateViewControllerWithIdentifier:@"AvailbleCoupons"];
                    [self.navigationController pushViewController:avc animated:YES];
            }
            else{
                [self showAlert:@"Error" MSG:[dictJson objectForKey:@"message"] BLOCK:^{
                    //[self.navigationController popViewControllerAnimated:YES];
                }];
            }
            
        }
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
