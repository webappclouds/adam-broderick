//
//  ReviewCell.m
//  Roys
//
//  Created by Nithin Reddy on 07/03/13.
//
//

#import "ReservationCell.h"

@implementation ReservationCell

@synthesize titleLabel,locationLabel,editButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
