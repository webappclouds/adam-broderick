//
//  ReviewCell.h
//  Roys
//
//  Created by Nithin Reddy on 07/03/13.
//
//

#import <UIKit/UIKit.h>

@interface StaffCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *staffName;

@end
