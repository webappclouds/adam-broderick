//
//  TGLViewController.m
//  TGLStackedViewExample
//
//  Created by Tim Gleue on 07.04.14.
//  Copyright (c) 2014 Tim Gleue ( http://gleue-interactive.com )
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "TGLViewController.h"
#import "TGLCollectionViewCell.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface UIColor (randomColor)

+ (UIColor *)randomColor;

@end

@implementation UIColor (randomColor)

+ (UIColor *)randomColor {
    
    CGFloat comps[3];
    
    for (int i = 0; i < 3; i++) {
        
        NSUInteger r = arc4random_uniform(256);
        comps[i] = (CGFloat)r/255.f;
    }
    
    return [UIColor colorWithRed:comps[0] green:comps[1] blue:comps[2] alpha:1.0];
}

@end

@interface TGLViewController ()
{
    AppDelegate *appDelegate;
}

@property (nonatomic, weak) IBOutlet UIBarButtonItem *deselectItem;
@property (nonatomic, strong) IBOutlet UIView *collectionViewBackground;
@property (nonatomic, weak) IBOutlet UIButton *backgroundButton;

@property (nonatomic, strong, readonly) NSMutableArray *cards;

@property (nonatomic, strong) NSTimer *dismissTimer;

@end

@implementation TGLViewController

@synthesize cards = _cards;

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
//        _cardCount = 20;
        _cardSize = CGSizeZero;
        
        _stackedLayoutMargin = UIEdgeInsetsMake(10.0, 0.0, 0.0, 0.0);
        _stackedTopReveal = 120.0;
        _stackedBounceFactor = 0.2;
        _stackedFillHeight = NO;
        _stackedCenterSingleItem = NO;
        _stackedAlwaysBounce = NO;
    }
    
    return self;
}

- (void)dealloc {

    [self stopDismissTimer];
}

#pragma mark - View life cycle

- (void)viewDidLoad {

    [super viewDidLoad];

    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [self fetchMyWalletItems];

    // KLUDGE: Using the collection view's `-backgroundView`
    //         results in layout glitches when transitioning
    //         between stacked and exposed layouts.
    //         Therefore we add our background in between
    //         the collection view and the view controller's
    //         wrapper view.
    //
    self.collectionViewBackground.hidden = !self.showsBackgroundView;
    [self.view insertSubview:self.collectionViewBackground belowSubview:self.collectionView];

    // KLUDGE: Since our background is below the collection
    //         view it won't receive any touch events.
    //         Therefore we install an invisible/empty proxy
    //         view as the collection view's `-backgroundView`
    //         with the sole purpose to forward events to
    //         our background view.
    //
//    TGLBackgroundProxyView *backgroundProxy = [[TGLBackgroundProxyView alloc] init];
//    
//    backgroundProxy.targetView = self.collectionViewBackground;
//    backgroundProxy.hidden = self.collectionViewBackground.hidden;
//    
//    self.collectionView.backgroundView = backgroundProxy;
    self.collectionView.showsVerticalScrollIndicator = self.showsVerticalScrollIndicator;

    self.exposedItemSize = self.cardSize;

    self.stackedLayout.itemSize = self.exposedItemSize;
    self.stackedLayout.layoutMargin = self.stackedLayoutMargin;
    self.stackedLayout.topReveal = self.stackedTopReveal;
    self.stackedLayout.bounceFactor = self.stackedBounceFactor;
    self.stackedLayout.fillHeight = self.stackedFillHeight;
    self.stackedLayout.centerSingleItem = self.stackedCenterSingleItem;
    self.stackedLayout.alwaysBounce = self.stackedAlwaysBounce;

    if (self.doubleTapToClose) {
        
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        
        recognizer.delaysTouchesBegan = YES;
        recognizer.numberOfTapsRequired = 2;
        
        [self.collectionView addGestureRecognizer:recognizer];
    }
}

- (void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    if (!self.collectionViewBackground.hidden) {

        // KLUDGE: Make collection view transparent
        //         to let background view show through
        //
        // See also: -viewDidLoad
        //
        self.collectionView.backgroundColor = [UIColor clearColor];
    }
    
    if (self.doubleTapToClose) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Double Tap to Close", nil)
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];

        __weak typeof(self) weakSelf = self;
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:^ (UIAlertAction *action) {
                                                           
                                                           [weakSelf.dismissTimer invalidate];
                                                           weakSelf.dismissTimer = nil;
                                                       }];
        
        [alert addAction:action];
        
        [self presentViewController:alert animated:YES completion:^ (void) {
            
            self.dismissTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(dismissTimerFired:) userInfo:nil repeats:NO];
        }];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {

    return UIStatusBarStyleLightContent;
}

#pragma mark - Accessors

- (void)setCardCount:(NSInteger)cardCount {

//    if (cardCount != _cardCount) {
//        
//        _cardCount = cardCount;
//        
//        _cards = nil;
    
        if (self.isViewLoaded) [self.collectionView reloadData];
//    }
}


 -(void) fetchMyWalletItems
 {
     NSString *specialUrl = [NSString stringWithFormat:@"%@", [appDelegate addSalonIdTo:URL_My_WALLET_URL]];
 
     NSMutableDictionary *paramDict = [NSMutableDictionary new];
     [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
 

     //IRIS67744
     NSLog(@"Params : %@", paramDict);
     [appDelegate showHUD];
 
     [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:specialUrl HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
         [appDelegate hideHUD];
         if(error)
             [self showAlert:@"Error" MSG:error.localizedDescription];
         else {
 
             NSDictionary *responseDict = (NSDictionary*) responseArray;
 
             NSLog(@"Resp :%@",responseDict);
//             [self MyGiftCard:[responseDict objectForKey:@"wallet"]];
             
//             if (_cards == nil) {
//                 
//                 _cards = [NSMutableArray array];
//                 [_cards addObjectsFromArray:[responseDict objectForKey:@"wallet"]];
//             }
             
             if([[responseDict objectForKey:@"wallet"] count] > 0)
             {
                 [_cards addObjectsFromArray:[responseDict objectForKey:@"wallet"]];
                 [self.collectionView reloadData];
             }
             else {
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:SALON_NAME message:[responseDict objectForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                 [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                     [self.navigationController popViewControllerAnimated:YES];
                 }]];
                 [self presentViewController:alertController animated:YES completion:Nil];

             }
         }
     }];
 
 }


- (NSMutableArray *)cards {

    if (_cards == nil) {
        
        _cards = [NSMutableArray array];
        
        // Adjust the number of cards here
        //
//        for (NSInteger i = 1; i <= self.cardCount; i++) {
        
//            NSDictionary *card = @{ @"name" : [NSString stringWithFormat:@"Card #%d", (int)i], @"color" : [UIColor randomColor] };
//            UIColor *backgroundColor = [UIColor blackColor];//, 222E40, 305B70, 2769B0
//            if(i==0 || i%4==0)
//                backgroundColor = [self colorFromHexString:@"#2769B0"];
//            else if(i%4==1)
//                backgroundColor = [self colorFromHexString:@"#305B70"];
//            else if(i%4==2)
//                backgroundColor = [self colorFromHexString:@"#222E40"];
//            else if(i%4==3)
//                backgroundColor = [self colorFromHexString:@"#2C4F60"];
        
        
//            NSDictionary *card = @{@"name" : @"$120", @"giftcardType" : @"GIFT CARDS", @"serialNumber": @"1AB7137CA13", @"messageField" : @"SENDER: Nithin Reddy\n\nMESSAGE: Happy Birthday", @"color" : [self colorFromHexString:@"#2769B0"]};
//            [_cards addObject:card];
//        
//        card = @{@"name" : @"1200", @"giftcardType" : @"POINTS", @"serialNumber": @"814HA714P4", @"messageField" : @"Points Available: 1200 points", @"color" : [self colorFromHexString:@"#305B70"]};
//        [_cards addObject:card];
//        
//        card = @{@"name" : @"$30", @"giftcardType" : @"GIFT CARD", @"serialNumber": @"29PA813N7", @"messageField" : @"SENDER: Dilan Desilva\n\nMESSAGE: Happy Anniversary!!", @"color" : [self colorFromHexString:@"#2C4F60"]};
//        [_cards addObject:card];
//        
//        card = @{@"name" : @"20% OFF", @"giftcardType" : @"COUPON", @"serialNumber": @"3941HAFG7", @"messageField" : @"OFFER: 20% off on all retail products.\nExpires July 31, 2017.", @"color" : [self colorFromHexString:@"#222E40"]};
//        [_cards addObject:card];
//        
//        card = @{@"name" : @"FREE HAIR CUT", @"giftcardType" : @"COUPON", @"serialNumber": @"318741A&164", @"messageField" : @"REFERRAL COUPON: FREE HAIR CUT", @"color" : [self colorFromHexString:@"#2769B0"]};
//        [_cards addObject:card];
//        
//        card = @{@"name" : @"$130", @"giftcardType" : @"GIFT CARD", @"serialNumber": @"HAD8137AJL9", @"messageField" : @"SENDER: Dilan Desilva\n\nMESSAGE: Happy Holidays!!", @"color" : [self colorFromHexString:@"#305B70"]};
//        [_cards addObject:card];

        
//        }
    }
    
    return _cards;
}

#pragma mark - Key-Value Coding

- (void)setValue:(id)value forKeyPath:(nonnull NSString *)keyPath {
    
    // Add key-value coding capabilities for some extra properties
    //
    if ([keyPath hasPrefix:@"cardSize."]) {
        
        CGSize cardSize = self.cardSize;
        
        if ([keyPath hasSuffix:@".width"]) {
            
            cardSize.width = [value doubleValue];
            
        } else if ([keyPath hasSuffix:@".height"]) {
            
            cardSize.height = [value doubleValue];
        }
        
        self.cardSize = cardSize;
        
    } else if ([keyPath containsString:@"edLayoutMargin."]) {
        
        NSString *layoutKey = [keyPath componentsSeparatedByString:@"."].firstObject;
        UIEdgeInsets layoutMargin = [layoutKey isEqualToString:@"stackedLayoutMargin"] ? self.stackedLayoutMargin : self.exposedLayoutMargin;
        
        if ([keyPath hasSuffix:@".top"]) {
            
            layoutMargin.top = [value doubleValue];
            
        } else if ([keyPath hasSuffix:@".left"]) {
            
            layoutMargin.left = [value doubleValue];
            
        } else if ([keyPath hasSuffix:@".right"]) {
            
            layoutMargin.right = [value doubleValue];
        }
        
        [self setValue:[NSValue valueWithUIEdgeInsets:layoutMargin] forKey:layoutKey];
        
    } else {
        
        [super setValue:value forKeyPath:keyPath];
    }
}

#pragma mark - Actions

- (IBAction)backgroundButtonTapped:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Background Button Tapped", nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                     style:UIAlertActionStyleDefault
                                                   handler: nil];
    
    [alert addAction:action];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)handleDoubleTap:(UITapGestureRecognizer *)recognizer {
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dismissTimerFired:(NSTimer *)timer {
    
    if (timer == self.dismissTimer && self.presentedViewController) {

        [self dismissViewControllerAnimated:YES completion:^ (void) {
            
            [self stopDismissTimer];
        }];
    }
}

- (IBAction)collapseExposedItem:(id)sender {
    
    self.exposedItemIndexPath = nil;
}

#pragma mark - UICollectionViewDataSource protocol

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.cards.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TGLCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CardCell" forIndexPath:indexPath];
    NSDictionary *card = self.cards[indexPath.item];
    
    cell.title = card[@"wallet_title"];
    

    cell.layer.cornerRadius = 10;
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.clipsToBounds = YES;

    UIColor *walletColor = [UIColor orangeColor];
    if([card[@"wallet_type"] isEqualToString:@"Specials"])
    {
        walletColor = [UIColor purpleColor];
    }
    
    
    NSString *colorStr = [NSString stringWithFormat:@"%@",card[@"color_code"] ];
    cell.color = [self colorFromHexString:colorStr];

    NSString *amountString = [NSString stringWithFormat:@"$ %@",card[@"wallet_value"] ];

    cell.amount = amountString;
    cell.giftcardType = card[@"wallet_type"];
    cell.serialNumber = card[@"unique_code"];
    cell.messageField = card[@"wallet_description"];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    // Update data source when moving cards around
    //
    NSDictionary *card = self.cards[sourceIndexPath.item];
    
    [self.cards removeObjectAtIndex:sourceIndexPath.item];
    [self.cards insertObject:card atIndex:destinationIndexPath.item];
}

#pragma mark - Helpers

- (void)stopDismissTimer {
    
    [self.dismissTimer invalidate];
    self.dismissTimer = nil;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


@end
