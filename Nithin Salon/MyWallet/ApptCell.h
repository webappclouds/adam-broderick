//
//  ApptCell.h
//  Eleven Salon and Spa
//
//  Created by Webappclouds on 20/09/16.
//  Copyright © 2016 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApptCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *nameLable;
@property (nonatomic, retain) IBOutlet UILabel *desLable;
@property (nonatomic, retain) IBOutlet UILabel *timeLable;
@property (nonatomic, retain) IBOutlet UILabel *priceLable;

@property (nonatomic, retain) IBOutlet UIView *lineView;

@property (nonatomic, retain) IBOutlet UIButton *bookApptButton;


@end
