//
//  AvailbleCoupons.m
//  Nithin Salon
//
//  Created by Webappclouds on 05/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "AvailbleCoupons.h"
#import "ScanCouponVC.h"
#import "AppDelegate.h"
@interface AvailbleCoupons ()
{
    AppDelegate *appDelegate;
    NSMutableArray *couponsArray;
}
@end

@implementation AvailbleCoupons

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.title =@"Available Coupons";
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"CheckOut" style:UIBarButtonItemStylePlain target:self action:@selector(checkOut)];
    //self.navigationItem.rightBarButtonItem = anotherButton;
    // Do any additional setup after loading the view.
    [self getAvailbleCoupons];
}
-(void)checkOut{
    
}

-(void)getAvailbleCoupons{
    [appDelegate showHUD];
    //GET_PROFILE_INFO
    NSMutableDictionary *paramDict = [NSMutableDictionary new];
    [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    
    
    
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:GET_AVAILBLE_COUPONS HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *dictJson = (NSDictionary *)responseArray;
            
            NSString *status = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"status"]];
            if ([status isEqualToString:@"1"]){
                
                couponsArray = [NSMutableArray new];
                couponsArray = [dictJson objectForKey:@"availableClientCoupons"];
                if(couponsArray.count){
                    [self.availTV reloadData];
                }
                
                
            }
            else{
                [self showAlert:@"Error" MSG:[dictJson objectForKey:@"message"] BLOCK:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            
        }
        
    }];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return couponsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CouponCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *valueLbl = (UILabel *)[cell.contentView viewWithTag:10];
    UILabel *statusLbl = (UILabel *)[cell.contentView viewWithTag:20];
    UILabel *dateLbl =  (UILabel *)[cell.contentView viewWithTag:30];
    UIButton *statusButton = (UIButton *)[cell.contentView viewWithTag:40];
    UIImageView *cellImage = (UIImageView *)[cell.contentView viewWithTag:100];
    cellImage.layer.cornerRadius  = 6;
    cellImage.layer.shadowColor   =[UIColor darkGrayColor].CGColor;
    cellImage.layer.shadowOffset  = CGSizeMake(0, 0);
    cellImage.layer.shadowRadius  = 1.0;
    cellImage.layer.borderWidth = 0.0f;
    cellImage.layer.shadowOpacity = 0.2;
    UIBezierPath *cornersPath = [UIBezierPath bezierPathWithRoundedRect:cellImage.bounds  cornerRadius:cellImage.layer.cornerRadius];
    cellImage.layer.shadowPath = (__bridge CGPathRef _Nullable)(cornersPath);
    cellImage.layer.masksToBounds = NO;
    

    
     statusButton.hidden = YES;
    
    NSDictionary *coupondict = [couponsArray objectAtIndex:indexPath.row];
   valueLbl.text =[NSString stringWithFormat:@"$ %@ Off",[coupondict objectForKey:@"coupon_offer"]];
    BOOL status = [[coupondict objectForKey:@"is_available_used"] boolValue];
    if(!status == YES){
    statusLbl.text = [coupondict objectForKey:@"coupon_name"];
    [statusButton setTitle:@"Available" forState:UIControlStateNormal];
        statusButton.hidden = NO;
    }
    
    NSString *dateStr  = [coupondict objectForKey:@"created_on"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date = [formatter dateFromString:dateStr];
    [formatter setDateFormat:@"MM/dd/yyyy"];

    dateStr = [formatter stringFromDate:date];
    
    
    dateLbl.text = dateStr;
    
    //statusButton.layer.borderColor = [UIColor blueColor].CGColor;
    //statusButton.layer.borderWidth = 1.0f;
    statusButton.layer.cornerRadius = 1.0f;
    [statusButton addTarget:self action:@selector(scanPush) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(void)scanPush{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    // UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Gallery" bundle:nil];
    ScanCouponVC *wmt = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanCouponVC"];
    [self.navigationController pushViewController:wmt animated:YES];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
