//
//  ServicesTableViewController.m
//  Nithin Salon
//
//  Created by Nithin Reddy on 23/01/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "OnlineServicesTableViewController.h"
#import "OnlineServicesViewController.h"
#import "SubServices.h"
#import "AppDelegate.h"
#import "Globals.h"
//#import "CategoryCell.h"
#import "GlobalSettings.h"
#import "Globals.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "ServicesCell.h"
@interface OnlineServicesTableViewController ()
{
    NSMutableArray *globalResponseArray,*sectionArray,*globalArray,*globalCheckArray;
    NSMutableArray *arraySelectedValue;
    NSMutableDictionary *selectParamDic;
    NSArray *filteredArray;
    
    NSString *globalServiceID;
    BOOL loadValue;
    
    int sectionVal,selectedIndex;
    
    
    AppDelegate *appDelegate;
    NSMutableDictionary * checkPlus;
    NSString *selectionString;
    NSMutableDictionary *globalDIct;
    NSMutableArray *duumyArray;
    
    NSMutableArray *addOnsArray;

}
@end

@implementation OnlineServicesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    sectionVal=-1;
    selectedIndex=-1;
    checkPlus = [[NSMutableDictionary alloc] init];
    
    globalArray=[[NSMutableArray alloc]init];
    globalCheckArray=[NSMutableArray new];
    self.data = [NSMutableArray new];
    
    sectionArray=[NSMutableArray new];
    [self loadDataFromServer];
    //    self.tableView.estimatedRowHeight = 125;
    self.tableView.allowsMultipleSelection = YES;
    //self.automaticallyAdjustsScrollViewInsets = YES;
    
    
    self.tableView.tableHeaderView.backgroundColor = (__bridge UIColor *)([UIColor whiteColor].CGColor);
    
    self.tableView.tableHeaderView.autoresizingMask = UIViewAutoresizingNone;
    
    [self.tableView reloadData];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //    self.tableView.estimatedRowHeight = 125;
    
    NSLog(@"sectonValue %i",sectionVal);
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    if(globalResponseArray.count==0)
        globalResponseArray = [[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ServiceSearch" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receive:) name:@"ServiceSearch" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshAllData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAll) name:@"RefreshAllData" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cartDeleteNotification:) name:@"DelectedCartList" object:nil];
    
}

-(void)cartDeleteNotification:(NSNotification *) notification
{
    sectionVal=-1;
    selectedIndex=-1;
    [self.tableView reloadData];
    
}


-(void) loadDataFromServer
{
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [paramDic setObject:self.menuId forKey:@"category_iid"];
    

    NSString *urlString = [NSString stringWithFormat:@"%@",[appDelegate addSalonIdTo:SUB_SERVICES_LIST]];
   [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
      [MBProgressHUD hideHUDForView:self.view animated:YES];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSDictionary *dict = (NSDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
    
    
}


#pragma mark -
#pragma mark SendRequest delegate
-(void)getResponse:(NSDictionary *)dict
{
    
    
    int status =[[dict objectForKey:@"status"] intValue];
    
    if (dict.count==0||dict==nil || status == 0 )
        [self showAlert:SALON_NAME MSG:@"No services found. Please try again later."];
    
    else {
        NSMutableArray *arrJson = [dict objectForKey:@"services"];
        
        globalArray=arrJson.mutableCopy;
        
        for (int k=0; k<[arrJson count]; k++)
        {
            NSString *sectionNames = [[arrJson objectAtIndex:k] objectForKey:@"service_cdescript"];
            NSMutableDictionary *dictvalues=[[NSMutableDictionary alloc]init];
            [dictvalues setObject:sectionNames forKey:@"service_cdescript"];
            [dictvalues setObject:[[arrJson objectAtIndex:k] objectForKey:@"service_iid"] forKey:@"service_iid"];
            [dictvalues setObject:[[arrJson objectAtIndex:k] objectForKey:@"service_desc"] forKey:@"service_desc"];
            [dictvalues setObject:[[arrJson objectAtIndex:k] objectForKey:@"m_from_to_price"] forKey:@"m_from_to_price"];
            [dictvalues setObject:[[arrJson objectAtIndex:k] objectForKey:@"narration"] forKey:@"narration"];
            //[dictvalues setObject:@"No Washing" forKey:@"narration"];
            
            //[dictvalues setObject:@"" forKey:@"service_desc"];
            
            
            
            NSArray *addons=[[arrJson objectAtIndex:k]objectForKey:@"addon"];
            
            if (addons.count)
            {
                [dictvalues setObject:@"isAddons" forKey:@"isAddons"];
            }
            else{
                
                [dictvalues setObject:@"noAddons" forKey:@"isAddons"];
            }
            //  [dictvalues setObject:addons forKey:@"addons"];
            
            [sectionArray addObject:dictvalues];
            
        }
        
        
        
        
        
        selectParamDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        for (int i=0; i<sectionArray.count; i++)
        {
            
            [globalCheckArray addObject:@"NO"];
            
            
        }
        
        
        globalResponseArray=[arrJson mutableCopy];
        [self.tableView reloadData];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (!sectionArray.count==0) {
        
        return sectionArray.count;
    }
    return 0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(sectionVal == section){
        if (self.data.count)
        {
            return self.data.count;
        }
    }
    
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
//    static NSString *CellIdentifier = @"ServicesCell";
//    ServicesCell *cell = (ServicesCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (cell == nil) {
//        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ServicesCell" owner:nil options:nil];
//        
//        for(id currentObject in topLevelObjects)
//        {
//            if([currentObject isKindOfClass:[ServicesCell class]])
//            {
//                cell = (ServicesCell *)currentObject;
//                break;
//            }
//        }
//    }
    
    static NSString *CellIdentifier = @"ServicesCell";
    
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"ServicesCell" owner:nil options:nil];
    
    ServicesCell *cell = [[ServicesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if ([self.data count] > 0)
    {
        SubServices *serviceObj = [self.data objectAtIndex:indexPath.row];
        
        
        
        
//        if (indexPath.row ==[self.data count] -1 ) {
//            //this method will get called when you will
//            
//            cell.doneBtn.hidden=NO;
//            
//        }
//        else {
//            cell.doneBtn.hidden=YES;
//        }
        
        
        cell.plusBtn.hidden=NO;
        if (checkedArray.count)
        {
            if ([[checkedArray objectAtIndex:indexPath.row] isEqualToString:@"YES"]) {
                // cell.accessoryType = UITableViewCellAccessoryCheckmark;
                cell.serviceTitle .textColor = [self colorFromHexString:@"#D93829"];
                
                [cell.plusBtn setImage:[UIImage imageNamed:@"radio-fill"] forState:UIControlStateNormal];
                 cell. serviceTitle.text=[NSString stringWithFormat:@"%@",serviceObj.serviceName];
                cell.stausLabel.hidden = NO;
                
            }
            else
            {
                
                cell.serviceTitle.textColor = [UIColor blackColor];
                [cell.plusBtn setImage:[UIImage imageNamed:@"radio-empty"] forState:UIControlStateNormal];

                cell.serviceTitle.text=serviceObj.serviceName;
                cell.stausLabel.hidden = YES;
                
            }
        }
        
        
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SubServices *serviceObj = [self.data objectAtIndex:indexPath.row];
    
    // NSString *data = [self.data objectAtIndex:indexPath.row];
    //
    //    [selectParamDic setObject:serviceObj.serviceName forKey:@"addon_servicename"];
    //    [selectParamDic setObject:serviceObj.serviceId forKey:@"addon_service_iid"];
    
    NSDictionary *dict = [sectionArray objectAtIndex:sectionVal];
    
    globalServiceID = [dict objectForKey:@"service_iid"];
    globalDIct = [NSMutableDictionary new];
    [globalDIct setObject:[dict objectForKey:@"service_cdescript"] forKey:@"service_cdescript"];
    [globalDIct setObject:[dict objectForKey:@"service_iid"] forKey:@"service_iid"];
    [globalDIct setObject:serviceObj.serviceName forKey:@"addon_servicename"];
    [globalDIct setObject:serviceObj.serviceId forKey:@"addon_service_iid"];
    
    
    if ([[checkedArray objectAtIndex:indexPath.row] isEqualToString:@"YES"])
    {
        [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"NO"];
        
        [self.reqAppoitmnetDelegate getAddondata:globalDIct type:@"REMOVE"];
        
        
        for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
        {
            
            NSString *serviceID=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"];
            NSMutableDictionary *addOnsDict=[NSMutableDictionary new];
            if([serviceID isEqualToString:globalServiceID])
            {
                
                addOnsDict =[appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
                
                [addOnsDict removeObjectForKey:@"AddOn"];
                NSMutableArray *addon=[NSMutableArray new];
                
                // [addon addObject:globalDIct];
                
                [addOnsDict setObject:[appDelegate.globaldummyArray mutableCopy] forKey:@"AddOn"];
                
                [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:i withObject:addOnsDict];
                
                
                
                
                
            }
            
            
            
        }
        
    }
    else
    {
        [checkedArray replaceObjectAtIndex:indexPath.row withObject:@"YES"];
        
        [self.reqAppoitmnetDelegate getAddondata:globalDIct type:@"ADD"];
        
        
        for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
        {
            
            NSString *serviceID=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"];
            NSMutableDictionary *addOnsDict=[NSMutableDictionary new];
            if([serviceID isEqualToString:globalServiceID])
            {
                
                addOnsDict =[appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
                
                [addOnsDict removeObjectForKey:@"AddOn"];
                NSMutableArray *addon=[NSMutableArray new];
                
                // [addon addObject:globalDIct];
                
                [addOnsDict setObject:[appDelegate.globaldummyArray mutableCopy] forKey:@"AddOn"];
                
                [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:i withObject:addOnsDict];
                
                
                
                
                
            }
            
        }
    }
    
    [self.reqAppoitmnetDelegate setTopBadgeValue:sectionVal];
    
    [self.tableView reloadData];
    
    
}

-(void)doneClicked:(id) sender
{
    
    
    
    for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
    {
        
        NSString *serviceID=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"];
        NSMutableDictionary *addOnsDict=[NSMutableDictionary new];
        if([serviceID isEqualToString:globalServiceID])
        {
            
            addOnsDict =[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] mutableCopy];
            
            [addOnsDict removeObjectForKey:@"AddOn"];
            
            
            [addOnsDict setObject:[appDelegate.globaldummyArray mutableCopy] forKey:@"AddOn"];
            
            [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:i withObject:addOnsDict];
            
            sectionVal=-1;
            selectedIndex=-1;
            
            //[appDelegate];
            
        }
        
        
    }
    
    if(appDelegate.globaldummyArray.count)
    {
        appDelegate.globaldummyArray = [[NSMutableArray alloc]init];
        
        //[appDelegate.globaldummyArray removeAllObjects];
    }
    
    
    [self.tableView reloadData];
    
    
    
    
}
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    NSString *serviceName=[NSString stringWithFormat:@"%@ %@",[[sectionArray objectAtIndex:section] objectForKey:@"service_cdescript"],[[sectionArray objectAtIndex:section] objectForKey:@"narration"]];
    
    NSString *descrpitionString=[[sectionArray objectAtIndex:section] objectForKey:@"service_desc"];
    
    
    UILabel *descriptionLabel=[[UILabel alloc]init];
    descriptionLabel.frame=CGRectMake(10, 35, [UIScreen mainScreen].bounds.size.width-45, 40);
    descriptionLabel.text = descrpitionString;
    
    CGFloat dynamicHeight =  [self getLabelHeight:descriptionLabel];
    NSLog(@"\n");
    NSLog(@"descrpitionString = %@",descrpitionString);
    NSLog(@"vlaue = %f",(float)dynamicHeight);

    if (dynamicHeight > 21)
    {
        return  20 + dynamicHeight;
    }
    else
        return 50;
    
    
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    NSLog(@"Section array :%@",sectionArray);
    
    if (sectionArray.count)
    {
        
        
        
        UIView *selectedView=[[UIView alloc]init];
        selectedView.frame=CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width,40);
        
        selectedView.backgroundColor = [UIColor whiteColor];
        
        NSDictionary *dict=[sectionArray objectAtIndex:section];
        
        NSString *serviceName=[NSString stringWithFormat:@"%@",[[sectionArray objectAtIndex:section] objectForKey:@"service_cdescript"]];
        
        NSString *narration=[NSString stringWithFormat:@"%@",[[sectionArray objectAtIndex:section] objectForKey:@"narration"]];
        
        UILabel *serviceNameLabel=[[UILabel alloc]init];
        if ((narration.length && [[[sectionArray objectAtIndex:section] objectForKey:@"isAddons"] isEqualToString:@"noAddons"]))
        {
            serviceNameLabel.frame=CGRectMake(10, -3,[UIScreen mainScreen].bounds.size.width-30 ,50);
            serviceNameLabel.numberOfLines=4;
            
            
        }
        else
        {
            serviceNameLabel.frame=CGRectMake(10, -3,[UIScreen mainScreen].bounds.size.width-30 ,40);
            serviceNameLabel.numberOfLines=3;
            
        }
        
        serviceNameLabel.font=[UIFont fontWithName:@"GOTHAM-MEDIUM_0" size:8];
        
        
        
        serviceNameLabel.textColor=[UIColor blackColor];
        
        
        NSMutableAttributedString *descriptionString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",serviceName,narration]];
        
        NSRange range1 = [[NSString stringWithFormat:@"%@ %@",serviceName,narration] rangeOfString:narration];
        [descriptionString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:(217/255.0) green:(56/255.0) blue:(41/255.0) alpha:1.0] range:range1];
        

        [descriptionString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:12] range:range1];
        
        
        
        
        serviceNameLabel.attributedText=descriptionString;
        
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(10,4, [UIScreen mainScreen].bounds.size.width,40)];
        btn.backgroundColor = [UIColor whiteColor];
        
        
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.tag = section;
        [btn addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UILabel *descriptionLabel=[[UILabel alloc]init];
        
        if (narration.length>20)
        {
            
            descriptionLabel.frame=CGRectMake(10, 35, [UIScreen mainScreen].bounds.size.width-45, 40);
        }
        else
        {
            descriptionLabel.frame=CGRectMake(10, 35, [UIScreen mainScreen].bounds.size.width-45, 40);
        }
        
        descriptionLabel.textColor=[UIColor blackColor];
        descriptionLabel.text=[NSString stringWithFormat:@"%@",[[sectionArray objectAtIndex:section] objectForKey:@"service_desc"]];
        descriptionLabel.font=[UIFont fontWithName:@"Helvetica" size:12];
        descriptionLabel.numberOfLines=0;
        
        
        CGFloat dynamicHeight =  [self getLabelHeight:descriptionLabel];
        descriptionLabel.frame = CGRectMake(10, 35, descriptionLabel.frame.size.width, dynamicHeight);
//        descriptionLabel.frame.size.height = (float)dynamicHeight;
        

        [btn setFrame:CGRectMake(10, 4, [UIScreen mainScreen].bounds.size.width, 30 + dynamicHeight)];
        UIButton *symbolbBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        symbolbBtn.backgroundColor=[UIColor whiteColor];
        
        symbolbBtn.tag = section;
        
        
        UIButton *checkBox = [[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-25,20, 20,20)];
        [checkBox setImage:[UIImage imageNamed:@"tick-30"] forState:UIControlStateNormal];
        checkBox.backgroundColor=[UIColor clearColor];
        
        checkBox.tag = section;
        [checkBox addTarget:self action:@selector(checkAndUnCheck:) forControlEvents:UIControlEventTouchUpInside];
        [selectedView addSubview:btn];
        
        checkBox.hidden=YES;
        
        
        
        
        for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
        {
            
            NSString *serviceID=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"];
            NSString *sectionServiceID=[dict objectForKey:@"service_iid"];
            
            if ([serviceID isEqualToString:sectionServiceID]) {
                checkBox.hidden=NO;
                
            }
            
            
            
        }
        
        
        UILabel *titleLabel1;
        
        
        
        // Label Text
        for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
        {
            
            NSString *serviceID=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"];
            NSString *sectionServiceID=[dict objectForKey:@"service_iid"];
            
            if ([serviceID isEqualToString:sectionServiceID]) {
                
                NSArray *aadons=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"AddOn"];
                
                if (aadons.count)
                {
                    
                    NSString *addOnserviceName;
                    
                    
                    for (NSDictionary *dictValues in aadons) {
                        
                        if (addOnserviceName.length==0)
                        {
                            addOnserviceName=[[NSString alloc]initWithString:[dictValues objectForKey:@"addon_servicename"]];
                        }
                        else{
                            
                            
                            addOnserviceName =[addOnserviceName stringByAppendingString:[NSString stringWithFormat:@",%@",[dictValues objectForKey:@"addon_servicename"]]];
                            
                            
                        }
                        
                        
                    }
                    
                    
                }
                else
                {
                    
                    if ([[[sectionArray objectAtIndex:section] objectForKey:@"isAddons"] isEqualToString:@"isAddons"])
                    {
                        if(sectionVal == section){
                            
                            titleLabel1.text=[NSString stringWithFormat:@""];
                            symbolbBtn.hidden=NO;
                            [symbolbBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                            [symbolbBtn addTarget:self action:@selector(addonaction:) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                    else{
                        
                    }
                }
            }
            
            
            
        }
        
        
        
        titleLabel1.font=[UIFont fontWithName:@"Helvetica" size:11];
        
        
        //    [titleLabel1 setBackgroundColor:[UIColor redColor]];
        
        titleLabel1.textAlignment=NSTextAlignmentLeft;
        descriptionLabel.textAlignment=NSTextAlignmentLeft;
        //    priceLabel.textAlignment=NSTextAlignmentCenter;
        
        [selectedView addSubview:btn];
        [selectedView addSubview:symbolbBtn];
        [selectedView addSubview:checkBox];
        [selectedView addSubview:serviceNameLabel];
        [selectedView addSubview:titleLabel1];
        [selectedView addSubview:descriptionLabel];
        //    [selectedView addSubview:priceLabel];
        
        return selectedView;
    }
    return nil;
    
}

-(void)addonaction:(id)sender{
    
    
    appDelegate.globaldummyArray=[[NSMutableArray alloc] init];
    
    
    UIButton *btn = (UIButton *)sender;
    NSLog(@"sectonValue %i",sectionVal);
    NSLog(@"Tag: %li",(long)btn.tag);
    
    selectionString=[NSString stringWithFormat:@"%@",[[sectionArray objectAtIndex:btn.tag] objectForKey:@"service_cdescript"]];
    
    
    
    
    selectedIndex=-1;
    
    if (sectionVal==btn.tag)
    {
        
        
        sectionVal=-1;
        
        [self.data removeAllObjects];
        
        
        
    }
    else
    {
        sectionVal =(int)btn.tag;
        NSLog(@"selectionString %@",selectionString);
        
        checkedArray = [NSMutableArray new];
        if (selectionString.length) {
            NSLog(@"title Labe is %@",btn.titleLabel.text);
            
            NSLog(@"selectionString %@",selectionString);
            
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(service_cdescript= %@)",selectionString];
            filteredArray = [globalArray filteredArrayUsingPredicate:bPredicate];
            NSLog(@"HERE %@",filteredArray);
            
            if (self.data.count) {
                [self.data removeAllObjects];
            }
            
            NSMutableArray *arrJson = [[filteredArray objectAtIndex:0] objectForKey:@"addon"];
            
            if (arrJson.count)
            {
                for (int i=0; i<[arrJson count]; i++) {
                    NSDictionary * dictJson=[arrJson objectAtIndex:i];
                    
                    SubServices *subSerObj = [[SubServices alloc] init];
                    [subSerObj setServiceId:[dictJson objectForKey:@"addon_service_iid"]];
                    [subSerObj setServiceName:[dictJson objectForKey:@"addon_servicename"]];
                    [self.data addObject:subSerObj];
                    [checkedArray addObject:@"NO"];
                }
            }
            
            
        }
        
        
        
        
    }
    
    
    
    
    [self.tableView reloadData];
    
}

//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    return [[UIView alloc] init];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 26;
    
}

-(void)deletedAddon:(id)sender{
    
    if(appDelegate.globaldummyArray.count){
        appDelegate.globaldummyArray = [[NSMutableArray alloc] init];
    }
    
    UIButton *btn = (UIButton *)sender;
    
    NSLog(@"Tag: %li",(long)btn.tag);
    
    sectionVal =(int)btn.tag;
    
    NSDictionary *dict = [sectionArray objectAtIndex:sectionVal];
    globalServiceID = [dict objectForKey:@"service_iid"];
    
    
    
    for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
    {
        
        NSString *serviceID=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_iid"];
        NSMutableDictionary *addOnsDict=[NSMutableDictionary new];
        
        
        
        if([serviceID isEqualToString:globalServiceID])
        {
            
            
            addOnsDict =[appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
            
            [addOnsDict removeObjectForKey:@"AddOn"];
            NSMutableArray *addon=[NSMutableArray new];
            
            // [addon addObject:globalDIct];
            
            [addOnsDict setObject:addon forKey:@"AddOn"];
            
            [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:i withObject:addOnsDict];
            //[self.reqAppoitmnetDelegate getcheckeddata:dict type:@"REMOVE" index:(int)btn.tag];
            
            
            
            
            
        }
        
        
    }
    
    [self.reqAppoitmnetDelegate setTopBadgeValue:sectionVal];
    NSString *str = [[sectionArray  objectAtIndex:btn.tag] objectForKey:@"service_cdescript"];
    
    
    
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(service_cdescript = %@)", str];
    filteredArray = [globalArray filteredArrayUsingPredicate:bPredicate];
    NSLog(@"HERE %@",filteredArray);
    
    
    
    if (self.data.count) {
        [self.data removeAllObjects];
    }
    if (checkedArray.count)
    {
        [checkedArray removeAllObjects];
    }
    NSMutableArray *arrJson = [[filteredArray objectAtIndex:0] objectForKey:@"addon"];
    
    if (arrJson.count)
    {
        for (int i=0; i<[arrJson count]; i++) {
            NSDictionary * dictJson=[arrJson objectAtIndex:i];
            
            SubServices *subSerObj = [[SubServices alloc] init];
            [subSerObj setServiceId:[dictJson objectForKey:@"addon_service_iid"]];
            [subSerObj setServiceName:[dictJson objectForKey:@"addon_servicename"]];
            [self.data addObject:subSerObj];
            [checkedArray addObject:@"NO"];
        }
    }
    selectedIndex=-1;
    
    
    [self.tableView reloadData];
    
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView=[[UIView alloc]initWithFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width,1) ];
    [footerView setBackgroundColor:[UIColor lightGrayColor]];
    return footerView;
    
}

-(void)touchDown:(id)sender{
    
    loadValue =YES;
    appDelegate.globaldummyArray=[[NSMutableArray alloc] init];
    
    
    UIButton *btn = (UIButton *)sender;
    NSLog(@"sectonValue %i",sectionVal);
    NSLog(@"Tag: %li",(long)btn.tag);
    
    selectionString=[NSString stringWithFormat:@"%@",[[sectionArray objectAtIndex:btn.tag] objectForKey:@"service_cdescript"]];
    
    
    
    selectedIndex=-1;
    
    if (sectionVal==btn.tag || [[globalCheckArray objectAtIndex:btn.tag] isEqualToString:@"YES"])
    {
        
        
        [self.reqAppoitmnetDelegate getcheckeddata:[sectionArray objectAtIndex:btn.tag] type:@"REMOVE" index:(int)btn.tag];
        [globalCheckArray replaceObjectAtIndex:btn.tag withObject:@"NO"];
        
        
        
        sectionVal = -1;
        [self.data removeAllObjects];
        [self.tableView reloadData];
        
        
    }
    else
    {
        sectionVal =(int)btn.tag;
        NSLog(@"selectionString %@",selectionString);
        
        NSMutableArray *addOnArray = [NSMutableArray new];
        NSMutableDictionary *dict = [sectionArray objectAtIndex:btn.tag];
        [dict setObject:addOnArray forKey:@"AddOn"];
        
        
        
        [self.reqAppoitmnetDelegate getcheckeddata:dict type:@"ADD" index:(int)btn.tag];
        
        [globalCheckArray replaceObjectAtIndex:btn.tag withObject:@"YES"];
        
        checkedArray = [NSMutableArray new];
        if (selectionString.length)
        {
            NSLog(@"title Labe is %@",btn.titleLabel.text);
            
            NSLog(@"selectionString %@",selectionString);
            
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"service_cdescript=%@",selectionString];
            filteredArray = [globalArray filteredArrayUsingPredicate:bPredicate];
            NSLog(@"HERE %@",filteredArray);
            
            if (self.data.count) {
                [self.data removeAllObjects];
            }
            
            NSMutableArray *arrJson = [[filteredArray objectAtIndex:0] objectForKey:@"addon"];
            
            if (arrJson.count)
            {
                for (int i=0; i<[arrJson count]; i++) {
                    NSDictionary * dictJson=[arrJson objectAtIndex:i];
                    
                    SubServices *subSerObj = [[SubServices alloc] init];
                    [subSerObj setServiceId:[dictJson objectForKey:@"addon_service_iid"]];
                    [subSerObj setServiceName:[dictJson objectForKey:@"addon_servicename"]];
                    [self.data addObject:subSerObj];
                    [checkedArray addObject:@"NO"];
                }
            }
            
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    
    [self.tableView reloadData];
    
}



-(void)checkAndUnCheck:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    
    if ([[globalCheckArray objectAtIndex:btn.tag] isEqualToString:@"YES"])
    {
        [self.reqAppoitmnetDelegate getcheckeddata:[sectionArray objectAtIndex:btn.tag] type:@"REMOVE" index:(int)btn.tag];
        [globalCheckArray replaceObjectAtIndex:btn.tag withObject:@"NO"];
        
        
        
        sectionVal = -1;
        [self.data removeAllObjects];
        
        
    }
    else
    {
        
        
        [self.reqAppoitmnetDelegate getcheckeddata:[sectionArray objectAtIndex:btn.tag] type:@"ADD" index:(int)btn.tag];
        
        [globalCheckArray replaceObjectAtIndex:btn.tag withObject:@"YES"];
        sectionVal = -1;
        selectedIndex=-1;
        
    }
    
    [self.tableView reloadData];
}




@end
