//
//  ReservationSummaryVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 21/08/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ReservationSummaryVC.h"
#import "AppDelegate.h"
#import "ReservationCell.h"
#import "OnlineServicesViewController.h"
@interface ReservationSummaryVC ()
{
    NSMutableArray *serviceNamesArray,*empNamesArray;
    NSArray *timesArray;
    
    NSString *staffName;
    AppDelegate *appDelegate;
    NSString *staffString;

}

@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;

@end

@implementation ReservationSummaryVC
@synthesize selectedAppDict;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Reservation Summary";
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    _cartTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.cartTV reloadData];
    
    
    // Check in Time
    NSString *checkInTime=[selectedAppDict objectForKey:@"mCstarttime"];
    timesArray=[checkInTime componentsSeparatedByString:@","];
    
    //timesArray=[[[timesArray reverseObjectEnumerator]allObjects] mutableCopy];
    
    /*  if (timesArray.count>2)
     {
     timesArray = [timesArray mutableCopy];
     
     NSOrderedSet *setArray = [NSOrderedSet orderedSetWithArray:timesArray];
     
     timesArray = [setArray array];
     }
     */
    
    
    _bookNowbtn.layer.borderColor=[UIColor whiteColor].CGColor;
    _bookNowbtn.layer.borderWidth=2.0;
    _bookNowbtn.layer.cornerRadius=4.0;
    
    
    serviceNamesArray=[[NSMutableArray alloc]init];
    empNamesArray=[[NSMutableArray alloc] init];
    
    
    
    
    for(int i=0;i<appDelegate.globalCheckAndUncheckArray.count;i++){
        
        staffString=[[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"StaffDetails"]objectForKey:@"employeeName"];
        if(staffString.length)
            [empNamesArray addObject:staffString];
        [serviceNamesArray addObject:[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"service_cdescript"]];
        NSArray *addonArray=[[appDelegate.globalCheckAndUncheckArray objectAtIndex:i] objectForKey:@"AddOn"];
        
        if (addonArray.count) {
            for (int i=0; i<addonArray.count; i++)
            {
                NSString *addonServiceName=[[addonArray objectAtIndex:i] objectForKey:@"addon_servicename"];
                if(addonServiceName)
                    [serviceNamesArray addObject:addonServiceName];
                if(staffString.length)
                    
                    [empNamesArray addObject:staffString];
                
            }
        }
        
        
    }
    
    //
    //    // ServicesNames
    //
    //    for (NSMutableDictionary *obj in appDelegate.globalCheckAndUncheckArray)
    //    {
    //
    //        NSString *serviceId=[obj objectForKey:@"service_iid"];
    //
    //
    //        //staff
    //        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(service_iid=%@)",serviceId];
    //      NSArray  *filtereStaffdArray = [appDelegate.globalStaffListkArray filteredArrayUsingPredicate:bPredicate];
    //        NSLog(@" filtereStaffdArray %@",filtereStaffdArray);
    //
    //        if(filtereStaffdArray.count)
    //        staffString=[[filtereStaffdArray objectAtIndex:0] objectForKey:@"employeeName"];
    //
    //        //add ons
    //
    //        NSArray *filtereStaffdArray1 = [appDelegate.globalCheckAndUncheckArray filteredArrayUsingPredicate:bPredicate];
    //
    //        NSLog(@" filtereStaffdArray1 %@",filtereStaffdArray1);
    //
    //        if(staffString.length)
    //        [empNamesArray addObject:staffString];
    //
    //
    //        [serviceNamesArray addObject:[obj objectForKey:@"service_cdescript"]];
    //
    //        NSArray *addonArray=[obj objectForKey:@"AddOn"];
    //
    //        if (addonArray.count) {
    //            for (int i=0; i<addonArray.count; i++)
    //            {
    //                NSString *addonServiceName=[[addonArray objectAtIndex:i] objectForKey:@"addon_servicename"];
    //                if(addonServiceName)
    //                [serviceNamesArray addObject:addonServiceName];
    //                if(staffString.length)
    //
    //                [empNamesArray addObject:staffString];
    //
    //            }
    //        }
    //
    //
    //
    //
    //
    //
    //    }
    //
    // Showing location Name
    NSString *locationName=[self loadSharedPreferenceValue:@"locationName"];
    _locationNameLabel.text=locationName;
    
    //    for (NSMutableDictionary *obj in appDelegate.globalCheckAndUncheckArray)
    //    {
    //        [serviceNamesArray addObject:[obj objectForKey:@"service_cdescript"]];
    //
    //    }
    //    for (NSMutableDictionary *staffobj in appDelegate.globalStaffListkArray)
    //    {
    //        [empNamesArray addObject:[staffobj objectForKey:@"employeeName"]];
    //
    //    }
    
    //    if (serviceNamesArray.count>5)
    //    {
    //        _cartTV.scrollEnabled=YES;
    //    }
    //    else
    //        _cartTV.scrollEnabled=NO;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    
    return serviceNamesArray.count;;
    
    
}



// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
  /*  static NSString *CellIdentifier = @"ReservationCell";
    ReservationCell *cell = (ReservationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ReservationCell" owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[ReservationCell class]])
            {
                cell = (ReservationCell *)currentObject;
                break;
            }
        }
    }
    */
    static NSString *CellIdentifier = @"ReservationCell";
    
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"ReservationCell" owner:nil options:nil];
    
    ReservationCell *cell = [[ReservationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    NSString *empStr=[empNamesArray objectAtIndex:indexPath.row];
    
    
    NSString *fullName=[NSString stringWithFormat:@"%@ %@",empStr,[serviceNamesArray objectAtIndex:indexPath.row]];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:fullName];
    
    NSRange range=[fullName rangeOfString:empStr];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    
    
    cell.titleLabel.attributedText=string;
    
    NSString *appointmentDate=[selectedAppDict objectForKey:@"AppointmentDate"];
    
    NSString *myString = appointmentDate;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"MM-dd-yyyy";
    NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
    
    
    
    NSString *fromTimes =[timesArray objectAtIndex:indexPath.row];
    
    NSMutableAttributedString * string1 =[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ at %@",[dateFormatter stringFromDate:yourDate],fromTimes]];
    
    // NSRange range1 = [[NSString stringWithFormat:@"%@ at %@",appointmentDate,fromTimes] rangeOfString:@"at"];
    //[string1 addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:range1];
    
    //[string1 addAttribute:NSForegroundColorAttributeName value:[UIFont fontWithName:@"GothamLight" size:12] range:range1];
    
    
    cell.locationLabel.attributedText=string1;
    
    [cell.editButton addTarget:self action:@selector(navigateTo:) forControlEvents:UIControlEventTouchUpInside];
    cell.editButton.tag = indexPath.row;
    
    
    return cell;
}

-(NSDate *)getDateFromString:(NSString *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* myDate = [myFormatter dateFromString:pstrDate];
    return myDate;
}
-(void)navigateTo:(UIButton *)sender
{
    NSString *serviceStr = [serviceNamesArray  objectAtIndex:sender.tag];
    //NSString *str = [dictionaary objectForKey:@"service_iid"];
    
    for(int i =0; i<appDelegate.globalCheckAndUncheckArray.count;i++)
    {
        NSMutableDictionary *dict= [appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
        
        if([serviceStr isEqualToString:[dict objectForKey:@"service_cdescript"]])
        {
            [dict removeObjectForKey:@"StaffDetails"];
            NSMutableDictionary *staffDict = [NSMutableDictionary new];
            [dict setObject:staffDict forKey:@"StaffDetails"];
            [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:i withObject:dict];
        }
        
    }
    
    //    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(service_iid != %@)",serviceStr];
    //    NSArray* filteredArray = [appDelegate.globalStaffListkArray filteredArrayUsingPredicate:bPredicate];
    //    NSLog(@"HERE %@",filteredArray);
    
    //
    //
    //    [dict removeObjectForKey:@"employeeName"];
    //    [dict setObject:@"" forKey:@"employeeName"];
    //
    //    [appDelegate.globalStaffListkArray replaceObjectAtIndex:sender.tag withObject:dict];
    //
    
    for(UIViewController *navView in self.navigationController.viewControllers)
    {
        if([navView isKindOfClass:[OnlineServicesViewController class]]){
            [self.navigationController popToViewController:navView animated:YES];
        }
        
    }
    
    
}


//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,40)];
//
//    [headerView setBackgroundColor:[UIColor clearColor]];
//
//    UILabel *objLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,0,320,40)];
//
//    objLabel.text = @"SERVICE PROVIDER";
//    objLabel.font=[UIFont fontWithName:@"OpenSans-Bold" size:17];
//    objLabel.textColor=[Globals colorFromHexString:@"#D93829"];
//    [headerView addSubview:objLabel];   //Similarly You can Add any UI Component on header
//
//    return headerView;;
//}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section==3)
    {
        CGSize maximumLabelSize = CGSizeMake(296,9999);
        
        CGFloat height1,height2;
        height1=21;
        
        CGSize actualLabelSize1;
        
        NSString *serviceName = [serviceNamesArray componentsJoinedByString:@","];
        
        actualLabelSize1 = [serviceName sizeWithFont:[UIFont fontWithName:@"OpenSans" size:14]  constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeTailTruncation];
        
        height2=actualLabelSize1.height;
        
        return height1+height2+80;
    }
    
    return 80;
}

-(NSString*)convert:(NSMutableString *)strValue
{
    // strValue = [NSMutableString stringWithString:@"1015"];
    [strValue insertString:@":" atIndex:2];
    NSString *conStr = [NSString stringWithString:strValue];
    NSLog(@"%@",conStr);
    return conStr;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)bookNowAction:(id)sender {
    
    [self bookAndConfirmAppointment:selectedAppDict];
    
}
-(void)bookAndConfirmAppointment :(NSMutableDictionary *)paramDic
{
    NSLog(@"checkForAvailability Prarams : %@",paramDic);
    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:[appDelegate addSalonIdTo:BOOKING_CONFIRMATION] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription tag:0];
        else
        {
            NSDictionary *dict = (NSDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
}


-(void)getResponse:(NSDictionary *)dict
{
    int status =[[dict objectForKey:@"status"] intValue];
    
    if (dict==nil){
        
        [self showAlert:@"Error" MSG:@"Your appointment has been not booked."];
    }
    else {
        int apptMess=[[dict objectForKey:@"AppointmentId"] intValue];
        NSString *apptIdStr=[NSString stringWithFormat:@"Thank you. Your appointment has been booked. Your booking Id# %d",apptMess];
        [self showAlert:@"" MSG:apptIdStr TAG:2 DELEGATE:self];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==2)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)editAction:(id)sender {
    
    [self showAlert:@"" MSG:@"Online booking is now available only for Cherry Hill location."];
    
}
- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) showAlert : (NSString *)title MSG:(NSString *)message TAG:(int)tag DELEGATE:(id)delegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert setTag:tag];
    [alert show];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
