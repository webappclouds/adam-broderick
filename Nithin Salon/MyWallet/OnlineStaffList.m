//
//  OnlineStaffList.m
//  Nithin Salon
//
//  Created by Webappclouds on 08/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "OnlineStaffList.h"
#import "AppDelegate.h"
#import "Staff.h"
#import "BBBadgeBarButtonItem.h"
#import "OnlineCalendarVC.h"
#import "CartListVC.h"
#import "StaffCell.h"
@interface OnlineStaffList ()
{
    AppDelegate *appDelegate;
    NSMutableArray *staffListData;
    NSString *secltedStaffNameStr,*timeStr;
    int selectedIndex,sectionVal;
    BBBadgeBarButtonItem *barButton;
    NSMutableDictionary *paramDic;
    NSMutableArray *serviceIDArray;
    NSMutableArray *globalResponseArray,*sectionArray,*globalArray,*globalCheckArray;
    NSArray *filteredArray;
    
    NSMutableArray *staffSelectonArray,*dummyStaffArrayValues;
    
    NSMutableArray *arraySelectedValue;
    NSMutableDictionary *selectParamDic;
    NSString *selectionString;
}
@end

@implementation OnlineStaffList
@synthesize staffID;
- (void)viewDidLoad {
    [super viewDidLoad];
    sectionVal=-1;
    
    
    selectedIndex =-1;
    
    
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.globalStaffDummyArray=[[NSMutableArray alloc]init];
    
    for(int i=0;i<appDelegate.globalCheckAndUncheckArray.count;i++){
        
        NSMutableDictionary *dictt = [appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
        
        if([[dictt objectForKey:@"StaffDetails"] count] == 0){
            NSMutableDictionary *staffDict = [NSMutableDictionary new];
            
            [dictt setObject:staffDict forKey:@"StaffDetails"];
            
            [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:i withObject:dictt];
        }
        
    }
    
    
    
    
    if (appDelegate.globalStaffListkArray.count)
    {
        [appDelegate.globalStaffListkArray removeAllObjects];
    }
    
    serviceIDArray = [NSMutableArray new];
    
    
    
    
    
    
    
    
    self.title = @"Service Providers";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.segmentController.selectedSegmentIndex = 0;
    timeStr =@"Morning";
    
    _nextButton.layer.borderColor=[UIColor whiteColor].CGColor;
    _nextButton.layer.borderWidth=2.0;
    _nextButton.layer.cornerRadius=4.0;
    
    
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [customButton addTarget:self action:@selector(barButtonItemPressed) forControlEvents:UIControlEventTouchUpInside];
    [customButton setImage:[UIImage imageNamed:@"cart_icon"] forState:UIControlStateNormal];
    
    // Then create and add our custom BBBadgeBarButtonItem
    barButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
    // Set a value for the badge
    barButton.badgeBGColor = [self colorFromHexString:@"#3BC651"];
    barButton.badgeOriginX = 0;
    barButton.badgeOriginY = 0;
    
    self.navigationItem.rightBarButtonItem = barButton;
    
    
    // Add it as the leftBarButtonItem of the navigation bar
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _staffTAV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.staffTAV.allowsMultipleSelection = NO;
    
    
    staffListData = [[NSMutableArray alloc] init];
    
    staffSelectonArray=[[NSMutableArray alloc] init];
    sectionArray=[[NSMutableArray alloc] init];
    globalCheckArray=[NSMutableArray new];
    
    
    globalArray=[NSMutableArray new];
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    sectionVal=-1;
    
    
    selectedIndex =-1;
    [staffListData removeAllObjects];
    [sectionArray removeAllObjects];

    [self RefreshCount];
    [self updateStaff];
}
-(void)updateStaff
{
    [serviceIDArray removeAllObjects];
    for(NSDictionary *dict in appDelegate.globalCheckAndUncheckArray)
    {
        [serviceIDArray addObject:[dict objectForKey:@"service_iid"]];
    }
    
    NSArray *dummyArray = [serviceIDArray mutableCopy];
    
    NSOrderedSet *setArray = [NSOrderedSet orderedSetWithArray:dummyArray];
    
    dummyArray = [setArray array];
    
    NSString *servicesIdsStr = [NSString stringWithFormat:@"%@", [dummyArray componentsJoinedByString:@","]];
    
    if(appDelegate.globalCheckAndUncheckArray.count>1){
        
        staffID = [[appDelegate.globalCheckAndUncheckArray objectAtIndex:0] objectForKey:@"service_iid"];
        
    }
    NSLog(@"here : %@",appDelegate.globalCheckAndUncheckArray);
    
    if(servicesIdsStr.length)
    {
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [paramDic setObject:servicesIdsStr forKey:@"service_iid"];
        
        NSString *urlString = [NSString stringWithFormat:@"%@",[appDelegate addSalonIdTo:SERVICES_STAFF_LIST_CHERRY]];
        [appDelegate showHUD];
        [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
            [appDelegate hideHUD];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                NSDictionary *dict = (NSDictionary *)responseArray;
                [self getResponse:dict];
            }
        }];
    }

}
-(void)RefreshCount{
    [self setTopBadgeValue:nil];
    [self.staffTAV reloadData];

}

-(void)setTopBadgeValue:(int)sectionVal
{
    
    int totalCount = 0;
    
    for(int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++)
    {
        NSMutableDictionary *dict = [appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
        
        if(dict.count){
            
            NSArray *array = [[dict objectForKey:@"AddOn"] mutableCopy];
            int k = (int)array.count;
            totalCount = totalCount+k;
        }
        
    }
    
    int globalCount = (int)appDelegate.globalCheckAndUncheckArray.count;
    totalCount =  totalCount + globalCount;
    
    appDelegate.totalCount = totalCount;
    barButton.badgeValue =[NSString stringWithFormat:@"%i", totalCount];
}

-(void) barButtonItemPressed
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    if(appDelegate.globalCheckAndUncheckArray.count){
        CartListVC *clv=(CartListVC *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CartListVC"];
        clv.selectedApptData = [appDelegate.globalCheckAndUncheckArray mutableCopy];
        [self.navigationController pushViewController:clv animated:YES];
    }
}
-(void) showAlert : (NSString *) title MSG:(NSString *) message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)closeView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark -
#pragma mark SendRequest delegate
-(void)getResponse:(NSDictionary *)dict
{
    int status =[[dict objectForKey:@"status"] intValue];
    
    if (dict.count==0|| status == 0 )
        [self showAlert:SALON_NAME MSG:@"No staff found. Please try again later."];
    
    else {
        NSMutableArray *arrJson = [dict objectForKey:@"serviceproviders"];
        
        
        globalArray=arrJson.mutableCopy;
        
        for (int k=0; k<[arrJson count]; k++)
        {
            NSString *sectionNames = [[arrJson objectAtIndex:k] objectForKey:@"service_name"];
            NSMutableDictionary *dictvalues=[[NSMutableDictionary alloc]init];
            [dictvalues setObject:sectionNames forKey:@"service_cdescript"];
            [dictvalues setObject:[[arrJson objectAtIndex:k] objectForKey:@"service_id"] forKey:@"service_iid"];
            
            NSArray *addons=[[arrJson objectAtIndex:k]objectForKey:@"employees"];
            
            if (addons.count)
            {
                [dictvalues setObject:@"isAddons" forKey:@"isemployees"];
            }
            else{
                
                [dictvalues setObject:@"noAddons" forKey:@"isemployees"];
            }
            
            [sectionArray addObject:dictvalues];
            
        }
        
        arraySelectedValue=[[NSMutableArray alloc]init];
        
        selectParamDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        for (int i=0; i<sectionArray.count; i++)
        {
            [arraySelectedValue addObject:@"NO"];
            [globalCheckArray addObject:@"NO"];
            
        }
        
        [_staffTAV reloadData];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (!sectionArray.count==0) {
        
        return sectionArray.count;
    }
    return 0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(sectionVal == section){
        if (staffListData.count)
        {
            return staffListData.count;
        }
    }
    
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
//    static NSString *CellIdentifier = @"StaffCell";
//    StaffCell *cell = (StaffCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (cell == nil) {
//        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"StaffCell" owner:nil options:nil];
//        
//        for(id currentObject in topLevelObjects)
//        {
//            if([currentObject isKindOfClass:[CartCell class]])
//            {
//                cell = (CartCell *)currentObject;
//                break;
//            }
//        }
//    }
    
    
    static NSString *CellIdentifier = @"StaffCell";
    
    NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"StaffCell" owner:nil options:nil];
    
    StaffCell *cell = [[StaffCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell = [arrData objectAtIndex:0];

    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //
    //    if ([self.data count] > 0)
    //    {
    Staff *staffObj = [staffListData objectAtIndex:indexPath.row];
    
    cell.staffName.text=[NSString stringWithFormat:@"%@ %@",staffObj.employeeFName,staffObj.employeeLName];
    
    NSLog(@"\n");
    NSLog(@"Cell for Row : %d",indexPath.row);
    NSLog(@"Name : %@ AND id : %@",[NSString stringWithFormat:@"%@ %@",staffObj.employeeFName,staffObj.employeeLName], staffObj.employeeId);
    //after selection
    if(indexPath.row  == selectedIndex)
    {
        cell.staffName.textColor = [self colorFromHexString:@"#D93829"];
        
    }
    else
    {
        cell.staffName.textColor = [self colorFromHexString:@"#145A32"];
    }
    
    
    
    
    // }
    
    return cell;
}
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 24;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView=[[UIView alloc]initWithFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width,1) ];
    [footerView setBackgroundColor:[UIColor lightGrayColor]];
    return footerView;
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    UIView *selectedView=[[UIView alloc]initWithFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width,60) ];
    [selectedView setBackgroundColor:[UIColor clearColor]];
    
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(10,0, [UIScreen mainScreen].bounds.size.width-10,30)];
    btn.backgroundColor = [UIColor whiteColor];
    //    SubCatergory *ss = [sectionArray objectAtIndex:section];
    
    
    [btn setTitle:[NSString stringWithFormat:@"%@",[[sectionArray objectAtIndex:section] objectForKey:@"service_cdescript"]] forState:UIControlStateNormal];
    
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn.titleLabel.font=[UIFont fontWithName:@"Gotham-Book" size:12];
    
    // btn.titleLabel.text = [NSString stringWithFormat:@"%@",ss.categoryName];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.tag = section;
    [btn addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchUpInside];
    [selectedView addSubview:btn];
    
    
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(5,28, 20,20)];
    
    UIButton *symbolbBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,28, 100,20)];
    symbolbBtn.backgroundColor=[UIColor whiteColor];
    
    symbolbBtn.tag = section;
    [selectedView addSubview:symbolbBtn];
    
    
    
    
    
    UILabel *titleLabel1=[[UILabel alloc]initWithFrame:CGRectMake(27, 23, [UIScreen mainScreen].bounds.size.width, 30) ];
    titleLabel1.numberOfLines=2;
    
    symbolbBtn.frame = titleLabel1.frame;
    titleLabel1.font=[UIFont fontWithName:@"Gotham-light" size:10];
    
    titleLabel1.textColor = [UIColor blueColor];
    
    
    
    
    
    NSMutableDictionary *dict = [appDelegate.globalCheckAndUncheckArray objectAtIndex:section];
    
    if([[dict objectForKey:@"StaffDetails"] count] == 0)
    {
        titleLabel1.text=[NSString stringWithFormat:@"Select a Service Provider ⬇\uFE0E"];
        symbolbBtn.hidden=NO;
        //image.image=[UIImage imageNamed:@"plus-20"];
        [symbolbBtn addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else{
        
        
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Service Provider: %@ %@",[[dict objectForKey:@"StaffDetails"] objectForKey:@"employeeName"],@"Added"]];
        
        NSRange range = [[NSString stringWithFormat:@"Service Provider: %@ %@",[[dict objectForKey:@"StaffDetails"] objectForKey:@"employeeName"],@"Added"] rangeOfString:@"Added"];
        NSRange range2 = [[NSString stringWithFormat:@"Service Provider: %@ %@",[[dict objectForKey:@"StaffDetails"] objectForKey:@"employeeName"],@"Added"] rangeOfString:@"Service Provider:"];

        [attString addAttribute:NSForegroundColorAttributeName value:[self colorFromHexString:@"#F92E2E"] range:range];
        [attString addAttribute:NSForegroundColorAttributeName value:[self colorFromHexString:@"#F92E2E"] range:range2];

        titleLabel1.attributedText = attString;
        
        
        symbolbBtn.hidden=NO;
        //                [symbolbBtn setImage:[UIImage imageNamed:@"minus-20"] forState:UIControlStateNormal];
        //image.image=[UIImage imageNamed:@"minus-20"];
        [symbolbBtn addTarget:self action:@selector(deletedAddon:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
    }
    
    
    
    
    
    
    titleLabel1.textAlignment=NSTextAlignmentLeft;
    
    [selectedView addSubview:titleLabel1];
    [selectedView addSubview:image];
    
    
    return selectedView;
    
}
-(void)touchDown:(id)sender{
    
    
    
    UIButton *btn = (UIButton *)sender;
    NSLog(@"sectonValue %i",sectionVal);
    NSLog(@"Tag: %li",(long)btn.tag);
    
    selectionString=[NSString stringWithFormat:@"%@",[[sectionArray objectAtIndex:btn.tag] objectForKey:@"service_cdescript"]];
    
    selectedIndex=-1;
    
    if (sectionVal==btn.tag)
    {
        sectionVal=-1;
        
        [globalCheckArray replaceObjectAtIndex:btn.tag withObject:@"NO"];
        
        [arraySelectedValue replaceObjectAtIndex:btn.tag withObject:@"NO"];
        [staffListData removeAllObjects];
        
    }
    else
    {
        sectionVal =(int)btn.tag;
        
        if (selectionString.length) {
            NSLog(@"title Labe is %@",btn.titleLabel.text);
            
            
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(service_name =%@)",selectionString];
            filteredArray = [globalArray filteredArrayUsingPredicate:bPredicate];
            NSLog(@"HERE %@",filteredArray);
            
            if (staffListData.count) {
                [staffListData removeAllObjects];
            }
            
            NSMutableArray *arrJson = [[filteredArray objectAtIndex:0] objectForKey:@"employees"];
            
            if (arrJson.count)
            {
                for (int i=0; i<[arrJson count]; i++) {
                    NSDictionary *empDict = [arrJson objectAtIndex:i];
                    Staff *staffObj = [[Staff alloc] init];
                    [staffObj setEmployeeId:[empDict objectForKey:@"employee_iid"]];
                    [staffObj setEmployeeFName:[empDict objectForKey:@"firstname"]];
                    [staffObj setEmployeeLName:[empDict objectForKey:@"lastname"]];
                    [staffListData addObject:staffObj];
                }
                
                
            }
            
            Staff *staffObj = [[Staff alloc] init];
            [staffObj setEmployeeId:@"0"];
            [staffObj setEmployeeFName:@"No"];
            [staffObj setEmployeeLName:@"Preference"];
            [staffListData  insertObject:staffObj atIndex:0];
            
        }
        [globalCheckArray replaceObjectAtIndex:btn.tag withObject:@"YES"];
        
        
        
        
    }
    
    [self.staffTAV reloadData];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    selectedIndex = (int) indexPath.row;
    
    
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //     [tableView cellForRowAtIndexPath:indexPath].textLabel.textColor = [self colorFromHexString:@"#D93829"];
    Staff *obj = [staffListData objectAtIndex:selectedIndex];
    
    [selectParamDic setObject:obj.employeeId forKey:@"EmpId"];
    [selectParamDic setObject:obj.employeeFName forKey:@"EmpFName"];
    [selectParamDic setObject:obj.employeeLName forKey:@"EmpLName"];
    
    NSString *str=[NSString stringWithFormat:@"%@ %@",obj.employeeFName,obj.employeeLName];
    
    
    
    NSDictionary *dict = [sectionArray objectAtIndex:sectionVal];
    
    
    
    NSMutableDictionary *globalDIct = [NSMutableDictionary new];
    [globalDIct setObject:obj.employeeId forKey:@"employeeId"];
    [globalDIct setObject:str forKey:@"employeeName"];
    
    [globalDIct setObject:[dict objectForKey:@"service_cdescript"] forKey:@"service_cdescript"];;
    
    [globalDIct setObject:[dict objectForKey:@"service_iid"] forKey:@"service_iid"];
    
    
    
    NSMutableDictionary *dictt = [appDelegate.globalCheckAndUncheckArray objectAtIndex:sectionVal];
    
    NSMutableDictionary *staffDict = [NSMutableDictionary new];
    [staffDict setObject:obj.employeeId forKey:@"employeeId"];
    [staffDict setObject:str forKey:@"employeeName"];
    
    [dictt removeObjectForKey:@"StaffDetails"];
    
    [dictt setObject:staffDict forKey:@"StaffDetails"];
    
    [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:sectionVal withObject:dictt];
    
    
    [appDelegate.globalStaffListkArray addObject:globalDIct];
    
    [appDelegate.globalStaffDummyArray addObject:obj.employeeId];
    
    [staffSelectonArray addObject:selectParamDic];
    
    [arraySelectedValue replaceObjectAtIndex:sectionVal withObject:selectParamDic];
    
    [self saveSharedPreferenceValue:[NSString stringWithFormat:@"%@ %@",obj.employeeFName,obj.employeeLName] KEY:@"empNameshow"];
    
    sectionVal=-1;
    selectedIndex=-1;
    [self.staffTAV reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentAction:(id)sender {
    UISegmentedControl *seg = (UISegmentedControl*)sender;
    timeStr = [seg titleForSegmentAtIndex:seg.selectedSegmentIndex];

    
}


- (IBAction)nextAction:(id)sender {
    
    BOOL equalCount = NO;
    for (int i=0; i<appDelegate.globalCheckAndUncheckArray.count; i++) {
        
        NSMutableDictionary *dict = [appDelegate.globalCheckAndUncheckArray objectAtIndex:i];
        
        if([[dict objectForKey:@"StaffDetails"] count] == 0)
        {
            equalCount = YES;
            
        }
        else{
            
        }
        
    }
    
    
    if (equalCount == NO)
    {
        NSLog(@"Equal");
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
        OnlineCalendarVC *osl = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineCalendarVC"];
        //    osl.staffID = [[appDelegate.globalCheckAndUncheckArray objectAtIndex:0] objectForKey:@"serviceID"];
        
        
        
        paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [osl setSelectedEmp:@"No"];
        [osl setSelectedApptData:appDelegate.globalCheckAndUncheckArray];
        [paramDic setObject:timeStr forKey:@"Time"];
        [osl setParamDict:paramDic];
        
        
        [self.navigationController pushViewController:osl animated:YES];
    }
    else
    {
        NSLog(@"not equal");
        [self showAlert:@"" MSG:@"Please select a service provider"];
        
    }
    
}
/*
- (IBAction)nextAction:(id)sender {
    
    
    if (appDelegate.globalCheckAndUncheckArray.count>1)
    {
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
        OnlineCalendarVC *osl = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineCalendarVC"];
        //    osl.staffID = [[appDelegate.globalCheckAndUncheckArray objectAtIndex:0] objectForKey:@"serviceID"];
        
        
      
            paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
            [osl setSelectedEmp:@"No"];
            [osl setSelectedApptData:appDelegate.globalCheckAndUncheckArray];
            [paramDic setObject:timeStr forKey:@"Time"];
             [osl setParamDict:paramDic];
            

        [self.navigationController pushViewController:osl animated:YES];
    }
    
    else
    {
        if(selectedIndex==-1)
        {
            [self showAlert:@"Specilalist" MSG:@"Please select at least one Specilalist"];
        }
        else
        {
            
            
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
            OnlineCalendarVC *osl = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineCalendarVC"];
            //    osl.staffID = [[appDelegate.globalCheckAndUncheckArray objectAtIndex:0] objectForKey:@"serviceID"];
            
            
            if (appDelegate.globalCheckAndUncheckArray.count==1) {
                [paramDic setObject:timeStr forKey:@"Time"];
                [osl setParamDict:paramDic];
                [osl setSelectedEmp:@"Yes"];
                [osl setSelectedApptData:appDelegate.globalCheckAndUncheckArray];
            }
            else
            {
                paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
                [osl setSelectedEmp:@"No"];
                [osl setSelectedApptData:appDelegate.globalCheckAndUncheckArray];
                [paramDic setObject:timeStr forKey:@"Time"];
                 [osl setParamDict:paramDic];
                
            }
            [self.navigationController pushViewController:osl animated:YES];
        }
    }
   
    [appDelegate.onlineAppointmentBookDetails setObject:timeStr forKey:@"time"];

    
}

*/


-(void)doneClicked:(UIButton*) sender
{
    Staff *serviceObj = [staffListData objectAtIndex:[sender tag]];
    [arraySelectedValue replaceObjectAtIndex:sectionVal withObject:selectParamDic];
    sectionVal=-1;
    selectedIndex=-1;
    [self.staffTAV reloadData];
    
}

-(void)deletedAddon:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    
    NSLog(@"Tag: %li",(long)btn.tag);
    
    
    
    NSMutableDictionary *dictt = [appDelegate.globalCheckAndUncheckArray objectAtIndex:btn.tag];
    
    NSMutableDictionary *staffDict = [NSMutableDictionary new];
    
    
    [dictt setObject:staffDict forKey:@"StaffDetails"];
    
    [appDelegate.globalCheckAndUncheckArray replaceObjectAtIndex:btn.tag withObject:dictt];
    
    
    
    
    
    [arraySelectedValue replaceObjectAtIndex:btn.tag withObject:@"HA"];
    
    
    
    NSString *str = [[sectionArray  objectAtIndex:btn.tag] objectForKey:@"service_cdescript"];
    
    
    
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"(service_cdescript = %@)", str];
    
    NSArray* filteredArray1 = [appDelegate.globalStaffListkArray filteredArrayUsingPredicate:bPredicate];
    [appDelegate.globalStaffListkArray removeObjectsInArray:filteredArray1];
    
    
    NSLog(@"After Remove appDelegate.globalAddonsListkArray %@",appDelegate.globalStaffListkArray);
    
    [self.staffTAV reloadData];
    sectionVal=-1;
    // [self.reqAppoitmnetDelegate getcheckeddata:[sectionArray objectAtIndex:btn.tag] type:@"REMOVE"];
    
    
    
    
    
}
- (IBAction)cancelAction:(id)sender {
    for(UIViewController *controller in self.navigationController.viewControllers){
//        if([controller isKindOfClass:[HomeScreen class]]){
//            
//            
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        }
    }
    
}

@end
