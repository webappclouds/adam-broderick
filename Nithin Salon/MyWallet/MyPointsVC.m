//
//  MyPointsVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 05/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "MyPointsVC.h"
#import "ReddemVC.h"
#import "AvailbleCoupons.h"
#import "AppDelegate.h"
#import "Constants.h"
@interface MyPointsVC ()
{
    AppDelegate *appDelegate;
    NSString *totalPointsStr;
}
@end

@implementation MyPointsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"My Points";
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.redeemButton.layer.cornerRadius = 5.0f;
    self.availButton.layer.cornerRadius = 5.0f;
    totalPointsStr =@"0";
  
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
      [self getRewardPoints];
}
-(void)getRewardPoints{
    [appDelegate showHUD];
    //GET_PROFILE_INFO
    NSMutableDictionary *paramDict = [NSMutableDictionary new];
    [paramDict setObject:appDelegate.salonId forKey:@"salon_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"clientid"]?:@"" forKey:@"client_id"];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    
    NSLog(@"paramDict : %@",paramDict);
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:GET_CLIENT_REWARD_POINTS HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *dictJson = (NSDictionary *)responseArray;
            
            NSLog(@"dictJson : %@",dictJson);
            NSString *status = [NSString stringWithFormat:@"%@",[dictJson objectForKey:@"status"]];
            if ([status isEqualToString:@"1"]){
                totalPointsStr =[dictJson objectForKey:@"client_reward_points"]?:@"";
                _valueOfPoints.text = [NSString stringWithFormat:@"%@",totalPointsStr];
                
            }
            else
                [self showAlert:@"Error" MSG:[dictJson objectForKey:@"message"]];
            
        }
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)availbleClick:(id)sender {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    // UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Gallery" bundle:nil];
    AvailbleCoupons *avc = [self.storyboard instantiateViewControllerWithIdentifier:@"AvailbleCoupons"];
    [self.navigationController pushViewController:avc animated:YES];
}

- (IBAction)reddemLick:(id)sender {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    // UIStoryboard *storyBoard =[UIStoryboard storyboardWithName:@"Gallery" bundle:nil];
    ReddemVC *redvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReddemVC"];
    redvc.pointsStr = totalPointsStr?:@"0";
    [self.navigationController pushViewController:redvc animated:YES];

}
@end
