//
//  StaffList.m
//  Nithin Salon
//
//  Created by Webappclouds on 07/02/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "StaffList.h"
#import "Constants.h"
#import "StaffObj.h"
@interface StaffList ()

@end

@implementation StaffList

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Our Staff";
    [self loadStaffServiceCall];
    
    // Do any additional setup after loading the view.
}

-(void) loadStaffServiceCall{
    [self showHud];
    self.data = [[NSMutableArray alloc]init];
    NSString *str = [NSString stringWithFormat:@"%@%@",URL_STAFF,self.moduleObj.moduleId];
   [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:str HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [self hideHud];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:@"Error" MSG:@"No services found"];
                return;
            }
            
            for(NSDictionary *dictJson in responseArray)
            {
                NSString * strTitles=[dictJson objectForKey:@"name"];
                NSString * strDesignation=[dictJson objectForKey:@"designation"];
                NSString *imageUrl = [dictJson objectForKey:@"image"];
                NSString * strDesc=[dictJson objectForKey:@"description"];
                NSString * strProd1=[dictJson objectForKey:@"product1"];
                NSString * strProd2=[dictJson objectForKey:@"product2"];
                NSString * strProd3=[dictJson objectForKey:@"product3"];
                NSString * strProd4=[dictJson objectForKey:@"product4"];
                
                StaffObj *staffObj = [[StaffObj alloc] init];
                staffObj.staffId = [dictJson objectForKey:@"staff_id"];
                staffObj.name = strTitles;
                staffObj.designation = strDesignation;
                staffObj.imageUrl = imageUrl;
                staffObj.description = strDesc;
                staffObj.product1 = strProd1;
                staffObj.product2 = strProd2;
                staffObj.product3 = strProd3;
                staffObj.product4 = strProd4;
                staffObj.shareLink = [dictJson objectForKey:@"url"];
                staffObj.myWorks = [dictJson objectForKey:@"gallery_url"];
                staffObj.hours = [dictJson objectForKey:@"hours"];
                staffObj.product1Text = [dictJson objectForKey:@"product1_name"];
                staffObj.product2Text = [dictJson objectForKey:@"product2_name"];
                staffObj.product3Text = [dictJson objectForKey:@"product3_name"];
                staffObj.product4Text = [dictJson objectForKey:@"product4_name"];
                
                [self.data addObject:staffObj];
               
            }
             [self.staffTableView reloadData];
        }
    }];
    
}

#pragma mark tableview Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    if(self.data.count){
    StaffObj *staffObj = [self.data objectAtIndex:indexPath.row];
        UILabel *nameLabel = (UILabel*)[cell viewWithTag:10];
         UILabel *designationLabel = (UILabel*)[cell viewWithTag:20];
        UIImageView *imageView = (UIImageView*)[cell viewWithTag:30];
        
    nameLabel.text=[NSString stringWithFormat:@"%@",staffObj.name];
    designationLabel.text=[NSString stringWithFormat:@"%@",staffObj.designation];
        
        if([staffObj.imageUrl length]>0)
        {
            NSString *imageUrl = staffObj.imageUrl;
            if(![imageUrl containsString:IMAGES_URL])
                imageUrl = [NSString stringWithFormat:@"%@%@", IMAGES_URL, staffObj.imageUrl];
            
            
                imageView.bounds = CGRectInset(imageView.frame, 20.0f, 20.0f);
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
        }
        else
            [imageView setImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
        
    }
   
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
