//
//  AddGalleryImage.m
//  MyGallery
//
//  Created by Nithin Reddy on 24/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "AddGalleryImage.h"
#import "DYRateView.h"
#import "UIViewController+NRFunctions.h"
#import "GlobalSettings.h"
#import "Constants.h"
@interface AddGalleryImage ()

@end

@implementation AddGalleryImage

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Add Image";
    [self addImageClicked:Nil];
    }
-(void)share{
    [self shareClicked:nil];
}
-(IBAction)addImageClicked:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Profile Picture" message:@"Please choose an option" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setEditing:YES];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Capture from Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [self presentViewController:alertController animated:YES completion:Nil];
}

-(IBAction)shareClicked:(id)sender
{
    if(self.selectedImage==Nil)
        [self showAlert:@"Image" MSG:@"Please upload your picture"];
    else if([[self.stylistField text] length]==0)
        [self showAlert:@"Stylist" MSG:@"Please enter the name of the stylist"];
    else
    {
        NSDictionary *params = @{@"client_id":[self loadSharedPreferenceValue:@"clientid"] , @"stylist" : @"Gavin"};
        [self showHud];
        [[GlobalSettings sharedManager] uploadImageOverHttp:self.selectedImage URL:MY_GALLERY_URL HEADERS:Nil PARAMS:params COMPLETIONHANDLER:^(NSData *data, NSDictionary *responseDict, NSError *error) {
            [self hideHud];
            if(error)
                [self showAlert:@"Error" MSG:error.localizedDescription];
            else
            {
                BOOL status = [[responseDict objectForKey:@"status"] boolValue];
                if(status)
                {
                    NSString *key = [responseDict objectForKey:@"key"];
                    NSString *shareUrlStr = [NSString stringWithFormat:@"https://saloncloudsplus.com/imagesharingweb/add_review/%@", key];
                    
                    NSString *textToShare = @"Please rate my picture";
                    NSURL *shareUrl = [NSURL URLWithString:shareUrlStr];
                    
                    NSArray *objectsToShare = @[textToShare, shareUrl];
                    
                    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
                    
                    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                                   UIActivityTypePrint,
                                                   UIActivityTypeAssignToContact,
                                                   UIActivityTypeSaveToCameraRoll,
                                                   UIActivityTypeAddToReadingList,
                                                   UIActivityTypePostToFlickr,
                                                   UIActivityTypePostToVimeo];
                    
                    activityVC.excludedActivityTypes = excludeActivities;
                    
                    [self presentViewController:activityVC animated:YES completion:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"IMAGE_ADDED" object:Nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                }
                else
                {
                    [self showAlert:@"Error" MSG:[responseDict objectForKey:@"message"]];
                }
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark <UIImagePickerControllerDelegate> implementation

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    if(!image)
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    UIImage *image1 = [UIImage imageWithData:imageData];
    
    [self.imageView setImage:image1];
    self.selectedImage = image1;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
