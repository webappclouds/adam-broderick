//
//  ContactUsPageViewCon.m
//  SPA
//
//  Created by Nithin Reddy on 24/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ContactUs.h"
//#import "StringHelper.h"
#import "QuartzCore/QuartzCore.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "BottomBar.h"
#import "AppDelegate.h"
@implementation ContactUs
{
    AppDelegate *appDelegate;
}
@synthesize email, phone;
@synthesize textView;



- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

#pragma mark -
#pragma mark Lifecycle

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization.
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
}
-(void)viewDidLoad{
    [super viewDidLoad];
    
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setTitle:@"Contact Us"];
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_CONTACT_US] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary *)responseArray;
            [self getResponse:dict];
        }
    }];
    
    
    [textView.layer setCornerRadius:10.0];
    [textView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [textView.layer setBorderWidth:1.0];
    
    [UIViewController addBottomBar:self];
}

#pragma mark -
#pragma mark Compose Mail

-(IBAction)emailClicked
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil && [mailClass canSendMail])
        [self displayComposerSheet];
}

-(IBAction)callClicked
{
  
    AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSRange range;
    range.length = 3;
    range.location = 3;
    
    NSString * strPhoneNo=self.phone;
    
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@"(" withString:@""];
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@")" withString:@""];
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    
    NSString *strAreaCode = [strPhoneNo substringToIndex:3];
    NSString *strPhone1 = [strPhoneNo substringWithRange:range];
    NSString *strPhone2 = [strPhoneNo substringFromIndex:6];
    
    NSString *strProperPhone=[NSString stringWithFormat:@"Do you want to call (%@)%@-%@",strAreaCode,strPhone1,strPhone2];
    
    NSString * model=[[UIDevice currentDevice] model];
    if ([model hasPrefix:@"iPhone"]) {
        AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        NSString * strPhone=[NSString stringWithFormat:@"telprompt://%@",strProperPhone];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Call Us"
                                                                                 message:strProperPhone
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             
                                                             AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
                                                             NSString * strPhone=[NSString stringWithFormat:@"tel://%@",self.phone];
                                                             
                                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
                                                             
                                                         }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                             }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        
    }else {
        [self showAlert:@"Call Us" MSG:@"Call feature is not available on this device."];
        
    }
    
}
  
#pragma mark -

// Displays an email composition interface inside the application. Populates all the Mail fields.
-(void)displayComposerSheet
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"Contact Us"];
    
    NSArray *toRecipients = [NSArray arrayWithObject:self.email];
    NSString *body = @"";
    [picker setToRecipients:toRecipients];
    
    // Fill out the email body text
    [picker setMessageBody:body isHTML:NO];
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark SendRequest delegate
-(void)getResponse:(NSDictionary *)dictJson{
    
    NSLog(@"Response : %@",dictJson);
    if ([dictJson count]==0)
        [self showAlert:SALON_NAME MSG:@"Contact Us information is not available" tag:0];
    
    else{
        self.phone = [dictJson objectForKey:@"config_phone"];
        
        NSString *allDetails=[NSString stringWithFormat:@"%@\nPhone: %@\nEmail: %@",[dictJson objectForKey:@"config_contactus"],
                              phone,[dictJson objectForKey:@"config_emailid"]];
        self.email = [dictJson objectForKey:@"config_emailid"];
        
        [textView setEditable:YES];
        [textView setFont:[UIFont systemFontOfSize:17]];
        [textView setText:allDetails];
        [textView setEditable:NO];
    }
}


-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==2)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
