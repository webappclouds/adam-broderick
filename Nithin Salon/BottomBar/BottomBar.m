//
//  BottomBar.m
//  Riah
//
//  Created by Nithin Reddy on 12/02/13.
//
//

#import "BottomBar.h"
#import "ContactUs.h"
#import "Constants.h"
#import "DirectionsView.h"
#import "AppDelegate.h"
@interface BottomBar ()

@end

@implementation BottomBar

@synthesize shouldEmailNotWork;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)home:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)contactUs:(id)sender
{
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    if(shouldEmailNotWork==1)
        return;
   
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactUs *myNewVC = (ContactUs *)[storyboard instantiateViewControllerWithIdentifier:@"ContactUs"];
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:myNewVC animated:YES];
    
    //    ContactUs *contact = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUs"];
    //    UINavigationController *navigationController= [[UINavigationController alloc]init];
    //	[navigationController pushViewController:contact animated:NO];
}

-(IBAction)directions:(id)sender
{
    //[NRUtils loadDirections];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DirectionsView *myNewVC = (DirectionsView *)[storyboard instantiateViewControllerWithIdentifier:@"DirectionsView"];
    [self.navigationController pushViewController:myNewVC animated:YES];
    
}

-(IBAction)callUs:(id)sender
{
    AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSRange range;
    range.length = 3;
    range.location = 3;
    
    NSString * strPhoneNo=objAppdel.salonPhone;
    
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@"(" withString:@""];
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@")" withString:@""];
    strPhoneNo=[strPhoneNo stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    
    NSString *strAreaCode = [strPhoneNo substringToIndex:3];
    NSString *strPhone1 = [strPhoneNo substringWithRange:range];
    NSString *strPhone2 = [strPhoneNo substringFromIndex:6];
    
    NSString *strProperPhone=[NSString stringWithFormat:@"Do you want to call (%@)%@-%@",strAreaCode,strPhone1,strPhone2];
    
    NSString * model=[[UIDevice currentDevice] model];
    if ([model hasPrefix:@"iPhone"]) {
        AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        NSString * strPhone=[NSString stringWithFormat:@"telprompt://%@",strProperPhone];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Call Us"
                                                                                 message:strProperPhone
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             
                                                             AppDelegate * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
                                                             NSString * strPhone=[NSString stringWithFormat:@"tel://%@",objAppdel.salonPhone];
                                                             
                                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
                                                             
                                                         }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                             }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        
    }else {
        [self showAlert:@"Call Us" MSG:@"Call feature is not available on this device."];
        
    }
    
}



@end
