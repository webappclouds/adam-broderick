//
//  GiveGiftCardVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 22/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GiveGiftCardVC.h"
#import "CustomImageCellTableViewCell.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ShippingObj.h"
#import "GiftCheckOut.h"
#import "GoogleReviewVC.h"
#import <PayPalPayment.h>
#import "GiftcardGlobals.h"
#import <PayPalPaymentViewController.h>
#import "Preview.h"
#import "EventImagesList.h"
#import "ShippingInformation.h"
NSString *const kSelectorPush = @"selectorPush";
@interface GiveGiftCardVC ()<UIImagePickerControllerDelegate, MWPhotoBrowserDelegate>
{
    NSString *trid;
    AppDelegate *appDelegate;
    UIImagePickerController *imagePickerController;
    UIPopoverController *popoverController;
    UIAlertController *alertController;
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section1,*section2,*section3,*section4,*section5,*section6;
    XLFormRowDescriptor *nameRow,*emailRow,*imageRow,*previewRow,*recipientNameRow,*recipientEmailRow,*checkOutRow,*termsRow,*selectRow1,*selectRow2,*dateRow,*dateTimeRow,*messageRow,*quantitySeclectRow,*shippingRow,*giftCardImage;
    NSMutableDictionary *shippinginfoDictionary;
    
    NSMutableArray *galleryImagesArray;
}
@end

@implementation GiveGiftCardVC
@synthesize paymentType,dropDownList,finalParamDic,imageName,shippingInfoObj,imagesArray;
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        
    }
    return self;
}

-(void)initializeForm
{
    
    paymentType = [[NSString alloc] initWithString:[self loadSharedPreferenceValue:PAYMENT_KEY]];
    
    
    if([paymentType isEqualToString:PAYPAL_CONST_STR])
    {
        _payPalConfiguration = [[PayPalConfiguration alloc] init];
        [_payPalConfiguration setMerchantName:SALON_NAME];
        [_payPalConfiguration setAcceptCreditCards:YES];
        [_payPalConfiguration setRememberUser:YES];
        if(PAYMENT_STATUS==TEST_CONST)
            [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
        else
            [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
    }
    
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    if(appDelegate.giftcardType==1)
        self.title = @"e-Giftcard";
    else if(appDelegate.giftcardType==2 || appDelegate.giftcardType==3)
        self.title = @"Giftcard";
    
    
    
    appDelegate.globalButtonNameStr=@"";
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    formDescriptor.assignFirstResponderOnShow = NO;
    NSString *section1TitleStr;
    if(appDelegate.giftcardType==1){
        section1TitleStr = @"Recipient information (the e-giftcard will be emailed to the recipient's email address)" ;
    }
    else
        section1TitleStr = @"Recipient Information";
    
    //section ( rate view)
    section1 = [XLFormSectionDescriptor formSectionWithTitle:section1TitleStr];
    [formDescriptor addFormSection:section1];
    
    
    recipientNameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"name" rowType:XLFormRowDescriptorTypeText title:@"Recipient Name"];
    [recipientNameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    recipientNameRow.height =44;
    recipientNameRow.required =YES;
    [recipientNameRow.cellConfigAtConfigure setObject:@"Enter name" forKey:@"textField.placeholder"];
    [section1 addFormRow:recipientNameRow];
    
    recipientEmailRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"email" rowType:XLFormRowDescriptorTypeEmail title:@"Recipient Email"];
    // validate the email
    [recipientEmailRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    recipientEmailRow.required = YES;
    //[recipientEmailRow addValidator:[XLFormValidator emailValidator]];
    recipientEmailRow.height=44;
    [recipientEmailRow.cellConfigAtConfigure setObject:@"Enter email" forKey:@"textField.placeholder"];
    [section1 addFormRow:recipientEmailRow];
    
    section2 = [XLFormSectionDescriptor formSectionWithTitle:@"E-GIFTCARD INFORMATION"];
    [formDescriptor addFormSection:section2];
    
    selectRow1 = [XLFormRowDescriptor formRowDescriptorWithTag:@"Psuh" rowType:XLFormRowDescriptorTypeSelectorPush title:@"e-Giftcard Value (USD)"];
    
    if(appDelegate.giftcardType!=1)
    {
        [section2 setTitle:@"Giftcard Information"];
        [selectRow1 setTitle:@"e-Giftcard Value (USD)"];
    }
    NSMutableArray *valArray  = [NSMutableArray new];
    for(int i=0;i<dropDownList.count;i++){
        [valArray addObject:[XLFormOptionsObject formOptionsObjectWithValue:@(i) displayText:[dropDownList objectAtIndex:i]]];
    }
    
    
    selectRow1.selectorOptions = valArray;
    selectRow1.value = [valArray objectAtIndex:0];
    [section2 addFormRow:selectRow1];
    
    
    
    messageRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"textView" rowType:XLFormRowDescriptorTypeTextView title:@""];
    messageRow.height=100;
    [messageRow.cellConfigAtConfigure setObject:@"Enter message" forKey:@"textView.placeholder"];
    [section2 addFormRow:messageRow];
    
    dateTimeRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"date" rowType:XLFormRowDescriptorTypeDate title:@"Delivery Date"];
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd-mm-yyyy HH:mm"];
    dateTimeRow.value = [NSDate new];
    dateTimeRow.height =44;
    dateTimeRow.valueFormatter=df;
    [dateTimeRow.cellConfigAtConfigure setObject:[NSDate new] forKey:@"minimumDate"];
    
    if(appDelegate.giftcardType==1)
        [section2 addFormRow:dateTimeRow];
    
    
    
    if(appDelegate.giftcardType==1){
        section3 = [XLFormSectionDescriptor formSectionWithTitle:@"E-GIFTCARD IMAGE"];
        appDelegate.imageLinkURL =@"";
        imageRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"image" rowType:XLFormRowDescriptorTypeButton title:@"Select e-Giftcard Image"];
        appDelegate.globalButtonNameStr =@"Select e-Giftcard Image";
        imageRow.action.formSelector = @selector(selctGiftCard);
        [imageRow.cellConfig setObject:[UIFont boldSystemFontOfSize:16] forKey:@"textLabel.font"];
        [section3 addFormRow:imageRow];
        [formDescriptor addFormSection:section3];
    }
    else{
        
        section3 = [XLFormSectionDescriptor formSectionWithTitle:@"Quantity"];
        [formDescriptor addFormSection:section3];
        
        NSMutableArray *quantityArray = [[NSMutableArray alloc] init];
        for(int i=1; i<=30; i++)
            [quantityArray addObject:[NSString stringWithFormat:@"%d", i]];
        
        quantitySeclectRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Quanity" rowType:XLFormRowDescriptorTypeSelectorPush title:@"Select Quantity"];

        quantitySeclectRow.selectorOptions = quantityArray;
        quantitySeclectRow.value = [quantityArray objectAtIndex:0];
        [section3 addFormRow:quantitySeclectRow];
        
        if(appDelegate.giftcardType==3){
            shippingRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Shipping Information"];
            [shippingRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
            [shippingRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
            shippingRow.action.formSelector = @selector(shippingAllData);
            [section3 addFormRow:shippingRow];
        }
    }
    
    
    section4 = [XLFormSectionDescriptor formSectionWithTitle:@"YOUR INFORMATION"];
    [formDescriptor addFormSection:section4];
    nameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"name" rowType:XLFormRowDescriptorTypeText title:@"Name"];
    [nameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    nameRow.height =44;
    nameRow.required =YES;
    [nameRow.cellConfigAtConfigure setObject:@"Enter name" forKey:@"textField.placeholder"];
    [section4 addFormRow:nameRow];
    
    emailRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"email" rowType:XLFormRowDescriptorTypeEmail title:@"Email"];
    // validate the email
    [emailRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    emailRow.required = YES;
    [emailRow addValidator:[XLFormValidator emailValidator]];
    emailRow.height=44;
    [emailRow.cellConfigAtConfigure setObject:@"Enter email" forKey:@"textField.placeholder"];
    [section4 addFormRow:emailRow];
    
    if(appDelegate.giftcardType==1){
        
        section5 = [XLFormSectionDescriptor formSectionWithTitle:@""];
        [formDescriptor addFormSection:section5];
        
        checkOutRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Checkout"];
        [checkOutRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
        [checkOutRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
        checkOutRow.action.formSelector = @selector(checkOutAllData);
        [section5 addFormRow:checkOutRow];
        
        
        section6 = [XLFormSectionDescriptor formSectionWithTitle:@""];
        [formDescriptor addFormSection:section6];
        
        termsRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Terms and Conditions"];
        [termsRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
        [termsRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
        termsRow.action.formSelector = @selector(termsAndCOnditionsAction);
        [section6 addFormRow:termsRow];
    }
    else{
        section5 = [XLFormSectionDescriptor formSectionWithTitle:@""];
        [formDescriptor addFormSection:section5];
        
        checkOutRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Next"];
        [checkOutRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
        [checkOutRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
        checkOutRow.action.formSelector = @selector(checkOutAllData);
        [section5 addFormRow:checkOutRow];
    }
    
    
    
    self.form = formDescriptor;
    
    
    
}

-(void)formRowDescriptorValueHasChanged:(XLFormRowDescriptor *)formRow oldValue:(id)oldValue newValue:(id)newValue{
   
    
    if([formRow.title isEqualToString:@"e-Giftcard Value (USD)"]){
       if(![newValue isKindOfClass:[NSNull class]])
        [self reloadFormRow:formRow];
        else
        {
            formRow.value = oldValue;
            [self reloadFormRow:formRow];
            
            
        }
        
         }
   
    if([formRow.tag isEqualToString:@"Quanity"]){
        if(![newValue isKindOfClass:[NSNull class]])
        [self reloadFormRow:formRow];
        else
        {
            formRow.value = oldValue;
            [self reloadFormRow:formRow];
            
        
        }
        
    }
   
    
    
}

-(void)termsAndCOnditionsAction{
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    GoogleReviewVC *googlePlus = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [googlePlus setUrl:URL_GIFTCARDS_TNC];
    [googlePlus setTitle:@"Terms"];
    [self.navigationController pushViewController:googlePlus animated:YES];
    
}
-(void)checkOutScreen{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    GiftCheckOut *googlePlus = [self.storyboard instantiateViewControllerWithIdentifier:@"GiftCheckOut"];
    [self.navigationController pushViewController:googlePlus animated:YES];
}

-(void)shippingAllData{
    [self invokeShippingInfo];
}
-(void) invokeShippingInfo
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    ShippingInformation *sInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"ShippingInformation"];
    sInfo.delegate = self;
    [sInfo setShippingObj:shippingInfoObj];
    [self.navigationController pushViewController:sInfo animated:YES];
    
   }

-(void)checkOutAllData{
    NSString *rNameStr = [recipientNameRow displayTextValue]?:@"";
    NSString *rEmailStr = [recipientEmailRow displayTextValue]?:@"";
    NSString *msgStr =[messageRow displayTextValue]?:@"";
    NSString *sNameStr =[nameRow displayTextValue]?:@"";
    NSString *sEmailStr = [emailRow displayTextValue]?:@"";
    
    if([self.dropDownList count]==0)
        [self showAlert:@"Error" MSG:@"Error occured. Please try again." tag:0];
    else if([rNameStr length]<2)
        [self showAlert:@"Recipient name" MSG:@"Please enter a recipient name" tag:0];
    else if([rEmailStr length]==0)
        [self showAlert:@"Recipient email" MSG:@"Please enter  recipient email address" tag:0];
    else if(![UIViewController isValidEmail:rEmailStr])
        [self showAlert:@"Recipient email" MSG:@"Please enter a valid recipient email address" tag:0];
    else if([msgStr length]<2)
        [self showAlert:@"Message" MSG:@"Please enter a message" tag:0];
    else if([sNameStr length]<2)
        [self showAlert:@"Name" MSG:@"Please enter your name" tag:0];
    else if([sEmailStr length]==0)
        [self showAlert:@"Email" MSG:@"Please enter your  email address" tag:0];
    else if(![UIViewController isValidEmail:sEmailStr])
         [self showAlert:@"Email" MSG:@"Please enter a valid email address" tag:0];

    else if(appDelegate.giftcardType==3 && shippinginfoDictionary.count==0)
        [self showAlert:@"Shipping" MSG:@"Please enter the shipping information" tag:0];
    else
    {
        //[self setName:sNameStr];
         //[self setEmail:sEmailStr];
        
        
        NSString *voucherStr = [selectRow1 displayTextValue];
        voucherStr = [voucherStr stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        
        float totalVal = [voucherStr floatValue];
        //
        NSMutableArray *quantityArray = [[NSMutableArray alloc] init];
        for(int i=1; i<=30; i++)
            [quantityArray addObject:[NSString stringWithFormat:@"%d", i]];
        
        
        int quantityInInt = 1;
        if(appDelegate.giftcardType>1)
        {

//            quantityInInt = (int)[quantityArray objectAtIndex:[[quantitySeclectRow displayTextValue] intValue]];
            quantityInInt = [[quantitySeclectRow displayTextValue] intValue];


            totalVal = totalVal*quantityInInt;
        }

        float discountedVal = [GiftcardGlobals returnDiscountedPrice:totalVal];
        

        if(appDelegate.giftcardType==3)
            discountedVal+=[GiftcardGlobals getShippingCost];
        
        
        
        
        
        [self populateParamDic:sNameStr SEMAIL:sEmailStr RNAME:rNameStr REMAIL:rEmailStr MSG:msgStr AMNT:[NSString stringWithFormat:@"%0.2f", totalVal] DISAMNT:[NSString stringWithFormat:@"%0.2f", discountedVal] QUANTITY:[NSString stringWithFormat:@"%d", quantityInInt] DELIVDATE:nil];
       
        
               if([paymentType isEqualToString:PAYPAL_CONST_STR])
        {
            [self showAlert:@"Note" MSG:@"Please click the Done button on the top right after you have processed your payment, for the transaction to be complete." tag:10];
            [self processPaypalPaymentTransaction];
        }
        else{
            
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            GiftCheckOut *giveGIftCard = [self.storyboard instantiateViewControllerWithIdentifier:@"GiftCheckOut"];
            giveGIftCard.amount = [NSString stringWithFormat:@"%0.2f", totalVal];
            giveGIftCard.discountedAmount = [NSString stringWithFormat:@"%0.2f", discountedVal];
                        giveGIftCard.finalParamDic =finalParamDic;
            [self.navigationController pushViewController:giveGIftCard animated:YES];
            
        }
        
    }
}



-(void) processPaypalPaymentTransaction
{
    [appDelegate showHUD];
    float discountedVal = [GiftcardGlobals returnDiscountedPrice:[[selectRow1 displayTextValue] floatValue]];
    NSString *discountedAmount = [NSString stringWithFormat:@"%0.2f",discountedVal];
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = [[NSDecimalNumber alloc] initWithString:discountedAmount];
    payment.currencyCode = @"USD";
    NSString *shortDescription = [NSString stringWithFormat:@"Giftcard Purchase from %@", SALON_NAME];
    payment.shortDescription = shortDescription;
    payment.intent = PayPalPaymentIntentSale;
    
    if(!payment.processable)
        [self showAlert:@"Error" MSG:@"Error occured. Please try again later"];
    else
    {
        [appDelegate hideHUD];
        PayPalPaymentViewController *paymentViewController;
        paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment configuration:self.payPalConfiguration delegate:self];
        
        // Present the PayPalPaymentViewController.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController presentViewController:paymentViewController animated:YES completion:nil];
        });
        
        
    }
}



-(void) populateParamDic : (NSString *) sName SEMAIL:(NSString *) sEmail RNAME:(NSString *)rName1 REMAIL:(NSString *)rEmail1 MSG:(NSString *)message AMNT:(NSString *)amnt DISAMNT:(NSString *)disAmount QUANTITY:(NSString *)quantity DELIVDATE:(NSString *)date1
{
    finalParamDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    if(appDelegate.giftcardType==1)
        [finalParamDic setObject:@"email" forKey:@"delivery_method"];
    else if(appDelegate.giftcardType==2)
        [finalParamDic setObject:@"pickup" forKey:@"delivery_method"];
    else
        [finalParamDic setObject:@"mail" forKey:@"delivery_method"];
    
    [finalParamDic setObject:sName forKey:@"customer_name"];
    [finalParamDic setObject:sEmail forKey:@"customer_email"];
    [finalParamDic setObject:@"" forKey:@"customer_phone"];
    [finalParamDic setObject:@"" forKey:@"customer_address"];
    [finalParamDic setObject:rName1 forKey:@"receiver_name"];
    [finalParamDic setObject:rEmail1 forKey:@"receiver_email"];
    [finalParamDic setObject:@"" forKey:@"receiver_phone"];
    [finalParamDic setObject:@"" forKey:@"receiver_address"];
    [finalParamDic setObject:message forKey:@"message"];
    [finalParamDic setObject:@"amount" forKey:@"giftcard_type"];
    [finalParamDic setObject:@"" forKey:@"service_name"];
    [finalParamDic setObject:amnt forKey:@"voucher_value"];
    [finalParamDic setObject:disAmount forKey:@"paid_amount"];
    [finalParamDic setObject:@"0" forKey:@"redeemed"];
    [finalParamDic setObject:@"iPhone" forKey:@"source"];
    
    if(appDelegate.giftcardType==1){
        
        NSDateFormatter *inFormat = [[NSDateFormatter alloc] init];
        [inFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *strSelectedDate = [inFormat stringFromDate:[dateTimeRow value]];
        
        [finalParamDic setObject:strSelectedDate?:@"" forKey:@"date_delivery"];
    }
    
    //For Image//[imageRow value]
//    NSData *userImageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(appDelegate.giftCardImage, 0.1)];
    
    NSData *userImageData = UIImagePNGRepresentation(appDelegate.giftCardImage);

    //    if([imageRow displayTextValue]!=nil && [[imageRow displayTextValue] length]>0)
    //        [finalParamDic setObject:imageName forKey:@"image"];
    if(userImageData != nil)
    {
        NSString *str = [[userImageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        [finalParamDic setObject:str forKey:@"user_image"];
    }
    
    //For Pickup and mail
    if(appDelegate.giftcardType>1)
        [finalParamDic setObject:quantity forKey:@"product_quantity"];
    
    //For mail
    if(appDelegate.giftcardType==3)
    {
        [finalParamDic setObject:shippingInfoObj.shippingFName forKey:@"shipping_first_name"];
        [finalParamDic setObject:shippingInfoObj.shippingLName forKey:@"shipping_last_name"];
        [finalParamDic setObject:shippingInfoObj.shippingAddr forKey:@"shipping_address"];
        [finalParamDic setObject:shippingInfoObj.shippingCity forKey:@"shipping_city"];
        [finalParamDic setObject:shippingInfoObj.shippingState forKey:@"shipping_state"];
        [finalParamDic setObject:shippingInfoObj.shippingZip forKey:@"shipping_zipcode"];
        [finalParamDic setObject:shippingInfoObj.shippingPhone forKey:@"shipping_phone"];
    }
}


#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    
    NSDictionary *proofOfPayment = completedPayment.confirmation;
    
    NSString *payment_id = [[proofOfPayment objectForKey:@"response"] objectForKey:@"id"];
    
        
    trid = payment_id ;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    [self sendToServer];
    
}

-(void) sendToServer
{
    [self.finalParamDic setObject:trid forKey:@"transaction_number"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_ADD_GIFTCARD] HEADERS:nil GETPARAMS:nil POSTPARAMS:self.finalParamDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        
        NSLog(@"Response : %@",responseArray);
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *resp =(NSDictionary*)responseArray;
            BOOL status = [[resp objectForKey:@"status"] boolValue];
            
            if(status)
            {
                NSString *code = [resp objectForKey:@"code"];
                NSString *msg = [NSString stringWithFormat:@"Your transaction was successful. Your gift certificate number is %@. You will receive an email receipt and the e-gift certificate has been sent to the recipient. Please note your transaction id %@ for future reference.", code, trid];
                [self showAlert:@"Thank you!" MSG:msg tag:1];
            }
            else
            {
                NSString *msg = [NSString stringWithFormat:@"Payment successful, but the server isn't available. Please save the transaction id %@, and contact support.", trid];
                [self showAlert:@"Error" MSG:msg tag:0];
            }
            
        }
    }];
}




- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        if(tag==10){
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            [self processPaypalPaymentTransaction];
        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    return;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    galleryImagesArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    shippinginfoDictionary = [[NSMutableDictionary alloc] initWithCapacity:0];
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    if(appDelegate.giftcardType==1)
        self.title = @"e-Giftcard";
    else if(appDelegate.giftcardType==2 || appDelegate.giftcardType==3)
        self.title = @"Giftcard";
    
    [self initializeForm];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)selctGiftCard
{
    alertController = [UIAlertController alertControllerWithTitle:@"Select e-Giftcard Image"
                                                          message: nil
                                                   preferredStyle: UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Choose From Library", nil)
                                                        style: UIAlertActionStyleDefault
                                                      handler: ^(UIAlertAction * _Nonnull action) {
                                                          [self openImage:UIImagePickerControllerSourceTypePhotoLibrary];
                                                      }]];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Take Photo", nil)
                                                            style: UIAlertActionStyleDefault
                                                          handler: ^(UIAlertAction * _Nonnull action) {
                                                              [self openImage:UIImagePickerControllerSourceTypeCamera];
                                                          }]];
    }
    
    
    
    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Select from our Gallery", nil)
                                                        style: UIAlertActionStyleDefault
                                                      handler: ^(UIAlertAction * _Nonnull action) {
         self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//        EventImagesList *eml = [self.storyboard instantiateViewControllerWithIdentifier:@"EventImagesList"];
//        [self.navigationController pushViewController:eml animated:YES];
                                           
                                                          EventImagesList *eml = [self.storyboard instantiateViewControllerWithIdentifier:@"EventImagesList"];
                                                          [eml setData:imagesArray];
                                                                  [self.navigationController pushViewController:eml animated:YES];


//                                                          [self invokeGalleryModule:appDelegate.galleryModule];
                                                          
                                                      }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel", nil)
                                                        style: UIAlertActionStyleCancel
                                                      handler: nil]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        alertController.modalPresentationStyle = UIModalPresentationPopover;
       
        alertController.popoverPresentationController.sourceRect = self.view.bounds;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated: true completion: nil];
    });
}

-(void) invokeGalleryModule : (ModuleObj *) moduleObj
{
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:[NSString stringWithFormat:@"%@%@/", [appDelegate addSalonIdTo:URL_GALLERY], moduleObj.moduleId] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                [self showAlert:@"Gallery" MSG:@"There are no images in the Gallery. Please try again later."];
                return;
            }
            
            EventImagesList *eml = [self.storyboard instantiateViewControllerWithIdentifier:@"EventImagesList"];
            [eml setData:[responseArray copy]];
            
            [self.navigationController pushViewController:eml animated:YES];

//            for(NSDictionary *dict in responseArray)
//            {
//                MWPhoto *photoObj = [MWPhoto photoWithURL:[NSURL URLWithString:[dict objectForKey:@"gallery_image"]]];
//                photoObj.caption = [dict objectForKey:@"gallery_title"];
//                [galleryImagesArray addObject:photoObj];
//            }
//
//            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//            [browser setDelegate:self];
//            browser.displayNavArrows = YES;
//            browser.startOnGrid = YES;
//            browser.displayActionButton = NO;
//            [self.navigationController pushViewController:browser animated:YES];
        }
    }];
}

- (NSUInteger) numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return galleryImagesArray.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return galleryImagesArray[index];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index
{
    return [galleryImagesArray objectAtIndex:index];
}


- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index
{
    
}
- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index
{
    
}
- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index
{
    return YES;
}
- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected{
    
}
- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser
{
    
}



- (void)openImage:(UIImagePickerControllerSourceType)source
{
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = source;
    
    
        [self presentViewController:imagePickerController
                                              animated: YES
                                            completion: nil];
   }

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navigateToPreviewScreen) name:@"GiftCard" object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receive:) name:@"Event_images" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveGiftGallery:) name:@"GiftCardGallery" object:nil];

}

-(void)receiveGiftGallery: (NSNotification *) notification
{
//    imageName = [[notification.userInfo objectForKey:@"image"] objectForKey:@"gallery_image"];
    appDelegate.giftCardImage = [notification.userInfo objectForKey:@"image"];
    
    if(giftCardImage)
        [self.form removeFormRow:giftCardImage];
    giftCardImage = [XLFormRowDescriptor formRowDescriptorWithTag:@"GiftImage" rowType:XLFormRowDescriptorTypeGiftCardFullImage title:@""];
    
    giftCardImage.height =100;
    //imageRow.action.formSelector = @selector(navigateToPreview);
    [giftCardImage.cellConfig setObject:[UIFont boldSystemFontOfSize:16] forKey:@"textLabel.font"];
    [section3 addFormRow:giftCardImage];
    [self reloadFormRow:giftCardImage];
    [self reloadFormRow:imageRow];
}

-(IBAction)receive: (NSNotification *) notification
{
    imageName = [notification.userInfo objectForKey:@"image"];
    appDelegate.giftCardImage = imageName;
    
    if(giftCardImage)
        [self.form removeFormRow:giftCardImage];
    giftCardImage = [XLFormRowDescriptor formRowDescriptorWithTag:@"GiftImage" rowType:XLFormRowDescriptorTypeGiftCardFullImage title:@""];
    
    giftCardImage.height =100;
    //imageRow.action.formSelector = @selector(navigateToPreview);
    [giftCardImage.cellConfig setObject:[UIFont boldSystemFontOfSize:16] forKey:@"textLabel.font"];
    [section3 addFormRow:giftCardImage];
    [self reloadFormRow:giftCardImage];
    [self reloadFormRow:imageRow];
}

#pragma mark -  UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    
    if (editedImage) {
        appDelegate.giftCardImage = editedImage;
           } else {
               appDelegate.giftCardImage = originalImage;

    }
    
    if(giftCardImage)
    [self.form removeFormRow:giftCardImage];
    giftCardImage = [XLFormRowDescriptor formRowDescriptorWithTag:@"GiftImage" rowType:XLFormRowDescriptorTypeGiftCardFullImage title:@""];

    giftCardImage.height =100;
    //imageRow.action.formSelector = @selector(navigateToPreview);
    [giftCardImage.cellConfig setObject:[UIFont boldSystemFontOfSize:16] forKey:@"textLabel.font"];
    [section3 addFormRow:giftCardImage];
    [self reloadFormRow:giftCardImage];
    [self reloadFormRow:imageRow];

    
        [self dismissViewControllerAnimated: YES completion: nil];
    
}
-(void)getShippingInfo:(ShippingObj *)shippingInfoObj1{
    
        shippingInfoObj = [[ShippingObj alloc] init];
        self.shippingInfoObj.shippingFName = shippingInfoObj1.shippingFName;
        self.shippingInfoObj.shippingLName = shippingInfoObj1.shippingLName;
        self.shippingInfoObj.shippingAddr = shippingInfoObj1.shippingAddr;
        self.shippingInfoObj.shippingCity = shippingInfoObj1.shippingCity;
        self.shippingInfoObj.shippingState = shippingInfoObj1.shippingState;
        self.shippingInfoObj.shippingZip = shippingInfoObj1.shippingZip;
        self.shippingInfoObj.shippingPhone = shippingInfoObj1.shippingPhone;
    
    [shippinginfoDictionary setObject:shippingInfoObj forKey:@"shippingInfo"];
    }


-(void)navigateToPreviewScreen
{
   self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    Preview *pv = [self.storyboard instantiateViewControllerWithIdentifier:@"Preview"];
    [self.navigationController pushViewController:pv animated:YES];
}



#pragma mark - actions

/*-(void)validateForm:(UIBarButtonItem *)buttonItem
{
    NSArray * array = [self formValidationErrors];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XLFormValidationStatus * validationStatus = [[obj userInfo] objectForKey:XLValidationStatusErrorKey];
        if ([validationStatus.rowDescriptor.tag isEqualToString:kValidationName]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            cell.backgroundColor = [UIColor orangeColor];
            [UIView animateWithDuration:0.3 animations:^{
                cell.backgroundColor = [UIColor whiteColor];
            }];
        }
        else if ([validationStatus.rowDescriptor.tag isEqualToString:kValidationEmail]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }
        else if ([validationStatus.rowDescriptor.tag isEqualToString:kValidationPassword]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }
        else if ([validationStatus.rowDescriptor.tag isEqualToString:kValidationInteger]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }
    }];
}
*/

#pragma mark - Helper

-(void)animateCell:(UITableViewCell *)cell
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values =  @[ @0, @20, @-20, @10, @0];
    animation.keyTimes = @[@0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1];
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.additive = YES;
    
    [cell.layer addAnimation:animation forKey:@"shake"];
}


@end
