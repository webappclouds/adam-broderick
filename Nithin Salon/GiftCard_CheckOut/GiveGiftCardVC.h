//
//  GiveGiftCardVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 22/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormViewController.h"
#import "XLForm.h"
#import "ShippingObj.h"
#import <PayPalMobile.h>
#import "GiftCheckOut.h"

@interface GiveGiftCardVC : XLFormViewController<PayPalPaymentDelegate>
{
    GiftCheckOut *give;
}
@property (nonatomic, retain) NSMutableArray *dropDownList;
@property (nonatomic, retain) ShippingObj *shippingInfoObj;
@property (nonatomic, retain) NSString *paymentType;
@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfiguration;
@property (nonatomic, retain) NSMutableDictionary *finalParamDic;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) NSMutableArray *imagesArray;
-(void)getShippingInfo:(ShippingObj *)shippingInfoObj1;

@end
