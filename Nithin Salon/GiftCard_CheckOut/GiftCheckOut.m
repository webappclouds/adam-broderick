//
//  GiftCheckOut.m
//  Nithin Salon
//
//  Created by Webappclouds on 24/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "GiftCheckOut.h"
#import "CardIO.h"
#import "AppDelegate.h"
#import "GoogleReviewVC.h"
#import "Constants.h"
#import <Bluefin/Bluefin.h>
#import "GiftcardGlobals.h"
#import <SHSPhoneComponent/SHSPhoneNumberFormatter+UserConfig.h>
@interface GiftCheckOut ()<CardIOPaymentViewControllerDelegate>
{
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section1,*section2,*section3,*section4,*section5;
    XLFormRowDescriptor *valueRow,*amountRow,*billingNameRow, *billingAddressRow,*StateRow,*zipRow,*cardNumberRow,*cardExpiryRow,*cvvRow,*checkoutRow,*termsRow,*scanRow;
    
    NSString *cardNumStr, *cardCodeStr, *cardExpiryStr;
    NSString *billingNameStr, *billingAddrStr, *billingStateStr, *billingZipStr;
    
    int statusCode;
    AppDelegate *appDelegate;
}
@end

@implementation GiftCheckOut
@synthesize paymentType,trid;
@synthesize amount;
@synthesize discountedAmount;
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
       
    }
    return self;
}

-(void)initializeForm
{
    
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    formDescriptor.assignFirstResponderOnShow = NO;
    
    // section1---> value row, amount Row
    section1 = [XLFormSectionDescriptor formSectionWithTitle:@"GIFTCARD VALUE"];
    [formDescriptor addFormSection:section1];
    valueRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Value" rowType:XLFormRowDescriptorTypeInfo title:@"Giftcard Value"];
    valueRow.height =44;
    valueRow.required =YES;
    valueRow.value = [NSString stringWithFormat:@"$%@",amount];
    [section1 addFormRow:valueRow];
    
    amountRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Amount" rowType:XLFormRowDescriptorTypeInfo title:@"Giftcard Amount"];
    amountRow.value = [NSString stringWithFormat:@"$%@",discountedAmount];
    
    amountRow.height =44;
    amountRow.required =YES;
    [section1 addFormRow:amountRow];
    
    
    
    //section2 -----> billingNameRow,billing Address Row, state ROw, ZIp Row
    section2 = [XLFormSectionDescriptor formSectionWithTitle:@"Billing Information"];
    [formDescriptor addFormSection:section2];
    billingNameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"billName" rowType:XLFormRowDescriptorTypeText title:@"Billing Name"];
    [billingNameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    billingNameRow.height =44;
    billingNameRow.required =YES;
    [billingNameRow.cellConfigAtConfigure setObject:@"Name" forKey:@"textField.placeholder"];
    [section2 addFormRow:billingNameRow];
    
    
    billingAddressRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Address" rowType:XLFormRowDescriptorTypeText title:@"Billing Address"];
    [billingAddressRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    billingAddressRow.height =44;
    billingAddressRow.required =YES;
    [billingAddressRow.cellConfigAtConfigure setObject:@"State,City" forKey:@"textField.placeholder"];
    [section2 addFormRow:billingAddressRow];
    
    StateRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Address" rowType:XLFormRowDescriptorTypeText title:@"State"];
    [StateRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    StateRow.height =44;
    StateRow.required =YES;
    [StateRow.cellConfigAtConfigure setObject:@"State" forKey:@"textField.placeholder"];
    [section2 addFormRow:StateRow];
    
    zipRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Address" rowType:XLFormRowDescriptorTypeNumber title:@"ZIP Code"];
    [zipRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    zipRow.height =44;
    zipRow.required =YES;
    [zipRow.cellConfigAtConfigure setObject:@"Zip" forKey:@"textField.placeholder"];
    [section2 addFormRow:zipRow];
    
    
    //Section 3-----> scan Row, card number row, expiry row, cvv row
    section3 = [XLFormSectionDescriptor formSectionWithTitle:@"CARD INFORMATION"];
    [formDescriptor addFormSection:section3];
    scanRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Click to Scan Card"];
    [scanRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [scanRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    scanRow.action.formSelector = @selector(scanAction);
    [section3 addFormRow:scanRow];
    
    cardNumberRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"cardNumber" rowType:XLFormRowDescriptorTypeInteger title:@"Card Number"];
    [cardNumberRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    cardNumberRow.height =44;
    cardNumberRow.required =YES;
    [section3 addFormRow:cardNumberRow];
    
    
    SHSPhoneNumberFormatter *formatter = [[SHSPhoneNumberFormatter alloc] init];
    [formatter setDefaultOutputPattern:@"##/####" imagePath:nil];
    cardExpiryRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"cardExpiry" rowType:XLFormRowDescriptorTypeInteger title:@"Card Expiry"];
    [cardExpiryRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    cardExpiryRow.height =44;
    cardExpiryRow.required =YES;
    cardExpiryRow.valueFormatter = formatter;
    cardExpiryRow.useValueFormatterDuringInput =YES;
    [cardExpiryRow.cellConfigAtConfigure setObject:@"MM/YYYY" forKey:@"textField.placeholder"];
    [section3 addFormRow:cardExpiryRow];
    
    cvvRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"CVV" rowType:XLFormRowDescriptorTypeInteger title:@"CVV Code"];
    [cvvRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    cvvRow.height =44;
    cvvRow.required =YES;
    [cvvRow.cellConfigAtConfigure setObject:@"CVV" forKey:@"textField.placeholder"];
    [section3 addFormRow:cvvRow];
    
    
    
    // Section 4 -----> checkOut Row
    section4 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section4];
    checkoutRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"CheckOut" rowType:XLFormRowDescriptorTypeButton title:@"Next"];
    [checkoutRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [checkoutRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    checkoutRow.action.formSelector = @selector(checkOutAction);
    [section4 addFormRow:checkoutRow];
    
    
    // Section 5 -----> Terms and Conditions Row
    section5 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section5];
    
    termsRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"CheckOut" rowType:XLFormRowDescriptorTypeButton title:@"Terms and Conditions"];
    [termsRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [termsRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    termsRow.action.formSelector = @selector(termsAndCOnditionsAction);
    [section5 addFormRow:termsRow];
    self.form = formDescriptor;
    
    
}

-(void)checkOutAction{
    billingNameStr =[billingNameRow displayTextValue]?:@"";
    billingAddrStr = [billingAddressRow displayTextValue]?:@"";
    billingStateStr =[StateRow displayTextValue]?:@"";
    billingZipStr = [zipRow displayTextValue]?:@"";
    
    cardNumStr = [cardNumberRow displayTextValue]?:@"";
    cardExpiryStr = [cardExpiryRow displayTextValue]?:@"";
    cardCodeStr = [cvvRow displayTextValue]?:@"";
    
    NSString *amountValue = [amountRow displayTextValue]?:@"";
    amountValue = [amountValue stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    if([billingNameStr length]<2)
        [self showAlert:@"Error" MSG:@"Please enter the billing name" tag:0];
    else if([billingAddrStr length]<2)
        [self showAlert:@"Error" MSG:@"Please enter the billing address" tag:0];
    else if([billingStateStr length]<2)
        [self showAlert:@"Error" MSG:@"Please enter the billing state" tag:0];
    else if([billingZipStr length]<5)
        [self showAlert:@"Error" MSG:@"Please enter the billing ZIP code" tag:0];
    else if([cardNumStr length]==0 || ![self cardCheck:cardNumStr])
        [self showAlert:@"Error" MSG:@"Please enter a valid credit card number" tag:0];
    else if([cardExpiryStr length]!=7)
        [self showAlert:@"Error" MSG:@"Your card's expiry date should be in MM-YYYY format" tag:0];
    else if([cardCodeStr length]==0)
        [self showAlert:@"Error" MSG:@"Please enter your card's CVV code." tag:0];
    else
    {
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        NSString *encryptedCard = [self encryptCard:cardNumStr];
        
        [paramDic setObject:encryptedCard forKey:@"creditcard"];
        [paramDic setObject:cardExpiryStr forKey:@"expiration"];
        [paramDic setObject:appDelegate.salonId forKey:@"salon_id"];
        [paramDic setObject:cardCodeStr forKey:@"cvv"];
        [paramDic setObject:@"0" forKey:@"tax"];
        [paramDic setObject:amountValue forKey:@"total"];
        [paramDic setObject:[NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970] ]forKey:@"invoice"];
        
        paymentType = [[NSString alloc] initWithString:[self loadSharedPreferenceValue:PAYMENT_KEY]];
       // paymentType =STRIPE_CONST_STR;
        if([paymentType isEqualToString:AUTH_NET_CONST_STR])
        {
            
            [paramDic setObject:billingNameStr forKey:@"business_firstname"];
            [paramDic setObject:billingNameStr forKey:@"business_lastname"];
            [paramDic setObject:billingAddrStr forKey:@"business_address"];
            [paramDic setObject:billingAddrStr forKey:@"business_city"];
            [paramDic setObject:billingStateStr forKey:@"business_state"];
            [paramDic setObject:billingZipStr forKey:@"business_zipcode"];
            [paramDic setObject:@"00000000000" forKey:@"business_telephone"];
            
            
            [paramDic setObject:billingNameStr forKey:@"shipping_firstname"];
            [paramDic setObject:billingNameStr forKey:@"shipping_lastname"];
            [paramDic setObject:billingAddrStr forKey:@"shipping_address"];
            [paramDic setObject:billingAddrStr forKey:@"shipping_city"];
            [paramDic setObject:billingStateStr forKey:@"shipping_state"];
            [paramDic setObject:billingZipStr forKey:@"shipping_zipcode"];
            
            [paramDic setObject:[self.finalParamDic objectForKey:@"customer_name"] forKey:@"user_id"];
            [paramDic setObject:[self.finalParamDic objectForKey:@"customer_email"] forKey:@"email"];
            [paramDic setObject:@"Giftcard" forKey:@"product"];
            [appDelegate showHUD];
            [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)URL_AUTH_NET HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                [appDelegate hideHUD];
                if(error)
                    [self showAlert:@"Error" MSG:error.localizedDescription];
                else
                {
                    NSDictionary *resp =(NSDictionary*)responseArray;
                    [self serviceResponse:resp];
                }
            }];
        }
        
        else if([paymentType isEqualToString:CPAY_CONST_STR])
        {
            [appDelegate showHUD];
            [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)URL_AUTH_NET HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                [appDelegate hideHUD];
                if(error)
                    [self showAlert:@"Error" MSG:error.localizedDescription];
                else
                {
                    NSDictionary *resp =(NSDictionary*)responseArray;
                    [self serviceResponse:resp];
                }
            }];
        }
        
        else if([paymentType isEqualToString:BLUEFIN_CONST_STR])
        {
            [appDelegate showHUD];
            
            Bluefin *bluefin = [Bluefin bluefinInstanceWithAccount:[GiftcardGlobals getBluefinAccountNumber] APIAccessKey:[GiftcardGlobals getBluefinAccessKey]];
            
            //            if(PAYMENT_STATUS==TEST_CONST)
            //                [bluefin setUseCertEnvironment:YES];
            
            [bluefin reserveTransactionToken:^(BluefinTransactionInfo *transactionInfo, NSError *error) {
                [appDelegate showHUD];
                if (error)
                    [self showAlert:@"Error" MSG:[error description]];
                else {
                    
                    BluefinCard *card = [BluefinCard new];
                    card.firstName = billingNameStr;
                    card.lastName = @"";
                    card.cardNumber = cardNumStr;
                    cardExpiryStr = [cardExpiryStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    cardExpiryStr = [cardExpiryStr stringByReplacingOccurrencesOfString:@"/" withString:@""];
                    
                    NSRange rangeOf20 = [cardExpiryStr rangeOfString:@"20"];
                    if(rangeOf20.location!=NSNotFound)
                        cardExpiryStr = [cardExpiryStr stringByReplacingCharactersInRange:rangeOf20 withString:@""];
                    
                    card.cardExpiration = cardExpiryStr;
                    card.cardCVC = cardCodeStr;
                    
                    //                        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navController.view animated:YES];
                    //                        hud.labelText = @"Posting transaction...";
                    [appDelegate showHUD];
                    
                    Bluefin *bluefin = [Bluefin bluefinInstance];
                    [bluefin postTransactionSale:card
                                          amount:[NSNumber numberWithFloat:[[amountRow displayTextValue] floatValue]]
                                transactionToken:transactionInfo.transactionToken
                              optionalParameters:nil
                               completionHandler:^(BluefinTransaction *transaction, NSError *error) {
                                   
                                   [appDelegate hideHUD];
                                   
                                   if (error) {
                                       [self showAlert:@"Error" MSG:[error description]];
                                   } else {
                                       trid =transaction.transactionToken;
                                       
                                       statusCode = 1;
                                       
                                       [self.finalParamDic setObject:trid forKey:@"transaction_number"];
                                       
                                       
                                       [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_ADD_GIFTCARD] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                                           [appDelegate hideHUD];
                                           
                                           NSLog(@"ERRor   : %@",error);

                                           NSLog(@"Response  : %@",responseArray);
                                           if(error)
                                               [self showAlert:@"Error" MSG:error.localizedDescription];
                                           else
                                           {
                                               NSDictionary *resp =(NSDictionary*)responseArray;
                                               [self serviceResponse:resp];
                                           }
                                       }];
                                   }
                               }];
                }
            }];
        }
        
        else if([paymentType isEqualToString:STRIPE_CONST_STR])
        {
            [appDelegate showHUD];
            
            STPCardParams *card = [[STPCardParams alloc] init];
            
            card.number = cardNumStr;
            NSString *delimiter = @"";
            if([cardExpiryStr containsString:@"/"])
                delimiter = @"/";
            else if([cardExpiryStr containsString:@"-"])
                delimiter = @"-";
            
            //To make sure the month and year is there
            if([delimiter length]>0)
            {
                NSArray *monthYearArr = [cardExpiryStr componentsSeparatedByString:delimiter];
                
                card.expMonth = [[monthYearArr objectAtIndex:0] integerValue];
                card.expYear = [[monthYearArr objectAtIndex:1] integerValue];
                card.cvc = cardCodeStr;
                [[STPAPIClient sharedClient] createTokenWithCard:card
                                                      completion:^(STPToken *token, NSError *error) {
                                                          [appDelegate hideHUD];
                                                          if (error) {
                                                              [self showAlert:@"Error" MSG:[error localizedDescription]];
                                                          } else {
                                                              NSLog(@"%@", token.tokenId);
                                                              float discountedFlot =[amountValue floatValue];
                                                              
                                                              NSInteger discountedInt = (int)(discountedFlot*100);
                                                              
                                                              
                                                              NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] initWithCapacity:0];
                                                              [paramDic setObject:token.tokenId forKey:@"stripeToken"];
                                                              [paramDic setObject:[NSNumber numberWithInteger:discountedInt] forKey:@"Total"];
                                                              
                                                              [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_STRIPE] HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
                                                                  
                                                                  NSLog(@"Response 11 :%@",responseArray);
                                                                  [appDelegate hideHUD];
                                                                  if(error)
                                                                      [self showAlert:@"Error" MSG:error.localizedDescription];
                                                                  else
                                                                  {
                                                                      if(responseArray.count){
                                                                          NSMutableDictionary *responseDict = (NSMutableDictionary*)responseArray;
                                                                      [self serviceResponse:responseDict];
                                                                      }
                                                                  }
                                                              }];
                                                              
                                                              
                                                              //   [self createBackendChargeWithToken:token];
                                                          }
                                                      }];
                
            }
            else
            {
                [self showAlert:@"Error" MSG:@"Error with payment. Please try again later."];
                
            }
        }
        
    }
    
}

-(void)termsAndCOnditionsAction{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    GoogleReviewVC *googlePlus = [self.storyboard instantiateViewControllerWithIdentifier:@"GoogleReviewVC"];
    [googlePlus setUrl:URL_GIFTCARDS_TNC];
    [googlePlus setTitle:@"Terms"];
    [self.navigationController pushViewController:googlePlus animated:YES];
    
}
-(void)scanAction{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    [scanViewController setUseCardIOLogo:YES];
    [self.navigationController presentViewController:scanViewController animated:YES completion:Nil];
}


- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    [scanViewController dismissViewControllerAnimated:YES completion:Nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    cardNumberRow.value = [NSString stringWithFormat:@"%@",info.cardNumber];
    cardExpiryRow.value = [NSString stringWithFormat:@"%02lu/%lu",(unsigned long)info.expiryMonth,(unsigned long)info.expiryYear];
    cvvRow.value = [NSString stringWithFormat:@"%@",info.cvv];
    
    cardNumberRow.value = [NSString stringWithFormat:@"%@",info.cardNumber];
    cardExpiryRow.value = [NSString stringWithFormat:@"%02lu/%lu",(unsigned long)info.expiryMonth,(unsigned long)info.expiryYear];
    cvvRow.value = [NSString stringWithFormat:@"%@",info.cvv];
    self.form =formDescriptor;
    [scanViewController dismissViewControllerAnimated:YES completion:Nil];
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1){
    
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    self.title =@"Check Out";
     [self initializeForm];
    // Do any additional setup after loading the view.
}

- (NSMutableArray *) toChararray : (NSString *) str {
    
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[str length]];
    for (int i=0; i < [str length]; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [str characterAtIndex:i]];
        [characters addObject:ichar];
    }
    
    return characters;
}
#pragma mark card check method
- (BOOL) cardCheck:(NSString *)stringToTest {
    
    NSMutableArray *stringAsChars = [self toChararray:stringToTest];
    
    BOOL isOdd = YES;
    int oddSum = 0;
    int evenSum = 0;
    
    for (int i = [stringToTest length] - 1; i >= 0; i--) {
        
        int digit = [(NSString *)[stringAsChars objectAtIndex:i] intValue];
        
        if (isOdd)
            oddSum += digit;
        else
            evenSum += digit/5 + (2*digit) % 10;
        
        isOdd = !isOdd;
    }
    
    return ((oddSum + evenSum) % 10 == 0);
}


-(NSString *) encryptCard : (NSString *) cardStr
{
    for(int i=0;i<5;i++)
    {
        cardStr = [self base64Encode:cardStr];
        cardStr = [self reverseString:cardStr];
    }
    return cardStr;
}
- (NSString *)base64Encode:(NSString *)plainText
{
    NSData *plainTextData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
    return [plainTextData base64Encoding];
}

- (NSString *)reverseString:(NSString *)string {
    NSMutableString *reversedString = [[NSMutableString alloc] init];
    NSRange fullRange = [string rangeOfString:string];
    NSStringEnumerationOptions enumerationOptions = (NSStringEnumerationReverse | NSStringEnumerationByComposedCharacterSequences);
    [string enumerateSubstringsInRange:fullRange options:enumerationOptions usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        [reversedString appendString:substring];
    }];
    return reversedString;
}


-(void)serviceResponse:(NSDictionary*)responseDictitonary{
    //    NSLog(@"%@", strResponse);
    BOOL status = [[responseDictitonary objectForKey:@"status"] boolValue];
    
    if(statusCode==0)
    {
        if(status)
        {
            trid = [responseDictitonary objectForKey:@"transaction_id"];
            if([trid length]>0)
                [self sendToServer];
            else
                [self showAlert:@"Error" MSG:@"Error occured. Please try an alternative payment method."];
            
        }
        else{
            NSString *msg = [responseDictitonary objectForKey:@"msg"];
            if(msg==nil || [msg length]==0 || [msg isEqualToString:@"This transaction has been declined."])
                msg = @"This transaction has been declined. Please try an alternative payment method.";
            [self showAlert:@"Error" MSG:msg];
        }
    }
    
    else if(statusCode==1)
    {
        if(status)
        {
            statusCode = 2;
            NSString *code = [responseDictitonary objectForKey:@"code"];
            NSString *msg = [NSString stringWithFormat:@"Your transaction was successful. Your giftcertificate number is %@. You will receive an email receipt. Please note your transaction id %@ for future reference.", code, trid];
            [self showAlert:@"Thank you!" MSG:msg tag:1];
        }
        else
        {
            NSString *msg = [NSString stringWithFormat:@"Payment successful, but the server isn't available. Please save the transaction id %@, and contact support.", trid];
            [self showAlert:@"Error" MSG:msg];
        }
        
    }
    
}


-(void) sendToServer
{
    statusCode = 1;
    [self.finalParamDic setObject:trid forKey:@"transaction_number"];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:(NSMutableString*)[appDelegate addSalonIdTo:URL_ADD_GIFTCARD] HEADERS:nil GETPARAMS:nil POSTPARAMS:self.finalParamDic COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        
        NSLog(@"Response : %@",responseArray);
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *resp =(NSDictionary*)responseArray;
            [self serviceResponse:resp];
        }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
