//
//  ShippingInformation.h
//  Nithin Salon
//
//  Created by Webappclouds on 18/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormViewController.h"
#import "XLForm.h"
#import "ShippingObj.h"
@interface ShippingInformation :XLFormViewController

@property(nonatomic,retain)id delegate;
@property (nonatomic, retain) ShippingObj *shippingObj;
@end
