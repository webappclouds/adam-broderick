//
//  GiftCheckOut.h
//  Nithin Salon
//
//  Created by Webappclouds on 24/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormViewController.h"
#import "XLForm.h"
@interface GiftCheckOut : XLFormViewController

@property (nonatomic, retain) NSString *paymentType;
@property (nonatomic, retain) NSString *trid;
@property (nonatomic, retain) NSString *amount;
@property (nonatomic, retain) NSString *discountedAmount;
@property (nonatomic, retain) NSMutableDictionary *finalParamDic;

@end
