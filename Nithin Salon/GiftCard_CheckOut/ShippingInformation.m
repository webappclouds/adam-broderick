//
//  ShippingInformation.m
//  Nithin Salon
//
//  Created by Webappclouds on 18/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "ShippingInformation.h"
#import <SHSPhoneComponent/SHSPhoneNumberFormatter+UserConfig.h>
#import "Constants.h"
#import "UIViewController+NRFunctions.h"
#import "GiveGiftCardVC.h"

@interface ShippingInformation ()
{
    XLFormDescriptor *formDescriptor;
    XLFormSectionDescriptor *section1,*section2;
       XLFormRowDescriptor *firstNameRow,*lastNameRow,*addressRow,*cityRow,*stateRow,*zipCodeRow,*phoneRow,*doneButtonRow;
   
}
@end

@implementation ShippingInformation
@synthesize shippingObj;


-(void)initilizeForm{
    
     formDescriptor = [XLFormDescriptor formDescriptorWithTitle:@""];
    
    section1 = [XLFormSectionDescriptor formSectionWithTitle:@"SHIPPING INFORMATION"];
    [formDescriptor addFormSection:section1];
    
    firstNameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"First Name" rowType:XLFormRowDescriptorTypeText title:@"First Name"];
    [firstNameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    firstNameRow.height =44;
    firstNameRow.required =YES;
    [firstNameRow.cellConfigAtConfigure setObject:@"First Name" forKey:@"textField.placeholder"];
    [section1 addFormRow:firstNameRow];
    
    
    
    lastNameRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Last Name" rowType:XLFormRowDescriptorTypeText title:@"Last Name"];
    [lastNameRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    lastNameRow.height =44;
    lastNameRow.required =YES;
    [lastNameRow.cellConfigAtConfigure setObject:@"Last Name" forKey:@"textField.placeholder"];
    [section1 addFormRow:lastNameRow];
    
   
    
    addressRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Address" rowType:XLFormRowDescriptorTypeTextView title:@""];
    addressRow.height =100;
    addressRow.required =YES;
    [addressRow.cellConfigAtConfigure setObject:@"Enter your address" forKey:@"textView.placeholder"];
    [section1 addFormRow:addressRow];
    
        
    cityRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"CIty" rowType:XLFormRowDescriptorTypeText title:@"City"];
    [cityRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    cityRow.height =44;
    cityRow.required =YES;
    [cityRow.cellConfigAtConfigure setObject:@"City" forKey:@"textField.placeholder"];
    [section1 addFormRow:cityRow];
    
    
    
    
    stateRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"State" rowType:XLFormRowDescriptorTypeText title:@"State"];
    [stateRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    stateRow.height =44;
    stateRow.required =YES;
    [stateRow.cellConfigAtConfigure setObject:@"State" forKey:@"textField.placeholder"];
    [section1 addFormRow:stateRow];
    
    
    zipCodeRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Zip Code" rowType:XLFormRowDescriptorTypeText title:@"Zip Code"];
    [zipCodeRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    zipCodeRow.height =44;
    zipCodeRow.required =YES;
    [zipCodeRow.cellConfigAtConfigure setObject:@"Zip" forKey:@"textField.placeholder"];
    [section1 addFormRow:zipCodeRow];
    
    
    SHSPhoneNumberFormatter *formatter = [[SHSPhoneNumberFormatter alloc] init];
    [formatter setDefaultOutputPattern:@"(###) ###-####" imagePath:nil];
    phoneRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Phone Number" rowType:XLFormRowDescriptorTypeInteger title:@"Phone Number"];
     phoneRow.valueFormatter = formatter;
    [phoneRow.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    phoneRow.height =44;
    phoneRow.required =YES;
    phoneRow.useValueFormatterDuringInput = YES;

    [phoneRow.cellConfigAtConfigure setObject:@"Phone" forKey:@"textField.placeholder"];
    [section1 addFormRow:phoneRow];
    
    section2 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [formDescriptor addFormSection:section2];

    doneButtonRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Button" rowType:XLFormRowDescriptorTypeButton title:@"Done"];
    [doneButtonRow.cellConfig setObject:[UIFont boldSystemFontOfSize:18] forKey:@"textLabel.font"];
    [doneButtonRow.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.textColor"];
    doneButtonRow.action.formSelector = @selector(checkOutAllData);
    [section2 addFormRow:doneButtonRow];
    
    if(shippingObj!=nil){
        firstNameRow.value = [NSString stringWithFormat:@"%@",shippingObj.shippingFName];
        lastNameRow.value = [NSString stringWithFormat:@"%@",shippingObj.shippingLName];
        addressRow.value = [NSString stringWithFormat:@"%@",shippingObj.shippingAddr];
        cityRow.value = [NSString stringWithFormat:@"%@",shippingObj.shippingCity];
        stateRow.value = [NSString stringWithFormat:@"%@",shippingObj.shippingState];
        zipCodeRow.value = [NSString stringWithFormat:@"%@",shippingObj.shippingZip];
        phoneRow.value = [NSString stringWithFormat:@"%@",shippingObj.shippingPhone];

    }
    else
        shippingObj = [[ShippingObj alloc]init];
    
    
    
    self.form = formDescriptor;
    
    
}
-(void)checkOutAllData{
    
    if([[firstNameRow displayTextValue] length]==0){
        [self showAlert:@"First Name" MSG:@"Please enter your first name" tag:0];
        return;
    }
    else if([[lastNameRow displayTextValue] length]==0){
        [self showAlert:@"Last Name" MSG:@"Please enter your last name" tag:0];
        return;
    }
    
    else if([[addressRow displayTextValue] length]==0){
        [self showAlert:@"Address" MSG:@"Please enter your address" tag:0];
        return;
    }
    else if([[cityRow displayTextValue] length]==0){
        [self showAlert:@"City" MSG:@"Please enter your City" tag:0];
        return;
    }
    else if([[stateRow displayTextValue] length]==0){
        [self showAlert:@"Last Name" MSG:@"Please enter your state" tag:0];
        return;
    }
    else if([[zipCodeRow displayTextValue] length]==0){
        [self showAlert:@"Zip Code" MSG:@"Please enter your zip code" tag:0];
        return;
    }
    else if([[phoneRow displayTextValue] length]==0){
        [self showAlert:@"Phone number" MSG:@"Please enter your phone number" tag:0];
        return;
    }

    
    else if(([[phoneRow displayTextValue] length]<14)){
        [self showAlert:@"Phone number" MSG:@"Please enter a valid phone number" tag:0];
        return;
    }
    
    
    [shippingObj setShippingFName:[firstNameRow displayTextValue]];
    [shippingObj setShippingLName:[lastNameRow displayTextValue]];
    [shippingObj setShippingAddr:[addressRow displayTextValue]];
    [shippingObj setShippingCity:[cityRow displayTextValue]];
    [shippingObj setShippingState:[stateRow displayTextValue]];
    [shippingObj setShippingZip:[zipCodeRow displayTextValue]];
    [shippingObj setShippingPhone:[phoneRow displayTextValue]];
    
    
    
    [self.delegate getShippingInfo:shippingObj];
    [self.navigationController popViewControllerAnimated:YES];
    

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initilizeForm];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
