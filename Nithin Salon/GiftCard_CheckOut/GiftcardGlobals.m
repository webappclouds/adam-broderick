//
//  GiftcardGlobals.m
//  Visage
//
//  Created by Nithin Reddy on 15/11/12.
//
//

#import "GiftcardGlobals.h"
#import "DiscountObj.h"

@implementation GiftcardGlobals

static NSString *paypalEmail, *paypalClientId;
static NSString *blueFinAccountNumber, *bluefinAccessKey;
static NSMutableArray *equalToArray, *greaterThanArray;
static NSString *stripeSecretKey, *stripePublishableKey;

+(NSMutableArray *)getGiftCards
{
    NSArray *giftcardsList;
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath2 = [documentsDirectory stringByAppendingPathComponent:@"Giftcards.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath2]){
		giftcardsList = [NSArray arrayWithContentsOfFile: plistPath2];
	} else
		giftcardsList = [[NSArray alloc] init];
        
    NSMutableArray *mast = [[NSMutableArray alloc] initWithArray:giftcardsList];
    return mast;
}

+(BOOL) isExistsGiftcard: (NSString *) gc
{
    if([[self getGiftCards] containsObject:gc])
        return TRUE;
    return FALSE;
}

+(void) addGiftcard:(NSString *) word
{
    NSMutableArray *arr = [self getGiftCards];
    if(![arr containsObject:word])
        [arr addObject:word];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"Giftcards.plist"];
    [arr writeToFile:plistPath atomically:YES];
}

+(void) removeGiftcard:(NSString *) word
{
    NSMutableArray *arr = [self getGiftCards];
    if([arr containsObject:word])
        [arr removeObject:word];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"Giftcards.plist"];
    [arr writeToFile:plistPath atomically:YES];
}

+(float) getShippingCost
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *shipping = [prefs stringForKey:@"shipping"];
    if(shipping==Nil || [shipping isEqualToString:@""])
        return 0.0;
    return [shipping floatValue];
}

+(void) setShippingCost : (float) cost
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:[NSString stringWithFormat:@"%f", cost] forKey:@"shipping"];
    [prefs synchronize];
}

+(NSString *) getPaypalEmail
{
    return paypalEmail;
}

+(void) setPaypalEmail : (NSString *) email
{
    paypalEmail = email;
}

+(NSString *) getClientId
{
    return paypalClientId;
}

+(void) setClientId : (NSString *) client
{
    paypalClientId = client;
}

+(NSString *) getBluefinAccountNumber
{
    return blueFinAccountNumber;
}

+(void) setBluefinAccountNumber : (NSString *) acntNumber
{
    blueFinAccountNumber = acntNumber;
}

+(NSString *) getBluefinAccessKey
{
    return bluefinAccessKey;
}

+(void) setBluefinAccessKey : (NSString *) accessKey
{
    bluefinAccessKey = accessKey;
}

+(NSString *) getStripeSecretKey
{
    return stripeSecretKey;
}

+(void) setStripeSecretKey : (NSString *) secret
{
    stripeSecretKey = secret;
}

+(NSString *) getStripePublishableKey
{
    return stripePublishableKey;
}

+(void) setStripePublishableKey : (NSString *) publishableKey
{
    stripePublishableKey = publishableKey;
}


+(void) setEqualTo : (NSArray *)equalJSON
{
    if(equalToArray==Nil)
        equalToArray = [[NSMutableArray alloc] init];
    [equalToArray removeAllObjects];
    for(NSDictionary *dict in equalJSON)
    {
        DiscountObj *obj = [[DiscountObj alloc] init];
        [obj setAmount:[[dict objectForKey:@"amount"] intValue]];
        [obj setPercentage:[[dict objectForKey:@"percentage"] intValue]];
        [equalToArray addObject:obj];
    }
}

+(void) setGreaterThan : (NSArray *)greaterThanJSON
{
    if(greaterThanArray==Nil)
        greaterThanArray = [[NSMutableArray alloc] init];
    [greaterThanArray removeAllObjects];
    for(NSDictionary *dict in greaterThanJSON)
    {
        DiscountObj *obj = [[DiscountObj alloc] init];
        [obj setAmount:[[dict objectForKey:@"amount"] intValue]];
        [obj setPercentage:[[dict objectForKey:@"percentage"] intValue]];
        [greaterThanArray addObject:obj];
    }
}

+(float) returnDiscountedPrice : (float) price
{
    float discountPercent = 0.0;
    if(equalToArray==Nil && greaterThanArray==Nil)
        return price;
    for(DiscountObj *obj in equalToArray)
    {
        if(obj.amount==price)
            return price - price*obj.percentage/100;
    }
    
    for(DiscountObj *obj in greaterThanArray)
    {
        if(price>obj.amount)
            discountPercent = obj.percentage;
    }
    return price - price*discountPercent/100;
}

@end
