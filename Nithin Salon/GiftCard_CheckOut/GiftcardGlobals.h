//
//  GiftcardGlobals.h
//  Visage
//
//  Created by Nithin Reddy on 15/11/12.
//
//

#import <Foundation/Foundation.h>

@interface GiftcardGlobals : NSObject

+(NSMutableArray *)getGiftCards;

+(void) addGiftcard:(NSString *) word;

+(void) removeGiftcard:(NSString *) word;

+(float) getShippingCost;

+(void) setShippingCost : (float) cost;

+(BOOL) isExistsGiftcard: (NSString *) gc;

+(NSString *) getPaypalEmail;

+(void) setPaypalEmail : (NSString *) email;

+(NSString *) getClientId;

+(void) setClientId : (NSString *) client;

+(NSString *) getBluefinAccountNumber;

+(void) setBluefinAccountNumber : (NSString *) acntNumber;

+(NSString *) getBluefinAccessKey;

+(void) setBluefinAccessKey : (NSString *) accessKey;

+(void) setEqualTo : (NSString *)equalJSON;

+(void) setGreaterThan : (NSString *)greaterThanJSON;

+(float) returnDiscountedPrice : (float) price;

+(NSString *) getStripeSecretKey;
+(void) setStripeSecretKey : (NSString *) secret;
+(NSString *) getStripePublishableKey;
+(void) setStripePublishableKey : (NSString *) publishableKey;


@end
