//
//  MessagesListVC.m
//  Nithin Salon
//
//  Created by Webappclouds on 28/03/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "MessagesListVC.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "ApptResponse.h"
#import "OnlineBookindViewCon.h"
@interface MessagesListVC ()
{
    AppDelegate *appDelegate;
}
@end

@implementation MessagesListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.listTV.estimatedRowHeight = 101;
    self.listTV.rowHeight = UITableViewAutomaticDimension;
    self.listTV.delegate = self;
    self.listTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
     appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setTitle:@"MESSAGES"];
    
    self.dataArray = [[NSMutableArray alloc] init];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",[appDelegate addSalonIdTo:URL_NOTIFICATIONS_LIST],appDelegate.pushToken];
    
    [appDelegate showHUD];
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:urlString HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            NSMutableDictionary *dict = (NSMutableDictionary*)responseArray;
            if(dict)
                [self getResponse:[dict objectForKey:@"notifications"]];
        }}];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"NotificationCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    
    if(self.dataArray.count){
         tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    UILabel *label = (UILabel*)[cell.contentView viewWithTag:10];
    UILabel *timeLabel = (UILabel*)[cell.contentView viewWithTag:20];
        timeLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
        label.font = [UIFont fontWithName:@"Helvetica" size:15];

    [label setText:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"push_message"]];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [df setDateFormat:@"MMM dd, yyyy hh:mm a"];
    [timeLabel setText:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"sent_on"]];
    }
    
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSDictionary *dictOfNofitication = [self.dataArray objectAtIndex:indexPath.row];
    int type = [[dictOfNofitication objectForKey:@"push_code"] intValue];
    if(type==1)
        [appDelegate loadReviews];
    else if(type==2)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        SpecialsList *lvc = [storyboard instantiateViewControllerWithIdentifier:@"SpecialsList"];
        lvc.specialsId = appDelegate.specialsModel.moduleId;
        [self.navigationController pushViewController:lvc animated:YES];
    }
    else if(type==3)
        [appDelegate pendingAppointments];
    else if(type==4){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ApptResponse *apptResponse = [storyboard instantiateViewControllerWithIdentifier:@"ApptResponse"];
        [apptResponse setLabelStr:[dictOfNofitication objectForKey:@"type"]];
        [apptResponse setStr:[dictOfNofitication objectForKey:@"push_message"]];
        [self.navigationController pushViewController:apptResponse animated:YES];
    } else if(type==5){
        [appDelegate showAlert:@"Customer Alert" MSG:@""];
    } else if(type==6)
        [appDelegate scracthView];
    else if(type==7)
        [appDelegate events];
    else if(type==10)
        [appDelegate lastMinuteAppointment];
    else if(type == 17)
        [appDelegate bookOnline];
    else if(type == 18){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        OnlineBookindViewCon *lvc = [storyboard instantiateViewControllerWithIdentifier:@"OnlineBookindViewCon"];
        [self.navigationController pushViewController:lvc animated:YES];

    }
}
-(void) getResponse:(NSArray *)responseArray
{
    NSLog(@"Messages : %@",responseArray);
    NSArray *array = [responseArray mutableCopy];
    if([array count]==0)
        [self showAlert:@"Messages" MSG:@"No messages found." tag:1];
    
    self.dataArray = [array copy];
    
    NSLog(@"Messages : %@",self.dataArray);
    [self.listTV reloadData];
}

-(void) showAlert : (NSString *) title MSG:(NSString *) message tag:(int)tag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(tag==1)
            [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
