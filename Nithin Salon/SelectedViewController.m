//
//  SelectedViewController.m
//  MyGallery
//
//  Created by Nithin Reddy on 22/05/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "SelectedViewController.h"
#import "DYRateView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GlobalSettings.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "FindBookVC.h"
@interface SelectedViewController ()
{
    AppDelegate *appDelegate;
}
@end

@implementation SelectedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title = @"My Gallery";
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"More" style:UIBarButtonItemStylePlain target:self action:@selector(More)];
    self.navigationItem.rightBarButtonItem = right;

    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[self.data objectForKey:@"url"]]];
    self.stylistLabel.text = [NSString stringWithFormat:@"Stylist: %@",[self.data objectForKey:@"stylist"]];
    DYRateView *rateView = [[DYRateView alloc] initWithFrame:CGRectMake(0, 0, self.rateContainer.frame.size.width, 30) fullStar:[UIImage imageNamed:@"StarFull"] emptyStar:[UIImage imageNamed:@"StarEmpty"]];
    //    rateView.padding = 20;
    rateView.rate = [[self.data objectForKey:@"avg_rating"] floatValue];
    rateView.alignment = RateViewAlignmentLeft;
    rateView.editable = NO;
    [self.totalReviews setText:[NSString stringWithFormat:@"(%@ ratings)", [self.data objectForKey:@"totalreviews"]]];
    [self.rateContainer addSubview:rateView];
    self.tableView.estimatedRowHeight =60;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.data objectForKey:@"reviews"] count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"CommentCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==Nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    cell.textLabel.text = [[[self.data objectForKey:@"reviews"] objectAtIndex:indexPath.row] objectForKey:@"comment"];
//    return cell;
    
    NSDictionary *reviewDict = [[self.data objectForKey:@"reviews"] objectAtIndex:indexPath.row];
    UIView *rateContainer = (UIView *) [cell.contentView viewWithTag:1];
    UILabel *poster = (UILabel *) [cell.contentView viewWithTag:2];
    UILabel *message = (UILabel *) [cell.contentView viewWithTag:3];
    
    DYRateView *rateView = [[DYRateView alloc] initWithFrame:CGRectMake(0, 0, rateContainer.frame.size.width, rateContainer.frame.size.height) fullStar:[UIImage imageNamed:@"StarFull"] emptyStar:[UIImage imageNamed:@"StarEmpty"]];
    //    rateView.padding = 20;
    rateView.rate = [[reviewDict objectForKey:@"rating"] floatValue];
    rateView.alignment = RateViewAlignmentLeft;
    rateView.editable = NO;
    for (UIView *v in [rateContainer subviews])
        [v removeFromSuperview];

    [rateContainer addSubview:rateView];
    
    [poster setText:[NSString stringWithFormat:@"By %@", [reviewDict objectForKey:@"name"]]];
    [message setText:[reviewDict objectForKey:@"comment"]];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)More{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"More" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        NSString * descriptionWithTitle = [NSString stringWithFormat:@"Stylist name: %@",[self.data objectForKey:@"stylist"]];
        
        NSArray *itemsToShare;
        if(self.imageView.image == nil){
            itemsToShare = @[descriptionWithTitle];
        }
        else{
            NSString *shareUrlStr = [NSString stringWithFormat:@"https://saloncloudsplus.com/imagesharingweb/add_review/%@", [self.data objectForKey:@"key"]];
            
            itemsToShare = @[descriptionWithTitle,shareUrlStr];
        }
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
        [activityViewController setValue:SHARE_SUBJECT forKey:@"subject"];
        activityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
        if ([activityViewController respondsToSelector:@selector(completionWithItemsHandler)]) {
            activityViewController.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            };
            
        }    //or whichever you don't need
        [self presentViewController:activityViewController animated:YES completion:nil];

        
        
           }];
    
    UIAlertAction *confirmAction1 = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteServiceCall];
       
    }];
    
    UIAlertAction *confirmAction2 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
        
                                        
    }];
    
    [alertController addAction:confirmAction];
    [alertController addAction:confirmAction1];
    [alertController addAction:confirmAction2];
    [self presentViewController:alertController animated:YES completion:nil];
    
    //[self.data objectForKey:@"url"]//stylist
    
    
                                        
                  }


-(void)deleteServiceCall{
    [appDelegate showHUD];
    [ [GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:[NSString stringWithFormat:@"https://saloncloudsplus.com/wsimagesharing/imageDelete/%@",[self.data objectForKey:@"key"]] HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            
            NSDictionary *dictJson = (NSDictionary *)responseArray;
            
            BOOL status = [[dictJson objectForKey:@"status"] boolValue];
            if(status)
            {
            
              [self showAlert:@"Delete" MSG:@"The image has been deleted." BLOCK:^{
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IMAGE_ADDED" object:Nil];
                [self.navigationController popViewControllerAnimated:YES];
            }];
        
            
        }
        
        }}];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)bookNow:(id)sender {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:Nil action:Nil];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FindBookVC *fbc  = (FindBookVC *)[storyBoard instantiateViewControllerWithIdentifier:@"FindBookVC"];
    fbc.compareStr =@"Gallery";
    [self.navigationController pushViewController:fbc animated:YES];

}
@end
