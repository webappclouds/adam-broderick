//
//  ApptMapView.h
//  Nithin Salon
//
//  Created by Nithin Reddy on 10/06/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ApptMapView : UIViewController

@property (nonatomic) IBOutlet GMSMapView *mapView;
@property (nonatomic, retain) IBOutlet UIView *mapContainer;

@end
