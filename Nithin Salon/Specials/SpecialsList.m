//
//  SpecialsList.m
//  Demo Salon
//
//  Created by Nithin Reddy on 09/03/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import "SpecialsList.h"
#import "Constants.h"
#import "SpecialsDesc.h"
#import "AppDelegate.h"
@interface SpecialsList ()
{
    AppDelegate *appDelegate;
}
@end

@implementation SpecialsList

@synthesize data, tableView, refreshControl;
@synthesize flag;
@synthesize specialsId;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =@"Specials";
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.tableView.estimatedRowHeight = 74;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    data = [[NSMutableArray alloc] init];
    self.title = self.moduleObj.moduleName;
    [UIViewController addBottomBar:self];

    [self loadDataFromServer];
}

-(void) loadDataFromServer
{
    [data removeAllObjects];
    NSString *specialUrl;
   specialUrl = [NSString stringWithFormat:@"%@%@", [appDelegate addSalonIdTo:URL_SPECIALS], specialsId];
    
    
    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"GET" URL:specialUrl HEADERS:nil GETPARAMS:nil POSTPARAMS:nil COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else
        {
            if(responseArray==Nil || [responseArray count]==0)
            {
                if(flag==SPECIALS_FLAG)
                    [self showAlert:@"Error" MSG:@"No specials found"];
                else
                    [self showAlert:@"Error" MSG:@"No services found"];
                return;
            }
            
            NSLog(@"Specials list = %@",responseArray);
            for(NSDictionary *dict in responseArray)
            {       SpecialsObj *specialObj = [[SpecialsObj alloc] init];
                    specialObj.idStr = [dict objectForKey:@"specials_id"];
                    specialObj.title = [dict objectForKey:@"specials_title"];
                    specialObj.desc = [dict objectForKey:@"specials_description"];
                    specialObj.image = [dict objectForKey:@"specials_image"];
                specialObj.unique_code = [dict objectForKey:@"unique_code"];
                    [data addObject:specialObj];
                    specialObj = Nil;
                            }
             self.navigationItem.title =@"Specials";
            [self.tableView reloadData];
            
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"SpecialsCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    if([data count]==0)
        return cell;
    SpecialsObj *splObj = [data objectAtIndex:indexPath.row];
    [(UILabel *)[cell.contentView viewWithTag:2]  setText:splObj.title];
    
    
    UIImageView *imv =(UIImageView *)[cell.contentView viewWithTag:100];
    imv.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    imv.layer.shadowOffset = CGSizeMake(0,0);
    imv.layer.shadowOpacity = 0.2;
    imv.layer.shadowRadius = 4.0;
    imv.layer.masksToBounds =NO;
    
    UIImageView *specialsImage = (UIImageView *) [cell.contentView viewWithTag:1];
    [specialsImage setContentMode:UIViewContentModeScaleAspectFit];
    if([splObj.image length]>0)
    {
        NSString *imageUrl = splObj.image;
        if(![imageUrl containsString:IMAGES_URL])
            imageUrl = [NSString stringWithFormat:@"%@%@", IMAGES_URL, splObj.image];
    
            specialsImage.bounds = CGRectInset(specialsImage.frame, 20.0f, 20.0f);
        
        [specialsImage setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
    }
    else
        [specialsImage setImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
        SpecialsObj *specialObj = [self.data objectAtIndex:indexPath.row];
        SpecialsDesc *specialDesc = [self.storyboard instantiateViewControllerWithIdentifier:@"SpecialsDesc"];
        [specialDesc setSpecialObj:specialObj];
        [self.navigationController pushViewController:specialDesc animated:YES];
    }

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == [tableView numberOfSections] - 1)
        return 20;
    else
        return 0;
    
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.tintColor = [UIColor clearColor];
}

@end
