//
//  SpecialsDesc.m
//  Demo Salon
//
//  Created by Nithin Reddy on 25/03/15.
//  Copyright (c) 2015 Webappclouds. All rights reserved.
//

#import "SpecialsDesc.h"
#import <UIImageView+WebCache.h>
#import "Constants.h"
#import "Globals.h"
#import "CustomIOSAlertView.h"
#import "WalletGiftCardVC.h"
#import "AppDelegate.h"
#import "SpeciasWalletGiftCardVC.h"
#import "TGLViewController.h"

@interface SpecialsDesc ()
{
    NSString *randomStringGenerateString;
    AppDelegate *appDelegate;
}
@end

@implementation SpecialsDesc

@synthesize specialObj, titleLabel, descView, imageView;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view.
    self.title =@"Special details"; //specialObj.title;
    [self.titleLabel setText:specialObj.title];
    [self.descView setText:specialObj.desc];
    
    
    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    AppDelegate  * objAppdel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if ([slcIdStr length]==0) {
        [_bottomView setHidden:YES];
    }
    else{
        [_bottomView setHidden:NO];
    }


    if(specialObj.image!=nil && [specialObj.image length]>0)
    {
        
       
       // [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", IMAGES_URL, specialObj.image]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", IMAGES_URL, specialObj.image]] placeholderImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
    }
    else
        [imageView setImage:[UIImage imageNamed:APP_PLACEHOLDER_IMAGE]];
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(clickProductImage:)];
    [tapRecognizer setNumberOfTouchesRequired:1];
    [tapRecognizer setDelegate:self];
    
    self.imageView.userInteractionEnabled = YES;
    [self.imageView addGestureRecognizer:tapRecognizer];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView1.contentMode = UIViewContentModeScaleAspectFit;

    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(share)];
    self.navigationItem.rightBarButtonItem = right;
}
-(void)share{
    NSString * descriptionWithTitle = [NSString stringWithFormat:@"%@\nTitle: %@ \nDescription: %@ %@ ",SHARE_LONG_TEXT_SPECIAL,specialObj.title,specialObj.desc,[Globals returnShareUrl]];
    
    NSArray *itemsToShare;
    if(specialObj.image.length ==0){
        itemsToShare = @[descriptionWithTitle];
    }
    else
    itemsToShare = @[descriptionWithTitle,imageView.image];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    [activityViewController setValue:SHARE_SUBJECT forKey:@"subject"];
    activityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    if ([activityViewController respondsToSelector:@selector(completionWithItemsHandler)]) {
        activityViewController.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            
            NSLog(@"Mail erro : %@", activityError.description);
            NSLog(@"completed : %d", completed);

            if(activityType == UIActivityTypeMail){
                if(completed)
                    [self showAlert:@"Mail" MSG:@"your mail has been successfully sent" tag:0];
            } // When completed flag is YES, user performed specific activity
        };
   
    }    //or whichever you don't need
    [self presentViewController:activityViewController animated:YES completion:nil];

    
    
    NSString *slcIdStr=[self loadSharedPreferenceValue:@"slc_id"];
    if(slcIdStr.length==0){
        [_bottomView setHidden:YES];
    }
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)clickProductImage :(id) sender
{
    if(specialObj.image.length){

    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createPopupView]];
    
    // Modify the parameters
    //[alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    //    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
    //        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
    //        [alertView close];
    //    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)inScroll {
    return imageView;
}

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}
- (UIView *)createPopupView
{
    
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, self.view.frame.size.height-100)];
    [demoView setBackgroundColor:[UIColor whiteColor]];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-40, self.view.frame.size.height-120)];
    _imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-40, self.view.frame.size.height-120)];
    
    _imageView1.contentMode = UIViewContentModeScaleAspectFit;
    scrollView.contentSize = CGSizeMake(imageView.frame.size.width , imageView.frame.size.height);
    scrollView.maximumZoomScale = 4;
    scrollView.minimumZoomScale = 1;
    scrollView.clipsToBounds = YES;
    scrollView.delegate = self;
    
    
    UIActivityIndicatorView *activityIndicator;
    [imageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
    activityIndicator.center = imageView.center;
    [activityIndicator startAnimating];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        _imageView1.image =imageView.image;
        [activityIndicator stopAnimating];
        //[activityIndicator re,,,,,,,,,,,,,,moveFromSuperview];
    });
    [scrollView addSubview:_imageView1];
    [demoView addSubview:scrollView];
    
    return demoView;
    
   
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)AddToWallet:(id)sender {
//    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//    
//    NSMutableString *randomString = [NSMutableString stringWithCapacity:15];
//        
//        for (int i=0; i<15; i++) {
//            [randomString appendFormat: @"%C", [letters characterAtIndex:arc4random_uniform((int)[letters length])]];
//        }
//    
//    randomStringGenerateString = [[NSString alloc]init];
//    randomStringGenerateString = [randomString mutableCopy];
//    if(randomStringGenerateString.length)
    [self addWalletToServer];
}


-(void) addWalletToServer
{
    NSString *specialUrl = [NSString stringWithFormat:@"%@", [appDelegate addSalonIdTo:URL_ADDTO_WALLET_URL]];
    
    NSMutableDictionary *paramDict = [NSMutableDictionary new];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    [paramDict setObject: specialObj.unique_code forKey:@"unique_code"];

    NSLog(@"Params : %@", paramDict);
    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:specialUrl HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else {
            NSLog(@"Resp :%@",responseArray);
            NSDictionary *responseDict = (NSDictionary*) responseArray;
            if([[responseDict objectForKey:@"status"] boolValue])
            {
                //[self fetchMyWalletItems];
                [self moveToMyWallet];
            }
            else {
                SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Specials" andMessage:[responseDict objectForKey:@"msg"]];
                [alertView addButtonWithTitle:@"Ok" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
                }];
                alertView.transitionStyle = SIAlertViewTransitionStyleFade;
                [alertView show];

            }
        }
    }];
    
}
-(void)moveToMyWallet
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    TGLViewController *tglViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TGLViewController"];
    [self.navigationController pushViewController:tglViewController animated:YES];

}

-(void) fetchMyWalletItems
{
    NSString *specialUrl = [NSString stringWithFormat:@"%@", [appDelegate addSalonIdTo:URL_My_WALLET_URL]];
    
    NSMutableDictionary *paramDict = [NSMutableDictionary new];
    [paramDict setObject:[self loadSharedPreferenceValue:@"slc_id"]?:@"" forKey:@"slc_id"];
    
    NSLog(@"Params : %@", paramDict);
    [appDelegate showHUD];
    
    [[GlobalSettings sharedManager] processHTTPNetworkCall:@"POST" URL:specialUrl HEADERS:nil GETPARAMS:nil POSTPARAMS:paramDict COMPLETIONHANDLER:^(NSArray *responseArray, NSURLResponse *urlResponse, NSError *error) {
        [appDelegate hideHUD];
        if(error)
            [self showAlert:@"Error" MSG:error.localizedDescription];
        else {
            
            NSDictionary *responseDict = (NSDictionary*) responseArray;

            NSLog(@"Resp :%@",responseDict);
            [self MyGiftCard:[responseDict objectForKey:@"wallet"]];
        }
    }];
    
}


-(void)MyGiftCard:(NSArray*) listOfWallets{
    
    NSLog(@"MyGiftCard wallet = %@",listOfWallets);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [appDelegate.walletArray addObjectsFromArray:listOfWallets];
    SpeciasWalletGiftCardVC *myGiftCardVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SpeciasWalletGiftCardVC"];
    [self.navigationController pushViewController:myGiftCardVC animated:YES];
}
@end
