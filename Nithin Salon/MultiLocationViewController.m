//
//  MultiLocationViewController.m
//  AdamBroderick
//
//  Created by Webappclouds on 20/09/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import "MultiLocationViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "LeftMenuViewController.h"

@interface MultiLocationViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation MultiLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (IBAction)locationSelected:(id)sender {
    
    appDelegate.salonId = @"691";
    [self addSideMenu];

}
- (IBAction)secondLocationSelected:(id)sender {
    appDelegate.salonId = @"692";
    [self addSideMenu];

}

-(void)addSideMenu
{
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    //    ViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    //    [self.navigationController pushViewController:lvc animated:YES];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:16],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };
    
    
    
    LeftMenuViewController *leftMenu = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"];
    appDelegate.container = [MFSideMenuContainerViewController
                             containerWithCenterViewController:self.navigationController
                             leftMenuViewController:leftMenu
                             rightMenuViewController:nil];
    
    appDelegate.container.leftMenuWidth = appDelegate.window.frame.size.width - 100;
    
//    [self.navigationController presentedViewController:appDelegate.container animated:YES];
    //    self.window.rootViewController = _container;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
