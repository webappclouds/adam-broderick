//
//  RecommendedTimingsVC.h
//  Nithin Salon
//
//  Created by Webappclouds on 19/04/17.
//  Copyright © 2017 Webappclouds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendedTimingsVC : UITableViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,retain)NSMutableArray *globalArray;
@end
